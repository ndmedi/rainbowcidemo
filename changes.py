#!/usr/bin/env python2.7

import sys

CHANGELOG = "CHANGELOG.md"

def extract_release(c, r):
    content_lines = c.split('\n')
    version = "[{}]".format(r)
    include_line = False
    valid_lines = []
    for line in content_lines:
        if line.startswith('## '):
            if version in line:
                include_line = True
                line = line.replace('## {} '.format(version), '').split('-')[0].rstrip(' ')
            else:
                include_line = False
        if include_line:
            if line.startswith('### '):
                line = line.replace('### ', '') + ':'
            line = line.replace('**', '')
            line = line.replace('*', '-')
            valid_lines.append("{}\n".format(line))
    valid_content = ''.join(str(l) for l in valid_lines)
    print valid_content

if __name__ == '__main__':
    if sys.version_info < (3, 0):
        # use UTF-8 encoding instead of unicode to support more characters
        reload(sys)
        sys.setdefaultencoding("utf-8")

    # parse command line
    if len(sys.argv) != 2:
        print "Usage: {0} version".format(sys.argv[0])
        sys.exit(1)
    sprint_version = sys.argv[1]

    with open(CHANGELOG, "r") as src:
        content = src.read()
        release_content = extract_release(content, sprint_version)
