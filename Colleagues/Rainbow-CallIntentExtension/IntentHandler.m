/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "IntentHandler.h"

@interface IntentHandler () <INStartAudioCallIntentHandling, INStartVideoCallIntentHandling>

@end

@implementation IntentHandler

- (nullable id)handlerForIntent:(INIntent *)intent {
    NSLog(@"JE SUIS L'INTENT EXTENSION");
    return [super handlerForIntent:intent];

}

#pragma mark - INSendMessageIntentHandling

-(void)handleStartVideoCall:(INStartVideoCallIntent *)intent completion:(void (^)(INStartVideoCallIntentResponse * _Nonnull))completion {
    INStartVideoCallIntentResponse *response;
    
    INPerson * person = intent.contacts.firstObject;
    INPersonHandle *personHandle = person.personHandle;
    if(!personHandle){
        response = [[INStartVideoCallIntentResponse alloc] initWithCode:INStartVideoCallIntentResponseCodeFailure userActivity:nil];
    } else {
        NSUserActivity *userActivity = [[NSUserActivity alloc] initWithActivityType:NSStringFromClass([INStartVideoCallIntent class])];
        response = [[INStartVideoCallIntentResponse alloc] initWithCode:INStartVideoCallIntentResponseCodeContinueInApp userActivity:userActivity];
    }
    
    if(completion)
        completion(response);
}

-(void)handleStartAudioCall:(INStartAudioCallIntent *)intent completion:(void (^)(INStartAudioCallIntentResponse * _Nonnull))completion {
    INStartAudioCallIntentResponse *response;
    
    INPerson * person = intent.contacts.firstObject;
    INPersonHandle *personHandle = person.personHandle;
    response = [[INStartAudioCallIntentResponse alloc] initWithCode:INStartAudioCallIntentResponseCodeFailure userActivity:nil];
    if(!personHandle){
        response = [[INStartAudioCallIntentResponse alloc] initWithCode:INStartAudioCallIntentResponseCodeFailure userActivity:nil];
    } else {
        NSUserActivity *userActivity = [[NSUserActivity alloc] initWithActivityType:NSStringFromClass([INStartAudioCallIntent class])];
        response = [[INStartAudioCallIntentResponse alloc] initWithCode:INStartAudioCallIntentResponseCodeContinueInApp userActivity:userActivity];
    }
    
    if(completion)
        completion(response);
}

//- (void)confirmStartAudioCall:(INStartAudioCallIntent *)intent
//                   completion:(void (^)(INStartAudioCallIntentResponse *response))completion {
//
//
//}

//- (void)resolveDestinationTypeForStartAudioCall:(INStartAudioCallIntent *)intent
//                                 withCompletion:(void (^)(INCallDestinationTypeResolutionResult *resolutionResult))completion {
//    NSMutableArray <INCallDestinationTypeResolutionResult *> *resolutionResults = [NSMutableArray new];
//    [resolutionResults addObject:[INPersonResolutionResult notRequired]];
//
//    completion(resolutionResults);
//}

- (void)resolveContactsForStartVideoCall:(INStartVideoCallIntent *)intent
                          withCompletion:(void (^)(NSArray<INPersonResolutionResult *> *resolutionResults))completion {
    
    NSMutableArray <INPersonResolutionResult *> *resolutionResults = [NSMutableArray new];
    [resolutionResults addObject:[INPersonResolutionResult unsupported]];
    
    completion(resolutionResults);
}

- (void)resolveContactsForStartAudioCall:(INStartAudioCallIntent *)intent
                          withCompletion:(void (^)(NSArray<INPersonResolutionResult *> *resolutionResults))completion {
    INPerson * person = intent.contacts.firstObject;
    NSMutableArray <INPersonResolutionResult *> *resolutionResults = [NSMutableArray new];
    [resolutionResults addObject:[INPersonResolutionResult successWithResolvedPerson:person]];
    
    completion(resolutionResults);
}

@end
