/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "NotificationService.h"
#import <Rainbow/Rainbow.h>

@interface NotificationService ()

@property (nonatomic, strong) void (^contentHandler)(UNNotificationContent *contentToDeliver);
@property (nonatomic, strong) UNMutableNotificationContent *bestAttemptContent;

@end

@implementation NotificationService

-(instancetype) init {
    self = [super init];
    if(self){
        [RainbowUserDefaults setSuiteName:@"group.com.alcatellucent.otcl"];
        [[LogsRecorder sharedInstance] startRecord];
        [ServicesManager sharedInstance];
    }
    return self;
}

-(void) dealloc {
    [[LogsRecorder sharedInstance] stopRecord];
}

- (void)didReceiveNotificationRequest:(UNNotificationRequest *)request withContentHandler:(void (^)(UNNotificationContent * _Nonnull))contentHandler {
    NSLog(@"[NotificationService] NotificationRequest: %@", [Tools anonymizeNotificationRequest:request.description]);
    if ([request.content.userInfo isKindOfClass:[NSDictionary class]]) {
        NSLog(@"[NotificationService] didReceiveNotificationRequest %@", [Tools anonymizeNotificationUserInfo:request.content.userInfo]);
    }
    self.contentHandler = contentHandler;
    self.bestAttemptContent = [request.content mutableCopy];

    [[ServicesManager sharedInstance].notificationsManager saveNotificationWithUserInfo:self.bestAttemptContent.userInfo];

    [[RainbowUserDefaults sharedInstance] setBool:YES forKey:@"shouldRefreshConversationUI"];
    
    // Adapt the display
    BOOL shouldRemoveTitle = NO;
    
    NSString *fileName = self.bestAttemptContent.userInfo[@"file-name"];
    NSString *lastMessageBody = self.bestAttemptContent.userInfo[@"last-message-body"];
    NSString *lastMessageType = self.bestAttemptContent.userInfo[@"last-message-type"];
    NSString *firstLastName = self.bestAttemptContent.userInfo[@"first-last-name"];
    NSString *fileContentType = self.bestAttemptContent.userInfo[@"file-content-type"];
    
    if ([fileContentType length] > 0 && [fileContentType isEqualToString:@"geolocation"]) {
        if ([lastMessageType isEqualToString:@"chat"])
            self.bestAttemptContent.body = NSLocalizedString(@"Current location shared with you", nil);
        else
            self.bestAttemptContent.body = [NSString stringWithFormat:@"%@ : %@", firstLastName, NSLocalizedString(@"Current location shared with you", nil)];
    } else if ([fileName length] > 0) {
        self.bestAttemptContent.body = [NSString stringWithFormat:NSLocalizedString(@"%@ shared a file with you", nil), firstLastName];
        shouldRemoveTitle = YES;
    } else if ([lastMessageBody length] > 0) {
        if ([lastMessageBody hasPrefix:@"/img"]) {
            NSArray *split = [lastMessageBody componentsSeparatedByString:@" "];
            if ([split count] == 3 && [split[1] hasSuffix:@".gif"]) { // Threat the GIF case
                self.bestAttemptContent.body = [NSString stringWithFormat:NSLocalizedString(@"%@ has shared an animated GIF", nil), firstLastName];
                shouldRemoveTitle = YES;
            }
        }
    }
    
    // In some cases, the sentence already contains the name of the user so we don't need to display it again in the title for P2P conversation
    if (shouldRemoveTitle && [lastMessageType isEqualToString:@"chat"])
        self.bestAttemptContent.title = @"";
    
    self.contentHandler(self.bestAttemptContent);
}

- (void)serviceExtensionTimeWillExpire {
    // Called just before the extension will be terminated by the system.
    // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
    NSLog(@"[NotificationService] serviceExtensionTimeWillExpire");
    self.contentHandler(self.bestAttemptContent);
}

@end
