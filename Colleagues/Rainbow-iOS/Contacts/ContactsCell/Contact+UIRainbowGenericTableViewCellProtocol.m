/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Contact+UIRainbowGenericTableViewCellProtocol.h"
#import <Rainbow/Peer.h>
#import "UITools.h"
#import <Rainbow/Tools.h>

@implementation Contact (UIRainbowGenericTableViewCellProtocol)

-(NSString *) mainLabel {
    if (self.displayName) {
        return self.displayName;
    } else if (self.sentInvitation && self.sentInvitation.status == InvitationStatusPending) {
        if ([self.phoneNumbers count] > 0) {
            return [[self.phoneNumbers firstObject] number];
        } else if ([self.emailAddresses count] > 0) {
            return [[self.emailAddresses firstObject] address];
        }
    }
    return nil;
}

- (NSString *)midLabel {
    if (self.rainbowTV) {
        if ([self.rainbowTV.location length] > 0 || [self.rainbowTV.locationDetail length] > 0) {
            if ([self.rainbowTV.location length] == 0)
                return self.rainbowTV.locationDetail;
            else if ([self.rainbowTV.locationDetail length] == 0)
                return self.rainbowTV.location;
            else
                return [NSString stringWithFormat:@"%@, %@", self.rainbowTV.location, self.rainbowTV.locationDetail];
        }
    }
    
    return nil;
}

-(NSString *) subLabel {
    if( self.isBot ) {
        return nil;
    } else if ([self.rainbowTV.roomName length] > 0) {
        return self.rainbowTV.roomName;
    } else if( self.isPresenceSubscribed ) {
        return [UITools getPresenceText:self];
    } else if( self.companyName ) {
        if([self.companyName isEqualToString:self.displayName])
            return nil;
        return self.companyName;
    } else if(!self.firstName && !self.lastName && [Tools isValidEmailAddress:self.displayName]) {
        // This is the case of guest participant
        return NSLocalizedString(@"Invited", nil);
    } else if (self.isPBXContact && [self.phoneNumbers count] > 0) {
        return self.phoneNumbers.firstObject.number;
    }
    
    return nil;
}

-(UIColor *) mainLabelTextColor {
    if( self.calendarPresence.presence == CalendarPresenceOutOfOffice || self.calendarPresence.automaticReply.isEnabled) {
        return [UITools defaultTintColor];
    } else if( self.calendarPresence.presence == CalendarPresenceBusy ) {
        return [UITools defaultTintColor];
    } else {
        return [UIColor blackColor];
    }
}

-(NSDate *) date {
    return nil;
}

-(NSString *) bottomRightLabel {
    return nil;
}

-(Peer *) avatar {
    return (Peer*)self;
}

-(NSArray *) observablesKeyPath {
    return @[kContactLastNameKey,kContactFirstNameKey];
}

-(NSInteger) badgeValue {
    return 0;
}

-(NSString *) cellButtonTitle {
    return nil;
}

-(UIImage *) cellButtonImage {
    // We hide the button for all sent invitations, we probably need to refactor the code below... because canChatWith returns YES for sent invitation
    if (self.sentInvitation && self.sentInvitation.status == InvitationStatusPending)
        return nil;
    
    if( !self.canChatWith && (self.phoneNumbers.count > 0 || self.emailAddresses.count > 0) ) {
        if (self.isPBXContact) {
            return [UIImage imageNamed:@"Phone"];
        } else if (self.sentInvitation && self.sentInvitation.status == InvitationStatusPending) {
            return [UIImage imageNamed:@"invitation"]; // Pending
        } else if(self.sentInvitation && self.sentInvitation.status == InvitationStatusFailed) {
            return [UIImage imageNamed:@"invitation"]; // Failed
        } else {
            return [UIImage imageNamed:@"invitation"]; // Invite
        }
    } else if (!self.rainbowTV) {
        return [UIImage imageNamed:@"Vcard"];
    }
    return nil;
}

-(UIColor *) cellButtonImageColor {
    if (self.sentInvitation && self.sentInvitation.status == InvitationStatusPending) {
        return [UIColor lightGrayColor]; // Pending
    } else if(self.sentInvitation && self.sentInvitation.status == InvitationStatusFailed) {
        return [UIColor redColor]; // Failed
    }
    return nil;
}

-(UIImage *) rightIcon {
    return nil;
}
-(UIColor *) rightIconTintColor {
    return nil;
}

@end
