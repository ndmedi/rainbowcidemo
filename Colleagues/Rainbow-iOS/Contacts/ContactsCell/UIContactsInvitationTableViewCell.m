/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIContactsInvitationTableViewCell.h"
#import <Rainbow/CompanyInvitation.h>
#import <Rainbow/ServicesManager.h>
#import "GoogleAnalytics.h"

@implementation UIContactsInvitationTableViewCell

-(void) awakeFromNib {
    [super awakeFromNib];
    _acceptButton.tintColor = [UIColor colorWithRed:40.0/255.0 green:178.0/255.0 blue:67.0/255.0 alpha:0.8f];
    _declineButton.tintColor = [UIColor colorWithRed:213.0/255.0 green:0 blue:112.0/255.0 alpha:0.8f];
}

- (IBAction)acceptInvitation:(UIButton *)sender {
    NSLog(@"Accept invitation of contact %@", (Contact *)self.cellObject);
    if([_invitation isKindOfClass:[CompanyInvitation class]]) {
        [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"contact" action:@"accept invitation" label:@"company invitation" value:nil];
        [[ServicesManager sharedInstance].companiesService acceptCompanyInvitation:(CompanyInvitation*)_invitation];
    } else {
        [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"contact" action:@"accept invitation" label:@"user invitation" value:nil];
        [_contactsManagerService acceptInvitation:_invitation];
    }
}

- (IBAction)declineInvitation:(UIButton *)sender {
    NSLog(@"Decline invitation of contact %@", (Contact *)self.cellObject);
    if([_invitation isKindOfClass:[CompanyInvitation class]]){
        [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"contact" action:@"decline invitation" label:@"company invitation" value:nil];
        [[ServicesManager sharedInstance].companiesService declineCompanyInvitation:(CompanyInvitation*)_invitation];
    } else {
        [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"contact" action:@"decline invitation" label:@"user invitation" value:nil];
        [_contactsManagerService declineInvitation:_invitation];
    }
}

@end
