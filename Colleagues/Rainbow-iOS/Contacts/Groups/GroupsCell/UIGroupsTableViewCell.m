/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIGroupsTableViewCell.h"
#import "UIGroupAvatarView.h"
#import "UITools.h"
#import <Rainbow/GroupsService.h>

static int groupNameCenterConstraintDefaultValue = -15;

@interface UIGroupsTableViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *groupName;
@property (weak, nonatomic) IBOutlet UILabel *groupComment;
@property (weak, nonatomic) IBOutlet UIGroupAvatarView *groupAvatar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *groupNameCenterConstraint;

@end

@implementation UIGroupsTableViewCell

-(void) awakeFromNib {
    [super awakeFromNib];
    _groupAvatar.asCircle = YES;
    _groupAvatar.showPresence = NO;
    [UITools applyCustomFontTo:_groupName];
    [UITools applyCustomFontTo:_groupComment];
    
    [self setTintColor:[UITools defaultTintColor]];
    self.contentView.backgroundColor = [UITools defaultBackgroundColor];
    self.backgroundColor = [UITools defaultBackgroundColor];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateGroup:) name:kGroupsServiceDidUpdateGroup object:nil];
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kGroupsServiceDidUpdateGroup object:nil];
}

-(void) prepareForReuse {
    [super prepareForReuse];
    _groupName.text = nil;
    _groupName.hidden = NO;
    _groupNameCenterConstraint.constant = groupNameCenterConstraintDefaultValue;
    _groupComment.text = nil;
    _groupComment.hidden = NO;
}

-(void) setGroup:(Group *)group {
    if(_group != group){
        _group = nil;
        _group = group;
    }
    
    _groupAvatar.group = _group;
    _groupName.text = _group.name;
    _groupComment.text = _group.comment;
    [self adjustLayout];
}

-(void) adjustLayout {
    if(_group.name.length > 0){
        if(_group.name.length > 0 && _group.comment.length > 0){
            _groupNameCenterConstraint.constant = groupNameCenterConstraintDefaultValue;
            _groupComment.hidden = NO;
        } else {
            _groupNameCenterConstraint.constant = 0;
            _groupComment.hidden = YES;
        }
    }
}

-(void) didUpdateGroup:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateGroup:notification];
        });
        return;
    }
    
    Group *group = (Group *)notification.object;
    if([_group isEqual:group]){
        // An update as been received
    }
}


-(void) setSelected:(BOOL)selected animated:(BOOL)animated{
    [super setSelected:selected animated:animated];
    [_groupAvatar setSelected:selected animated:animated];
}

-(void) setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    [_groupAvatar setHighlighted:highlighted animated:animated];
}

@end
