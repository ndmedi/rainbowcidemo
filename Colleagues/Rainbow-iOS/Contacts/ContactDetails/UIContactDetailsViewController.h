/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>
#import <Rainbow/Contact.h>
#import "UINoConnectionTableViewController.h"

@interface UIContactDetailsViewController : UINoConnectionTableViewController
@property (nonatomic, weak) id fromView;
@property (nonatomic, strong) Contact *contact;
@property BOOL isPresenting3DTouch;
@end
