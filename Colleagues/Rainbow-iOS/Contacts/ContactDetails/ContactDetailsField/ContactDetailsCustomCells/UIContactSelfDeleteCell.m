//
//  UIContactSelfDeleteCell.m
//  Rainbow-iOS
//
//  Created by Cyril PAYAN on 11/06/2018.
//  Copyright © 2018 ALE International. All rights reserved.
//

#import "UIContactSelfDeleteCell.h"

@implementation UIContactSelfDeleteCell
@synthesize content = _content;

- (void)awakeFromNib {
    [super awakeFromNib];
}

-(CGFloat) computedHeight {
    return 60.0f;
}

@end
