/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIContactGroupListCell.h"
#import "CLTokenInputView.h"
#import <Rainbow/Group.h>
#import "UITools.h"
#import <Rainbow/ServicesManager.h>

@interface UIContactGroupListCell () <CLTokenInputViewDelegate>

@property (weak, nonatomic) IBOutlet CLTokenInputView *groupList;
@property (weak, nonatomic) IBOutlet UIButton *fieldAction;
@property (weak, nonatomic) IBOutlet UILabel *fieldName;

@end

@implementation UIContactGroupListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self commonInit];
    }
    return self;
}

-(instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

-(void) awakeFromNib {
    [super awakeFromNib];
    _fieldName.textColor = [UITools defaultTintColor];
    [UITools applyCustomFontTo:_fieldName];
    [_fieldAction setTitle:@"" forState:UIControlStateNormal];
    [_fieldAction.imageView setTintColor:[UITools defaultTintColor]];
    _groupList.placeholderText = NSLocalizedString(@"Add to list", nil);
    _groupList.tintColor = [UITools defaultTintColor];
    _groupList.fieldColor = [UIColor darkGrayColor];
    _groupList.drawBottomBorder = NO;
    _groupList.backgroundColor = [UIColor whiteColor];
    _groupList.delegate = self;
    [_groupList setUserInteractionEnabled:NO];
}

-(void) prepareForReuse {
    [super prepareForReuse];
    _fieldName.textColor = [UITools defaultTintColor];
    [_fieldAction.imageView setTintColor:[UITools defaultTintColor]];
}

-(void) commonInit {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLostConnection:) name:kLoginManagerDidLostConnection object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kLoginManagerDidReconnect object:nil];
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLostConnection object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidReconnect object:nil];
}

-(void) didLostConnection:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLostConnection:notification];
        });
        return;
    }
    
    switch (_content.type) {
        case SectionContentTypeGroups: {
            self.userInteractionEnabled = NO;
            break;
        }
        default:
            break;
    }
}

-(void) didReconnect:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didReconnect:notification];
        });
        return;
    }
    switch (_content.type) {
        case SectionContentTypeGroups:{
            self.userInteractionEnabled = YES;
            break;
        }
        default:
            break;
    }
}

-(void) setContent:(SectionContent *)content {
    if(_content != content){
        _content = content;
        if(_content.type == SectionContentTypeGroups){
            NSArray <Group *> *groupList = (NSArray <Group *>*)_content.content;
            [groupList enumerateObjectsUsingBlock:^(Group * group, NSUInteger idx, BOOL * stop) {
                CLToken *token = [[CLToken alloc] initWithDisplayText:group.name context:group];
                [_groupList addToken:token];
            }];
            
            if(_groupList.allTokens.count > 1)
                _fieldName.text = NSLocalizedString(@"Lists", nil);
            else
                _fieldName.text = NSLocalizedString(@"List", nil);
        }
    }
}

-(float) computedHeight {
    
    // Hide cell for guest user
    if ([ServicesManager sharedInstance].myUser.isGuest)
        return 0.0f;
        
    NSMutableArray *array = [NSMutableArray array];
    [_groupList.allTokens enumerateObjectsUsingBlock:^(CLToken * token, NSUInteger idx, BOOL * stop) {
        [array addObject:((Group*)token.context).name];
    }];
    
    NSString *string = [array componentsJoinedByString:@","];
    CGSize tokenListSize = [UITools findHeightForText:string havingWidth:_fieldName.frame.size.width andFont:[UIFont fontWithName:[UITools defaultFontName] size:15]];
    
    float size = tokenListSize.height + _fieldName.frame.size.height + _groupList.frame.size.height;
    return size;
}


@end
