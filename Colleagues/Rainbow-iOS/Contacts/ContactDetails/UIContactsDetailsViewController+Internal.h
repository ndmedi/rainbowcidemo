/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>
#import "UIContactDetailsViewController.h"
#import "SectionContent.h"
#import "MFMailComposeViewController+StatusBarStyle.h"
#import "UIScrollView+APParallaxHeader.h"
#import "UIAvatarView.h"
#import "OrderedOptionalSectionedContent.h"

@interface UIContactDetailsViewController () <UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate, APParallaxViewDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, strong) OrderedOptionalSectionedContent<SectionContent*> *sectionsContent;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *maskView;
@property (nonatomic, strong) IBOutlet UIView *fullScreenAvatarView;
@property (weak, nonatomic) IBOutlet UIImageView *fullScreenAvatarImage;
@property (weak, nonatomic) IBOutlet UIAvatarView *backgroundAvatarView;
@property (weak, nonatomic) IBOutlet UIAvatarView *avatarView;
@property (weak, nonatomic) IBOutlet UILabel *contactName;
@property (weak, nonatomic) IBOutlet UILabel *subName;
@property (weak, nonatomic) IBOutlet UILabel *companyName;
@property (weak, nonatomic) IBOutlet UIButton *calendarPresence;
@property (weak, nonatomic) IBOutlet UILabel *textualPresence;
@property (nonatomic, strong) UIVisualEffectView *visualEffectView;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *headerTapGesture;
@end
