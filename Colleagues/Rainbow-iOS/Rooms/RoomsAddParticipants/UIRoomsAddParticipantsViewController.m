/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */


#import "UIRoomsAddParticipantsViewController.h"
#import "UITools.h"
#import "UIGenericListOfObjectWithTokenViewController.h"
#import "GoogleAnalytics.h"
#import "UIContactDetailsViewController.h"
#import "UIStoryboardManager.h"

@interface UIRoomsAddParticipantsViewController ()
@property (strong, nonatomic) IBOutlet UIView *limitParticipantReachedView;
@property (weak, nonatomic) IBOutlet UILabel *limitParticipantReachedLabel;

@end

@implementation UIRoomsAddParticipantsViewController

-(void) awakeFromNib {
    [super awakeFromNib];
    [self.objects addObjectsFromArray:self.servicesManager.contactsManagerService.myNetworkContacts];
    [self sortUI];
}

-(void) setRoom:(Room *)room {
    _room = room;
    self.object = _room;
    [_room.participants enumerateObjectsUsingBlock:^(Participant * aParticipant, NSUInteger idx, BOOL * stop) {
        if(aParticipant.status == ParticipantStatusInvited || aParticipant.status == ParticipantStatusAccepted){
            if([self.objects containsObject:aParticipant.contact])
                [self.objects removeObject:aParticipant.contact];
        }
    }];
}

-(void) viewDidLoad {
    [super viewDidLoad];
    if(_room)
        self.title = NSLocalizedString(@"Add attendees", nil);
    else
        self.title = NSLocalizedString(@"Create a bubble", nil);
    
    if(self.bubbleName){
        self.nameTextField.text = self.bubbleName;
    }
    if(self.initialParticipants){
        [self.initialParticipants enumerateObjectsUsingBlock:^(Contact *contact, NSUInteger idx, BOOL * stop) {
            CLToken *token = [[CLToken alloc] initWithDisplayText:contact.displayName context:contact];
            [self.tokenInputView addToken:token];
            [[ServicesManager sharedInstance].roomsService inviteContact:contact inRoom:self.room error:nil];
        }];
    }
    [UITools applyCustomFontTo:_limitParticipantReachedLabel];
    _limitParticipantReachedLabel.textColor = [UIColor redColor];
    _limitParticipantReachedLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Limit of number of participant reached (%ld)", nil), [ServicesManager sharedInstance].myUser.maxNumberOfParticipantPerRoom];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UIRainbowGenericTableViewCell *cell = (UIRainbowGenericTableViewCell*)[tableView dequeueReusableCellWithIdentifier:kTableViewReusableKey forIndexPath:indexPath];
    if(indexPath.section == 0){
        if(!self.isTyping)
            cell.cellObject = [self.objects objectAtIndex:indexPath.row];
        else
            cell.cellObject = [self.filteredObjects objectAtIndex:indexPath.row];
    } else {
        cell.cellObject= [self.searchedObjects objectAtIndex:indexPath.row];
        // get the avatar.
        [[ServicesManager sharedInstance].contactsManagerService populateAvatarForContact:(Contact *)cell.cellObject];
    }
    __weak typeof(cell) weakCell = cell;
    cell.cellButtonTapHandler = ^(UIButton *sender) {
        [self showContactDetails:(Contact *)weakCell.cellObject];
    };
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    
    Contact *selectedContact = nil;
    if(indexPath.section == 0){
        if(!self.isTyping)
            selectedContact = [self.objects objectAtIndex:indexPath.row];
        else
            selectedContact = [self.filteredObjects objectAtIndex:indexPath.row];
    } else {
        selectedContact = [self.searchedObjects objectAtIndex:indexPath.row];
    }
    
    CLToken *token = [[CLToken alloc] initWithDisplayText:selectedContact.displayName context:selectedContact];
    [self.tokenInputView addToken:token];
    
    if(indexPath.section == 0){
        if(self.isTyping)
            [self.filteredObjects removeObjectAtIndex:indexPath.row];
        
        [self.objects removeObject:selectedContact];
    } else {
        [self.searchedObjects removeObject:selectedContact];
    }
    
    [tableView reloadData];
}

#pragma mark - Segues
- (IBAction)doneButtonClicked:(UIBarButtonItem *)sender {
    [super doneButtonClicked:sender];
    if(!_room)
        [self createRoomWithName:self.nameTextField.text topic:self.subNameTextField.text participants:self.tokenInputView.allTokens];
    else
        [self inviteParticipants:self.tokenInputView.allTokens toRoom:_room];
}


-(void) createRoomWithName:(NSString *) name topic:(NSString *) topic participants:(NSArray *) participants {
    MBProgressHUD *hud = [self showHUDWithMessage:NSLocalizedString(@"Creating new bubble ...", nil) mode:MBProgressHUDModeIndeterminate dismissCompletionBlock:nil];
    __block NSError *error = nil;
    [hud showAnimated:YES whileExecutingBlock:^{
        _room = [[ServicesManager sharedInstance].roomsService createRoom:name withTopic:topic error:&error];
    } completionBlock:^{
        if(error){
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hide:NO];
                NSString *msg = error.localizedDescription;
                if(error.code == 403620){
                    msg = NSLocalizedString(@"You cannot create a new bubble, the maximum number of collaboration spaces (bubbles or meetings) has been reached", nil);
                    [UITools showErrorPopupWithTitle:NSLocalizedString(@"Create a bubble", nil) message:msg inViewController:self];
                }
            });
            return;
        } else {
            [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"bubble" action:@"create" label:nil value:nil];
            
            if (_room) {
                [self showHUDWithMessage:NSLocalizedString(@"Bubble created",nil) mode:MBProgressHUDModeCustomView dismissCompletionBlock:^{
                    NSLog(@"THE ROOM %@ AND PARTICIPANTS %@", _room, participants);
                    [self inviteParticipants:participants toRoom:_room];
                }];
            }
            else{
                [self showHUDWithMessage:NSLocalizedString(@"Failed to create Bubble",nil) mode:MBProgressHUDModeCustomView dismissCompletionBlock:^{
                    
                }];
            }
        }
    }];
}

-(void) inviteParticipants:(NSArray *) contacts toRoom:(Room *) room {
    MBProgressHUD *hud = [self showHUDWithMessage:NSLocalizedString(@"Inviting attendees ...",nil) mode:MBProgressHUDModeIndeterminate dismissCompletionBlock:nil];
    
    [hud showAnimated:YES whileExecutingBlock:^{
        [contacts enumerateObjectsUsingBlock:^(CLToken * aToken, NSUInteger idx, BOOL * stop) {
            Participant *participant = [_room participantFromContact:(Contact*)aToken.context];
            if(participant.status == ParticipantStatusRejected)
                [[ServicesManager sharedInstance].roomsService deleteParticipant:participant fromRoom:room];
            
            NSError *error = nil;
            [[ServicesManager sharedInstance].roomsService inviteContact:(Contact*)aToken.context inRoom:room error:&error];
            if (error) {
                NSLog(@"the error is %@",error);
            }
            
            
            if(room.conference){
                Participant *theParticipant = [_room participantFromContact:(Contact*)aToken.context];
                [[ServicesManager sharedInstance].conferencesManagerService inviteParticipants:@[theParticipant] toJoinConference:room.conference inRoom:room completionBlock:nil];
            }
        }];
    } completionBlock:^{
        [self showHUDWithMessage:NSLocalizedString(@"Invitation sent",nil) mode:MBProgressHUDModeCustomView dismissCompletionBlock:^{
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        }];
    }];
}

-(NSPredicate *) predicateForText:(NSString *) text {
    return [NSPredicate predicateWithFormat:@"fullName contains[cd] %@", text];
}

-(void) sortUI {
    if(self.servicesManager.contactsManagerService.sortByFirstName){
        [self.objects sortUsingDescriptors:@[[UITools sortDescriptorForContactByFirstName], [UITools sortDescriptorForContactByLastName]]];
    } else {
        [self.objects sortUsingDescriptors:@[[UITools sortDescriptorForContactByLastName], [UITools sortDescriptorForContactByFirstName]]];
    }
}

#pragma mark - Token
- (void)tokenInputView:(CLTokenInputView *)view didAddToken:(CLToken *)token {
    [super tokenInputView:view didAddToken:token];
    NSInteger count = view.allTokens.count;
    if(_room)
        count += _room.participants.count;
    if(count > [ServicesManager sharedInstance].myUser.maxNumberOfParticipantPerRoom){
        self.tableView.tableHeaderView = _limitParticipantReachedView;
    } else {
        self.tableView.tableHeaderView = nil;
    }
}

- (void)tokenInputView:(CLTokenInputView *)view didRemoveToken:(CLToken *)token {
    [super tokenInputView:view didRemoveToken:token];
    if(view.allTokens.count <= [ServicesManager sharedInstance].myUser.maxNumberOfParticipantPerRoom){
        self.tableView.tableHeaderView = nil;
    } else {
        self.tableView.tableHeaderView = _limitParticipantReachedView;
    }
}

-(BOOL) validateUIContent {
    BOOL isValid = [super validateUIContent];
    NSInteger count = self.tokenInputView.allTokens.count;
    if(_room)
        count += _room.participants.count;
    if(count > [ServicesManager sharedInstance].myUser.maxNumberOfParticipantPerRoom)
        isValid = NO;
    
    if(!_room && [self roomNameMatchAnExistingRoom])
        isValid = NO;
    
    if(!_room && (self.nameTextField.text.length < 3 || ([[self.nameTextField.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]] length] == 0)))
        return NO;
    
    return isValid;
}

-(BOOL) roomNameMatchAnExistingRoom {
    NSArray<Room*> * rooms = [[ServicesManager sharedInstance].roomsService searchMyRoomMatchName:self.nameTextField.text];
    if(rooms.count > 0)
        return YES;
    
    return NO;
}

#pragma mark - TextField
-(void) textFieldDidChange:(ACFloatingTextField *) textField {
    if(textField == self.nameTextField){
        if(!_room){
            if([self roomNameMatchAnExistingRoom] || (textField.text.length < 3 && textField.text.length > 0) || ([[textField.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]] length] == 0)){
                textField.textColor = [UIColor redColor];
                textField.tintColor = [UIColor redColor];
            } else {
                textField.textColor = [UITools defaultTintColor];
                textField.tintColor = [UITools defaultTintColor];
            }
        }
    }
    
    [super textFieldDidChange:textField];
}

#pragma mark - show contact details
-(void) showContactDetails:(Contact *) contact {
    UIContactDetailsViewController* controller = (UIContactDetailsViewController*)[[UIStoryboardManager sharedInstance].contactsDetailsStoryBoard instantiateViewControllerWithIdentifier:@"contactDetailsViewControllerID"];
    controller.contact = contact;
    controller.fromView = self;
    [self.navigationController pushViewController:controller animated:YES];
}
@end
