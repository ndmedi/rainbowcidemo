/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIRoomsTableViewController.h"
#import "UITools.h"
#import <DZNEmptyDataSet/DZNEmptyDataSet.h>
#import <Rainbow/ServicesManager.h>
#import <Rainbow/defines.h>
#import <Rainbow/RoomsService.h>
#import "DZNSegmentedControl.h"
#import "UIRainbowGenericTableViewCell.h"
#import "Room+UIRainbowGenericTableViewCellProtocol.h"
#import "UIRoomsInvitationTableViewCell.h"
#import "UIRoomDetailsViewController.h"
#import "BGTableViewRowActionWithImage.h"
#import <JSQSystemSoundPlayer/JSQSystemSoundPlayer.h>
#import "JSQSystemSoundPlayer+JSQMessages.h"
#import "UINotificationManager.h"
#import "CustomSearchController.h"
#import "UIContactDetailsViewController.h"
#import "UIGroupDetailsViewController.h"
#import "CustomNavigationController.h"
#import "GoogleAnalytics.h"
#import "MyInfoNavigationItem.h"
#import "UIStoryboardManager.h"
#import "UIGenericBubbleDetailsViewController.h"

#import "FilteredSortedSectionedArray.h"
#import "UIStoryboardManager.h"

#import "UIViewController+Visible.h"
#import "UIPagingMessagesViewController.h"
#import "RecentsConversationsTableViewController.h"
#import "UINetworkLostViewController.h"
#import "UIGuestModeViewController.h"

#define kRoomsInvitationTableViewReusableKey @"roomInvitationCellID"
#define kRoomInvitationsSection @"Invitations"
#define kRoomsSection @"Bubbles"

#define dznButtonDisabledColor 0xB2B2B2FF

typedef NS_ENUM(NSInteger, SelectorType){
    SelectorTypeAll = 0,
    SelectorTypeMyRooms,
    SelectorTypeLeftRooms,
    SelectorTypeInActive
};

typedef NS_ENUM(NSInteger, SortOrder){
    SortOrderByCreationDate = 0,
    SortOrderByName,
    SortOrderStoredValue
};

typedef NS_ENUM(NSInteger, RoomSelectionType){
    RoomSelectionTypeOwnedOnly = 0,
    RoomSelectionTypeParticipantOnly,
    RoomSelectionTypeBoth
};

@interface UIRoomsTableViewController () <DZNEmptyDataSetDelegate, DZNEmptyDataSetSource, DZNSegmentedControlDelegate, UIViewControllerPreviewingDelegate,
UIPopoverPresentationControllerDelegate, UISearchResultDisplayDetailControllerProtocol> {
    MBProgressHUD *hud;
}
@property (nonatomic, strong) NSMutableArray * inactiveRoomsIDsArray;
@property (nonatomic, strong) ServicesManager *serviceManager;
@property (nonatomic, strong) RoomsService *roomsService;
@property (nonatomic, strong) ConversationsManagerService *conversationManagerService;
@property (nonatomic, strong) IBOutlet DZNSegmentedControl *roomsFilter;

// This outlet must be keep in strong reference because we must increase the retainCount of the button, to avoid releasing it when displaying it and removing it from the UI depending of search bar state.
@property (nonatomic, strong) IBOutlet UIBarButtonItem *roomRightBarButtonItem;
@property (nonatomic, strong) NSMutableArray *presentedLocalNotification;
@property (nonatomic, strong) CustomSearchController *searchController;
@property (nonatomic, strong) id previewingContext;

@property (nonatomic, strong) FilteredSortedSectionedArray <Room *> *rooms;
@property (nonatomic, strong) NSObject *roomsMutex;
@property (nonatomic, strong) NSPredicate *globalFilterAll;
@property (nonatomic, strong) NSPredicate *globalFilterMyRooms;
@property (nonatomic, strong) NSPredicate *globalFilterInactive;
@property (nonatomic, strong) NSPredicate *globalFilterArchived;
@property (nonatomic, strong) SectionNameComputationBlock sectionedByName;
@property (nonatomic, strong) NSSortDescriptor *sortSectionAsc;

@property (nonatomic, strong) NSPredicate *filterDisplayMoreButton;
@property (nonatomic, strong) UIAlertController *moreMenuActionSheet;
@property (nonatomic, strong) UIAlertController *sortRoomsMenuActionSheet;
@property (nonatomic, strong) IBOutlet UIBarButtonItem *moreMenuButton;
@property (nonatomic) SortOrder selectedSortOrder;

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic) BOOL lostNetworkViewAsBeenScrolled;
@property (nonatomic) BOOL guestBannerViewAsBeenScrolled;
@property (nonatomic) BOOL isNetworkConnected;
@property (nonatomic) BOOL canRefreshUI;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;

@property (nonatomic) BOOL populated;

@property (strong, nonatomic) IBOutlet UIToolbar *footerToolBar;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *doneButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *selectAllButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *archiveSelectionButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *deleteLeaveSelectionButton;
@end

@implementation UIRoomsTableViewController

-(instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleLocalNotification:) name:kNotificationsManagerHandleNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerDidLoginSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:kLoginManagerDidLogoutSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLostConnection:) name:kLoginManagerDidLostConnection object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kLoginManagerDidReconnect object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEndPopulatingMyNetwork:) name:kContactsManagerServiceDidEndPopulatingMyNetwork object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveAllRooms:) name:kRoomsServiceDidRemoveAllRooms object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveRoom:) name:kRoomsServiceDidRemoveRoom object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didAddRoom:) name:kRoomsServiceDidAddRoom object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateRoom:) name:kRoomsServiceDidUpdateRoom object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveRoomInvitation:) name:kRoomsServiceDidReceiveRoomInvitation object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRoomInvitationStatusChanged:) name:kRoomsServiceDidRoomInvitationStatusChanged object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dumpBubbles) name:@"dumpBubbles" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFailRemoveRoom:) name:kRoomsServiceDidFailRemoveRoom object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didStartConversation:) name:kConversationsManagerDidStartConversation object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEndLoadingRoomsFromCache:) name:kConversationsManagerDidEndLoadingRoomsFromCache object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willShowNetworkLostView:) name:CustomNavigationControllerWillShowNetworkLostView object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeNetworkLostViewFrame:) name:CustomNavigationControllerDidChangeNetworkLostViewFrame object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willShowGuestModeView:) name:CustomNavigationControllerWillShowGuestModeView object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didHideGuestModeView:) name:CustomNavigationControllerDidHideGuestModeView object:nil];
        
        _roomsMutex = [NSObject new];
        _presentedLocalNotification = [NSMutableArray array];
        
        _sectionedByName = ^NSString*(Room *room) {
            if(room.conference.endpoint.mediaType != ConferenceEndPointMediaTypePSTNAudio && room.myStatusInRoom == ParticipantStatusInvited)
                return kRoomInvitationsSection;
            return kRoomsSection;
        };
        
        _inactiveRoomsIDsArray = [NSMutableArray array];
        
        _globalFilterAll = [NSPredicate predicateWithBlock:^BOOL(Room *room, NSDictionary<NSString *,id> * bindings) {
            
            if([room.topic isEqualToString:@"Rainbow_OutlookCreation_InternalUseOnly"])
                return NO;
            if(room.conference.mediaType == ConferenceMediaTypePSTN)
                return NO;
            if(room.myStatusInRoom != ParticipantStatusAccepted && room.myStatusInRoom != ParticipantStatusInvited)
                return NO;
            if (!room.isActive && room.isMyRoom)
                return NO;
                
            return YES;
        }];
        
        _globalFilterMyRooms = [NSPredicate predicateWithBlock:^BOOL(Room *evaluatedObject, NSDictionary<NSString *,id> *  bindings) {
            if([evaluatedObject.topic isEqualToString:@"Rainbow_OutlookCreation_InternalUseOnly"])
                return NO;
            if(evaluatedObject.conference.mediaType == ConferenceMediaTypePSTN)
                return NO;
            if (evaluatedObject.myStatusInRoom != ParticipantStatusAccepted)
                return NO;
            if (!evaluatedObject.isActive)
                return NO;
            if (![evaluatedObject.creator isEqual: [ServicesManager sharedInstance].myUser.contact])
                return NO;
            
            return YES;
        }];
        
        _globalFilterInactive = [NSPredicate predicateWithBlock:^BOOL(Room *evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
            if([evaluatedObject.topic isEqualToString:@"Rainbow_OutlookCreation_InternalUseOnly"])
                return NO;
            if(evaluatedObject.conference.mediaType == ConferenceMediaTypePSTN)
                return NO;
            if(evaluatedObject.myStatusInRoom != ParticipantStatusAccepted && evaluatedObject.myStatusInRoom != ParticipantStatusInvited)
                return NO;
            if (evaluatedObject.isActive)
                return NO;
            if (!evaluatedObject.isMyRoom)
                return NO;
            
            return YES;
        }];
        
        _globalFilterArchived = [NSPredicate predicateWithBlock:^BOOL(Room *evaluatedObject, NSDictionary<NSString *,id> *  bindings) {
            if([evaluatedObject.topic isEqualToString:@"Rainbow_OutlookCreation_InternalUseOnly"])
                return NO;
            if(evaluatedObject.conference.mediaType == ConferenceMediaTypePSTN)
                return NO;
            
            if (evaluatedObject.myStatusInRoom != ParticipantStatusUnsubscribed)
                return NO;
            return YES;
        }];
        
        _filterDisplayMoreButton = [NSPredicate predicateWithBlock:^BOOL(Room *room, NSDictionary<NSString *,id> *  bindings) {
            if ([room.topic isEqualToString:@"Rainbow_OutlookCreation_InternalUseOnly"])
                return NO;
            if (room.conference.mediaType == ConferenceMediaTypePSTN)
                return NO;
            if (room.myStatusInRoom == ParticipantStatusAccepted || room.myStatusInRoom == ParticipantStatusUnsubscribed)
                return YES;
            return NO;
        }];
        
        _sortSectionAsc = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES comparator:^NSComparisonResult(NSString* obj1, NSString* obj2) {
            return [obj1 isEqualToString:kRoomInvitationsSection] ? NSOrderedAscending : NSOrderedDescending;
        }];
        
        _rooms = [FilteredSortedSectionedArray new];
        
        // Default filters/sorter/.. values
        _rooms.sectionNameFromObjectComputationBlock = _sectionedByName;
        _rooms.globalFilteringPredicate = _globalFilterAll;
        _rooms.sectionSortDescriptor = _sortSectionAsc;
        
        // __default__ has a special meaning : any other sections not found in the dictionary
        _rooms.objectSortDescriptorForSection = @{@"__default__": @[[UITools sortDescriptorByCreationDate]]};
        
        _serviceManager = [ServicesManager sharedInstance];
        _roomsService = _serviceManager.roomsService;
        _conversationManagerService = _serviceManager.conversationsManagerService;
        
        // When the room is opened in a conversation, sometimes we get the didAddRoom before this init, so we lose it, so just add initial rooms
        for (Room *room in [_roomsService.rooms copy]) {
            [self addRoom:room];
        }
        [_rooms reloadData];
        
        // Don't enable the add button until the roster is populated
        _populated = NO;
        _roomRightBarButtonItem.enabled = NO;
        _canRefreshUI = _roomsService.minimalCacheLoaded;
    }
    return self;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLogoutSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLostConnection object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidReconnect object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidEndPopulatingMyNetwork object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidChangeContactDisplayUserSettings object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidRemoveAllRooms object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidRemoveRoom object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidAddRoom object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidUpdateRoom object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidReceiveRoomInvitation object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidRoomInvitationStatusChanged object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"dumpBubbles" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidFailRemoveRoom object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidStartConversation object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidEndLoadingRoomsFromCache object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerDidHideNetworkLostView object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerWillShowNetworkLostView object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerDidChangeNetworkLostViewFrame object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerDidHideGuestModeView object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CustomNavigationControllerWillShowGuestModeView object:nil];
    
    
    [self didLogout:nil];
    
    @synchronized (_roomsMutex) {
        [_rooms removeAllObjects];
    }
    _roomsMutex = nil;
    
    [_presentedLocalNotification removeAllObjects];
    _presentedLocalNotification = nil;
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if(![ServicesManager sharedInstance].loginManager.isConnected)
        [self didLostConnection:nil];
    
    BOOL isGuestBannerView = ((CustomNavigationController *)self.navigationController).isDisplayingGuestModeView;
    if(isGuestBannerView  && !_guestBannerViewAsBeenScrolled){
        [self willShowGuestModeView:nil];
    }
    
    if (_inactiveRoomsIDsArray.count > 1) {
        //warn the user that there are inactive bubbles
        hud = [UITools showHUDWithMessage:@"" forView:self.view mode:MBProgressHUDModeText dismissCompletionBlock:nil];
        UIFont * aFont = [UIFont fontWithName:[UITools defaultFontName] size:14.0];
        [hud setDetailsLabelFont:aFont];
        hud.detailsLabelText = NSLocalizedString(@"You have several bubbles without activity for at least 60 days. You can archive or delete them.", nil);
    
        CGFloat bottomPadding = 0;
        if (@available(iOS 11.0, *)) {
            UIWindow *window = UIApplication.sharedApplication.keyWindow;
            bottomPadding = window.safeAreaInsets.bottom;
        }
        hud.yOffset = self.view.bounds.size.height/2 - 100 - bottomPadding;
        hud.color = [UIColor colorWithRed:225/255.0 green:114/255.0 blue:31/255.0 alpha:0.8] ;
        [hud hide:YES afterDelay:5.0];
        hud.userInteractionEnabled = NO;
        [hud removeFromSuperViewOnHide];
        [hud show:YES];
    }
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if(![ServicesManager sharedInstance].loginManager.isConnected)
        [self didReconnect:nil];
    if(self.tableView.isEditing)
        [self setEditModeEnabled:NO];
    
    if (hud) {
        [hud removeFromSuperview];
        hud = nil;
    }
    
}

#pragma mark - Login/logout notification

-(void) didLogin:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLogin:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && _canRefreshUI)
        [self.tableView reloadData];
    
    [self checkAndEnableAddBubbleButton];
}

-(void) didLogout:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLogout:notification];
        });
        return;
    }
    
    @synchronized (_roomsMutex) {
        [_rooms removeAllObjects];
    }
    
    [self.tableView reloadData];
    
    self.tabBarController.selectedIndex = 0;
    [self.navigationController popToRootViewControllerAnimated:NO];
    
    _populated = NO;
    _roomRightBarButtonItem.enabled = NO;
    [_inactiveRoomsIDsArray removeAllObjects];

}

-(void) didLostConnection:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLostConnection:notification];
        });
        return;
    }
    
    if([self isViewLoaded]){
        _roomRightBarButtonItem.enabled = NO;
        [self.tableView reloadData];
    }
}

-(void) didReconnect:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didReconnect:notification];
        });
        return;
    }
    if([self isViewLoaded]) {
        [self checkAndEnableAddBubbleButton];
        [self.tableView reloadData];
    }
    
}

-(void) didEndPopulatingMyNetwork:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didEndPopulatingMyNetwork:notification];
        });
        return;
    }
    
    NSLog(@"[UIRoomsTableViewController] didEndPopulatingMyNetwork notifications");
    _populated = YES;
    [self checkAndEnableAddBubbleButton];
}

#pragma mark - network lost & guest mode notifications
-(void) didChangeNetworkLostViewFrame:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideNetworkLostView:notification];
        });
        return;
    }
}

-(void) willShowNetworkLostView:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideNetworkLostView:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && !_lostNetworkViewAsBeenScrolled){
        self.topConstraint.constant += kNetworkLostHeight;
        _lostNetworkViewAsBeenScrolled = YES;
        [UIView animateWithDuration:0.75f animations:^{
            [self.view setNeedsLayout];
        } completion:^(BOOL finished) {
            _isNetworkConnected = NO;
        }];
    }
}

-(void) didHideNetworkLostView:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideNetworkLostView:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && _lostNetworkViewAsBeenScrolled){
        self.topConstraint.constant -= kNetworkLostHeight;
        _lostNetworkViewAsBeenScrolled = NO;
        [UIView animateWithDuration:0.25f animations:^{
            [self.view setNeedsLayout];
        } completion:^(BOOL finished) {
            _isNetworkConnected = YES;
        }];
    }
}

-(void) didHideGuestModeView:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didHideGuestModeView:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && _guestBannerViewAsBeenScrolled){
        self.topConstraint.constant -= kGuestModeHeight;
        _guestBannerViewAsBeenScrolled = NO;
        [UIView animateWithDuration:0.75f animations:^{
            [self.view setNeedsLayout];
        } completion:nil];
    }
}

-(void) willShowGuestModeView:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self willShowGuestModeView:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && !_guestBannerViewAsBeenScrolled){
        self.topConstraint.constant += kGuestModeHeight;
        _guestBannerViewAsBeenScrolled = YES;
        [UIView animateWithDuration:0.25f animations:^{
            [self.view setNeedsLayout];
        } completion:^(BOOL finished) {
            _isNetworkConnected = NO;
        }];
    }
}

-(void) handleLocalNotification:(NSNotification *) notification {
    // TODO: Implement join/decline from local notification
}

#pragma mark - Conversation notifications
-(void) didStartConversation:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didStartConversation:notification];
        });
        return;
    }
    
    // We don't want to open the conversation view if the application is in background, case of message send from a push notification.
    if([UIApplication sharedApplication].applicationState == UIApplicationStateBackground)
        return;
    
    if(!self.isVisible)
        return;
    
    Conversation *theConversation = (Conversation *)notification.object;
    [RecentsConversationsTableViewController openConversationViewForConversation:theConversation inViewController:self.navigationController];
}


-(void) didEndLoadingRoomsFromCache:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didEndLoadingRoomsFromCache:notification];
        });
        return;
    }
    
    
    for (Room *room in [_roomsService.rooms copy]) {
        [self addRoom:room];
    }
    
    _canRefreshUI = YES;
    [self reloadUI];
}

-(void) addRoom:(Room *) room {
    if (room) {
        @synchronized (_roomsMutex) {
            if (![_rooms containsObject:room]){
                [_rooms addObject:room];
                if (!room.isActive && room.isMyRoom) {
                    if(![room.topic isEqualToString:@"Rainbow_OutlookCreation_InternalUseOnly"] && room.conference.mediaType != ConferenceMediaTypePSTN && room.myStatusInRoom == ParticipantStatusAccepted) {
                        [_inactiveRoomsIDsArray addObject:room.rainbowID];
                    }
                }
            }
        }
    }
}

# pragma mark - Rooms service notifications
-(void) didAddRoom:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didAddRoom:notification];
        });
        return;
    }
    
    Room *theRoom = (Room *) notification.object;
    [self addRoom:theRoom];
    
    [self reloadUI];
}

-(void) didUpdateRoom:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateRoom:notification];
        });
        return;
    }
    
    NSDictionary *userInfo = (NSDictionary *)notification.object;
    NSArray<NSString *> *changedKeys = [userInfo objectForKey:kRoomChangedAttributesKey];
    
    Room *theRoom = (Room *) [userInfo objectForKey:@"room"];
    if ([_inactiveRoomsIDsArray containsObject:theRoom.rainbowID]) {
        [_inactiveRoomsIDsArray removeObject:theRoom.rainbowID];
    }

    if([changedKeys containsObject:@"name"] || [changedKeys containsObject:@"myStatusInRoom"] || [changedKeys containsObject:@"topic"] || [changedKeys containsObject:@"users"] || [changedKeys containsObject:@"creator"] || [changedKeys containsObject:@"isActive"]){
        [self reloadUI];
    }
}

-(void) didRemoveRoom:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRemoveRoom:notification];
        });
        return;
    }
    
    Room *theRoom = (Room *) notification.object;
    @synchronized (_roomsMutex) {
        if([_rooms containsObject:theRoom]){
            [_rooms removeObject:theRoom];
        }
    }
    [self reloadUI];
}

-(void) didFailRemoveRoom:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didFailRemoveRoom:notification];
        });
        return;
    }
    Room *theRoom = (Room *) notification.object;
    NSString *errorMessage = [NSString stringWithFormat:NSLocalizedString(@"Failed to remove %@ 's bubble",nil),[theRoom displayName]];
    dispatch_async(dispatch_get_main_queue(), ^{
        [UITools showHUDWithMessage:errorMessage forView:self.view mode:MBProgressHUDModeCustomView imageName:@"Failed" dismissCompletionBlock:nil];
    });
}

-(void) didRemoveAllRooms:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRemoveAllRooms:notification];
        });
        return;
    }
    
    @synchronized (_roomsMutex) {
        [_rooms removeAllObjects];
    }
    [self reloadUI];
}

-(void) didReceiveRoomInvitation:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didReceiveRoomInvitation:notification];
        });
        return;
    }
    
    if([self isViewLoaded] && _canRefreshUI){
        [_rooms reloadData];
        [self.tableView reloadData];
    }
}

-(void) didRoomInvitationStatusChanged:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRoomInvitationStatusChanged:notification];
        });
        return;
    }
    
    Room *room = notification.object;
    
    if(room.myStatusInRoom == ParticipantStatusAccepted){
        [UITools showHUDWithMessage:NSLocalizedString(@"Accepted", nil) forView:self.view mode:MBProgressHUDModeCustomView imageName:@"Checkmark" dismissCompletionBlock:nil];
    }
    
    [self reloadUI];
    
    __block UILocalNotification *notifSent = nil;
    [_presentedLocalNotification enumerateObjectsUsingBlock:^(UILocalNotification *localNotification, NSUInteger idx, BOOL * stop) {
        if([[localNotification.userInfo valueForKey:@"from"] isEqualToString:room.jid]) {
            notifSent = localNotification;
            *stop = YES;
        }
    }];
    if(notifSent){
        [[UIApplication sharedApplication] cancelLocalNotification:notifSent];
        [_presentedLocalNotification removeObject:notifSent];
    }
}

#pragma mark - View life cycle

-(void) viewDidLoad {
    [super viewDidLoad];
    
    _serviceManager = [ServicesManager sharedInstance];
    _roomsService = _serviceManager.roomsService;
    _conversationManagerService = _serviceManager.conversationsManagerService;
    
    ((MyInfoNavigationItem*)self.navigationItem).parentViewController = self;
    ((MyInfoNavigationItem*)self.navigationItem).customRightItems = @[_moreMenuButton, _roomRightBarButtonItem];
    
    self.definesPresentationContext = YES;
    self.extendedLayoutIncludesOpaqueBars = YES;
    
    // Footer edit toolbar
    //_doneButton.tintColor = [UITools defaultTintColor];
    _selectAllButton.title = NSLocalizedString(@"Select all", nil);
    _selectAllButton.tintColor = [UITools defaultTintColor];
    _archiveSelectionButton.title = NSLocalizedString(@"Archive", nil);
    _archiveSelectionButton.tintColor = [UITools defaultTintColor];
    _deleteLeaveSelectionButton.tintColor = [UITools defaultTintColor];
    [self updateSelectionButtonTitle];
    // table view
    self.tableView.rowHeight = kRainbowGenericTableViewCellHeight;
    self.title = NSLocalizedString(@"Bubbles", nil);
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    self.tableView.allowsMultipleSelectionDuringEditing = YES;
    
    self.tableView.tableFooterView = [UIView new];
    [self.tableView setBackgroundColor:[UITools defaultBackgroundColor]];
    
    if ([UITools isForceTouchAvailableForTraitCollection:self.traitCollection]) {
        self.previewingContext = [self registerForPreviewingWithDelegate:self sourceView:self.view];
    }
    
    [self.tableView registerNib:[UINib nibWithNibName:@"UIRainbowGenericTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewReusableKey];
    
    [_roomsFilter setItems:@[NSLocalizedString(@"bubbles_all", nil), NSLocalizedString(@"My bubbles", nil), NSLocalizedString(@"Archives", nil), NSLocalizedString(@"Inactive", nil)]];
    _roomsFilter.delegate = self;
    _roomsFilter.showsCount = NO;
    _roomsFilter.autoAdjustSelectionIndicatorWidth = NO;
    _roomsFilter.height = 60;
    _roomsFilter.selectionIndicatorHeight = 4.0f;
    
    [_roomsFilter addTarget:self action:@selector(roomsFilterValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    CGRect frameTab = _roomsFilter.frame;
    frameTab.size.width = self.view.frame.size.width;
    _roomsFilter.frame = frameTab;
    
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, _roomsFilter.height, 0);
    
    [self sortBySortOrder:SortOrderStoredValue];
}

-(void) viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    if (@available(iOS 11.0, *)) {
        [_footerToolBar setFrame:CGRectMake(0, self.tabBarController.tabBar.frame.origin.y+self.tabBarController.tabBar.frame.size.height-44-self.view.safeAreaInsets.bottom, self.tabBarController.tabBar.frame.size.width, 44)];
    } else {
        [_footerToolBar setFrame:CGRectMake(0, self.tabBarController.tabBar.frame.origin.y+self.tabBarController.tabBar.frame.size.height-44, self.tabBarController.tabBar.frame.size.width, 44)];
    }
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if([_rooms objectsInSection:kRoomInvitationsSection].count > 0)
        _roomsFilter.selectedSegmentIndex = 0;
    
    [self reloadUI];
}

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(BOOL) shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (void)roomsFilterValueChanged:(UISegmentedControl *)sender {
    
    if(sender.selectedSegmentIndex == SelectorTypeAll)
        _rooms.globalFilteringPredicate = _globalFilterAll;
    if(sender.selectedSegmentIndex == SelectorTypeMyRooms)
        _rooms.globalFilteringPredicate = _globalFilterMyRooms;
    if(sender.selectedSegmentIndex == SelectorTypeInActive)
        _rooms.globalFilteringPredicate = _globalFilterInactive;
    if(sender.selectedSegmentIndex == SelectorTypeLeftRooms)
        _rooms.globalFilteringPredicate = _globalFilterArchived;

    [_rooms reloadData];
    [self.tableView reloadData];
    
    if(self.tableView.editing)
       [self updateSelectionButtonTitle];
}

+(BOOL) hasAcceptedParticipantsInRoom:(Room *) room {
    __block BOOL foundAcceptedParticipant = NO;
    [room.participants enumerateObjectsUsingBlock:^(Participant * participant, NSUInteger idx, BOOL * stop) {
        if(participant.status == ParticipantStatusAccepted && ![participant.contact isEqual:room.creator]){
            foundAcceptedParticipant = YES;
            *stop = YES;
        }
    }];
    
    return foundAcceptedParticipant;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[_rooms sections] count];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *sectionName = [[_rooms sections] objectAtIndex:section];
    if([_rooms objectsInSection:sectionName].count > 0){
        if([[_rooms sections] count] == 2 || [sectionName isEqualToString:kRoomInvitationsSection])
            return NSLocalizedString(sectionName, nil);
    }
    return @"";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *key = [[_rooms sections] objectAtIndex:section];
    return [[_rooms objectsInSection:key] count];
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *)view;
    tableViewHeaderFooterView.textLabel.font = [UIFont fontWithName:[UITools boldFontName] size:16.0f];
    tableViewHeaderFooterView.textLabel.textColor = [UITools defaultTintColor];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [[_rooms sections] objectAtIndex:indexPath.section];
    Room *room = [[_rooms objectsInSection:key] objectAtIndex:indexPath.row];
    
    if (room.myStatusInRoom == ParticipantStatusInvited) {
        UIRoomsInvitationTableViewCell *cell = (UIRoomsInvitationTableViewCell*)[tableView dequeueReusableCellWithIdentifier:kRoomsInvitationTableViewReusableKey forIndexPath:indexPath];
        cell.cellObject = room;
        cell.roomsService = _roomsService;
        return cell;
    }
    
    UIRainbowGenericTableViewCell *cell = (UIRainbowGenericTableViewCell*)[tableView dequeueReusableCellWithIdentifier:kTableViewReusableKey forIndexPath:indexPath];
        cell.cellObject = room;
    __weak typeof(cell) weakCell = cell;
    cell.cellButtonTapHandler = ^(UIButton *sender) {
        [self showRoomDetails:(Room*)weakCell.cellObject];
    };
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(!tableView.isEditing) {
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        NSString *key = [[_rooms sections] objectAtIndex:indexPath.section];
        Room *room = [[_rooms objectsInSection:key] objectAtIndex:indexPath.row];
        if (room.myStatusInRoom == ParticipantStatusInvited) {
            [self showRoomDetails:room];
            return;
        }
        
        [_conversationManagerService startConversationWithPeer:room withCompletionHandler:nil];
    } else {
        [self updateSelectionButtonTitle];
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self updateSelectionButtonTitle];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {    
    NSString *key = [[_rooms sections] objectAtIndex:indexPath.section];
    Room *room = [[_rooms objectsInSection:key] objectAtIndex:indexPath.row];
    if (room.myStatusInRoom == ParticipantStatusInvited) {
        return NO;
    }
    
    return YES;
}

+(void)leaveAction:(Room *)room inController:(UIViewController *) controller completionHandler:(void (^ __nullable)())completionHandler {
    NSString *message = room.isMyRoom?NSLocalizedString(@"This bubble will be deleted. Participants will no longer have access to the bubble.", nil):NSLocalizedString(@"You will leave this bubble and will no longer be able to publish information.", nil);
    
    UIAlertController *confirmDeleteActionSheet = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"Leave and archive", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [[self class] leaveAction:room createArchive:YES completionHandler:completionHandler];
    }];
    UIAlertAction *okWithoutArchive = [UIAlertAction actionWithTitle:NSLocalizedString(@"Leave without archive", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
        [[self class] leaveAction:room createArchive:NO completionHandler:completionHandler];
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    
    [confirmDeleteActionSheet addAction:ok];
    [confirmDeleteActionSheet addAction:okWithoutArchive];
    [confirmDeleteActionSheet addAction:cancel];
    // show the menu.
    [confirmDeleteActionSheet.view setTintColor:[UITools defaultTintColor]];
    [controller presentViewController:confirmDeleteActionSheet animated:YES completion:nil];
}

+(void)leaveAction:(Room*)room createArchive:(BOOL)createArchive completionHandler:(void (^ __nullable)())completionHandler {
    if(createArchive) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
            [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"bubble" action:@"leave" label:nil value:nil];
            [[ServicesManager sharedInstance].roomsService leaveRoom:room];
            if(completionHandler)
                completionHandler();
        });
    }
    else {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
            [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"bubble" action:@"delete" label:nil value:nil];
            [[ServicesManager sharedInstance].roomsService deleteParticipant:[room participantFromContact:[ServicesManager sharedInstance].myUser.contact] fromRoom:room];
            if(completionHandler)
                completionHandler();
        });
    }
}

+(void)archiveAction:(Room *)room inController:(UIViewController *) controller completionHandler:(void (^ __nullable)())completionHandler {
    NSString *message = room.isMyRoom?NSLocalizedString(@"This bubble will be archived. Participants will no longer be able publish information.", nil) : @"";
    
    UIAlertController *confirmDeleteActionSheet = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
        [[self class] archiveAction:room completionHandler:completionHandler];
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    
    [confirmDeleteActionSheet addAction:ok];
    [confirmDeleteActionSheet addAction:cancel];
    // show the menu.
    [confirmDeleteActionSheet.view setTintColor:[UITools defaultTintColor]];
    [controller presentViewController:confirmDeleteActionSheet animated:YES completion:nil];
}

+(void)archiveAction:(Room *)room completionHandler:(void (^)())completionHandler {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"bubble" action:@"archive" label:nil value:nil];
        [[ServicesManager sharedInstance].roomsService archiveRoom:room];
        if(completionHandler)
            completionHandler();
    });
}

+(void)deleteAction:(Room *)room inController:(UIViewController *) controller completionHandler:(void (^ __nullable)())completionHandler {
    NSString *message = room.isMyRoom?NSLocalizedString(@"This bubble will be deleted. Participants will no longer have access to the bubble.", nil):NSLocalizedString(@"Bubble will be removed for your archiving list. You will no longer have access to the information contained in this bubble.", nil);
    
    UIAlertController *confirmDeleteActionSheet = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
        Conversation *conversation = [[ServicesManager sharedInstance].conversationsManagerService getConversationWithPeerJID:room.jid];
        if (conversation.status != ConversationStatusInactive)
            [[ServicesManager sharedInstance].conversationsManagerService setStatus:ConversationStatusInactive forConversation:conversation];
        [[self class] deleteAction:room completionHandler:completionHandler];
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    
    [confirmDeleteActionSheet addAction:ok];
    [confirmDeleteActionSheet addAction:cancel];
    // show the menu.
    [confirmDeleteActionSheet.view setTintColor:[UITools defaultTintColor]];
    [controller presentViewController:confirmDeleteActionSheet animated:YES completion:nil];
}

+(void)deleteAction:(Room *)room completionHandler:(void (^)())completionHandler {
    if(room.isMyRoom){
        [[ServicesManager sharedInstance].roomsService deleteRoom:room];
        if(completionHandler)
            completionHandler();
    } else {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
            [[ServicesManager sharedInstance].roomsService deleteParticipant:[room participantFromContact:[ServicesManager sharedInstance].myUser.contact] fromRoom:room];
            if(completionHandler)
                completionHandler();
        });
    }

    [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"bubble" action:@"delete" label:nil value:nil];
}


- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *key = [[_rooms sections] objectAtIndex:indexPath.section];
    Room *room = [[_rooms objectsInSection:key] objectAtIndex:indexPath.row];
    // The quit button
    BGTableViewRowActionWithImage *quit = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Quit", nil) backgroundColor:[UITools redColor] image:nil forCellHeight:kRainbowGenericTableViewCellHeight+10 handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [[self class] leaveAction:room inController:self completionHandler:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                self.tableView.editing = NO;
            });
        }];
    }];
    
    
    // The Archive button
    BGTableViewRowActionWithImage *archive = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Archive", nil) backgroundColor:[UITools defaultTintColor] image:nil forCellHeight:kRainbowGenericTableViewCellHeight+10 handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [[self class] archiveAction:room inController:self completionHandler:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                self.tableView.editing = NO;
            });
        }];
    }];
    
    // The delete button
    BGTableViewRowActionWithImage *delete = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Delete", nil) backgroundColor:[UITools redColor] image:nil forCellHeight:kRainbowGenericTableViewCellHeight+10 handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
        [[self class] deleteAction:room inController:self completionHandler:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                self.tableView.editing = NO;
            });
        }];
    }];
    
    if(_roomsFilter.selectedSegmentIndex == 0) {
        return room.isMyRoom ? @[delete,archive] : @[quit];
    } else if(_roomsFilter.selectedSegmentIndex == 1){
        return room.isMyRoom ? @[delete,archive]:@[];
    } else {
        return @[delete];
    }
}

-(void) sortBySortOrder:(SortOrder) sortOrder {
    
    // Get saved sorting order (if exists)
    // It can be "Name" or "CreationDate"
    NSString *storedSortingOrder = [[NSUserDefaults standardUserDefaults] stringForKey:@"bubblesSortingOrder"];
    
    // If we ask for the stored value, we override the sortOrder parameter
    // with the stored value (or default value if no stored one)
    if (sortOrder == SortOrderStoredValue) {
        // Set the default to the creation date
        sortOrder = SortOrderByCreationDate;
        
        if ([storedSortingOrder isEqualToString:@"Name"])
            sortOrder = SortOrderByName;
    }
    
    // Avoid doing the sort if it is already well sorted
    if (_selectedSortOrder == sortOrder)
        return;
    
    // Then apply the sortOrder (which could be real parameter or override param with stored value)
    if (sortOrder == SortOrderByName) {
        _rooms.objectSortDescriptorForSection = @{@"__default__": @[[UITools sortDescriptorByDisplayName]]};
        storedSortingOrder = @"Name";
    } else if (sortOrder == SortOrderByCreationDate) {
        _rooms.objectSortDescriptorForSection = @{@"__default__": @[[UITools sortDescriptorByCreationDate]]};
        storedSortingOrder = @"CreationDate";
    }
    
    // Store new sorting order
    [[NSUserDefaults standardUserDefaults] setObject:storedSortingOrder forKey:@"bubblesSortingOrder"];

    _selectedSortOrder = sortOrder;
    
    [self reloadUI];
}

#pragma mark - DZNEmptyDataSet
- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {
    return [UIImage imageNamed:@"EmptyBubbles"];
}

- (UIColor *)imageTintColorForEmptyDataSet:(UIScrollView *)scrollView {
    return [UITools defaultTintColor];
}

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSMutableString *text = [[NSMutableString alloc] init];
    
    if(_roomsFilter.selectedSegmentIndex == 2) {
        [text appendString:NSLocalizedString(@"You have no archived bubbles.", nil)];
        [text appendString:@"\n"];
    }
    
    if(_roomsFilter.selectedSegmentIndex == 3) {
        [text appendString:NSLocalizedString(@"You have no inactive bubbles.", nil)];
        [text appendString:@"\n"];
    }
    
    [text appendString:NSLocalizedString(@"Create, share and work together with Bubbles.", nil)];
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:[UITools defaultFontName] size:16.0f], NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = NSLocalizedString(@"Invite Rainbow users in your Bubbles and start team conversations.", nil);
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:[UITools defaultFontName] size:16.0f], NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:[UITools defaultFontName] size:16.0f], NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    return [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Create a bubble", nil) attributes:attributes];
}

-(UIImage *) buttonBackgroundImageForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    // Change button color for guest user
    if ([ServicesManager sharedInstance].myUser.isGuest || !_roomRightBarButtonItem.enabled) {
        return [UITools imageWithColor:[UITools colorFromHexa:dznButtonDisabledColor] size:CGSizeMake(1, 40)];
    } else {
        return [UITools imageWithColor:[UITools defaultTintColor] size:CGSizeMake(1, 40)];
    }
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView {
    return [UITools defaultBackgroundColor];
}

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView {
    CGFloat offset = CGRectGetHeight([UIApplication sharedApplication].statusBarFrame);
    offset += CGRectGetHeight(self.navigationController.navigationBar.frame);
    offset -= CGRectGetHeight(_roomsFilter.frame);
    return -offset;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView {
    return 0.0f;
}

- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView {
    return YES;
}

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView {
    // Disable interaction if user is guest
    if ([ServicesManager sharedInstance].myUser.isGuest ||!_roomRightBarButtonItem.enabled) {
        return NO;
    }
    else
    return  YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView {
    return NO;
}

- (void)emptyDataSet:(UIScrollView *)scrollView didTapButton:(UIButton *)button {
    [self didTapOnAddButton:nil];
}
- (void) presentContactDetailController:(Contact*) contact{
    UIContactDetailsViewController* controller = (UIContactDetailsViewController*)[[UIStoryboardManager sharedInstance].contactsDetailsStoryBoard instantiateViewControllerWithIdentifier:@"contactDetailsViewControllerID"];
    controller.contact = contact;
    controller.fromView = self;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - 3D touch actions
- (void)traitCollectionDidChange:(UITraitCollection *)previousTraitCollection {
    [super traitCollectionDidChange:previousTraitCollection];
    if ([UITools isForceTouchAvailableForTraitCollection:self.traitCollection]) {
        if (!self.previewingContext) {
            self.previewingContext = [self registerForPreviewingWithDelegate:self sourceView:self.view];
        }
    } else {
        if (self.previewingContext) {
            [self unregisterForPreviewingWithContext:self.previewingContext];
            self.previewingContext = nil;
        }
    }
}


- (UIViewController *)previewingContext:(id<UIViewControllerPreviewing>)previewingContext viewControllerForLocation:(CGPoint)location {
    if ([self.presentedViewController isKindOfClass:[UIRoomDetailsViewController class]]) {
        return nil;
    }
    if(self.tableView.editing)
        return nil;
    
    CGPoint translatedLocation = [self.view convertPoint:location toView:self.tableView];
    NSIndexPath *idx = [self.tableView indexPathForRowAtPoint:translatedLocation];
    Room *theRoom = nil;
    if(idx){
        NSString *key = [[_rooms sections] objectAtIndex:idx.section];
        theRoom = [[_rooms objectsInSection:key] objectAtIndex:idx.row];
        UITableViewCell *tableCell = [self.tableView cellForRowAtIndexPath:idx];
        
        UIRoomDetailsViewController * roomDetails = [[UIStoryboardManager sharedInstance].roomDetailsStoryBoard instantiateViewControllerWithIdentifier:@"roomDetailsViewControllerID"];
        roomDetails.room = theRoom;

        CustomNavigationController *navCtrl = [[CustomNavigationController alloc] initWithRootViewController:roomDetails];
        
        previewingContext.sourceRect = [self.view convertRect:tableCell.frame fromView:self.tableView];
        
        return navCtrl;
    }
    return nil;
}

- (void)previewingContext:(id<UIViewControllerPreviewing>)previewingContext commitViewController:(UIViewController *)viewControllerToCommit {
    if([viewControllerToCommit isKindOfClass:[CustomNavigationController class]]){
        [self.navigationController pushViewController:((CustomNavigationController*)viewControllerToCommit).topViewController animated:YES];
    }
}

#pragma mark - show room details
-(void) showRoomDetails:(Room *) room {
    UIRoomDetailsViewController *roomDetailsViewController = [[UIStoryboardManager sharedInstance].roomDetailsStoryBoard instantiateViewControllerWithIdentifier:@"roomDetailsViewControllerID"];
    roomDetailsViewController.room = room;
    roomDetailsViewController.fromView = self;
    [self.navigationController pushViewController:roomDetailsViewController animated:YES];
}

- (IBAction)didTapOnAddButton:(UIBarButtonItem *)sender {
     UIGenericBubbleDetailsViewController *addAttendeeViewController = [[UIStoryboardManager sharedInstance].addAttendeesStoryBoard instantiateViewControllerWithIdentifier:@"addAttendeeViewControllerID"];
    
//    addAttendeeViewController.preferredContentSize = CGSizeMake(300, 300);
//    addAttendeeViewController.modalPresentationStyle = UIModalPresentationPopover;
//    
//    UIPopoverPresentationController *popoverCtrl = addAttendeeViewController.popoverPresentationController;
//    popoverCtrl.delegate = self;
//    popoverCtrl.sourceView = self.view;
//    [popoverCtrl setPermittedArrowDirections:UIPopoverArrowDirectionUnknown];
//    popoverCtrl.backgroundColor = [UITools defaultTintColor];
    [self presentViewController:addAttendeeViewController animated:YES completion:nil];
}

#pragma mark - more button clicked

- (IBAction)moreMenuClicked:(id)sender {
    if (_moreMenuActionSheet) {
        [_moreMenuActionSheet dismissViewControllerAnimated:NO completion:nil];
        _moreMenuActionSheet = nil;
    }
    
    _moreMenuActionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    // Sort rooms button
    UIAlertAction* sortRooms = [UIAlertAction actionWithTitle:NSLocalizedString(@"Sort ...", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self sortRoomsMenuClicked];
    }];
    [_moreMenuActionSheet addAction:sortRooms];
    
    // Edit rooms button
    UIAlertAction* editRooms = [UIAlertAction actionWithTitle:NSLocalizedString(@"Edit", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self editRoomsMenuClicked];
    }];
    [_moreMenuActionSheet addAction:editRooms];
    
    // Cancel action
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    [_moreMenuActionSheet addAction:cancel];
    
    // show the menu.
    [_moreMenuActionSheet.view setTintColor:[UITools defaultTintColor]];
    [self presentViewController:_moreMenuActionSheet animated:YES completion:nil];
}

-(void) sortRoomsMenuClicked {
    if (_sortRoomsMenuActionSheet) {
        [_sortRoomsMenuActionSheet dismissViewControllerAnimated:NO completion:nil];
        _sortRoomsMenuActionSheet = nil;
    }
    
    _sortRoomsMenuActionSheet = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Sort order", nil) message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    // Sort rooms by creation date button
    UIAlertAction* sortRoomsByCreationDate = [UIAlertAction actionWithTitle:NSLocalizedString(@"Creation date", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self sortBySortOrder:SortOrderByCreationDate];
    }];
    [sortRoomsByCreationDate setValue:_selectedSortOrder == SortOrderByCreationDate ? @true : @false forKey:@"checked"];
    [_sortRoomsMenuActionSheet addAction:sortRoomsByCreationDate];
    
    // Sort rooms by display name button
    UIAlertAction* sortRoomsByDisplayName = [UIAlertAction actionWithTitle:NSLocalizedString(@"Name", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        [self sortBySortOrder:SortOrderByName];
    }];
    [sortRoomsByDisplayName setValue:_selectedSortOrder == SortOrderByName ? @true : @false forKey:@"checked"];
    [_sortRoomsMenuActionSheet addAction:sortRoomsByDisplayName];
    
    // Cancel action
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    [_sortRoomsMenuActionSheet addAction:cancel];
    
    // show the menu.
    [_sortRoomsMenuActionSheet.view setTintColor:[UITools defaultTintColor]];
    [self presentViewController:_sortRoomsMenuActionSheet animated:YES completion:nil];
}

-(void) editRoomsMenuClicked {
    [self setEditModeEnabled:!self.tableView.editing];
}

#pragma mark - others

-(void) checkAndEnableAddBubbleButton {
    UIScrollView *scrollView = [[UIScrollView alloc]init];
    [self emptyDataSetShouldAllowTouch:scrollView];
    [self buttonBackgroundImageForEmptyDataSet:scrollView forState:UIControlStateNormal];
    if([ServicesManager sharedInstance].myUser.isGuest){
        _roomRightBarButtonItem.enabled = NO;
    } else if(_populated == YES || _serviceManager.contactsManagerService.endPopulatingRoster) {
        _roomRightBarButtonItem.enabled = YES;
    }
}

-(void) dumpBubbles {
    NSLog(@"DUMP BUBBLES : %@", [_rooms description]);
}

-(void) scrollToTop {
    [self.tableView setContentOffset:CGPointMake(0.0f, -self.tableView.contentInset.top) animated:YES];
}

-(void) reloadUI {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self reloadUI];
        });
        return;
    }
    
    [_rooms reloadData];
    if ([self isViewLoaded] && _canRefreshUI) {
        NSArray *filteredArray = [_rooms.objects filteredArrayUsingPredicate:_filterDisplayMoreButton];
        if (filteredArray.count == 0) {
            ((MyInfoNavigationItem*)self.navigationItem).customRightItems = @[_roomRightBarButtonItem];
        } else {
            if(self.tableView.editing)
                ((MyInfoNavigationItem*)self.navigationItem).customRightItems = @[_doneButton];
            else
                ((MyInfoNavigationItem*)self.navigationItem).customRightItems = @[_moreMenuButton, _roomRightBarButtonItem];
        }
        [self.tableView reloadData];
    }
}


#pragma mark - Edit mode

-(void) setEditModeEnabled:(BOOL)enabled {
    [self.tableView setEditing:enabled animated:YES];
    
    if(enabled) {
        [self.view addSubview:_footerToolBar];
        ((MyInfoNavigationItem*)self.navigationItem).customRightItems = @[_doneButton];
        [((MyInfoNavigationItem*)self.navigationItem) setSearchBarHidden:YES avatarViewHidden:YES];
        [self updateSelectionButtonTitle];
    } else {
        [_footerToolBar removeFromSuperview];
        ((MyInfoNavigationItem*)self.navigationItem).customRightItems = @[_moreMenuButton, _roomRightBarButtonItem];
        [((MyInfoNavigationItem*)self.navigationItem) setSearchBarHidden:NO avatarViewHidden:NO];
    }
    
    // Hide tabBar in edit mode
    [self.tabBarController.tabBar setHidden:enabled];
}

-(void) updateSelectionButtonTitle {
    // Update the delete button's title, based on how many items are selected
    NSArray *selectedRows = [self.tableView indexPathsForSelectedRows];
    // At least one item selected
    BOOL invalidSelection = selectedRows.count == 0;
    
    // My rooms tab / Archive, delete
    if( _rooms.globalFilteringPredicate == _globalFilterMyRooms ) {
        _archiveSelectionButton.title = NSLocalizedString(@"Archive", nil);
        _deleteLeaveSelectionButton.title = NSLocalizedString(@"Delete", nil);
    }
    // Archive / delete
    else if( _rooms.globalFilteringPredicate == _globalFilterArchived ) {
        _archiveSelectionButton.title = nil;
        _deleteLeaveSelectionButton.title = NSLocalizedString(@"Delete", nil);
    }
    // All / Leave
    else {
        RoomSelectionType type = [self checkSelectionForSelectionType];
        if(type == RoomSelectionTypeBoth)
            invalidSelection = YES;
        
        if(type == RoomSelectionTypeParticipantOnly || type == RoomSelectionTypeBoth) {
            _archiveSelectionButton.title = nil;
            _deleteLeaveSelectionButton.title = NSLocalizedString(@"Quit", nil);
        } else {
            _archiveSelectionButton.title = NSLocalizedString(@"Archive", nil);
            _deleteLeaveSelectionButton.title = NSLocalizedString(@"Delete", nil);
        }
    }
    
    _archiveSelectionButton.enabled = !invalidSelection;
    _deleteLeaveSelectionButton.enabled = !invalidSelection;
}

-(RoomSelectionType) checkSelectionForSelectionType {
    //
    __block BOOL foundOwnedRoom = NO;
    __block BOOL foundAsParticipantRoom = NO;
    __block RoomSelectionType selectionType = RoomSelectionTypeBoth;
    
    // Current selection
    NSArray *selectedRows = [self.tableView indexPathsForSelectedRows];
    
    // Loop through the selection and check which 'type' of room it is: Only owned, only as participant or both.
    [selectedRows enumerateObjectsUsingBlock:^(NSIndexPath * indexPath, NSUInteger idx, BOOL * stop) {
        NSString *key = [[_rooms sections] objectAtIndex:indexPath.section];
        Room *theRoom = [[_rooms objectsInSection:key] objectAtIndex:indexPath.row];
        
        if(theRoom.isMyRoom) {
            selectionType = RoomSelectionTypeOwnedOnly;
            foundOwnedRoom = YES;
        } else {
            selectionType = RoomSelectionTypeParticipantOnly;
            foundAsParticipantRoom = YES;
        }
        
        if(foundOwnedRoom && foundAsParticipantRoom) {
            selectionType = RoomSelectionTypeBoth;
            *stop = YES;
        }
    }];
    
    return selectionType;
}

- (IBAction)didTapDoneButton:(UIBarButtonItem *)sender {
    [self setEditModeEnabled:NO];
}

- (IBAction)didTapSelectAllButton:(UIBarButtonItem *)sender {
    for (int i = 0; i < self.tableView.numberOfSections; i++) {
        for (NSInteger r = 0; r < [self.tableView numberOfRowsInSection:i]; r++) {
            [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:r inSection:i] animated:NO scrollPosition:UITableViewScrollPositionNone];
        }
    }
    [self updateSelectionButtonTitle];
}

- (IBAction)didTapArchiveSelectionButton:(UIBarButtonItem *)sender {
    // Current selection
    NSArray *selectedRows = [self.tableView indexPathsForSelectedRows];
    
    if(selectedRows.count > 0) {
        NSString *message = selectedRows.count > 1 ? NSLocalizedString(@"All the selected bubbles will be archived. Participants will no longer be able publish information.", nil) : NSLocalizedString(@"This bubble will be archived. Participants will no longer be able publish information.", nil);
        
        UIAlertController *confirmActionSheet = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleActionSheet];

        UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ok", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
            
            // Loop through the selection and do delete action.
            [selectedRows enumerateObjectsUsingBlock:^(NSIndexPath * indexPath, NSUInteger idx, BOOL * stop) {
                NSString *key = [[_rooms sections] objectAtIndex:indexPath.section];
                Room *room = [[_rooms objectsInSection:key] objectAtIndex:indexPath.row];
                
                // Archive room
                [[self class] archiveAction:room completionHandler:nil];
            }];
            
        }];
        [confirmActionSheet addAction:ok];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
        [confirmActionSheet addAction:cancel];
        
        // show the menu.
        [confirmActionSheet.view setTintColor:[UITools defaultTintColor]];
        [self presentViewController:confirmActionSheet animated:YES completion:nil];
        
    }
}

- (IBAction)didTapDeleteLeaveSelectionButton:(UIBarButtonItem *)sender {
    BOOL doDeleteRoom = NO;
    
    // Current available action is a delete action
    if([sender.title isEqualToString:NSLocalizedString(@"Delete", nil)])
        doDeleteRoom = YES;

    // Current selection
    NSArray *selectedRows = [self.tableView indexPathsForSelectedRows];
    
    if(selectedRows.count > 0) {
        NSString *message = (doDeleteRoom) ? NSLocalizedString(@"All the selected bubbles will be deleted. Participants will no longer have access to these bubbles.", nil) : NSLocalizedString(@"You will leave these bubbles and will no longer be able to publish information.", nil);
        if(selectedRows.count == 1) {
            message = (doDeleteRoom) ? NSLocalizedString(@"This bubble will be deleted. Participants will no longer have access to the bubble.", nil) : NSLocalizedString(@"You will leave this bubble and will no longer be able to publish information.", nil);
        }
        
        UIAlertController *confirmActionSheet = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleActionSheet];
        
        if(doDeleteRoom) {
            UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"Ok", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
                
                // Loop through the selection and do delete action.
                [selectedRows enumerateObjectsUsingBlock:^(NSIndexPath * indexPath, NSUInteger idx, BOOL * stop) {
                    NSString *key = [[_rooms sections] objectAtIndex:indexPath.section];
                    Room *room = [[_rooms objectsInSection:key] objectAtIndex:indexPath.row];
                    
                    // Delete room
                    [[self class] deleteAction:room completionHandler:nil];
                }];

            }];
            [confirmActionSheet addAction:ok];
        }
        
        else {
            // Leave action block executed when user has confirmed action for the selection
            void (^leaveAction)(BOOL) = ^void(BOOL createArchive) {
                // Loop through the selection and do leave action.
                [selectedRows enumerateObjectsUsingBlock:^(NSIndexPath * indexPath, NSUInteger idx, BOOL * stop) {
                    NSString *key = [[_rooms sections] objectAtIndex:indexPath.section];
                    Room *room = [[_rooms objectsInSection:key] objectAtIndex:indexPath.row];
                    
                    // Leave room
                    [[self class] leaveAction:room createArchive:createArchive completionHandler:nil];
                }];
            };
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"Leave and archive", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                leaveAction(YES);
            }];
            [confirmActionSheet addAction:ok];

            UIAlertAction *okWithoutArchive = [UIAlertAction actionWithTitle:NSLocalizedString(@"Leave without archive", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
                leaveAction(NO);
            }];
            [confirmActionSheet addAction:okWithoutArchive];
        }
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
        [confirmActionSheet addAction:cancel];
        
        // show the menu.
        [confirmActionSheet.view setTintColor:[UITools defaultTintColor]];
        [self presentViewController:confirmActionSheet animated:YES completion:nil];
    }
}

@end
