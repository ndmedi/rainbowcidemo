/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>
#import "UIRainbowGenericTableViewCell.h"
#import <Rainbow/RoomsService.h>

@interface UIRoomsInvitationTableViewCell : UIRainbowGenericTableViewCell

@property (nonatomic, strong) RoomsService *roomsService;

@property (weak, nonatomic) IBOutlet UIButton *acceptButton;
@property (weak, nonatomic) IBOutlet UIButton *declineButton;
@property (nonatomic) BOOL isProcessingAcceptOrDeclineInvitation;

@end
