/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIRoomsInvitationTableViewCell.h"
#import "GoogleAnalytics.h"
#import "UITools.h"

@implementation UIRoomsInvitationTableViewCell

-(void) awakeFromNib {
    [super awakeFromNib];
    _acceptButton.tintColor = [UIColor colorWithRed:40.0/255.0 green:178.0/255.0 blue:67.0/255.0 alpha:0.8f];
    _declineButton.tintColor = [UIColor colorWithRed:213.0/255.0 green:0 blue:112.0/255.0 alpha:0.8f];
    _isProcessingAcceptOrDeclineInvitation = NO;
}

- (IBAction)acceptInvitation:(UIButton *)sender {
    NSLog(@"Accept invitation in room %@", (Room*)self.cellObject);
    [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"bubble" action:@"accept invitation" label:nil value:nil];
    
    if (_isProcessingAcceptOrDeclineInvitation) {
        NSLog(@"Click on the accept button while it's already currently being processed.");
        return;
    }
    
    [self onClickOnButton:_acceptButton];

    [_roomsService acceptInvitation:(Room*)self.cellObject completionBlock:^(NSError *error, BOOL success) {
        [self resetButtons];
        if (!success && error.code != NSURLErrorNotConnectedToInternet) {
            UIViewController *vc = self.window.rootViewController;
            [UITools showErrorPopupWithTitle:NSLocalizedString(@"Error", @"Error") message:NSLocalizedString(@"Cannot Accept the room", @"Cannot Accpet the room")  inViewController:vc];
        }
    }];
}

- (IBAction)declineInvitation:(UIButton *)sender {
    NSLog(@"Decline invitation in room %@", (Room*)self.cellObject);
    [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"bubble" action:@"cancel" label:nil value:nil];
    
    if (_isProcessingAcceptOrDeclineInvitation) {
        NSLog(@"Click on the decline button while it's already currently being processed.");
        return;
    }
    
    [self onClickOnButton:_declineButton];
    
    [_roomsService declineInvitation:(Room*)self.cellObject completionBlock:^(NSError *error, BOOL success) {
        [self resetButtons];
        if (!success && error.code != NSURLErrorNotConnectedToInternet) {
            UIViewController *vc = self.window.rootViewController;
            [UITools showErrorPopupWithTitle:NSLocalizedString(@"Error", @"Error") message:NSLocalizedString(@"Cannot Decline the room", @"Cannot Decline the room")  inViewController:vc];
        }
    }];
}

- (void)onClickOnButton:(UIButton *)button {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self onClickOnButton:button];
        });
        return;
    }
    
    _isProcessingAcceptOrDeclineInvitation = YES;
    
    if ([button isEqual:_acceptButton]) {
        _acceptButton.alpha = 0.5f;
        _declineButton.alpha = 0.1f;
    } else if ([button isEqual:_declineButton]) {
        _declineButton.alpha = 0.5f;
        _acceptButton.alpha = 0.1f;
    }
}

- (void)resetButtons {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self resetButtons];
        });
        return;
    }
    
    _acceptButton.alpha = 1.0f;
    _declineButton.alpha = 1.0f;
    _isProcessingAcceptOrDeclineInvitation = NO;
}

@end
