/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIRoomDetailsViewController.h"
#import "UIContactDetailsViewController.h"
#import <Rainbow/ServicesManager.h>
#import <Rainbow/defines.h>
#import "UITools.h"
#import "UIGenericAddObjectWithTokenViewController.h"
#import "UIAvatarView.h"
#import "UIScrollView+APParallaxHeader.h"
#import "BGTableViewRowActionWithImage.h"
#import <DZNEmptyDataSet/DZNEmptyDataSet.h>
#import "OrderedOptionalSectionedContent.h"
#import "GoogleAnalytics.h"
#import "UIRainbowGenericTableViewCell.h"
#import "Participant+UIRainbowTableViewCellProtocol.h"

#import "UIRoomDetailsViewController+Internal.h"
#import <Photos/Photos.h>
#if !defined(APP_EXTENSION)
#import "SCLAlertView.h"
#import "SCLAlertView+Rotation.h"
#endif
#import "UIStoryboardManager.h"


@interface UIRoomDetailsViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *roomNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *roomTopicLabel;
@property (nonatomic, strong) UIAlertAction *saveAction;
@property (nonatomic, strong) UIAlertController *actionSheet;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *fullScreenTapGesture;
@end

@implementation UIRoomDetailsViewController

-(void)awakeFromNib {
    [super awakeFromNib];
}

-(void) viewDidLoad {
    [super viewDidLoad];
    self.tableView.tableFooterView = [UIView new];
    self.tableView.backgroundColor = [UITools defaultBackgroundColor];
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    
    if(_room.conference && _room.conference.confId)
        self.title = NSLocalizedString(@"Meeting details", nil);
    else
        self.title = NSLocalizedString(@"Bubble details", nil);
    
    if(!_room.isAdmin) {
        self.navigationItem.rightBarButtonItem = nil;
        self.navigationItem.rightBarButtonItems = @[];
    } else {
        // If we use a standard "self.editButtonItem", the label will go from "Edit" to "Done"
        // when we edit a cell of the tableview. Which is NOT what we want.
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Modify"] style:UIBarButtonItemStylePlain target:self action:@selector(editButtonClicked:)];
    }
    [self setRightBarButtonItemsEnabled:(_room.isAdmin && _room.myStatusInRoom != ParticipantStatusUnsubscribed)];
    
    _headerView.clipsToBounds = NO;
    
    [self drawRoomInfos];
    
    if(_room.photoData) {
        _headerView.userInteractionEnabled = YES;
        
        UIImage *image = [[UIImage alloc] initWithData:_room.photoData];
        CGRect screen = [[UIScreen mainScreen] bounds];
        _fullScreenAvatarView.backgroundColor = [UIColor lightGrayColor];
        _fullScreenAvatarImage.image = image;
        
        [_fullScreenAvatarView setFrame: CGRectMake(0, 0, screen.size.width, screen.size.height)];
        
        UIWindow *keyWindow = [UIApplication sharedApplication].windows[0];
        [keyWindow addSubview: _fullScreenAvatarView];
        
        [self showFullscreenAvatar: NO];
    }
    
    [_headerView sendSubviewToBack:_backgroundAvatar];
    
    [self.tableView addParallaxWithView:_headerView andHeight:_headerView.frame.size.height andMinHeight:_headerView.frame.size.height andShadow:YES];
    
    UIVisualEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    _visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    _visualEffectView.frame = self.tableView.parallaxView.bounds;;
    [_backgroundAvatar addSubview:_visualEffectView];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self.tableView.parallaxView setDelegate:self];
    
    // Force the parallax view to be at the top position.
    // Without this the tableview section headers goes on top of the parallax view.
    self.tableView.parallaxView.layer.zPosition = 1;
    
    [UITools applyCustomBoldFontTo:_roomNameLabel];
    [UITools applyCustomFontTo:_roomTopicLabel];
    
    if(_room){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateRoom:) name:kRoomsServiceDidUpdateRoom object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(invitationStatusChanged:) name:kRoomsServiceDidRoomInvitationStatusChanged object:nil],
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveRoom:) name:kRoomsServiceDidRemoveRoom object:nil];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLostConnection:) name:kLoginManagerDidLostConnection object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kLoginManagerDidReconnect object:nil];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"UIRainbowGenericTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewReusableKey];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    

    if(![ServicesManager sharedInstance].loginManager.isConnected)
        [self didLostConnection:nil];
    
    [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if(![ServicesManager sharedInstance].loginManager.isConnected)
        [self didReconnect:nil];
}

-(void) dealloc {
    _saveAction = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidUpdateRoom object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidRoomInvitationStatusChanged object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidRemoveRoom object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLostConnection object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidReconnect object:nil];
    
    [_participants removeAllObjects];
    _participants = nil;
    
    _room = nil;
}

#pragma mark - Login manager notification
-(void) didLostConnection:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didLostConnection:notification];
        });
        return;
    }
    
    [self setRightBarButtonItemsEnabled:NO];
}

-(void) didReconnect:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didReconnect:notification];
        });
        return;
    }
    
    [self setRightBarButtonItemsEnabled:YES];
}

-(void) setRightBarButtonItemsEnabled:(BOOL) enabled {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setRightBarButtonItemsEnabled:enabled];
        });
        return;
    }
    
    if ([self isViewLoaded]) {
        if (_room.isAdmin) {
            for (UIBarButtonItem *item in self.navigationItem.rightBarButtonItems) {
                item.enabled = enabled;
            }
        }
    }
}

-(void) setRoom:(Room *)room {
    _room = nil;
    _room = room;

    [_participants removeAllObjects];
    _participants = nil;
    
    _participants = [[OrderedOptionalSectionedContent alloc] initWithSections:@[kAdministratorsSection, kRainbowTvsSection, kAttendeesSection]]; // kPendingSection, kAwaySection, kRefusedSection
    
    // Now we have to dispatch the _room.participants in the right sections.
    for (Participant *participant in _room.participants) {
        if (participant.contact.rainbowTV && participant.status != ParticipantStatusUnsubscribed)
            [_participants addObject:participant toSection:kRainbowTvsSection];
        else if ((participant.privilege == ParticipantPrivilegeModerator && participant.status != ParticipantStatusUnsubscribed) || participant.privilege == ParticipantPrivilegeOwner)
            [_participants addObject:participant toSection:kAdministratorsSection];
        else if (participant.status == ParticipantStatusInvited || participant.status == ParticipantStatusAccepted || participant.status == ParticipantStatusInvitedGuest)
            [_participants addObject:participant toSection:kAttendeesSection];
    }
    
    // Now we can sort them
    if([ServicesManager sharedInstance].contactsManagerService.sortByFirstName) {
        [[_participants sectionForKey:kAdministratorsSection] sortUsingDescriptors:@[[self participantPrivilegeFirstNameLastNameSortDescriptor]]];
        [[_participants sectionForKey:kAttendeesSection] sortUsingDescriptors:@[[self participantStatusFirstNameLastNameSortDescriptor]]];
        [[_participants sectionForKey:kRainbowTvsSection] sortUsingDescriptors:@[[self participantStatusFirstNameLastNameSortDescriptor]]];
    } else {
        [[_participants sectionForKey:kAdministratorsSection] sortUsingDescriptors:@[[self participantPrivilegeLastNameFirstNameSortDescriptor]]];
        [[_participants sectionForKey:kAttendeesSection] sortUsingDescriptors:@[[self participantStatusLastNameFirstNameSortDescriptor]]];
        [[_participants sectionForKey:kRainbowTvsSection] sortUsingDescriptors:@[[self participantStatusLastNameFirstNameSortDescriptor]]];
    }
}

#pragma mark - sort by status
-(NSSortDescriptor *) participantStatusLastNameFirstNameSortDescriptor {
    return [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES comparator:^NSComparisonResult(Participant* obj1, Participant* obj2) {
        // Fake participant (Add participant button) MUST always be at the top of member list
        if(obj1.contact == nil || obj2.contact == nil)
            return NSOrderedAscending;
        
        // Compose a string based on ParticipantStatus, contact.lastName and contact.firstName to be used to sort by all these criterias at once
        // status accepted must be first (0) and others status values +1.
        // ParticipantStatusAccepted -> 0, ParticipantStatusUnknown+1, ParticipantStatusInvited+1, etc...
        NSString *obj1Str = [NSString stringWithFormat:@"%lu %@ %@", (obj1.status == ParticipantStatusAccepted) ? 0 : obj1.status+1, obj1.contact.lastName, obj1.contact.firstName];
        NSString *obj2Str = [NSString stringWithFormat:@"%lu %@ %@", (obj2.status == ParticipantStatusAccepted) ? 0 : obj2.status+1, obj2.contact.lastName, obj2.contact.firstName];
        return [obj1Str compare:obj2Str options:NSCaseInsensitiveSearch];
    }];
}

-(NSSortDescriptor *) participantStatusFirstNameLastNameSortDescriptor {
    return [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES comparator:^NSComparisonResult(Participant* obj1, Participant* obj2) {
        // Fake participant (Add participant button) MUST always be at the top of member list
        if(obj1.contact == nil || obj2.contact == nil)
            return NSOrderedAscending;
        
        // Compose a string based on ParticipantStatus, contact.lastName and contact.firstName to be used to sort by all these criterias at once
        // status accepted must be first (0) and others status values +1.
        // ParticipantStatusAccepted -> 0, ParticipantStatusUnknown+1, ParticipantStatusInvited+1, etc...
        NSString *obj1Str = [NSString stringWithFormat:@"%lu %@ %@", (obj1.status == ParticipantStatusAccepted) ? 0 : obj1.status+1, obj1.contact.firstName, obj2.contact.lastName ];
        NSString *obj2Str = [NSString stringWithFormat:@"%lu %@ %@", (obj2.status == ParticipantStatusAccepted) ? 0 : obj2.status+1, obj2.contact.firstName, obj2.contact.lastName ];
        return [obj1Str compare:obj2Str options:NSCaseInsensitiveSearch];
    }];
}

#pragma mark - sort by privilege
-(NSSortDescriptor *) participantPrivilegeLastNameFirstNameSortDescriptor {
    return [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES comparator:^NSComparisonResult(Participant* obj1, Participant* obj2) {
        // Fake participant (Add participant button) MUST always be at the top of member list
        if(obj1.contact == nil || obj2.contact == nil)
            return NSOrderedAscending;
        
        // Compose a string based on ParticipantStatus, contact.lastName and contact.firstName to be used to sort by all these criterias at once
        // status accepted must be first (0) and others status values +1.
        // ParticipantStatusAccepted -> 0, ParticipantStatusUnknown+1, ParticipantStatusInvited+1, etc...
        NSString *obj1Str = [NSString stringWithFormat:@"%lu %@ %@", (obj1.privilege == ParticipantPrivilegeOwner) ? 0 : obj1.privilege+1, obj1.contact.lastName, obj1.contact.firstName];
        NSString *obj2Str = [NSString stringWithFormat:@"%lu %@ %@", (obj2.privilege == ParticipantPrivilegeOwner) ? 0 : obj2.privilege+1, obj2.contact.lastName, obj2.contact.firstName];
        return [obj1Str compare:obj2Str options:NSCaseInsensitiveSearch];
    }];
}

-(NSSortDescriptor *) participantPrivilegeFirstNameLastNameSortDescriptor {
    return [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES comparator:^NSComparisonResult(Participant* obj1, Participant* obj2) {
        // Fake participant (Add participant button) MUST always be at the top of member list
        if(obj1.contact == nil || obj2.contact == nil)
            return NSOrderedAscending;
        
        // Compose a string based on ParticipantStatus, contact.lastName and contact.firstName to be used to sort by all these criterias at once
        // status accepted must be first (0) and others status values +1.
        // ParticipantStatusAccepted -> 0, ParticipantStatusUnknown+1, ParticipantStatusInvited+1, etc...
        NSString *obj1Str = [NSString stringWithFormat:@"%lu %@ %@", (obj1.privilege == ParticipantPrivilegeOwner) ? 0 : obj1.privilege+1, obj1.contact.firstName, obj2.contact.lastName ];
        NSString *obj2Str = [NSString stringWithFormat:@"%lu %@ %@", (obj2.privilege == ParticipantPrivilegeOwner) ? 0 : obj2.privilege+1, obj2.contact.firstName, obj2.contact.lastName ];
        return [obj1Str compare:obj2Str options:NSCaseInsensitiveSearch];
    }];
}


-(void) drawRoomInfos {
    if(_room){
        _avatarView.peer = _room;
        _avatarView.showPresence = NO;
        _avatarView.asCircle = YES;
        _backgroundAvatar.peer = _room;
        _backgroundAvatar.showPresence = NO;
        _backgroundAvatar.cornerRadius = 0;
        _roomNameLabel.text = _room.displayName;
        _roomTopicLabel.text = _room.topic;
    }
}

- (IBAction)closeButtonTapped:(UIButton *)sender {
    [self showFullscreenAvatar:NO];
}

- (IBAction)didTapGesture:(UITapGestureRecognizer*)sender {
    if( sender == _headerTapGesture ) {
        [self showFullscreenAvatar:YES];
    } else {
        [self showFullscreenAvatar:NO];
    }
}


- (void) showFullscreenAvatar: (BOOL)visible {
    if(visible) {
        _fullScreenAvatarView.alpha = 1;
        self.tableView.userInteractionEnabled = NO;
    } else {
        _fullScreenAvatarView.alpha = 0;
        self.tableView.userInteractionEnabled = YES;
    }
}

#pragma mark - orientation & style

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(BOOL) shouldAutorotate {
    return NO;
}

-(UIInterfaceOrientationMask) supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

#pragma mark - APParallaxViewDelegate
- (void)parallaxView:(APParallaxView *)view willChangeFrame:(CGRect)frame {
}

- (void)parallaxView:(APParallaxView *)view didChangeFrame:(CGRect)frame {
    CGRect visualEffectFrame = _visualEffectView.frame;
    visualEffectFrame.size.height = frame.size.height;
    [_visualEffectView setFrame:visualEffectFrame];
}

#pragma mark - UITableView delegate

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return [[_participants allNotEmptySections] count];
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *key = [[_participants allNotEmptySections] objectAtIndex:section];
    return [[_participants sectionForKey:key] count];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *key = [[_participants allNotEmptySections] objectAtIndex:section];
    NSString *sectionStr = [[_participants allNotEmptySections] objectAtIndex:section];
    NSUInteger participantCount = [[_participants sectionForKey:key] count];
    
    if([sectionStr isEqualToString:kAttendeesSection]) {
        return (participantCount > 1) ? [NSString stringWithFormat:@"%@ (%lu)", NSLocalizedString(@"Members", nil), (unsigned long)participantCount] : NSLocalizedString(@"Member", nil);
    } else if( [sectionStr isEqualToString:kAdministratorsSection] ) {
        return (participantCount > 1) ? [NSString stringWithFormat:@"%@ (%lu)", NSLocalizedString(@"Organizers", nil), (unsigned long)participantCount] : NSLocalizedString(@"Organizer", nil);
    } else if ([sectionStr isEqualToString:kRainbowTvsSection]) {
        return (participantCount > 1) ? [NSString stringWithFormat:@"%@ (%lu)", NSLocalizedString(@"Rainbow TVs", nil), (unsigned long)participantCount] : NSLocalizedString(@"Rainbow TV", nil);
    }
    
    return NSLocalizedString(sectionStr, nil);
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *)view;
    tableViewHeaderFooterView.textLabel.font = [UIFont fontWithName:[UITools boldFontName] size:16.0f];
    tableViewHeaderFooterView.textLabel.textColor = [UITools defaultTintColor];
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self cellForRowAtIndexPath:indexPath];
}

-(UIRainbowGenericTableViewCell *) cellForRowAtIndexPath:(NSIndexPath *) indexPath {
    
    NSString *key = [[_participants allNotEmptySections] objectAtIndex:indexPath.section];
    NSArray<Participant *> *content = [_participants sectionForKey:key];
    
    UIRainbowGenericTableViewCell *cell = (UIRainbowGenericTableViewCell*)[self.tableView dequeueReusableCellWithIdentifier:kTableViewReusableKey forIndexPath:indexPath];
    
    cell.cellObject = [content objectAtIndex:indexPath.row];
    __weak typeof(cell) weakCell = cell;
    
    // Disable cell button with invited guest
    Participant *participant = [content objectAtIndex:indexPath.row];
    BOOL isGuestAndMyUser = [ServicesManager sharedInstance].myUser.isGuest && [participant.contact isEqual:[ServicesManager sharedInstance].myUser.contact];
    if (participant.status == ParticipantStatusInvitedGuest || isGuestAndMyUser || participant.contact.rainbowTV) {
        cell.cellButtonTapHandler = nil;
        cell.userInteractionEnabled = NO;
        return cell;
    } else {
        cell.cellButtonTapHandler = ^(UIButton *sender) {
            [self showContactDetails:((Participant *)weakCell.cellObject).contact];
        };
        cell.userInteractionEnabled = YES;
    }
    
    return cell;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kRainbowGenericTableViewCellHeight;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    UIRainbowGenericTableViewCell *cell = (UIRainbowGenericTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    [self showContactDetails:((Participant *)cell.cellObject).contact];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    UIRainbowGenericTableViewCell *cell = (UIRainbowGenericTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    if (![cell isKindOfClass:[UIRainbowGenericTableViewCell class]]) {
        return NO;
    }
    NSString *key = [[_participants allNotEmptySections] objectAtIndex:indexPath.section];
    NSArray<Participant *> *content = [_participants sectionForKey:key];
    Participant *participant = [content objectAtIndex:indexPath.row];
    Contact *contact = participant.contact;
    if((contact == [ServicesManager sharedInstance].myUser.contact && _room.isAdmin) || participant.privilege == ParticipantPrivilegeOwner)
        return NO;
    
    return _room.isAdmin && _room.myStatusInRoom == ParticipantStatusAccepted;
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UIRainbowGenericTableViewCell *cell = (UIRainbowGenericTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    
    Participant *selectedParticipant = (Participant*)cell.cellObject;
    Contact *selectedContact = selectedParticipant.contact;
    
    switch (selectedParticipant.status) {
        case ParticipantStatusInvitedGuest:
        case ParticipantStatusInvited: {
            BGTableViewRowActionWithImage *action = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Remove", nil) backgroundColor:[UITools redColor] image:[UIImage imageNamed:@"RemoveAttendees"] forCellHeight:kRainbowGenericTableViewCellHeight+10 handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
                    if (selectedParticipant.status == ParticipantStatusInvitedGuest) {
                        [[ServicesManager sharedInstance].roomsService cancelGuestUsers:@[selectedContact.emailAddresses.firstObject.address] forRoom:_room withCompletionHandler:^(NSDictionary *response, NSError *error) {
                            NSLog(@"Cancel invitation for guest email :  %@", selectedContact.displayName);
                        }];
                    } else {
                        [[ServicesManager sharedInstance].roomsService cancelInvitationForContact:selectedContact inRoom:_room];
                    }
                });
            }];
            action.accessibilityLabel = NSLocalizedString(@"Remove", nil);
            return @[action];
        }
        case ParticipantStatusAccepted: {
            NSMutableArray *actions = [NSMutableArray new];
            NSString *servicePlan = [[ServicesManager sharedInstance].myUser getServicePlan];
            
            BGTableViewRowActionWithImage *action = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Remove", nil) backgroundColor:[UITools redColor] image:[UIImage imageNamed:@"RemoveAttendees"] forCellHeight:kRainbowGenericTableViewCellHeight+10 handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
                    if (selectedParticipant.privilege == ParticipantPrivilegeModerator) {
                        if (_actionSheet) {
                            [_actionSheet dismissViewControllerAnimated:NO completion:nil];
                            _actionSheet = nil;
                        }
                        
                        NSString *messageToDisplay = [NSString stringWithFormat:NSLocalizedString(selectedContact.rainbowTV ? @"Rainbow TV %@ will be removed from this bubble" : @"Organizer %@ will be removed from this bubble", nil), selectedParticipant.contact.displayName];
                        _actionSheet = [UIAlertController alertControllerWithTitle:nil message:messageToDisplay preferredStyle:UIAlertControllerStyleActionSheet];
                        
                        // Set the attributedTitle in bold
                        NSString *titleActionSheet = NSLocalizedString(selectedContact.rainbowTV ? @"Remove the Rainbow TV" : @"Remove organizer", nil);
                        NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:titleActionSheet attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:30.0f]}];
                        [_actionSheet setValue:attrString forKey:@"_attributedTitle"];
                        
                        // Remove action
                        UIAlertAction *okButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"Remove", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                            [[ServicesManager sharedInstance].roomsService removeParticipant:selectedParticipant fromRoom:_room];
                        }];
                        [_actionSheet addAction:okButton];
                        
                        // Cancel action
                        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
                        [_actionSheet addAction:cancel];
                        
                        // show the menu.
                        [_actionSheet.view setTintColor:[UITools defaultTintColor]];
                        [self presentViewController:_actionSheet animated:YES completion:nil];
                    } else {
                        [[ServicesManager sharedInstance].roomsService removeParticipant:selectedParticipant fromRoom:_room];
                    }
                });
            }];
            action.accessibilityLabel = NSLocalizedString(@"Remove", nil);
            [actions addObject:action];

            if (!selectedContact.rainbowTV) {
                // Transfer ownership
                if (selectedParticipant.privilege == ParticipantPrivilegeModerator && ([servicePlan containsString:@"Business"] || [servicePlan containsString:@"Enterprise"]) && _room.isMyRoom && [[ServicesManager sharedInstance].myUser isAllowedToUsePromoteDemoteFeature]) {
                    BGTableViewRowActionWithImage *actionTransferOwnership = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Give ownership", nil) backgroundColor:[UITools defaultTintColor] image:nil forCellHeight:kRainbowGenericTableViewCellHeight+10 handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
                            if (_actionSheet) {
                                [_actionSheet dismissViewControllerAnimated:NO completion:nil];
                                _actionSheet = nil;
                            }
                            
                            NSString *messageToDisplay = [NSString stringWithFormat:NSLocalizedString(@"%@ will become the new owner of the bubble. You will no longer have the control of this bubble.", nil), selectedParticipant.contact.displayName];
                            _actionSheet = [UIAlertController alertControllerWithTitle:nil message:messageToDisplay preferredStyle:UIAlertControllerStyleActionSheet];
                            
                            // Set the attributedTitle in bold
                            NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Transfer ownership", nil) attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:30.0f]}];
                            [_actionSheet setValue:attrString forKey:@"_attributedTitle"];
                            
                            UIAlertAction *actionSheetOkButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"Transfer", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                [[ServicesManager sharedInstance].roomsService changeOwner:_room withParticipant:selectedParticipant];
                            }];
                            [_actionSheet addAction:actionSheetOkButton];
                            
                            // Cancel action
                            UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
                            [_actionSheet addAction:cancel];
                            
                            // show the menu.
                            [_actionSheet.view setTintColor:[UITools defaultTintColor]];
                            [self presentViewController:_actionSheet animated:YES completion:nil];
                        });
                    }];
                    actionTransferOwnership.accessibilityLabel = NSLocalizedString(@"Give ownership", nil);
                    [actions addObject:actionTransferOwnership];
                }
                
                // Action promote / demote
                if (([servicePlan containsString:@"Business"] || [servicePlan containsString:@"Enterprise"]) && [[ServicesManager sharedInstance].myUser isAllowedToUsePromoteDemoteFeature]) {
                    NSString *buttonTitle = nil;
                    UIColor *buttonBackgroundColor = nil;
                    NSString *actionSheetTitle = nil;
                    NSString *actionSheetMessage = nil;
                    UIAlertAction *actionSheetOkButton = nil;
                    
                    if (selectedParticipant.privilege == ParticipantPrivilegeUser) {
                        buttonTitle = NSLocalizedString(@"Promote", nil);
                        buttonBackgroundColor = [UITools defaultTintColor];
                        actionSheetTitle = NSLocalizedString(@"Promote to Organizer role", nil);
                        actionSheetMessage = NSLocalizedString(@"%@ will be promoted to the Organizer role", nil);
                        actionSheetOkButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"Promote", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                            [[ServicesManager sharedInstance].roomsService updateParticipant:selectedParticipant withPrivilege:ParticipantPrivilegeModerator inRoom:_room withCompletionBlock:nil];
                        }];
                    } else if (selectedParticipant.privilege == ParticipantPrivilegeModerator) {
                        buttonTitle = NSLocalizedString(@"Demote", nil);
                        buttonBackgroundColor = [UIColor lightGrayColor];
                        actionSheetTitle = NSLocalizedString(@"Demote to Member role", nil);
                        actionSheetMessage = NSLocalizedString(@"%@ will be demoted to the Member role", nil);
                        actionSheetOkButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"Demote", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                            [[ServicesManager sharedInstance].roomsService updateParticipant:selectedParticipant withPrivilege:ParticipantPrivilegeUser inRoom:_room withCompletionBlock:nil];
                        }];
                    }
                    
                    BGTableViewRowActionWithImage *actionPromoteDemote = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDestructive title:buttonTitle backgroundColor:buttonBackgroundColor image:nil forCellHeight:kRainbowGenericTableViewCellHeight+10 handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
                            if (_actionSheet) {
                                [_actionSheet dismissViewControllerAnimated:NO completion:nil];
                                _actionSheet = nil;
                            }
                            
                            NSString *messageToDisplay = [NSString stringWithFormat:actionSheetMessage, selectedParticipant.contact.displayName];
                            _actionSheet = [UIAlertController alertControllerWithTitle:nil message:messageToDisplay preferredStyle:UIAlertControllerStyleActionSheet];
                            
                            // Set the attributedTitle in bold
                            NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:actionSheetTitle attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:30.0f]}];
                            [_actionSheet setValue:attrString forKey:@"_attributedTitle"];
                            
                            // Promote or demote action
                            [_actionSheet addAction:actionSheetOkButton];
                            
                            // Cancel action
                            UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
                            [_actionSheet addAction:cancel];
                            
                            // show the menu.
                            [_actionSheet.view setTintColor:[UITools defaultTintColor]];
                            [self presentViewController:_actionSheet animated:YES completion:nil];
                        });
                    }];
                    actionPromoteDemote.accessibilityLabel = buttonTitle;
                    [actions addObject:actionPromoteDemote];
                }
            }

            return actions;
        }
        case ParticipantStatusUnsubscribed:
        case ParticipantStatusDeleted:
        case ParticipantStatusRejected: {
            BGTableViewRowActionWithImage *action = [BGTableViewRowActionWithImage rowActionWithStyle:UITableViewRowActionStyleDestructive title:NSLocalizedString(@"Re-invite", nil) backgroundColor:[UIColor blueColor] image:[UIImage imageNamed:@"JoinBubble"] forCellHeight:kRainbowGenericTableViewCellHeight+10 handler:^(UITableViewRowAction *action, NSIndexPath *indexPath) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
                    if(selectedParticipant.status == ParticipantStatusRejected || selectedParticipant.status == ParticipantStatusUnsubscribed)
                        [[ServicesManager sharedInstance].roomsService deleteParticipant:selectedParticipant fromRoom:_room];
                    [[ServicesManager sharedInstance].roomsService inviteContact:selectedContact inRoom:_room error:nil];
                });
            }];
            action.accessibilityLabel = NSLocalizedString(@"Re-invite", nil);
            return @[action];
        }
        case ParticipantStatusUnknown:
        default:
            break;
    }
    
    return @[];
}

#pragma mark - Segues
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"addParticipantsSegueID"] || [[segue identifier] isEqualToString:@"addRainbowTVsSegueID"]) {
        UINavigationController *destNavController = [segue destinationViewController];
        UIGenericAddObjectWithTokenViewController *destViewController = [destNavController.viewControllers firstObject];
        
        if ([[segue identifier] isEqualToString:@"addRainbowTVsSegueID"])
            destViewController.isAddingTVs = YES;
        
        destViewController.room = _room;
    }
}

#pragma mark - Navigation bar buttons clicked

- (IBAction)addParticipantsButtonClicked:(id)sender {
    if ([[ServicesManager sharedInstance].myUser isAllowedToUseAndroidTv]) {
        if (_actionSheet) {
            [_actionSheet dismissViewControllerAnimated:NO completion:nil];
            _actionSheet = nil;
        }
        
        _actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        // Add participants action
        UIAlertAction *addParticipantsButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"Add participants", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [self performSegueWithIdentifier:@"addParticipantsSegueID" sender:nil];
        }];
        [_actionSheet addAction:addParticipantsButton];
        
        // Add android TVs action
        UIAlertAction *addAndroidTVsButton = [UIAlertAction actionWithTitle:NSLocalizedString(@"Add Rainbow TVs", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
             [self performSegueWithIdentifier:@"addRainbowTVsSegueID" sender:nil];
        }];
        [_actionSheet addAction:addAndroidTVsButton];
        
        // Cancel action
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
        [_actionSheet addAction:cancel];
        
        // show the menu.
        [_actionSheet.view setTintColor:[UITools defaultTintColor]];
        [self presentViewController:_actionSheet animated:YES completion:nil];
    } else {
        [self performSegueWithIdentifier:@"addParticipantsSegueID" sender:nil];
    }
}


- (void) editButtonClicked:(UIBarButtonItem *)sender {
    UIAlertController *editActionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* changeNameAction = [UIAlertAction actionWithTitle:_room.conference?NSLocalizedString(@"Change meeting name", nil):NSLocalizedString(@"Change bubble name", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        UIAlertController *newNameAlert = [UIAlertController alertControllerWithTitle:_room.conference?NSLocalizedString(@"Enter new meeting name", nil):NSLocalizedString(@"Enter new bubble name", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
        [newNameAlert addTextFieldWithConfigurationHandler:^(UITextField * textField) {
            [UITools applyCustomFontToTextField:textField];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldDidChange:) name:UITextFieldTextDidChangeNotification object:textField];
        }];
        _saveAction = nil;
        _saveAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            NSString *newName = [newNameAlert.textFields firstObject].text;
            [[ServicesManager sharedInstance].roomsService updateRoom:_room withName:newName];
            [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:nil];
            
        }];
        _saveAction.enabled = NO;
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:nil];
            _saveAction = nil;
        }];
        
        [newNameAlert addAction:_saveAction];
        [newNameAlert addAction:cancel];
        [newNameAlert.view setTintColor:[UITools defaultTintColor]];
        [self presentViewController:newNameAlert animated:YES completion:nil];
    }];
    
    [editActionSheet addAction:changeNameAction];
    
    
    UIAlertAction* changeTopicAction = [UIAlertAction actionWithTitle:_room.conference?NSLocalizedString(@"Change meeting subject", @""):NSLocalizedString(@"Change bubble topic", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        UIAlertController *newNameAlert = [UIAlertController alertControllerWithTitle:_room.conference?NSLocalizedString(@"Enter new meeting subject", nil):NSLocalizedString(@"Enter new bubble topic", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
        [newNameAlert addTextFieldWithConfigurationHandler:^(UITextField * textField) {
            [UITools applyCustomFontToTextField:textField];
        }];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            NSString *newTopic = [newNameAlert.textFields firstObject].text;
            [[ServicesManager sharedInstance].roomsService updateRoom:_room withTopic:newTopic];
        }];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
        
        [newNameAlert addAction:ok];
        [newNameAlert addAction:cancel];
        [newNameAlert.view setTintColor:[UITools defaultTintColor]];
        [self presentViewController:newNameAlert animated:YES completion:nil];
    }];
    
    [editActionSheet addAction:changeTopicAction];
    /*
    UIAlertAction* removeAllParticipantsAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Remove all attendees", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        NSString *message = NSLocalizedString(@"By removing all attendees, they will not be able to access to this bubble, this operation is irresversible", nil);
        
        UIAlertController *confirmDeleteActionSheet = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
                for (Participant *participant in _room.participants) {
                    [[ServicesManager sharedInstance].roomsService removeContact:participant.contact fromRoom:_room];
                }
            });
        }];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
        
        [confirmDeleteActionSheet addAction:ok];
        [confirmDeleteActionSheet addAction:cancel];
        // show the menu.
        [confirmDeleteActionSheet.view setTintColor:[UITools defaultTintColor]];
        [self presentViewController:confirmDeleteActionSheet animated:YES completion:nil];
    }];
    */
    
    // check > 1 because we are a participant
    /*if(_room.participants.count > 1)
        [editActionSheet addAction:removeAllParticipantsAction];
     */
    
    UIAlertAction* changeAvatarAction = [UIAlertAction actionWithTitle:_room.conference?NSLocalizedString(@"Change meeting avatar", @""):NSLocalizedString(@"Change bubble avatar", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        UIAlertController *chooseSource = [UIAlertController alertControllerWithTitle: NSLocalizedString(@"Choose your source", nil)
                                                              message: nil
                                                       preferredStyle: UIAlertControllerStyleActionSheet];
        
        [chooseSource addAction:[UIAlertAction actionWithTitle: NSLocalizedString(@"Choose From Library", nil) style: UIAlertActionStyleDefault handler: ^(UIAlertAction * action) {
            [UITools openImagePicker:UIImagePickerControllerSourceTypePhotoLibrary delegate:self presentingViewController:self allowsEditing:YES];
        }]];
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            [chooseSource addAction:[UIAlertAction actionWithTitle: NSLocalizedString(@"Take Photo", nil) style: UIAlertActionStyleDefault handler: ^(UIAlertAction * _Nonnull action) {
                [UITools openImagePicker:UIImagePickerControllerSourceTypeCamera delegate:self presentingViewController:self allowsEditing:YES];
            }]];
        }
        
        // Custom 'cancel' button
        [chooseSource addAction:[UIAlertAction actionWithTitle: NSLocalizedString(@"Cancel", nil) style: UIAlertActionStyleCancel handler: ^(UIAlertAction * _Nonnull action) {
            [chooseSource dismissViewControllerAnimated:YES completion:nil];
        }]];
        [chooseSource.view setTintColor:[UITools defaultTintColor]];
        [self presentViewController:chooseSource animated:YES completion:nil];
    }];
    
    [editActionSheet addAction:changeAvatarAction];
    
    if( _room.photoData ) {
        UIAlertAction* deleteAvatarAction = [UIAlertAction actionWithTitle:_room.conference?NSLocalizedString(@"Delete meeting avatar", @""):NSLocalizedString(@"Delete bubble avatar", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            
            UIAlertController *deleteAvatarAlert = [UIAlertController alertControllerWithTitle:_room.conference?NSLocalizedString(@"Do you want to delete the customized meeting avatar ?", nil):NSLocalizedString(@"Do you want to delete the customized bubble avatar ?", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                [[ServicesManager sharedInstance].roomsService deleteAvatar:_room];
            }];
            
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
            
            [deleteAvatarAlert addAction:ok];
            [deleteAvatarAlert addAction:cancel];
            [deleteAvatarAlert.view setTintColor:[UITools defaultTintColor]];
            [self presentViewController:deleteAvatarAlert animated:YES completion:nil];
            
        }];
        
        [editActionSheet addAction:deleteAvatarAction];
    }
    
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    [editActionSheet addAction:cancel];
    
    // show the menu.
    [editActionSheet.view setTintColor:[UITools defaultTintColor]];
    [self presentViewController:editActionSheet animated:YES completion:nil];
}

#pragma mark -  UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    UIImage *editedImage = info[UIImagePickerControllerEditedImage];
    UIImage *originalImage = info[UIImagePickerControllerOriginalImage];
    NSData *imageData;
    
    if (editedImage) {
        NSLog(@"Edited image %@", editedImage);
        imageData = [NSData dataWithData:UIImageJPEGRepresentation(editedImage, kJPEGCompressionQuality)];
    } else {
        NSLog(@"Original image %@", originalImage);
        imageData = [NSData dataWithData:UIImageJPEGRepresentation(originalImage, kJPEGCompressionQuality)];
    }
    
    // display a loading spinner
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = NSLocalizedString(@"Saving", nil);
    hud.removeFromSuperViewOnHide = YES;
    [hud setLabelFont:[UIFont fontWithName:[UITools defaultFontName] size:14.0f]];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        [[ServicesManager sharedInstance].roomsService updateAvatar:_room withPhotoData: imageData withCompletionBlock:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if(error) {
                    // Hide our loading spinner
                    [hud hide:YES];
                    NSLog(@"An error occured while updating the room avatar %@", error);
                } else {
                    // If everything is OK,
                    // Display the checkmark, wait 1 sec and go back to MyInfos
                    hud.mode = MBProgressHUDModeCustomView;
                    hud.labelText = NSLocalizedString(@"Saved", nil);
                    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark"]];
                    [hud showAnimated:YES whileExecutingBlock:^{sleep(1);} completionBlock:nil];
                }
            });
        }];
    });

    [picker dismissViewControllerAnimated: YES completion: nil];
}

#pragma mark - RoomsService notifications
-(void) didUpdateRoom:(NSNotification *) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateRoom:notification];
        });
        return;
    }
    
    NSDictionary *userInfo = (NSDictionary *)notification.object;
    Room *room = [userInfo objectForKey:kRoomKey];
    if([room isEqual:_room]){
        //NSArray<NSString *> *changedKeys = [userInfo objectForKey:kRoomChangedAttributesKey];
        
        [self setRoom:room];
        if([self isViewLoaded]){
            [self drawRoomInfos];
            [self.tableView reloadData];
        }
    }
}

-(void) invitationStatusChanged:(NSNotification *) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self invitationStatusChanged:notification];
        });
        return;
    }
    
    Room *room = (Room *) notification.object;
    if([room isEqual:_room]){
        if(_room.myStatusInRoom == ParticipantStatusAccepted) {
            [self drawRoomInfos];
            [self.tableView reloadData];
        } else {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
}

-(void) didRemoveRoom:(NSNotification *) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didRemoveRoom:notification];
        });
        return;
    }
    
    Room *room = (Room *) notification.object;
    if([room isEqual:_room]){
        // We are displaying a room that as been removed so leaving this view.
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - DZNEmptyDataSet
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = NSLocalizedString(@"No attendees",nil);
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:[UITools boldFontName] size:18.0f], NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

#pragma mark - 3D Touch actions
- (NSArray<id<UIPreviewActionItem>> *)previewActionItems {
    UIPreviewAction *archiveAction = [UIPreviewAction actionWithTitle:NSLocalizedString(@"Archive", nil) style:UIPreviewActionStyleDefault handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
        [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"bubble" action:@"save" label:nil value:nil];
        [[ServicesManager sharedInstance].roomsService archiveRoom:_room];
    }];
    
    UIPreviewAction *deleteAction = [UIPreviewAction actionWithTitle:NSLocalizedString(@"Delete", nil) style:UIPreviewActionStyleDestructive handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
        [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"bubble" action:@"delete" label:nil value:nil];
        [[ServicesManager sharedInstance].roomsService deleteParticipant:[_room participantFromContact:[ServicesManager sharedInstance].myUser.contact] fromRoom:_room];
    }];
    
    UIPreviewAction *leaveAction = [UIPreviewAction actionWithTitle:NSLocalizedString(@"Quit", nil) style:UIPreviewActionStyleDestructive handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
        [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"bubble" action:@"leave" label:nil value:nil];
        [[ServicesManager sharedInstance].roomsService leaveRoom:_room];
    }];
    
    UIPreviewAction *acceptAction = [UIPreviewAction actionWithTitle:NSLocalizedString(@"Accept",nil) style:UIPreviewActionStyleDefault handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
        [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"bubble" action:@"accept invitation" label:nil value:nil];
        [[ServicesManager sharedInstance].roomsService acceptInvitation:_room completionBlock:^(NSError *error, BOOL success) {
            if (!success && error.code != NSURLErrorNotConnectedToInternet) {
                [UITools showErrorPopupWithTitle:NSLocalizedString(@"Error", @"Error") message:NSLocalizedString(@"Cannot accept the invitation to join the bubble", nil)  inViewController:self];
            }
        }];
    }];
    
    UIPreviewAction *declineAction = [UIPreviewAction actionWithTitle:NSLocalizedString(@"Decline",nil) style:UIPreviewActionStyleDestructive handler:^(UIPreviewAction * _Nonnull action, UIViewController * _Nonnull previewViewController) {
        [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:@"bubble" action:@"cancel" label:nil value:nil];
        [[ServicesManager sharedInstance].roomsService declineInvitation:_room completionBlock:^(NSError *error, BOOL success) {
            if (!success && error.code != NSURLErrorNotConnectedToInternet) {
                [UITools showErrorPopupWithTitle:NSLocalizedString(@"Error", @"Error") message:NSLocalizedString(@"Cannot decline the invitation to join the bubble", nil)  inViewController:self];
            }
        }];
    }];
    
    if(_room.myStatusInRoom == ParticipantStatusInvited)
        return @[acceptAction, declineAction];
    else {
        if(_room.isMyRoom)
            return @[archiveAction, deleteAction];
        else
            return @[leaveAction];
    }
    return nil;
}

-(void) textFieldDidChange:(NSNotification *) notification {
    UITextField *textField = (UITextField *)notification.object;
    _saveAction.enabled = (textField.text.length >= 3) && ![self roomNameMatchAnExistingRoomWithName:textField.text] && ([[textField.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]] length] != 0);
}

-(BOOL) roomNameMatchAnExistingRoomWithName:(NSString *) name {
    NSArray<Room*> * rooms = [[ServicesManager sharedInstance].roomsService searchMyRoomMatchName:name];
    if(rooms.count > 0)
        return YES;
    
    return NO;
}

#pragma mark - show contact details
-(void) showContactDetails:(Contact *) contact {
    UIContactDetailsViewController* controller = (UIContactDetailsViewController*)[[UIStoryboardManager sharedInstance].contactsDetailsStoryBoard instantiateViewControllerWithIdentifier:@"contactDetailsViewControllerID"];
    controller.contact = contact;
    controller.fromView = self;
    [self.navigationController pushViewController:controller animated:YES];
}
@end
