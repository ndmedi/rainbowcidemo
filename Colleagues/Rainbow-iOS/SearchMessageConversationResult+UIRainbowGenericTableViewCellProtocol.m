/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "SearchMessageConversationResult+UIRainbowGenericTableViewCellProtocol.h"
#import "UITools.h"

@implementation  SearchMessageConversationResult (UIRainbowGenericTableViewCellProtocol)

-(NSString *) mainLabel {
    return self.peer.displayName;
}

-(UIColor *) mainLabelTextColor {
    return [UIColor blackColor];
}

-(UIFont *) mainLabelFont {
    return [UIFont fontWithName:[UITools defaultFontName] size:16];
}

-(NSString *)subLabel {
    if(self.occurences > 1)
        return [NSString stringWithFormat:NSLocalizedString(@"%d results", nil), self.occurences];
    
    return NSLocalizedString(@"1 result", nil);
}

-(NSDate *) date {
    return nil;
}

-(NSString *) bottomRightLabel {
    return nil;
}

-(Peer *) avatar {
    return self.peer;
}

-(NSInteger) badgeValue {
    return 0;
}

-(NSArray *) observablesKeyPath {
    return @[];
}

-(NSString *) cellButtonTitle {
    return nil;
}

-(UIImage *) cellButtonImage {
    return nil;
}

-(UIImage *) rightIcon {
    return nil;
}
-(UIColor *) rightIconTintColor {
    return nil;
}

@end
