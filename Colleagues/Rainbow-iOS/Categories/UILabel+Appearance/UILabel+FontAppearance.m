/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UILabel+FontAppearance.h"
#import "UITools.h"

@implementation UILabel (FontAppearance)

- (void)setAppearanceFont:(UIFont *)font
{
    if (self.tag == 1001) {
        return;
    }
    
    BOOL isBold = (self.font.fontDescriptor.symbolicTraits & UIFontDescriptorTraitBold);
    const CGFloat* colors = CGColorGetComponents(self.textColor.CGColor);
    if (self.font.pointSize == 14) {
        // set font for UIAlertController title
        self.font = [UIFont fontWithName:[UITools defaultFontName] size:16];
    } else if (self.font.pointSize == 13) {
        // set font for UIAlertController message
        self.font = [UIFont fontWithName:[UITools defaultFontName] size:16];
    } else if (isBold) {
        // set font for UIAlertAction with UIAlertActionStyleCancel
        self.font = [UIFont fontWithName:[UITools boldFontName] size:16];
    } else if ((*colors) == 1) {
        // set font for UIAlertAction with UIAlertActionStyleDestructive
        self.font = [UIFont fontWithName:[UITools defaultFontName] size:16];
    } else {
        // set font for UIAlertAction with UIAlertActionStyleDefault
        self.font = [UIFont fontWithName:[UITools defaultFontName] size:16];
    }
    self.tag = 1001;
}

-(UIFont *)appearanceFont {
    return self.font;
}

@end
