/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Message+BodyForGroupChatEvent.h"
#import "UITools.h"

@implementation Message (BodyForGroupChatEvent)

- (BOOL) isDisplayable {
    
    BOOL isdisplayable ;
    switch(self.groupChatEventType){
        case MessageGroupChatEventInvitation:
            isdisplayable =  NO;
            break;
        case MessageGroupChatEventJoin:
            isdisplayable =  NO;
            break;
        case MessageGroupChatEventLeave:
            isdisplayable =  NO;
            break;
        default:
            isdisplayable =  YES;
    }
    return isdisplayable;
            
}

-(NSString *) bodyForGroupChatEvent {
    // Message text for the different kind of group chat event
    NSString *messageText = @"";
    NSString *fullName = @"";
    if([self.groupChatEventPeer isKindOfClass:[Contact class]]){
        fullName = ((Contact *)self.groupChatEventPeer).fullName;
    }
    switch(self.groupChatEventType){
        case MessageGroupChatEventWelcome: {
            Room *room = (Room*) self.via;
            if (room.creationDate) {
                messageText = [NSString stringWithFormat:NSLocalizedString(@"%@ has created the bubble on %@", nil), fullName, [UITools shortFormatForDate:room.creationDate]];
            } else {
                messageText = [NSString stringWithFormat:NSLocalizedString(@"%@ has created the bubble", nil), fullName];
            }
            break;
        }
        case MessageGroupChatEventInvitation:
            messageText = [NSString stringWithFormat:NSLocalizedString(@"%@ has been invited to join the bubble", ""), fullName];
            break;
            
        case MessageGroupChatEventJoin:
            messageText = [NSString stringWithFormat:NSLocalizedString(@"%@ has joined the bubble", ""), fullName];
            break;
            
        case MessageGroupChatEventLeave:
            messageText = [NSString stringWithFormat:NSLocalizedString(@"%@ has left the bubble", ""), fullName];
            break;
            
        case MessageGroupChatEventClose:
            messageText = [NSString stringWithFormat:NSLocalizedString(@"%@ has closed the bubble", ""), fullName];
            break;
            
        case MessageGroupChatEventConferenceAdd:
            messageText = [NSString stringWithFormat:NSLocalizedString(@"%@ has shared a conference bridge", ""), fullName];
            break;
            
        case MessageGroupChatEventConferenceRemove:
            messageText = [NSString stringWithFormat:NSLocalizedString(@"%@ has stopped sharing a conference bridge", ""), fullName];
            break;

        case MessageGroupChatEventConferenceReminder:
            messageText = [NSString stringWithFormat:NSLocalizedString(@"You could now join the meeting", "")];
            break;
            
        case MessageGroupChatEventNone:
            break;
    }
    return messageText;
}

@end
