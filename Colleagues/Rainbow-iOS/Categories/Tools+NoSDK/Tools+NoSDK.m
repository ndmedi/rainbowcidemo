//
//  Tools+NoSDK.m
//  Rainbow-iOS
//
//  Created by Jerome Heymonet on 30/05/2018.
//  Copyright © 2018 ALE International. All rights reserved.
//

#import <Rainbow/Tools.h>

@implementation Tools (NoSDK)
+(BOOL) isUsingSDK {
    return NO;
}
@end
