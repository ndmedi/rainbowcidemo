/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "NSDate+MediumFormat.h"

@implementation NSDate (MediumFormat)

-(NSString *) mediumDateStringNoYear {
    NSDateFormatter *formatter = [NSDateFormatter new];
    NSLocale *currentLocale = [NSLocale currentLocale];
    NSString *dateComponents = @"MMM d H:mm";
    NSString *dateFormat = [NSDateFormatter dateFormatFromTemplate:dateComponents options:0 locale:currentLocale];
    [formatter setDateFormat:dateFormat];
    formatter.locale = currentLocale;
    return [formatter stringFromDate:self];
}
@end
