/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "FirstStartViewController.h"
#import "UITools.h"
#import "LoginViewController.h"
#import <Rainbow/ServicesManager.h>

@interface FirstStartViewController ()
@property (weak, nonatomic) IBOutlet UIButton *createAccountButton;
@property (weak, nonatomic) IBOutlet UIButton *alreadyAMemberButton;
@property (weak, nonatomic) IBOutlet UIImageView *aleLogo;
@property (weak, nonatomic) IBOutlet UILabel *integrationLabel;
@property (weak, nonatomic) IBOutlet UILabel *welcomeLabel;
@end

@implementation FirstStartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeServer:) name:kLoginManagerDidChangeServer object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginViewDismissed:) name:@"loginViewDismissed" object:nil];
    
    self.view.backgroundColor = [UITools backgroundGrayColor];
    
    _welcomeLabel.text = NSLocalizedString(@"Welcome to Rainbow", nil);
    
    _createAccountButton.layer.borderColor = [UIColor whiteColor].CGColor;
    _createAccountButton.layer.cornerRadius = 12.0;
    _createAccountButton.layer.masksToBounds = YES;
    _createAccountButton.layer.zPosition = 1;
    [UITools applyCustomFontTo:_createAccountButton.titleLabel];
    _createAccountButton.backgroundColor = [UITools colorFromHexa:0x34B233FF];
    [_createAccountButton setTitle:[NSLocalizedString(@"Create account", nil) uppercaseString] forState:UIControlStateNormal];
    
    _alreadyAMemberButton.layer.cornerRadius = 12;
    _alreadyAMemberButton.layer.masksToBounds = YES;
    [UITools applyCustomFontTo:_alreadyAMemberButton.titleLabel];
    _alreadyAMemberButton.backgroundColor = [UITools colorFromHexa:0x0085CAFF];
    [_alreadyAMemberButton setTitle:[NSLocalizedString(@"Already a member", nil) uppercaseString] forState:UIControlStateNormal];
    _alreadyAMemberButton.layer.zPosition = 1;
    [UITools applyCustomFontTo:_integrationLabel];
    _integrationLabel.hidden = YES;
    [self displayIntegrationLabel];
    
    if([[[UIDevice currentDevice] model] hasPrefix:@"iPad"]){
        _aleLogo.transform =  CGAffineTransformMakeScale(0.6, 0.6);
    }
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidChangeServer object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"loginViewDismissed" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)didTapCreateAccountButton:(UIButton *)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"createAccountTapped" object:nil];
}

- (IBAction)didTapAlreadyMemberButton:(UIButton *)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"alreadyMemberTapped" object:nil];
}

-(void) didChangeServer:(NSNotification *) notification {
    if(![[NSThread currentThread] isEqual:[NSThread mainThread]]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didChangeServer:notification];
        });
        return;
    }
    
    [self displayIntegrationLabel];
}

-(void) displayIntegrationLabel {
    MyUser *myUser = [ServicesManager sharedInstance].myUser;
    if(myUser.server && !myUser.server.defaultServer){
        _integrationLabel.text = [NSString stringWithFormat:@"Domain : %@", myUser.server.serverDisplayedName];
        _integrationLabel.hidden = NO;
    } else {
        _integrationLabel.hidden = YES;
    }
}

-(void) loginViewDismissed:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self loginViewDismissed:notification];
        });
        return;
    }
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark - Rotation
-(BOOL) shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

@end
