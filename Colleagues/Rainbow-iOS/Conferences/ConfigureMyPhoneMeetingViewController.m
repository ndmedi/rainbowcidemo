/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ConfigureMyPhoneMeetingViewController.h"
#import "UITools.h"
#import <Rainbow/ServicesManager.h>
#import <Rainbow/ConferencesManagerService.h>
#import <Rainbow/RoomsService.h>
#import <Rainbow/defines.h>
#import "UIAvatarView.h"

@interface ConfigureMyPhoneMeetingViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextView *changeMyPhoneMeetingNameText;
@property (weak, nonatomic) IBOutlet UILabel *myPhoneMeetingNameLabel;
@property (weak, nonatomic) IBOutlet UITextField *myPhoneMeetingName;
@property (weak, nonatomic) IBOutlet UIAvatarView *myPhoneMeetingPicture;
@property (weak, nonatomic) IBOutlet UILabel *myPhoneMeetingPictureLabel;
@property (weak, nonatomic) IBOutlet UILabel *myPhoneMeetingNewLinkLabel;
@property (weak, nonatomic) IBOutlet UITextView *myPhoneMeetingNewLinkText;
@property (weak, nonatomic) IBOutlet UIButton *myPhoneMeetingNewLinkButton;

@property (nonatomic, strong) ConferencesManagerService *conferencesManager;
@property (nonatomic, strong) RoomsService *roomsManager;
@end

@implementation ConfigureMyPhoneMeetingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _conferencesManager = [ServicesManager sharedInstance].conferencesManagerService;
    _roomsManager = [ServicesManager sharedInstance].roomsService;
    
    self.title = NSLocalizedString(@"Configure phone meeting", nil);
    UIFont *defaultFont = [UIFont fontWithName:UITools.defaultFontName size:15.0];
    _changeMyPhoneMeetingNameText.font = defaultFont;
    _changeMyPhoneMeetingNameText.text = NSLocalizedString(@"Change the meeting name and meeting picture, create a new access link to the meeting", nil);
    [_changeMyPhoneMeetingNameText setTextContainerInset:UIEdgeInsetsMake(16, 8, 16, 8)];
    _changeMyPhoneMeetingNameText.backgroundColor = UITools.backgroundGrayColor;
    [UITools applyCustomFontTo:_myPhoneMeetingNameLabel];
    _myPhoneMeetingNameLabel.text = NSLocalizedString(@"Meeting name", nil);
    _myPhoneMeetingName.font = defaultFont;
    _myPhoneMeetingName.text = _conferencesManager.myOpenInviteRoom.displayName;
    [UITools applyCustomFontTo:_myPhoneMeetingPictureLabel];
    _myPhoneMeetingPictureLabel.text = NSLocalizedString(@"Change the meeting picture", nil);
    [UITools applyCustomFontTo:_myPhoneMeetingNewLinkLabel];
    _myPhoneMeetingNewLinkLabel.text = NSLocalizedString(@"Create a new link", nil);
    _myPhoneMeetingNewLinkText.font = [UIFont fontWithName:UITools.defaultFontName size:13.0];
    _myPhoneMeetingNewLinkText.text = NSLocalizedString(@"MyPhoneMeetingNewLinkText", nil);
    
    self.myPhoneMeetingPicture.peer = _conferencesManager.myOpenInviteRoom;
    self.myPhoneMeetingPicture.asCircle = YES;
    self.myPhoneMeetingPicture.showPresence = NO;
    
    [_myPhoneMeetingNewLinkButton setTitle:NSLocalizedString(@"Create a new link", nil) forState:UIControlStateNormal];
    [UITools applyCustomFontTo:_myPhoneMeetingNewLinkButton.titleLabel];
    [_myPhoneMeetingNewLinkButton setContentEdgeInsets:UIEdgeInsetsMake(0, 8, 0, 8)];
    [_myPhoneMeetingNewLinkButton setBackgroundImage:[UITools imageWithColor:UITools.redColor size:CGSizeMake(_myPhoneMeetingNewLinkButton.frame.size.width, 40)] forState:UIControlStateNormal];
    [_myPhoneMeetingNewLinkButton setBackgroundImage:[UITools imageWithColor:[UITools hilightColorFrom:UITools.redColor] size:CGSizeMake(_myPhoneMeetingNewLinkButton.frame.size.width, 40)] forState:UIControlStateHighlighted];
}

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Rotation

-(BOOL) shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(([[self.myPhoneMeetingName.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]] length] >= 3)){
        [textField resignFirstResponder];
        [self changeMeetingNameAction:nil];
        return NO;
    } else {
       return YES;
    }
}

#pragma mark - IBAction

- (IBAction)newLinkAction:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Create a new link", nil)
                                                                   message:NSLocalizedString(@"You will create a new link for your meeting that you need to share again.", nil)
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *createAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Create", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.conferencesManager resetMyOpenInviteIdWithCompletionHandler:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                hud.mode = MBProgressHUDModeCustomView;
                hud.removeFromSuperViewOnHide = YES;
                [hud setLabelFont:[UIFont fontWithName:[UITools defaultFontName] size:14.0f]];
                if(error){
                    hud.labelText = NSLocalizedString(@"Failed to create a new link to your phone meeting", nil);
                    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Failed"]];
                } else {
                    hud.labelText = NSLocalizedString(@"A new link to your phone meeting has been created", nil);
                    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark"]];
                }
                [hud showAnimated:YES whileExecutingBlock:^{sleep(2);} completionBlock:nil];
            });
        }];
    }];
    [alert addAction:createAction];

    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alert addAction:cancelAction];
    
    [self presentViewController:alert animated:YES completion:^{
    }];
}

- (IBAction)changeMeetingNameAction:(id)sender {
    if([[self.myPhoneMeetingName.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]] length] < 3){
        return;
    }
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = NSLocalizedString(@"Saving", nil);
    hud.removeFromSuperViewOnHide = YES;
    [hud setLabelFont:[UIFont fontWithName:[UITools defaultFontName] size:14.0f]];
    
    [_roomsManager updateRoom:_conferencesManager.myOpenInviteRoom withName:self.myPhoneMeetingName.text withCompletionBlock:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            hud.mode = MBProgressHUDModeCustomView;
            if(error){
                hud.labelText = NSLocalizedString(@"Failed", nil);
                hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Failed"]];
            } else {
                hud.labelText = NSLocalizedString(@"Saved", nil);
                hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark"]];
                NSString *roomTitle = [self.myPhoneMeetingName.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
                [[NSUserDefaults standardUserDefaults] setObject:roomTitle forKey:@"MyOpenInviteRoomTitle"];
                [self.conferencesManager setMyOpenInviteRoomTitle: roomTitle];
            }
            [hud showAnimated:YES whileExecutingBlock:^{sleep(1);} completionBlock:nil];
        });
    }];
    
}

- (IBAction)changeMeetingAvatarAction:(id)sender {
    
    UIAlertController *chooseSource = [UIAlertController alertControllerWithTitle: NSLocalizedString(@"Choose your source", nil)
                                                                          message: nil
                                                                   preferredStyle: UIAlertControllerStyleActionSheet];
    
    [chooseSource addAction:[UIAlertAction actionWithTitle: NSLocalizedString(@"Choose From Library", nil) style: UIAlertActionStyleDefault handler: ^(UIAlertAction * action) {
        [UITools openImagePicker:UIImagePickerControllerSourceTypePhotoLibrary delegate:self presentingViewController:self allowsEditing:YES];
    }]];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [chooseSource addAction:[UIAlertAction actionWithTitle: NSLocalizedString(@"Take Photo", nil) style: UIAlertActionStyleDefault handler: ^(UIAlertAction * _Nonnull action) {
            [UITools openImagePicker:UIImagePickerControllerSourceTypeCamera delegate:self presentingViewController:self allowsEditing:YES];
        }]];
    }
    
    // Custom 'cancel' button
    [chooseSource addAction:[UIAlertAction actionWithTitle: NSLocalizedString(@"Cancel", nil) style: UIAlertActionStyleCancel handler: ^(UIAlertAction * _Nonnull action) {
        [chooseSource dismissViewControllerAnimated:YES completion:nil];
    }]];
    [chooseSource.view setTintColor:[UITools defaultTintColor]];
    [self presentViewController:chooseSource animated:YES completion:nil];
}

#pragma mark -  UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    UIImage *editedImage = info[UIImagePickerControllerEditedImage];
    UIImage *originalImage = info[UIImagePickerControllerOriginalImage];
    NSData *imageData;
    
    if (editedImage) {
        NSLog(@"Edited image %@", editedImage);
        imageData = [NSData dataWithData:UIImageJPEGRepresentation(editedImage, kJPEGCompressionQuality)];
    } else {
        NSLog(@"Original image %@", originalImage);
        imageData = [NSData dataWithData:UIImageJPEGRepresentation(originalImage, kJPEGCompressionQuality)];
    }
    
    // display a loading spinner
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = NSLocalizedString(@"Saving", nil);
    hud.removeFromSuperViewOnHide = YES;
    [hud setLabelFont:[UIFont fontWithName:[UITools defaultFontName] size:14.0f]];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        [[ServicesManager sharedInstance].roomsService updateAvatar:_conferencesManager.myOpenInviteRoom withPhotoData: imageData withCompletionBlock:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if(error) {
                    // Hide our loading spinner
                    [hud hide:YES];
                    NSLog(@"An error occured while updating the room avatar %@", error);
                } else {
                    // If everything is OK,
                    // Display the checkmark, wait 1 sec and go back
                    hud.mode = MBProgressHUDModeCustomView;
                    hud.labelText = NSLocalizedString(@"Saved", nil);
                    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark"]];
                    [hud showAnimated:YES whileExecutingBlock:^{sleep(1);} completionBlock:nil];
                }
            });
        }];
    });
    
    [picker dismissViewControllerAnimated: YES completion: nil];
}


@end
