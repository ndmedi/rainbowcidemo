/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ShareMyPhoneMeetingViewController.h"
#import "UITools.h"
#import "Rainbow/Tools.h"
#import <Rainbow/ServicesManager.h>
#import <Rainbow/ConferencesManagerService.h>
#import <MobileCoreServices/MobileCoreServices.h>

@interface ShareMyPhoneMeetingViewController ()
@property (weak, nonatomic) IBOutlet UITextView *titleText;
@property (weak, nonatomic) IBOutlet UITextView *informationText;
@property (weak, nonatomic) IBOutlet UIButton *includeOrganizerAccessCodeButton;
@property (weak, nonatomic) IBOutlet UILabel *includeOrganizerAccessCodeLabel;
@property (weak, nonatomic) IBOutlet UIButton *includeAllPhoneNumbersButton;
@property (weak, nonatomic) IBOutlet UILabel *includeAllPhoneNumbersLabel;
@property (weak, nonatomic) IBOutlet UIButton *cpyInformationButton;

@property (nonatomic, strong) ConferencesManagerService *conferenceManager;
@property (nonatomic, strong) NSMutableParagraphStyle *paragraphStyle;
@end

@implementation ShareMyPhoneMeetingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _conferenceManager = [ServicesManager sharedInstance].conferencesManagerService;
    
    self.title = NSLocalizedString(@"Share meeting information", nil);
    _includeOrganizerAccessCodeLabel.text = NSLocalizedString(@"Include organizer access code", nil);
    _includeAllPhoneNumbersLabel.text = NSLocalizedString(@"Include all phone numbers", nil);
    self.view.backgroundColor = UITools.backgroundGrayColor;
    UIFont *defaultFont = [UIFont fontWithName:UITools.defaultFontName size:15.0];
    _titleText.font = defaultFont;
    _titleText.text = NSLocalizedString(@"Share following message with your invitees", nil);
    [_titleText setTextContainerInset:UIEdgeInsetsMake(8, 8, 8, 8)];
    [_informationText setTextContainerInset:UIEdgeInsetsMake(8, 8, 8, 8)];
    _informationText.font = defaultFont;
    [UITools applyCustomFontTo:_includeAllPhoneNumbersLabel];
    [UITools applyCustomFontTo:_includeOrganizerAccessCodeLabel];
    
    [_cpyInformationButton setTitle:NSLocalizedString(@"Share", nil) forState:UIControlStateNormal];
    [UITools applyCustomFontTo:_cpyInformationButton.titleLabel];
    
    [_cpyInformationButton setBackgroundImage:[UITools imageWithColor:UITools.defaultTintColor size:CGSizeMake(_cpyInformationButton.frame.size.width, 40)] forState:UIControlStateNormal];
    [_cpyInformationButton setBackgroundImage:[UITools imageWithColor:[UITools hilightColorFrom:UITools.defaultTintColor] size:CGSizeMake(_cpyInformationButton.frame.size.width, 40)] forState:UIControlStateHighlighted];
    
    CGFloat tabInterval = 120.0;
    
    _paragraphStyle = [NSMutableParagraphStyle new];
    NSCharacterSet *terms = [NSTextTab columnTerminatorsForLocale: NSLocale.currentLocale];
    NSTextTab *tabStop0 = [[NSTextTab alloc] initWithTextAlignment:NSTextAlignmentRight location:0 options:@{NSTabColumnTerminatorsAttributeName : terms}];
    NSTextTab *tabStop1 = [[NSTextTab alloc] initWithTextAlignment:NSTextAlignmentRight location:tabInterval options:@{NSTabColumnTerminatorsAttributeName : terms}];
    NSTextTab *tabStop2 = [[NSTextTab alloc] initWithTextAlignment:NSTextAlignmentRight location:2*tabInterval options:@{NSTabColumnTerminatorsAttributeName : terms}];
    [_paragraphStyle setTabStops:@[]];
    [_paragraphStyle addTabStop:tabStop0];
    [_paragraphStyle addTabStop:tabStop1];
    [_paragraphStyle addTabStop:tabStop2];
    
    [self fillInformationText];
}

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Rotation

-(BOOL) shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

-(NSString *)countryFromString:(NSString *)str {
    NSRange range = [str rangeOfString:@","];
    if(range.location != NSNotFound){
        return [str substringToIndex:range.location];
    }
    return str;
}

-(NSString *)cityFromString:(NSString *)str {
    NSRange range = [str rangeOfString:@","];
    if(range.location != NSNotFound){
        return [[str substringFromIndex:range.location + 1] stringByTrimmingCharactersInSet:NSCharacterSet.whitespaceCharacterSet];
    }
    return @"";
}

-(NSAttributedString *)attributedStringFromDialinNumber:(PhoneNumber *)dialInNumber sameCountry:(BOOL)sameCountry {
    UIFont *font = [UIFont fontWithName:UITools.defaultFontName size:15.0];
    UIFont *boldFont = [UIFont fontWithName:UITools.boldFontName size:15.0];
    UIFont *smallerFont = [UIFont fontWithName:UITools.defaultFontName size:13.0];
    
    NSString *country = [self countryFromString:dialInNumber.countryName];
    NSString *countryToDisplay;
    if(sameCountry){
        countryToDisplay = @" ";
    } else {
        countryToDisplay = country;
    }
    NSString *city = [self cityFromString:dialInNumber.countryName];
    
    NSString *cityToDisplay = @"";
    if(![city isEqualToString:@""]){
        cityToDisplay = [NSString stringWithFormat:@" (%@)", city];
    }
    NSString *addCR = @"";
    if(!sameCountry){
        addCR = @"\n";
    }
    
    NSString *phoneStr = [NSString stringWithFormat:@"%@%@\t\t%@%@\n",addCR,countryToDisplay,dialInNumber.numberE164,cityToDisplay];
    NSMutableAttributedString *phoneAttrStr = [[NSMutableAttributedString alloc] initWithString:phoneStr attributes:@{NSFontAttributeName : font,NSParagraphStyleAttributeName : _paragraphStyle}];
    [phoneAttrStr addAttributes:@{NSFontAttributeName : boldFont} range:NSMakeRange(addCR.length, countryToDisplay.length)];
    [phoneAttrStr addAttributes:@{NSFontAttributeName : font, NSForegroundColorAttributeName : UITools.defaultTintColor} range:NSMakeRange(addCR.length+countryToDisplay.length+2, dialInNumber.numberE164.length)];
    [phoneAttrStr addAttributes:@{NSFontAttributeName : smallerFont} range:NSMakeRange(addCR.length+countryToDisplay.length+dialInNumber.numberE164.length+2, cityToDisplay.length)];
    
    return phoneAttrStr;
}

- (void)fillInformationText {
    Room *room = _conferenceManager.myOpenInviteRoom;
    UIFont *font = [UIFont fontWithName:UITools.defaultFontName size:15.0];
    UIFont *boldFont = [UIFont fontWithName:UITools.boldFontName size:15.0];
    NSString *s1 = [NSString stringWithFormat: NSLocalizedString(@"Join my meeting \"%@\" using the following link: ", nil), room.displayName];
    NSString *s2 = [NSString stringWithFormat:@"https://meet.openrainbow.com/%@", _conferenceManager.myOpenInviteId];
    NSString *s3 = NSLocalizedString(@"You can also dial in following conference call number using a phone:", nil);
    NSString *s6 = [NSString stringWithFormat:@"%@\t", NSLocalizedString(@"Attendee access code", nil)];
    NSString *s7 = [Tools formatMeetingAccessCode: room.conference.endpoint.participantCode];
    NSAttributedString *cr = [[NSAttributedString alloc] initWithString:@"\n"];
    
    NSDictionary *linkAttributes = @{NSForegroundColorAttributeName: UITools.defaultTintColor,
                                     NSUnderlineColorAttributeName: UITools.defaultTintColor,
                                     NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    _informationText.linkTextAttributes = linkAttributes;
    
    NSMutableAttributedString *attrStr = [NSMutableAttributedString new];
    [attrStr appendAttributedString:[[NSAttributedString alloc] initWithString:s1 attributes:@{NSFontAttributeName : font}]];
    [attrStr appendAttributedString: cr];
    [attrStr appendAttributedString:[[NSAttributedString alloc] initWithString:s2 attributes:@{NSFontAttributeName : font, NSLinkAttributeName: s2}]];
    [attrStr appendAttributedString: cr];
    [attrStr appendAttributedString: cr];
    [attrStr appendAttributedString:[[NSAttributedString alloc] initWithString:s3 attributes:@{NSFontAttributeName : font}]];
    [attrStr appendAttributedString: cr];
    
    for(PhoneNumber *dialInNumber in _conferenceManager.dialInNumbersShortList){
        [attrStr appendAttributedString: [self attributedStringFromDialinNumber:dialInNumber sameCountry:NO]];
    }
    
    if(s7){
        [attrStr appendAttributedString: cr];
        [attrStr appendAttributedString:[[NSAttributedString alloc] initWithString:s6 attributes:@{NSFontAttributeName : boldFont}]];
        [attrStr appendAttributedString:[[NSAttributedString alloc] initWithString:s7 attributes:@{NSFontAttributeName : font, NSForegroundColorAttributeName : UITools.defaultTintColor}]];
    }
    
    if(_includeOrganizerAccessCodeButton.isSelected && room.conference.endpoint.moderatorCode){
        [attrStr appendAttributedString: cr];
        NSString *s8 = [NSString stringWithFormat:@"%@\t", NSLocalizedString(@"Organizer access code", nil)];
        NSString *s9 = [Tools formatMeetingAccessCode: room.conference.endpoint.moderatorCode];
        [attrStr appendAttributedString:[[NSAttributedString alloc] initWithString:s8 attributes:@{NSFontAttributeName : boldFont}]];
        [attrStr appendAttributedString:[[NSAttributedString alloc] initWithString:s9 attributes:@{NSFontAttributeName : font, NSForegroundColorAttributeName : UITools.defaultTintColor}]];
    }
    
    if(_includeAllPhoneNumbersButton.isSelected){
        NSString *s10 = NSLocalizedString(@"Other conference call numbers:", nil);
        [attrStr appendAttributedString: cr];
        [attrStr appendAttributedString: cr];
        [attrStr appendAttributedString:[[NSAttributedString alloc] initWithString:s10 attributes:@{NSFontAttributeName : font}]];
        [attrStr appendAttributedString: cr];
        
        NSString *previousCountry = nil;
        BOOL sameCountry = NO;
        for(PhoneNumber *dialInNumber in _conferenceManager.dialInNumbers){
            NSString *country = [self countryFromString:dialInNumber.countryName];
            if([previousCountry isEqualToString:country]){
                sameCountry = YES;
            } else {
                sameCountry = NO;
            }
            
            [attrStr appendAttributedString: [self attributedStringFromDialinNumber:dialInNumber sameCountry:sameCountry]];
            
            previousCountry = country;
        }
        
    }

    _informationText.attributedText = attrStr;
}

- (NSString *)getStringToShare {
    Room *room = _conferenceManager.myOpenInviteRoom;
    NSString *s1 = [NSString stringWithFormat: NSLocalizedString(@"Join my meeting \"%@\" using the following link: ", nil), room.displayName];
    NSString *s2 = [NSString stringWithFormat:@"https://meet.openrainbow.com/%@", _conferenceManager.myOpenInviteId];
    NSString *s3 = NSLocalizedString(@"You can also dial in following conference call number using a phone:", nil);
    NSString *s6 = [NSString stringWithFormat:@"%@\t", NSLocalizedString(@"Attendee access code", nil)];
    NSString *s7 = [Tools formatMeetingAccessCode: room.conference.endpoint.participantCode];
    
    NSMutableString *stringToShare = [NSMutableString new];
    
    [stringToShare appendString:[NSString stringWithFormat:@"%@%@", s1, s2]];
    [stringToShare appendString:@"\n\n"];
    [stringToShare appendString:s3];
    [stringToShare appendString:@"\n\n"];
    
    for (PhoneNumber *dialInNumber in _conferenceManager.dialInNumbersShortList) {
        [stringToShare appendString:[NSString stringWithFormat:@"%@ %@", dialInNumber.countryName, dialInNumber.numberE164]];
    }
    
    if(s7){
        [stringToShare appendString:@"\n\n"];
        [stringToShare appendString:s6];
        [stringToShare appendString:s7];
    }
    
    if(_includeOrganizerAccessCodeButton.isSelected && room.conference.endpoint.moderatorCode){
        [stringToShare appendString:@"\n"];
        NSString *s8 = [NSString stringWithFormat:@"%@\t", NSLocalizedString(@"Organizer access code", nil)];
        NSString *s9 = [Tools formatMeetingAccessCode: room.conference.endpoint.moderatorCode];
        [stringToShare appendString:s8];
        [stringToShare appendString:s9];
    }
    
    if(_includeAllPhoneNumbersButton.isSelected){
        NSString *s10 = NSLocalizedString(@"Other conference call numbers:", nil);
        [stringToShare appendString:@"\n\n"];
        [stringToShare appendString:s10];
        [stringToShare appendString:@"\n"];
        for(PhoneNumber *dialInNumber in _conferenceManager.dialInNumbers){
            [stringToShare appendString:@"\n"];
            [stringToShare appendString:[NSString stringWithFormat:@"%@ %@", dialInNumber.countryName, dialInNumber.numberE164]];
        }
    }
    
    return stringToShare;
}

#pragma mark - IBAction

- (IBAction)includeOrganizerAccessCodeAction:(id)sender {
    if(_includeOrganizerAccessCodeButton.isSelected){
        _includeOrganizerAccessCodeButton.selected = NO;
        [_includeOrganizerAccessCodeButton setImage:[UIImage imageNamed:@"RadioButton"] forState:UIControlStateNormal];
    } else {
        _includeOrganizerAccessCodeButton.selected = YES;
        [_includeOrganizerAccessCodeButton setImage:[UIImage imageNamed:@"RadioButton_selected"] forState:UIControlStateNormal];
    }
    [self fillInformationText];
}

- (IBAction)includeAllPhoneNumberAction:(id)sender {
    if(_includeAllPhoneNumbersButton.isSelected){
        _includeAllPhoneNumbersButton.selected = NO;
        [_includeAllPhoneNumbersButton setImage:[UIImage imageNamed:@"RadioButton"] forState:UIControlStateNormal];
    } else {
        _includeAllPhoneNumbersButton.selected = YES;
        [_includeAllPhoneNumbersButton setImage:[UIImage imageNamed:@"RadioButton_selected"] forState:UIControlStateNormal];
    }
    [self fillInformationText];
}

- (IBAction)cpyInformationAction:(id)sender {
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:@[[self getStringToShare]] applicationActivities:nil];
    [activityVC setValue:_conferenceManager.myOpenInviteRoom.displayName forKey:@"Subject"];
    activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll, UIActivityTypeAirDrop];
    [self presentViewController:activityVC animated:YES completion:nil];
}

@end
