/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "PublishersTableViewCell.h"
#import "UITools.h"
#import "UIAvatarView.h"

@interface PublishersTableViewCell ()
@property (weak, nonatomic) IBOutlet UIAvatarView *publisherAvatar;

@property (weak, nonatomic) IBOutlet UILabel *publisherName;
@property (weak, nonatomic) IBOutlet UIImageView *publisherImage;

@end

@implementation PublishersTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [UITools applyCustomFontTo:_publisherName];
    _publisherAvatar.asCircle = YES;
    _publisherAvatar.showPresence = NO;
    _publisherImage.tintColor = [UITools defaultTintColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void) setPublisher:(ConferencePublisher *)publisher {
    _publisher = publisher;
    
    if(!_publisher.subscribed)
        _publisherImage.image = [UIImage imageNamed:@"video"];
    else
        _publisherImage.image = [UIImage imageNamed:@"dropVideo"];
    
    _publisherName.text = _publisher.contact.displayName;
    _publisherAvatar.peer = _publisher.contact;

}

@end
