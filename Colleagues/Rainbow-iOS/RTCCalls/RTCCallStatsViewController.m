/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "RTCCallStatsViewController.h"
#import "RTCCallStatsView.h"
#import <Rainbow/RTCService.h>


#define chartHeigth 200

@implementation Chart
@end


@interface RTCCallStatsViewController ()
@property (nonatomic, strong) RTCCallStatsView *rtcCallStatsView;
@property (nonatomic, strong) NSMutableArray<Chart *> *charts;
@end



@implementation RTCCallStatsViewController

- (instancetype) init {
    if (self = [super init]) {
        _charts = [NSMutableArray new];
    }
    return self;
}

- (void)loadView {
    _rtcCallStatsView = [[RTCCallStatsView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.view = _rtcCallStatsView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newStatsAvailable:) name:kRTCServiceCallStatsNotification object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [_charts removeAllObjects];
    _charts = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(Chart *) getChartWithTrackId:(NSString *) trackId name:(NSString *) name {
    for (Chart *chart in _charts) {
        if ([chart.trackId isEqualToString:trackId] && [chart.name isEqualToString:name]) {
            return chart;
        }
    }
    return nil;
}

-(Chart *) addChart:(NSString *) trackId name:(NSString *) name {
    // Create our chart object
    Chart *chart = [Chart new];
    chart.trackId = trackId;
    chart.name = name;
    
    // Create the chart View
    chart.view = [[UIChartView alloc] initWithFrame:CGRectMake(0, chartHeigth * [_charts count], self.view.frame.size.width, chartHeigth)];
    [chart.view setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [_rtcCallStatsView.scrollview addSubview:chart.view];
        _rtcCallStatsView.scrollview.contentSize = CGSizeMake(self.view.frame.size.width, chartHeigth * [_charts count]);
    });
    
    // Add our chart object to our array
    [_charts addObject:chart];
    return chart;
}

-(void) newStatsAvailable:(NSNotification *) notification {
    
    // example :
    // @{@"trackId": @"RTCMSv0", @"timestamp": @(1484746093043.142090), @"values": @{@"key": @"value", ...}}
    
    NSString *trackId = [notification.object objectForKey:@"trackId"];
    double unix_timestamp = [[notification.object objectForKey:@"timestamp"] doubleValue]/1000.0f;
    NSNumber *timestamp = [NSNumber numberWithDouble:unix_timestamp];
    
    [[notification.object objectForKey:@"values"] enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *obj, BOOL *stop) {
        
        // The packet sent chart
        if ([key isEqualToString:@"packetsSent"]) {
            Chart *chart = [self getChartWithTrackId:trackId name:key];
            if (!chart) {
                chart = [self addChart:trackId name:key];
                chart.view.title = @"Packets sent / second";
                chart.view.minValue = @(0);
                // We make a differential chart (value = current - last)
                chart.view.differential = YES;
            }
            
            // Add the new value to data
            [chart.view.data addObject:@{@"value": @([obj intValue]), @"time": timestamp}];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [chart.view setNeedsDisplay];
            });
        }
        
        // The bytes sent chart
        if ([key isEqualToString:@"bytesSent"]) {
            Chart *chart = [self getChartWithTrackId:trackId name:key];
            if (!chart) {
                chart = [self addChart:trackId name:key];
                chart.view.title = @"Kilobytes sent / second";
                chart.view.minValue = @(0);
                // We make a differential chart (value = current - last)
                chart.view.differential = YES;
            }
            
            // Add the new value to data
            [chart.view.data addObject:@{@"value": @([obj intValue] / 1024.0f), @"time": timestamp}];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [chart.view setNeedsDisplay];
            });
        }
        
        // The audio input level chart
        else if ([key isEqualToString:@"audioInputLevel"]) {
            Chart *chart = [self getChartWithTrackId:trackId name:key];
            if (!chart) {
                chart = [self addChart:trackId name:key];
                chart.view.title = @"Audio input level";
                // Audio levels are values [0,32767], we divide them by 1024
                chart.view.minValue = @(0);
                chart.view.maxValue = @(35);
            }
            
            // Add the new value to data
            [chart.view.data addObject:@{@"value": @([obj intValue] / 1024.0f), @"time": timestamp}];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [chart.view setNeedsDisplay];
            });
        }
        
        // The audio output level chart
        else if ([key isEqualToString:@"audioOutputLevel"]) {
            Chart *chart = [self getChartWithTrackId:trackId name:key];
            if (!chart) {
                chart = [self addChart:trackId name:key];
                chart.view.title = @"Audio output level";
                // Audio levels are values [0,32767]
                chart.view.minValue = @(0);
                chart.view.maxValue = @(35);
            }
            
            // Add the new value to data
            [chart.view.data addObject:@{@"value": @([obj intValue] / 1024.0f), @"time": timestamp}];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [chart.view setNeedsDisplay];
            });
        }
    }];
}

@end
