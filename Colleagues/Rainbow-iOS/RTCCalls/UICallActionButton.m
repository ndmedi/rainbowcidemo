/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UICallActionButton.h"
#import "UITools.h"

@implementation UICallActionButton


- (id)initWithCoder:(NSCoder*)coder {
    self = [super initWithCoder:coder];
    if (self) {
        self.layer.cornerRadius = self.frame.size.width / 2;
        self.layer.masksToBounds = YES;
        self.layer.borderColor = [UIColor whiteColor].CGColor;
        self.layer.borderWidth = 2;
        self.tintColor = [UIColor whiteColor];
    }
    
    return self;
}

- (void)setEnabled:(BOOL)enabled {
    [super setEnabled:enabled];
    if (enabled) {
        self.layer.borderColor = [UIColor whiteColor].CGColor;
    } else {
        self.layer.borderColor = [UITools colorFromHexa:0XFFFFFF80].CGColor;
    }
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    // Call Settings buttons are :
    // just circles with border but no background IF NOT ACTIVE
    // round buttons with background IF ACTIVE
    if(selected) {
        self.backgroundColor = [UIColor whiteColor];
        self.tintColor = [UIColor blackColor];
    } else {
        self.backgroundColor = [UIColor clearColor];
        self.tintColor = [UIColor whiteColor];
    }
}


@end
