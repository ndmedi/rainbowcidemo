/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "RTCCallBubblesView.h"

@interface RTCCallBubblesView ()
@property (nonatomic, weak) IBOutlet UIView *topLeftBubble;
@property (nonatomic, weak) IBOutlet UIView *middleLeftBubble;
@property (nonatomic, weak) IBOutlet UIView *bottomLeftBubble;
@property (nonatomic, weak) IBOutlet UIView *bottomCenterBubble;
@property (nonatomic, weak) IBOutlet UIView *topCenterBubble;
@property (nonatomic, weak) IBOutlet UIView *bottomRightBubble;
@property (nonatomic, weak) IBOutlet UIView *topRightBubble;

@end

@implementation RTCCallBubblesView


-(instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self){
        [self commonInit];
    }
    return self;
}

-(void) commonInit {
    _topLeftBubble.layer.cornerRadius = _topLeftBubble.frame.size.width / 2;
    _middleLeftBubble.layer.cornerRadius = _middleLeftBubble.frame.size.width / 2;
    _bottomLeftBubble.layer.cornerRadius = _bottomLeftBubble.frame.size.width / 2;
    _topCenterBubble.layer.cornerRadius = _topCenterBubble.frame.size.width / 2;
    _bottomCenterBubble.layer.cornerRadius = _bottomCenterBubble.frame.size.width / 2;
    _bottomRightBubble.layer.cornerRadius = _bottomRightBubble.frame.size.width / 2;
    _topRightBubble.layer.cornerRadius = _topRightBubble.frame.size.width / 2;
    
    _topLeftBubble.layer.masksToBounds = YES;
    _middleLeftBubble.layer.masksToBounds = YES;
    _bottomLeftBubble.layer.masksToBounds = YES;
    _topCenterBubble.layer.masksToBounds = YES;
    _bottomCenterBubble.layer.masksToBounds = YES;
    _bottomRightBubble.layer.masksToBounds = YES;
    _topRightBubble.layer.masksToBounds = YES;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self commonInit];
}

@end
