/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "RTCCallStatsView.h"


@implementation RTCCallStatsView

-(instancetype) initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        // Create our scrollview
        _scrollview = [[UIScrollView alloc] initWithFrame:frame];
        _scrollview.contentSize = CGSizeMake(frame.size.width, frame.size.height);
        [self addSubview:_scrollview];
        
    }
    return self;
}

@end
