/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIChannelsGlobalViewController.h"
#import "UIChannelsListViewController.h"
#import <Foundation/Foundation.h>
#import "DZNSegmentedControl.h"
#import <Rainbow/defines.h>
#import "MyInfoNavigationItem.h"
#import "Rainbow/Channel.h"
#import "Channel+Rainbow.h"
#import "UITools.h"
#import "UIContactDetailsViewController.h"
#import "UIStoryboardManager.h"
#import "RecentsConversationsTableViewController.h"
#import "UIViewController+Visible.h"

#define dznButtonDisabledColor 0xB2B2B2FF

typedef NS_ENUM(NSInteger, SelectorType){
    SelectorTypeAllMessages = 0,
    SelectorTypeAllChannels
};

@interface UIChannelsGlobalViewController () <DZNSegmentedControlDelegate>

@property (nonatomic, strong) IBOutlet DZNSegmentedControl *channelsItemsFilter;
@property (nonatomic, strong) NSObject *mutex;
@property (nonatomic, strong) NSSortDescriptor *sortSectionAsc;
@property (nonatomic) NSInteger selectedSegmentIndex;
@property (weak, nonatomic) IBOutlet UIView *channelsItemsContainerView;
@property (weak, nonatomic) IBOutlet UIView *channelsContainerView;
@property (weak, nonatomic) IBOutlet UIView *channelsDiscoverContainerView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *addButton;
@property (strong, nonatomic) UIChannelsListViewController *channelsListViewController;

@end

@implementation UIChannelsGlobalViewController

-(instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self){
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didStartConversation:) name:kConversationsManagerDidStartConversation object:nil];
        
        _mutex = [NSObject new];
        
        if ([Channel imageCategoriesCount] == 0) {
            [Channel setImage:[UIImage imageNamed:@"NewsfeedCateg"] forCategory:ChannelCategoryGlobalNews];
            [Channel setImage:[UIImage imageNamed:@"DesignCateg"] forCategory:ChannelCategoryDesign];
            [Channel setImage:[UIImage imageNamed:@"IdeaCateg"] forCategory:ChannelCategoryInnovation];
            [Channel setImage:[UIImage imageNamed:@"PhoneCateg"] forCategory:ChannelCategorySocialMedia];
            [Channel setImage:[UIImage imageNamed:@"AtomCateg"] forCategory:ChannelCategoryScience];
            [Channel setImage:[UIImage imageNamed:@"CultureCateg"] forCategory:ChannelCategoryCulture];
            [Channel setImage:[UIImage imageNamed:@"BusinessCateg"] forCategory:ChannelCategoryBusiness];
            [Channel setImage:[UIImage imageNamed:@"CpuCateg"] forCategory:ChannelCategoryHitech];
            [Channel setImage:[UIImage imageNamed:@"ChartCateg"] forCategory:ChannelCategoryMarketing];
            [Channel setImage:[UIImage imageNamed:@"PlaneCateg"] forCategory:ChannelCategoryTravel];
        }
        
        if ([Channel colorCategoriesCount] == 0) {
            [Channel setColor:[UITools colorFromHexa:0xE03A12FF] forCategory:ChannelCategoryGlobalNews];
            [Channel setColor:[UITools colorFromHexa:0x852CC4FF] forCategory:ChannelCategoryDesign];
            [Channel setColor:[UITools colorFromHexa:0x48A70EFF] forCategory:ChannelCategoryInnovation];
            [Channel setColor:[UITools colorFromHexa:0xF2B204FF] forCategory:ChannelCategorySocialMedia];
            [Channel setColor:[UITools colorFromHexa:0x2B398FFF] forCategory:ChannelCategoryScience];
            [Channel setColor:[UITools colorFromHexa:0x919EB4FF] forCategory:ChannelCategoryCulture];
            [Channel setColor:[UITools colorFromHexa:0x494241FF] forCategory:ChannelCategoryBusiness];
            [Channel setColor:[UITools colorFromHexa:0x558EF8FF] forCategory:ChannelCategoryHitech];
            [Channel setColor:[UITools colorFromHexa:0xF05C48FF] forCategory:ChannelCategoryMarketing];
            [Channel setColor:[UITools colorFromHexa:0x4B6CE6FF] forCategory:ChannelCategoryTravel];
        }
    }
    return self;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConversationsManagerDidStartConversation object:nil];
}

#pragma mark - View life cycle

-(void) viewDidLoad {
    [super viewDidLoad];
    
    ((MyInfoNavigationItem*)self.navigationItem).parentViewController = self;
//    ((MyInfoNavigationItem*)self.navigationItem).customRightItems = @[_moreMenuButton, _roomRightBarButtonItem];
    
    self.definesPresentationContext = YES;
    self.extendedLayoutIncludesOpaqueBars = YES;
    self.navigationItem.rightBarButtonItems = nil;
    
    [_channelsItemsFilter setItems:@[NSLocalizedString(@"News", nil), NSLocalizedString(@"My channels", nil), NSLocalizedString(@"Explore", nil)]];
    _channelsItemsFilter.delegate = self;
    _channelsItemsFilter.showsCount = NO;
    _channelsItemsFilter.autoAdjustSelectionIndicatorWidth = NO;
    _channelsItemsFilter.height = 60;
    _channelsItemsFilter.selectionIndicatorHeight = 4.0f;
    
    [_channelsItemsFilter addTarget:self action:@selector(filterValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    CGRect frameTab = _channelsItemsFilter.frame;
    frameTab.size.width = self.view.frame.size.width;
    _channelsItemsFilter.frame = frameTab;
    
    [self updateContainerViewsVisibility];
    [self displayGoodActionButton];
}

-(void)filterValueChanged:(UISegmentedControl *)sender {
    _selectedSegmentIndex = sender.selectedSegmentIndex;
    [self updateContainerViewsVisibility];
    [self displayGoodActionButton];
}

-(void)updateContainerViewsVisibility {
    if (_selectedSegmentIndex == 1) {
        _channelsItemsContainerView.hidden = YES;
        _channelsContainerView.hidden = NO;
        _channelsDiscoverContainerView.hidden = YES;
    } else if (_selectedSegmentIndex == 2) {
        _channelsItemsContainerView.hidden = YES;
        _channelsContainerView.hidden = YES;
        _channelsDiscoverContainerView.hidden = NO;
    } else {
        _channelsItemsContainerView.hidden = NO;
        _channelsContainerView.hidden = YES;
        _channelsDiscoverContainerView.hidden = YES;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showChannelsListViewControllerSegueID"])
        _channelsListViewController = segue.destinationViewController;
}

-(void) displayGoodActionButton {
    if ([ServicesManager sharedInstance].myUser.isAllowedToCreateChannel)
        ((MyInfoNavigationItem*)self.navigationItem).customRightItems = @[_addButton];
    else
        ((MyInfoNavigationItem*)self.navigationItem).customRightItems = nil;
}

-(void) scrollToTop {
    if (_selectedSegmentIndex == 1)
        [[NSNotificationCenter defaultCenter] postNotificationName:@"channelsServiceDidScrollToTop" object:@"UIChannelsListViewController"];
    else
        [[NSNotificationCenter defaultCenter] postNotificationName:@"channelsServiceDidScrollToTop" object:@"UIChannelsItemsViewController"];
}

-(BOOL) shouldAutorotate {
    return NO;
}


- (IBAction)addButtonClicked:(id)sender {
    [self performSegueWithIdentifier:@"createChannelSegueID" sender:nil];
}

#pragma mark - Conversation notifications
-(void) didStartConversation:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didStartConversation:notification];
        });
        return;
    }
    
    // We don't want to open the conversation view if the application is in background, case of message send from a push notification.
    if([UIApplication sharedApplication].applicationState == UIApplicationStateBackground)
        return;
    
    if (!self.isVisible)
        return;
    
    Conversation *theConversation = (Conversation *)notification.object;
    [RecentsConversationsTableViewController openConversationViewForConversation:theConversation inViewController:self.navigationController];
}

#pragma mark - Search delegate
- (void) presentContactDetailController:(Contact*) contact{
    UIContactDetailsViewController* controller = (UIContactDetailsViewController*)[[UIStoryboardManager sharedInstance].contactsDetailsStoryBoard instantiateViewControllerWithIdentifier:@"contactDetailsViewControllerID"];
    controller.contact = contact;
    controller.fromView = self;
    [self.navigationController pushViewController:controller animated:YES];
}


@end
