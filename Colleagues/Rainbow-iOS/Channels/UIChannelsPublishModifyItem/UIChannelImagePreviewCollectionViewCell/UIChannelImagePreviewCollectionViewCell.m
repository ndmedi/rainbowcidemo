//
//  UIChannelImagePreviewCollectionViewCell.m
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 15/04/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import "UIChannelImagePreviewCollectionViewCell.h"
#import "UITools.h"

@interface UIChannelImagePreviewCollectionViewCell ()

@property (nonatomic, strong) UIActivityIndicatorView *activityIndicatorView;
@property (weak, nonatomic) IBOutlet UIView *blackBackground;

@end

@implementation UIChannelImagePreviewCollectionViewCell

-(void) awakeFromNib {
    [super awakeFromNib];
    
    // Set a black border for each cell
    self.layer.borderWidth = 0.2;
    self.layer.borderColor = [[UITools colorFromHexa:0xCCCCCCFF] CGColor];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeImagePreview)];
    tap.numberOfTapsRequired = 1;
    [_removeImageImageView setUserInteractionEnabled:YES];
    [_removeImageImageView addGestureRecognizer:tap];
    
    // Set the icon with original tint
    UIImage *image = [[UIImage imageNamed:@"CancelIcon"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [_removeImageImageView setImage:image];
    
    // Round the video icon
    _videoRoundedImageView.layer.cornerRadius = 8;
    _videoRoundedImageView.layer.masksToBounds = YES;
    _videoRoundedImageView.hidden = YES;
    
    _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    _activityIndicatorView.center = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2);
    [self addSubview:_activityIndicatorView];
    [self stopActivityIndicatorView];
}

- (void)prepareForReuse {
    _videoRoundedImageView.hidden = YES;
    [self stopActivityIndicatorView];
}

-(void) removeImagePreview {
    [_delegate removeImagePreview:_object reloadUI:YES];
}

- (void) setObject:(id) object {
    _object = object;
    
    if ([_object isKindOfClass:[NSString class]])
        _videoRoundedImageView.hidden = NO;
    
    if ([object isKindOfClass:[File class]])
        [self startActivityIndicatorView];
}

-(void) startActivityIndicatorView {
    [_activityIndicatorView startAnimating];
    _blackBackground.hidden = NO;
}

-(void) stopActivityIndicatorView {
    if ([_activityIndicatorView isAnimating])
        [_activityIndicatorView stopAnimating];
    
    _blackBackground.hidden = YES;
}

@end
