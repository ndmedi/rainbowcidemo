//
//  UIChannelsPublishModifyItemViewController.m
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 08/04/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import "UIChannelsPublishModifyItemViewController.h"
#import "UITools.h"
#import <Rainbow/defines.h>
#import "UIStoryboardManager.h"
#import <Photos/Photos.h>
#import <Rainbow/ServicesManager.h>
#import "FilteredSortedSectionedArray.h"
#import "UIChannelImagePreviewCollectionViewCell.h"
#import "ChannelImagePreviewProtocol.h"
#import "UIMyFilesOnServerTableViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UITextView+PlaceHolder.h"
#import "UIChannelItemAttachmentView.h"
#import <QBImagePicker/QBImagePicker.h>

#define MAX_ATTACHMENTS_DISPLAYED 5

@interface UIChannelsPublishModifyItemViewController () <UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIDocumentPickerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, ChannelImagePreviewProtocol, ChannelItemAttachmentProtocol, QBImagePickerControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextView *messageTextView;
@property (weak, nonatomic) IBOutlet UIImageView *cameraButton;
@property (weak, nonatomic) IBOutlet UIImageView *imagesButton;
@property (weak, nonatomic) IBOutlet UIImageView *videoButton;
@property (weak, nonatomic) IBOutlet UIImageView *attachmentsButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomSpaceStackViewConstraint;
@property (weak, nonatomic) IBOutlet UICollectionView *imagesPreviewCollectionView;

@property (weak, nonatomic) IBOutlet UIScrollView *attachmentsScrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *attachmentsViewHeightConstraint;

@property (nonatomic, strong) NSMutableArray *attachmentsArray;
@property (nonatomic, strong) UIStackView *attachmentsStackView;
@property int maxAttachmentsDisplayed;

@property (nonatomic, strong) FilteredSortedSectionedArray *imagesPreviewArray;
@property (nonatomic, strong) NSCondition *uploadLock;

@property (nonatomic, strong) UIAlertController *videoPopupController;

@end

@implementation UIChannelsPublishModifyItemViewController

-(instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        _imagesPreviewArray = [FilteredSortedSectionedArray new];
        _uploadLock = [NSCondition new];
        
        _imagesPreviewArray.sectionNameFromObjectComputationBlock = ^NSString*(id obj) {
            return @"ImagesPreviewSection";
        };
        _imagesPreviewArray.objectSortDescriptorForSection = @{@"__default__": @[[UITools noSortDescriptorInAscending:NO]]};
        _imagesPreviewArray.sectionSortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO comparator:^NSComparisonResult(NSString* obj1, NSString* obj2) {
            return [obj1 compare:obj2 options:NSCaseInsensitiveSearch];
        }];
        
        // Init attachments list and stackview
        _attachmentsArray = [NSMutableArray new];
        _attachmentsStackView = [UIStackView new];
        [_attachmentsStackView setAxis:UILayoutConstraintAxisVertical];
        [_attachmentsStackView setSpacing:2];
        _maxAttachmentsDisplayed = MAX_ATTACHMENTS_DISPLAYED;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateFile:) name:kFileSharingServiceDidUpdateFile object:nil];
    
    _messageTextView.delegate = self;
    _messageTextView.placeholder = NSLocalizedString(@"Share an article, an idea or a picture", nil);
    [_messageTextView becomeFirstResponder];
    [UITools applyCustomFontToTextView:_messageTextView andSize:17];
    
    // Init collection view with images preview
    _imagesPreviewCollectionView.dataSource = self;
    _imagesPreviewCollectionView.delegate = self;
    
    if (_channel) {
        self.title = _channel.name;
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Publish", nil) style:UIBarButtonItemStylePlain target:self action:@selector(publishButtonClicked)];
    } else if (_channelItem) {
        Channel *channel = [_channelService getChannel:_channelItem.channelId];
        self.title = channel.name;
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Modify", nil) style:UIBarButtonItemStylePlain target:self action:@selector(modifyButtonClicked)];
        _messageTextView.text = [_channelItem.message stringByReplacingOccurrencesOfString:@"<p><br></p>" withString:@"<p></p>"];
        _messageTextView.text = [_messageTextView.text stringByReplacingOccurrencesOfString:@"</p><p>" withString:@"\n"];
        _messageTextView.text = [_messageTextView.text stringByReplacingOccurrencesOfString:@"<br>" withString:@"\n"];
        _messageTextView.text = [_messageTextView.text stringByReplacingOccurrencesOfString:@"<p>" withString:@""];
        _messageTextView.text = [_messageTextView.text stringByReplacingOccurrencesOfString:@"</p>" withString:@""];
        
        // Fill with images in the post
        for (ChannelItemImage *itemImage in _channelItem.images) {
            [self addImagePreview:itemImage reloadUI:NO needUpload:NO];
        }
        
        // Add also the youtube video link
        if ([_channelItem.youtubeVideoId length] > 0)
            [self addImagePreview:_channelItem.youtubeVideoId reloadUI:NO needUpload:NO];
        
        // Reload the UI
        [_imagesPreviewCollectionView reloadData];
        
        // Add attachments
        for (File *file in _channelItem.attachments) {
            [self addAttachment:file reloadUI:NO];
        }
        [self updateAttachmentsHeight];
    }
    
    self.navigationItem.rightBarButtonItem.enabled = NO;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    UITapGestureRecognizer *tapCameraAction = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cameraAction)];
    [_cameraButton addGestureRecognizer:tapCameraAction];
    
    UITapGestureRecognizer *tapImagesAction = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imagesAction)];
    [_imagesButton addGestureRecognizer:tapImagesAction];
    
    UITapGestureRecognizer *tapVideoAction = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(videoAction)];
    [_videoButton addGestureRecognizer:tapVideoAction];
    
    UITapGestureRecognizer *tapAttachmentsAction = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(attachmentsAction)];
    [_attachmentsButton addGestureRecognizer:tapAttachmentsAction];
    
    [_attachmentsScrollView addSubview:_attachmentsStackView];
    _attachmentsStackView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [_attachmentsStackView.leadingAnchor constraintEqualToAnchor:_attachmentsScrollView.leadingAnchor].active = YES;
    [_attachmentsStackView.trailingAnchor constraintEqualToAnchor:_attachmentsScrollView.trailingAnchor].active = YES;
    [_attachmentsStackView.topAnchor constraintEqualToAnchor:_attachmentsScrollView.topAnchor].active = YES;
    [_attachmentsStackView.bottomAnchor constraintEqualToAnchor:_attachmentsScrollView.bottomAnchor].active = YES;
    [_attachmentsStackView.widthAnchor constraintEqualToAnchor:_attachmentsScrollView.widthAnchor].active = YES;
    
    [self updateDisplayImagesPreviewCollectionView];
    [self updateAttachmentsHeight];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.hidesBottomBarWhenPushed = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (@available(iOS 11, *)) // iOS 11 (or newer)
        [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
}

#pragma mark - Publish / Modify

-(void) publishButtonClicked {
    [self uploadAndPushItemInPublishMode:YES];
}

-(void) modifyButtonClicked {
    [self uploadAndPushItemInPublishMode:NO];
}

-(void) uploadAndPushItemInPublishMode:(BOOL) publishMode {
    self.navigationItem.rightBarButtonItem.enabled = NO;
    [self dismissKeyboard];
    
    void (^publishOrUpdateBlock)() = ^void() {
        NSString *stringToPublish = [_messageTextView.text stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"]; // Set the message
        NSMutableArray *images = [NSMutableArray new]; // Set the list of images
        NSString *videoId = @""; // Set the video youtube id
        
        for (id object in _imagesPreviewArray.objects) {
            if ([object isKindOfClass:[NSString class]])
                videoId = object;
            if ([object isKindOfClass:[ChannelItemImage class]])
                [images addObject:object];
        }
        
        __weak typeof(self) weakSelf = self;
        void (^endRequestBlock)() = ^void(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.navigationItem.rightBarButtonItem setEnabled:YES];
                if (!error)
                    [weakSelf.navigationController popViewControllerAnimated:YES];
            });
        };
        
        if (publishMode) {
            [_channelService addItemToChannel:_channel type:ChannelItemTypeBasic message:stringToPublish title:nil url:nil images:images attachments:_attachmentsArray youtubeVideoId:videoId completionHandler:^(NSError *error) {
                if (error)
                    [UITools showErrorPopupWithTitle:NSLocalizedString(@"An error has occurred", nil) message:NSLocalizedString(@"The post cannot be added.", nil) inViewController:weakSelf];
                endRequestBlock(error);
            }];
        } else {
            [_channelService updateItem:_channelItem type:ChannelItemTypeBasic message:stringToPublish title:nil url:nil images:images attachments:_attachmentsArray youtubeVideoId:videoId completionHandler:^(NSError *error) {
                if (error)
                    [UITools showErrorPopupWithTitle:NSLocalizedString(@"An error has occurred", nil) message:NSLocalizedString(@"The post cannot be modified.", nil) inViewController:weakSelf];
                endRequestBlock(error);
            }];
        }
    };
    
    NSMutableArray *attachmentsToUpload = [NSMutableArray new];
    for (File *file in _attachmentsArray) {
        if ([file.rainbowID length] == 0 || ![file.viewersChannels containsObject:_channel.id])
            [attachmentsToUpload addObject:file];
    }
    
    if ([attachmentsToUpload count] > 0) {
        MBProgressHUD *hud = [UITools showHUDWithMessage:NSLocalizedString(publishMode ? @"Publish in progress..." : @"Update in progress...", nil) forView:self.view mode:MBProgressHUDModeDeterminate dismissCompletionBlock:nil];
        [hud setProgress:0];
        
        [hud showAnimated:YES whileExecutingBlock:^{
            [self uploadSynchronouslyAttachments:attachmentsToUpload withHud:hud];
        } completionBlock:^{
            publishOrUpdateBlock();
        }];
    } else {
        publishOrUpdateBlock();
    }
}

-(void) uploadSynchronouslyAttachments:(NSMutableArray *) attachmentsToUpload withHud:(MBProgressHUD *) hud {
    __weak __typeof__(_uploadLock) weakUploadLock = _uploadLock;
    
    CGFloat progressStep = 1.0f/[attachmentsToUpload count];
    
    // In modify mode, the _channel is nil
    Channel *channel = _channel;
    if (!channel)
        channel = [_channelService getChannel:_channelItem.channelId];
    
    for (File *file in [attachmentsToUpload copy]) {
        [_channelService uploadFile:file forChannel:channel completionHandler:^(NSString *rainbowID, NSError *error) {
            [weakUploadLock lock];
            
            // Here we are not updating the file instance but only the rainbowID. If needed, get the correct instance from the rainbowID and update
            [attachmentsToUpload removeObject:file];
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud setProgress:hud.progress + progressStep];
            });
            [weakUploadLock signal];
            [weakUploadLock unlock];
        }];
    }
    
    [_uploadLock lock];
    while ([attachmentsToUpload count] != 0) {
        [_uploadLock wait];
    }
    [_uploadLock unlock];
}

#pragma mark - Replace / Add / Remove objects from the _imagesPreviewArray (images to upload, images uploaded or even youtube preview)

-(void) replaceFile:(File *) object byChannelItemImage:(ChannelItemImage *) itemImage {
    NSUInteger row = [_imagesPreviewArray indexOfObject:object];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    
    NSUInteger index = [_imagesPreviewArray indexOfObject:object];
    [_imagesPreviewArray replaceObjectAtIndex:index withObject:itemImage];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [_imagesPreviewCollectionView reloadItemsAtIndexPaths:@[indexPath]];
    });
}

-(void) addYoutubeImagePreview:(NSString *) youtubeIdentifier {
    NSUInteger index = -1;
    
    for (NSUInteger i = 0; i < [_imagesPreviewArray count] && index == -1; i++) {
        if ([[_imagesPreviewArray.objects objectAtIndex:i] isKindOfClass:[NSString class]])
            index = i;
    }

    if (index != - 1)
        [_imagesPreviewArray replaceObjectAtIndex:index withObject:youtubeIdentifier];
    else
        [_imagesPreviewArray addObject:youtubeIdentifier];
    
    [_imagesPreviewCollectionView reloadData];
    [self updateDoneButton];
    [self updateDisplayImagesPreviewCollectionView];
}

-(void) addImagePreview:(id) object reloadUI:(BOOL) reloadUI needUpload:(BOOL) needUpload {
    if ([object isKindOfClass:[File class]] && [((File *)object).rainbowID length] > 0) // This is from the sharing
        [_imagesPreviewArray addObject:[[ChannelItemImage alloc] initWithRainbowId:((File *)object).rainbowID]];
    else
        [_imagesPreviewArray addObject:object];
    
    if (reloadUI) {
        [_imagesPreviewCollectionView reloadData];
        [self updateDisplayImagesPreviewCollectionView];
        if (!needUpload)
            [self updateDoneButton];
    }
    
    // The added object is a File image which is not uploaded yet. So do it.
    if (needUpload && [object isKindOfClass:[File class]]) {
        self.navigationItem.rightBarButtonItem.enabled = NO;
        Channel *channel = _channel;
        if (!channel)
            channel = [_channelService getChannel:_channelItem.channelId];
        
        [_channelService uploadFile:object forChannel:channel completionHandler:^(NSString *rainbowID, NSError *error) {
            if (error)
                [_imagesPreviewArray removeObject:object];
            else
                [self updateDoneButton];
        }];
    }
}

-(void) removeImagePreview:(id) object reloadUI:(BOOL) reloadUI {
    NSUInteger row = [_imagesPreviewArray indexOfObject:object];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
    
    [_imagesPreviewArray removeObject:object];
    
    if (reloadUI) {
        if ([_imagesPreviewArray count] == 0)
            [_imagesPreviewCollectionView reloadData];
        else
            [_imagesPreviewCollectionView deleteItemsAtIndexPaths:@[indexPath]];
        [self updateDoneButton];
        [self updateDisplayImagesPreviewCollectionView];
    }
}

-(void) updateDisplayImagesPreviewCollectionView {
    _imagesPreviewCollectionView.hidden = [_imagesPreviewArray count] == 0;
}

#pragma mark - Add / Remove objects from attachments

-(void) addAttachment:(File *) file reloadUI:(BOOL) reloadUI {
    // Insert the subview
    UIChannelItemAttachmentView *view = [UIChannelItemAttachmentView new];
    view.file = file;
    view.delegate = self;
    [view configureEditingMode];
    [_attachmentsStackView addArrangedSubview:view];
    
    // Insert the file object
    [_attachmentsArray addObject:file];
    
    if (reloadUI) {
        // Update the height of the view where we display attachments
        [self updateAttachmentsHeight];
        [self updateDoneButton];
    }
}

-(void) removeAttachment:(File *) file andView:(UIChannelItemAttachmentView *) view {
    // Remove the subview
    [_attachmentsStackView removeArrangedSubview:view];
    
    // Remove the file object
    [_attachmentsArray removeObject:file];
    
    // Update the height of the view where we display attachments
    [self updateAttachmentsHeight];
    [self updateDoneButton];
}

-(void) updateAttachmentsHeight {
    if ([_attachmentsArray count] == 0) {
        _attachmentsViewHeightConstraint.constant = 0;
        return;
    }
    
    if ([_attachmentsArray count] <= _maxAttachmentsDisplayed) {
        _attachmentsViewHeightConstraint.constant = CHANNEL_ITEM_ATTACHMENT_HEIGHT*[_attachmentsArray count] + 2*([_attachmentsArray count] - 1);
        return;
    }
    
    _attachmentsViewHeightConstraint.constant = CHANNEL_ITEM_ATTACHMENT_HEIGHT*_maxAttachmentsDisplayed + 2*_maxAttachmentsDisplayed + CHANNEL_ITEM_ATTACHMENT_HEIGHT/2;
}

#pragma mark - Collection view delegates

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return [[_imagesPreviewArray sections] count];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [[_imagesPreviewArray objectsInSection:[[_imagesPreviewArray sections] objectAtIndex:section]] count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [[_imagesPreviewArray sections] objectAtIndex:indexPath.section];
    id object = [[_imagesPreviewArray objectsInSection:key] objectAtIndex:indexPath.row];
    
    UIChannelImagePreviewCollectionViewCell *cell = [_imagesPreviewCollectionView dequeueReusableCellWithReuseIdentifier:@"channelImagePreviewCellID" forIndexPath:indexPath];
    cell.delegate = self;
    cell.object = object;
    
    if ([object isKindOfClass:[ChannelItemImage class]]) {
        [_channelService loadItemImageWith:object forceOriginalFile:NO completionBlock:^(UIImage *image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.previewImageView.image = image;
            });
        }];
    } else if ([object isKindOfClass:[File class]]) {
        // From the camera, we have data and not thumbnailData
        // From the rainbow sharing, we don't have data but thumbnailData
        // From gallery, we have both
        File *file = object;
        cell.previewImageView.image = [UIImage imageWithData:file.thumbnailData ? file.thumbnailData : file.data];
    } else if ([object isKindOfClass:[NSString class]]) {
        // Youtube video link preview
        [cell.previewImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://img.youtube.com/vi/%@/mqdefault.jpg", object]] placeholderImage:nil];
    }
    
    return cell;
}

#pragma mark - Buttons actions

-(void) cameraAction {
    [UITools openImagePicker:UIImagePickerControllerSourceTypeCamera delegate:self presentingViewController:self allowsEditing:NO];
}

-(void) imagesAction {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(@"Select the source", nil) preferredStyle:UIAlertControllerStyleActionSheet];
    
    // Set the attributedTitle in bold
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Add a picture", nil) attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:30.0f]}];
    [actionSheet setValue:attrString forKey:@"_attributedTitle"];
    
    UIAlertAction* galleryAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Gallery", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        QBImagePickerController *imagePickerController = [QBImagePickerController new];
        imagePickerController.delegate = self;
        imagePickerController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        imagePickerController.mediaType = QBImagePickerMediaTypeImage;
        imagePickerController.assetCollectionSubtypes = @[@(PHAssetCollectionSubtypeSmartAlbumRecentlyAdded),
                                                          @(PHAssetCollectionSubtypeSmartAlbumUserLibrary),
                                                          @(PHAssetCollectionSubtypeSmartAlbumLivePhotos),
                                                          @(PHAssetCollectionSubtypeSmartAlbumScreenshots)];
        imagePickerController.allowsMultipleSelection = YES;
        imagePickerController.showsNumberOfSelectedAssets = YES;
        
        if (@available(iOS 11, *)) {// iOS 11 (or newer)
            [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
        }
        
        [self presentViewController:imagePickerController animated:YES completion:nil];
        
    }];
    [actionSheet addAction:galleryAction];
    
    UIAlertAction* rainbowSharingAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"My Rainbow share", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        UINavigationController *viewController = (UINavigationController*) [[UIStoryboardManager sharedInstance].myFilesStoryBoard instantiateViewControllerWithIdentifier:@"myFilesOnServerNavControllerID"];
        viewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        
        // Set file type to image but keep current state to restore
        NSNumber *savedFileType = (NSNumber*) [[NSUserDefaults standardUserDefaults] objectForKey:@"selectedFilterType"];
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInteger:FilterTypeImage] forKey:@"selectedFilterType"];
        
        ((UIMyFilesOnServerTableViewController *)viewController.viewControllers.firstObject).onDismissed = ^(File *file) {
            [[NSUserDefaults standardUserDefaults] setObject:savedFileType forKey:@"selectedFilterType"]; // Restore state
            if (file && file.type == FileTypeImage)
                [self addImagePreview:file reloadUI:YES needUpload:YES];
        };
        [self presentViewController:viewController animated:YES completion:nil];
    }];
    [actionSheet addAction:rainbowSharingAction];
    
    // Cancel action
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    [actionSheet addAction:cancel];
    
    // show the menu.
    [actionSheet.view setTintColor:[UITools defaultTintColor]];
    [self presentViewController:actionSheet animated:YES completion:nil];
}

-(void) videoAction {
    if (!_videoPopupController) {
        _videoPopupController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Add a YouTube video", nil) message:nil preferredStyle:UIAlertControllerStyleAlert];;
        
        // Cancel button
        [_videoPopupController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleDefault handler:nil]];
        
        // Confirm button
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            // Add the video preview
            UITextField *textField = _videoPopupController.textFields.firstObject;
            [self addYoutubeImagePreview:[self getYoutubeIdentifierFromLink:textField.text]];
        }];
        [_videoPopupController addAction:okAction];
        okAction.enabled = NO;
        
        // Add the textfield
        [_videoPopupController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            textField.placeholder = NSLocalizedString(@"Enter the video link", nil);
        }];
        [_videoPopupController.textFields.firstObject addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }

    // Reset textfield
    _videoPopupController.textFields.firstObject.text = @"";
    
    [self presentViewController:_videoPopupController animated:YES completion:nil];
}

-(void) attachmentsAction {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:NSLocalizedString(@"Select the source", nil) preferredStyle:UIAlertControllerStyleActionSheet];
    
    // Set the attributedTitle in bold
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Add a file", nil) attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:30.0f]}];
    [actionSheet setValue:attrString forKey:@"_attributedTitle"];
    
    UIAlertAction* iCloudAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"iCloud Drive", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        UIDocumentPickerViewController *documentPicker = [[UIDocumentPickerViewController alloc] initWithDocumentTypes:@[@"public.data"] inMode:UIDocumentPickerModeImport];
        documentPicker.delegate = self;
        documentPicker.modalPresentationStyle = UIModalPresentationFormSheet;
        documentPicker.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        if (@available(iOS 11, *)) { // iOS 11 (or newer)
            documentPicker.allowsMultipleSelection = YES;
            [[UINavigationBar appearance] setTintColor:[UITools defaultTintColor]];
        }
        
        [self presentViewController:documentPicker animated:YES completion:nil];
    }];
    [actionSheet addAction:iCloudAction];
    
    UIAlertAction* rainbowSharingAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"My Rainbow share", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        UINavigationController *viewController = (UINavigationController*) [[UIStoryboardManager sharedInstance].myFilesStoryBoard instantiateViewControllerWithIdentifier:@"myFilesOnServerNavControllerID"];
        viewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        
        ((UIMyFilesOnServerTableViewController *)viewController.viewControllers.firstObject).onDismissed = ^(File *file) {
            if (file)
                [self addAttachment:file reloadUI:YES];
        };
        [self presentViewController:viewController animated:YES completion:nil];
    }];
    [actionSheet addAction:rainbowSharingAction];
    
    // Cancel action
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    [actionSheet addAction:cancel];
    
    // show the menu.
    [actionSheet.view setTintColor:[UITools defaultTintColor]];
    [self presentViewController:actionSheet animated:YES completion:nil];
}

#pragma mark - Youtube

-(void) textFieldDidChange:(UITextField *) textField {
    [[_videoPopupController actions] objectAtIndex:1].enabled = [[self getYoutubeIdentifierFromLink:textField.text] length] > 0;
}

-(NSString *) getYoutubeIdentifierFromLink:(NSString *) youtubeLink {
    if ([youtubeLink length] > 15) {
        // Get the correct regex
        NSRegularExpression *regex = nil;
        if ([youtubeLink containsString:@"watch?v="])
            regex = [NSRegularExpression regularExpressionWithPattern:@"watch\\?v=(.{11,})" options:NSRegularExpressionCaseInsensitive error:nil];
        else if ([youtubeLink containsString:@"youtu.be/"])
            regex = [NSRegularExpression regularExpressionWithPattern:@"youtu\\.be\\/(.{11,})" options:NSRegularExpressionCaseInsensitive error:nil];
        
        if (regex) {
            NSTextCheckingResult *match = [regex firstMatchInString:youtubeLink options:0 range:NSMakeRange(0, youtubeLink.length)];
            return [youtubeLink substringWithRange:[match rangeAtIndex:1]];
        }
    }
    
    return nil;
}

#pragma mark - Others

-(void) dismissKeyboard {
    [_messageTextView resignFirstResponder];
}

- (void)textViewDidChange:(UITextView *)textView {
    [self updateDoneButton];
}

-(void) updateDoneButton {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self updateDoneButton];
        });
        return;
    }
    
    BOOL containImageWithoutPreview = NO;
    for (id object in [_imagesPreviewArray.objects copy]) {
        if ([object isKindOfClass:[File class]]) {
            containImageWithoutPreview = YES;
            break;
        }
    }
    
    if (containImageWithoutPreview) {
        self.navigationItem.rightBarButtonItem.enabled = NO;
    } else {
        self.navigationItem.rightBarButtonItem.enabled =    ![_messageTextView.text isEqualToString:@""] || // Message not empty
                                                            [_imagesPreviewArray count] > 0 || // At least one image
                                                            [_attachmentsArray count] > 0; // At least one attachment
    }
}

#pragma mark -  UIImagePickerControllerDelegate (for Camera)

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    UIImage *pickedImage = info[UIImagePickerControllerEditedImage] ? info[UIImagePickerControllerEditedImage] : info[UIImagePickerControllerOriginalImage];
    NSData *imageData = [NSData dataWithData:UIImageJPEGRepresentation(pickedImage, kJPEGCompressionQuality)];
    
    // Get the filename
    PHFetchResult<PHAsset *> *asset = nil;
    NSURL *assetURL;
    if([info objectForKey:UIImagePickerControllerReferenceURL])
        assetURL = info[UIImagePickerControllerReferenceURL];
    else if([info objectForKey:UIImagePickerControllerMediaURL])
        assetURL = info[UIImagePickerControllerMediaURL];
    
    if([picker sourceType] == UIImagePickerControllerSourceTypeCamera) {
        __block NSString *assetLocalIdentifier;
        [[PHPhotoLibrary sharedPhotoLibrary] performChangesAndWait:^{
            PHAssetChangeRequest *assetChangeRequest = [PHAssetChangeRequest creationRequestForAssetFromImage:pickedImage];
            
            NSLog(@"PlaceHolder %@",assetChangeRequest.placeholderForCreatedAsset);
            assetLocalIdentifier = assetChangeRequest.placeholderForCreatedAsset.localIdentifier;
            
        } error:nil];
        
        if (assetLocalIdentifier)
            asset = [PHAsset fetchAssetsWithLocalIdentifiers:@[assetLocalIdentifier] options:nil];
    } else if (assetURL) {
        asset = [PHAsset fetchAssetsWithALAssetURLs:@[assetURL] options:nil];
    }
    
    __block NSString *filename = nil;
    if (asset)
        filename = [asset.firstObject valueForKey:@"filename"];

    File *fileToSend = [[ServicesManager sharedInstance].fileSharingService createTemporaryFileWithFileName:filename andData:imageData andURL:assetURL];
    
    if (fileToSend.type == FileTypeImage) {
        if ([[assetURL.pathExtension lowercaseString] isEqualToString:@"png"] ||
            [[assetURL.pathExtension lowercaseString] isEqualToString:@"jpg"] ||
            [[assetURL.pathExtension lowercaseString] isEqualToString:@"jpeg"]||
            [fileToSend.mimeType isEqualToString:@"image/jpeg"]) {
            
            [self addImagePreview:fileToSend reloadUI:YES needUpload:YES];
        }
    }
    
    [picker dismissViewControllerAnimated: YES completion: nil];
}

#pragma mark - QBImagePickerControllerDelegate (for Gallery)

- (void)qb_imagePickerController:(QBImagePickerController *)imagePickerController didFinishPickingAssets:(NSArray *)assets {
    
    PHImageRequestOptions *requestOptions = [PHImageRequestOptions new];
    requestOptions.resizeMode = PHImageRequestOptionsResizeModeExact;
    requestOptions.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
    requestOptions.synchronous = YES;
    
    for (PHAsset *asset in assets) {
        [[PHImageManager defaultManager] requestImageForAsset:asset targetSize:PHImageManagerMaximumSize contentMode:PHImageContentModeDefault options:requestOptions resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
            
            NSData *imageData = [NSData dataWithData:UIImageJPEGRepresentation(result, kJPEGCompressionQuality)];
            NSString *filename = [asset valueForKey:@"filename"];
            NSURL *assetURL = info[@"PHImageFileURLKey"];
            
            File *fileToSend = [[ServicesManager sharedInstance].fileSharingService createTemporaryFileWithFileName:filename andData:imageData andURL:assetURL];

            if (fileToSend.type == FileTypeImage)
                [self addImagePreview:fileToSend reloadUI:YES needUpload:YES];
        }];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)qb_imagePickerControllerDidCancel:(QBImagePickerController *)imagePickerController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - iCloud files UIDocumentPickerDelegate
- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url {
    if (controller.documentPickerMode == UIDocumentPickerModeImport) {
        NSData *data = [NSData dataWithContentsOfURL:url];
        
        BOOL alreadyInAttachments = NO;
        for (File *file in _attachmentsArray) {
            if ([file.data isEqualToData:data]) {
                alreadyInAttachments = YES;
                break;
            }
        }
        
        if (!alreadyInAttachments) {
            File *attachmentToSend = [[ServicesManager sharedInstance].fileSharingService createTemporaryFileWithFileName:[url lastPathComponent] andData:data andURL:url];
            [self addAttachment:attachmentToSend reloadUI:YES];
        }
    }
}

- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentsAtURLs:(NSArray<NSURL *> *)urls {
    for (NSURL *url in urls) {
        [self documentPicker:controller didPickDocumentAtURL:url];
    }
}

#pragma mark - Keyboard will show / hide notifications

-(void) keyboardWillShow:(NSNotification *) notification {
    if ([notification.userInfo[UIKeyboardFrameEndUserInfoKey] isKindOfClass:[NSValue class]]) {
        CGRect rect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
        
        [self.view layoutIfNeeded];
        [UIView animateWithDuration:1 animations:^{
            _bottomSpaceStackViewConstraint.constant = rect.size.height;
            // If the height of the main stack view after the keyboard is opened is less than 220 (example: iPhone SE --> 190), we only display one and a half
            _maxAttachmentsDisplayed = _messageTextView.superview.frame.size.height - rect.size.height < 220 ? 1 : 3;
            [self updateAttachmentsHeight];
            [self.view layoutIfNeeded];
        }];
    }
}

-(void) keyboardWillHide:(NSNotification *) notification {
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:1 animations:^{
        _bottomSpaceStackViewConstraint.constant = 0;
        _maxAttachmentsDisplayed = MAX_ATTACHMENTS_DISPLAYED;
        [self updateAttachmentsHeight];
        [self.view layoutIfNeeded];
    }];
}

#pragma mark - File notifications

-(void) didUpdateFile:(NSNotification *) notification {
    
    File *file = notification.object;
    
    if (file && file.hasThumbnailOnServer && file.thumbnailData) {
        for (id object in [_imagesPreviewArray.objects copy]) {
            if ([object isKindOfClass:[File class]] && [((File *)object).rainbowID isEqualToString:file.rainbowID]) {
                [self replaceFile:object byChannelItemImage:[[ChannelItemImage alloc] initWithRainbowId:file.rainbowID]];
                [self updateDoneButton];
                break;
            }
        }
    }
}


@end
