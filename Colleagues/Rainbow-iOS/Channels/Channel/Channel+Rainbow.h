/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Rainbow/Rainbow.h>

/**
 *  The Rainbow private channel categories
 */
typedef NS_ENUM(NSInteger, ChannelCategory) {
    ChannelCategoryGlobalNews = 0,
    ChannelCategoryDesign,
    ChannelCategoryInnovation,
    ChannelCategorySocialMedia,
    ChannelCategoryScience,
    ChannelCategoryCulture,
    ChannelCategoryBusiness,
    ChannelCategoryHitech,
    ChannelCategoryMarketing,
    ChannelCategoryTravel
};

@interface Channel(Rainbow)

@property (nonatomic, readonly) ChannelCategory categoryID;

+(NSString *)stringFromChannelCategory:(ChannelCategory)categoryID;
+(ChannelCategory)channelCategoryFromString:(NSString *)str;

#pragma mark - Rainbow private image categories

/**
 *  Return the image for a categoryID (nil if not set)
 */
+(UIImage *) imageForCategory:(ChannelCategory) categoryID;

/**
 *  Set the image for a categoryID
 */
+(void) setImage:(UIImage *) image forCategory:(ChannelCategory) categoryID;

/**
 *  Return the number of image categories set
 */
+(int) imageCategoriesCount;

#pragma mark - Rainbow private image color categories

/**
 *  Return the color for a categoryID (nil if not set)
 */
+(UIColor *) colorForCategory:(ChannelCategory) categoryID;

/**
 *  Return the number of color categories set
 */
+(void) setColor:(UIColor *) color forCategory:(ChannelCategory) categoryID;

/**
 *  Set the color for a categoryID
 */
+(int) colorCategoriesCount;

#pragma mark - Private channel mode facility

+(NSString *)stringFromChannelMode:(ChannelMode)mode;

+(ChannelMode)channelModeFromString:(NSString *)str;


@end
