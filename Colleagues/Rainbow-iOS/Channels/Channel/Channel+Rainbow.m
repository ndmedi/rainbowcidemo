/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */


#import "Channel+Rainbow.h"

@implementation Channel(Rainbow)

static NSMutableDictionary *imageCategories;
static NSMutableDictionary *colorCategories;

+(UIImage *) imageForCategory:(ChannelCategory) categoryID {
    if (!imageCategories)
        return nil;
    
    return [imageCategories objectForKey:[self stringFromChannelCategory:categoryID]];
}

+(void) setImage:(UIImage *) image forCategory:(ChannelCategory) categoryID {
    if (!imageCategories)
        imageCategories = [NSMutableDictionary new];
    
    [imageCategories setValue:image forKey:[self stringFromChannelCategory:categoryID]];
}

+(int) imageCategoriesCount {
    if (!imageCategories)
        return 0;
    
    return (int)[imageCategories count];
}

+(UIColor *) colorForCategory:(ChannelCategory) categoryID {
    return [colorCategories objectForKey:[self stringFromChannelCategory:categoryID]];
}

+(void) setColor:(UIColor *) color forCategory:(ChannelCategory) categoryID {
    if (!colorCategories)
        colorCategories = [NSMutableDictionary new];
    
    [colorCategories setValue:color forKey:[self stringFromChannelCategory:categoryID]];
}

+(int) colorCategoriesCount {
    if (!colorCategories)
        return 0;
    
    return (int)[colorCategories count];
}

+(NSString *)stringFromChannelCategory:(ChannelCategory) categoryID {
    NSString *categoryStr = nil;
    
    switch (categoryID) {
        case ChannelCategoryGlobalNews:
            categoryStr = @"globalnews";
            break;
        case ChannelCategoryDesign:
            categoryStr = @"design";
            break;
        case ChannelCategoryInnovation:
            categoryStr = @"innovation";
            break;
        case ChannelCategorySocialMedia:
            categoryStr = @"social";
            break;
        case ChannelCategoryScience:
            categoryStr = @"science";
            break;
        case ChannelCategoryCulture:
            categoryStr = @"culture";
            break;
        case ChannelCategoryBusiness:
            categoryStr = @"business";
            break;
        case ChannelCategoryHitech:
            categoryStr = @"hitech";
            break;
        case ChannelCategoryMarketing:
            categoryStr = @"marketing";
            break;
        case ChannelCategoryTravel:
            categoryStr = @"travel";
            break;
    }
    
    return categoryStr;
}

+(ChannelCategory) channelCategoryFromString:(NSString *) str {
    if ([str isEqualToString:@"design"]) {
        return ChannelCategoryDesign;
    } else if ([str isEqualToString:@"innovation"]) {
        return ChannelCategoryInnovation;
    } else if ([str isEqualToString:@"social"]) {
        return ChannelCategorySocialMedia;
    } else if ([str isEqualToString:@"science"]) {
        return ChannelCategoryScience;
    } else if ([str isEqualToString:@"culture"]) {
        return ChannelCategoryCulture;
    } else if ([str isEqualToString:@"business"]) {
        return ChannelCategoryBusiness;
    } else if ([str isEqualToString:@"hitech"]) {
        return ChannelCategoryHitech;
    } else if ([str isEqualToString:@"marketing"]) {
        return ChannelCategoryMarketing;
    } else if ([str isEqualToString:@"travel"]) {
        return ChannelCategoryTravel;
    } else {
        return ChannelCategoryGlobalNews;
    }
}

-(ChannelCategory)categoryID {
    return [Channel channelCategoryFromString: self.category];
}

@end
