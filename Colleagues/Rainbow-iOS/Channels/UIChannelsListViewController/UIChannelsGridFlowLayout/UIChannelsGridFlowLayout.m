//
//  UIChannelsGridFlowLayout.m
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 05/02/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import "UIChannelsGridFlowLayout.h"

@interface UIChannelsGridFlowLayout ()

@end

@implementation UIChannelsGridFlowLayout

-(instancetype) init {
    if (self = [super init]) {
        [self setupLayout];
    }
    return self;
}

-(instancetype) initWithPadding:(int) padding {
    if (self = [super init]) {
        _padding = padding;
        _collectionViewScrollDirection = UICollectionViewScrollDirectionVertical;
        [self setupLayout];
    }
    return self;
}

-(instancetype) initWithPadding:(int) padding scrollDirection:(UICollectionViewScrollDirection) scrollDirection {
    if (self = [super init]) {
        _padding = padding;
        _collectionViewScrollDirection = scrollDirection;
        [self setupLayout];
    }
    return self;
}

-(void) setupLayout {
    self.minimumInteritemSpacing = _padding;
    self.minimumLineSpacing = _padding;
    self.scrollDirection = _collectionViewScrollDirection;
}

-(CGFloat) itemWidth {
    if (self.scrollDirection == UICollectionViewScrollDirectionHorizontal)
        return (CGRectGetWidth(self.collectionView.frame) - 2*_padding)/9*4;
    
    return (CGRectGetWidth(self.collectionView.frame) - _padding*3)/2;
}

- (CGSize) itemSize {
    return CGSizeMake([self itemWidth], [self itemWidth]);
}

@end
