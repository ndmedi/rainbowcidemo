//
//  UIChannelHeaderViewCellCollectionReusableView.h
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 11/02/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIChannelHeaderViewCell : UICollectionReusableView

@property (weak, nonatomic) IBOutlet UILabel *sectionLabel;

@end
