//
//  UIChannelHeaderViewCellCollectionReusableView.m
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 11/02/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import "UIChannelHeaderViewCell.h"
#import "UITools.h"

@implementation UIChannelHeaderViewCell

-(void) awakeFromNib {
    [super awakeFromNib];
    
    [UITools applyCustomBoldFontTo:_sectionLabel];
    [_sectionLabel setTextColor:[UITools defaultTintColor]];
}

@end
