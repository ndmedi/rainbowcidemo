//
//  UIChannelViewMoreCollectionViewCell.h
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 01/03/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIChannelViewMoreCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *arrowRightImageView;
@property (weak, nonatomic) IBOutlet UILabel *viewMoreLabel;

@end
