//
//  UIChannelsDiscoverByCategoryFlowLayout.m
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 26/02/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import "UIChannelsDiscoverByCategoryFlowLayout.h"

@implementation UIChannelsDiscoverByCategoryFlowLayout

-(instancetype) init {
    if (self = [super init]) {
        [self setupLayout];
    }
    return self;
}

-(instancetype) initWithPadding:(int) padding {
    if (self = [super init]) {
        _padding = padding;
        [self setupLayout];
    }
    return self;
}

-(void) setupLayout {
    self.minimumInteritemSpacing = _padding;
    self.minimumLineSpacing = _padding;
    self.scrollDirection = UICollectionViewScrollDirectionHorizontal;
}

-(CGFloat) itemWidth {
    return (CGRectGetWidth(self.collectionView.frame) - 2*_padding)/9*4;
}

- (CGSize) itemSize {
    return CGSizeMake([self itemWidth], [self itemWidth]/2);
}

@end
