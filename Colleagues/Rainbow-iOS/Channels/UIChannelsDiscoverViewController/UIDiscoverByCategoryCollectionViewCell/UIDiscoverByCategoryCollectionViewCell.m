//
//  UIDiscoverByCategoryCollectionViewCell.m
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 26/02/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import "UIDiscoverByCategoryCollectionViewCell.h"

@interface UIDiscoverByCategoryCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *categoryImageView;
@property (weak, nonatomic) IBOutlet UILabel *categoryName;


@end

@implementation UIDiscoverByCategoryCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [_categoryImageView setTintColor:[UIColor whiteColor]];
    self.layer.cornerRadius = 5;
    self.layer.masksToBounds = YES;
}

- (void) setCategory:(ChannelCategory)category {
    _category = category;
    
    _categoryImageView.image = [Channel imageForCategory:_category];
    _categoryName.text = [self getLabelCategory:_category];
    [self setBackgroundColor:[Channel colorForCategory:_category]];
}

-(NSString *) getLabelCategory:(ChannelCategory) category {
    NSString *categoryStr = nil;
    
    switch (category) {
        case ChannelCategoryGlobalNews:
            categoryStr = NSLocalizedString(@"globalnews", nil);
            break;
        case ChannelCategoryDesign:
            categoryStr = NSLocalizedString(@"design", nil);
            break;
        case ChannelCategoryInnovation:
            categoryStr = NSLocalizedString(@"innovation", nil);
            break;
        case ChannelCategorySocialMedia:
            categoryStr = NSLocalizedString(@"social", nil);
            break;
        case ChannelCategoryScience:
            categoryStr = NSLocalizedString(@"science", nil);
            break;
        case ChannelCategoryCulture:
            categoryStr = NSLocalizedString(@"culture", nil);
            break;
        case ChannelCategoryBusiness:
            categoryStr = NSLocalizedString(@"business", nil);
            break;
        case ChannelCategoryHitech:
            categoryStr = NSLocalizedString(@"hitech", nil);
            break;
        case ChannelCategoryMarketing:
            categoryStr = NSLocalizedString(@"marketing", nil);
            break;
        case ChannelCategoryTravel:
            categoryStr = NSLocalizedString(@"travel", nil);
            break;
    }
    
    return categoryStr;
}

@end
