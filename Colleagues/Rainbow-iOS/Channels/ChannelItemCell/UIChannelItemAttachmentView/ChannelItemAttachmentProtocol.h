//
//  ChannelImagePreviewProtocol.h
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 18/04/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import <Rainbow/ChannelsService.h>

@protocol ChannelItemAttachmentProtocol <NSObject>

-(void) removeAttachment:(File *) file andView:(UIView *) view;

@end
