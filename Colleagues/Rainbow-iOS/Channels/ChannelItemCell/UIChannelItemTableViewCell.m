/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIChannelItemTableViewCell.h"
#import "UITools.h"
#import <Rainbow/ServicesManager.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImageView+Letters.h"
#import "Contact+Extensions.h"
#import "Channel+Rainbow.h"
#import "UIChannelItemAttachmentView.h"
#import "UIChannelsPublishModifyItemViewController.h"
#import "ChannelItemCellProtocol.h"
#import "YTPlayerView.h"

@import SKPhotoBrowser;

@interface UIChannelItemTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *channelName;
@property (weak, nonatomic) IBOutlet UILabel *channelItemDetails;
@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UIView *actionView;
@property (weak, nonatomic) IBOutlet UIView *isNewItemIndicator;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *separatorViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *separatorView;
@property (weak, nonatomic) IBOutlet UIStackView *cellStackView;

// Youtube player
@property (weak, nonatomic) IBOutlet YTPlayerView *youtubePlayerView;
@property (nonatomic) BOOL isYoutubePlayerViewInitialized;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *youtubePlayerViewHeightConstraint;

// Horizontal views

@property (weak, nonatomic) IBOutlet UIStackView *horizontalStackView1;
@property (weak, nonatomic) IBOutlet UIImageView *horizontalSV1Image1;
@property (weak, nonatomic) IBOutlet UIImageView *horizontalSV1Image2;

@property (weak, nonatomic) IBOutlet UIStackView *horizontalStackView2;
@property (weak, nonatomic) IBOutlet UIImageView *horizontalSV2Image1;
@property (weak, nonatomic) IBOutlet UIImageView *horizontalSV2Image2;
@property (weak, nonatomic) IBOutlet UIImageView *horizontalSV2Image3;

// Vertical views

@property (weak, nonatomic) IBOutlet UIStackView *verticalParentStackView;
@property (weak, nonatomic) IBOutlet UIStackView *verticalStackView;

@property (weak, nonatomic) IBOutlet UIImageView *verticalImage1;
@property (weak, nonatomic) IBOutlet UIImageView *verticalImage2;
@property (weak, nonatomic) IBOutlet UIImageView *verticalImage3;
@property (weak, nonatomic) IBOutlet UIImageView *verticalImage4;

@property (weak, nonatomic) IBOutlet UILabel *imagesButNoSubscribedLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imagesButNoSubscribedHeightConstraint;

@property (strong, nonatomic) UILabel *moreImagesLabel;

@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UITextView *message;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *horizontalStackView1HeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *horizontalStackView2HeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalStackViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalImageView1WidthConstraint;

@property (nonatomic, strong) UIAlertController *actionSheet;

@property (atomic, assign) BOOL canceledHUD;

@end

@implementation UIChannelItemTableViewCell

-(void) awakeFromNib {
    [super awakeFromNib];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateChannel:) name:kChannelsServiceDidUpdateChannel object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateChannelItem:) name:kChannelsServiceDidUpdateItem object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateContact:) name:kContactsManagerServiceDidUpdateContact object:nil];
    
    [UITools applyCustomBoldFontTo:_channelName];
    [UITools applyCustomFontTo:_channelItemDetails];
    _channelName.textColor = [UITools defaultTintColor];
    [_isNewItemIndicator setBackgroundColor:[UITools defaultTintColor]];
    
    _separatorViewHeightConstraint.constant = 12;
    
    _youtubePlayerViewHeightConstraint.constant = _youtubePlayerView.frame.size.width*9/16;
    _isYoutubePlayerViewInitialized = NO;
    
    // Tap on action icon
    UITapGestureRecognizer *tapOnAction = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnActionIcon:)];
    [tapOnAction setNumberOfTapsRequired:1];
    [_actionView setUserInteractionEnabled:YES];
    [_actionView addGestureRecognizer:tapOnAction];
    
    [self setBorderToImageView:_horizontalSV1Image1];
    [self setBorderToImageView:_horizontalSV1Image2];
    [self setBorderToImageView:_horizontalSV2Image1];
    [self setBorderToImageView:_horizontalSV2Image2];
    [self setBorderToImageView:_horizontalSV2Image3];
    
    
    [self setBorderToImageView:_verticalImage1];
    [self setBorderToImageView:_verticalImage2];
    [self setBorderToImageView:_verticalImage3];
    [self setBorderToImageView:_verticalImage4];
    
    // More labbel
    _moreImagesLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, _horizontalStackView1.frame.size.width/3, _horizontalStackView1.frame.size.width/3)];
    _moreImagesLabel.text = nil;
    _moreImagesLabel.textAlignment = NSTextAlignmentCenter;
    _moreImagesLabel.textColor = [UIColor whiteColor];
    [_moreImagesLabel setFont:[_moreImagesLabel.font fontWithSize:30]];
    _moreImagesLabel.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    [_horizontalSV2Image3 addSubview:_moreImagesLabel];
    
    [_imagesButNoSubscribedLabel setTextColor:[UITools defaultTintColor]];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToSubscribe:)];
    tap.numberOfTapsRequired = 1;
    [_imagesButNoSubscribedLabel setUserInteractionEnabled:YES];
    [_imagesButNoSubscribedLabel addGestureRecognizer:tap];
    
    [self configureTapGestureRecognizer:_horizontalSV1Image1];
    [self configureTapGestureRecognizer:_horizontalSV1Image2];
    [self configureTapGestureRecognizer:_horizontalSV2Image1];
    [self configureTapGestureRecognizer:_horizontalSV2Image2];
    [self configureTapGestureRecognizer:_horizontalSV2Image3];
    
    [self configureTapGestureRecognizer:_verticalImage1];
    [self configureTapGestureRecognizer:_verticalImage2];
    [self configureTapGestureRecognizer:_verticalImage3];
    [self configureTapGestureRecognizer:_verticalImage4];
    
    [self resetGUI];
    
    [self setTintColor:[UITools defaultTintColor]];
}

-(void) tapToSubscribe:(UITapGestureRecognizer *) recognizer {
    if (_selectedChannel && !_selectedChannel.subscribed)
        [_channelsService subscribeToChannel:_selectedChannel completionHandler:nil];
}

-(void) configureTapGestureRecognizer:(UIImageView *) imageView {
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected:)];
    singleTap.numberOfTapsRequired = 1;
    [imageView setUserInteractionEnabled:YES];
    [imageView addGestureRecognizer:singleTap];
}

-(void) tapDetected:(UITapGestureRecognizer *) recognizer {
    if ([recognizer.view isKindOfClass:[UIImageView class]]) {
        UIImageView *imageView = (UIImageView *)recognizer.view;
        
        if ([_channelItem.images count] > 0) {
            NSMutableArray *images = [NSMutableArray new];
            int pageIndex = 0;
            
            BOOL displayHUD = NO;
            for (int i = 0; i < [_channelItem.images count]; i++) {
                ChannelItemImage *itemImage = ((ChannelItemImage *)[_channelItem.images objectAtIndex:i]);
                if (itemImage.image == imageView.image)
                    pageIndex = i;
                if (itemImage.isOriginalFile)
                    [images addObject:[SKPhoto photoWithImage:itemImage.image]];
                else
                    displayHUD = YES;
            }
            
            // Init the lib + page index and present the controller
            UIViewController *activeViewController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
            
            if (displayHUD) {
                MBProgressHUD *hud = [UITools showHUDWithMessage:nil forView:activeViewController.view mode:MBProgressHUDModeIndeterminate dismissCompletionBlock:nil];
                [hud addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hudWasCancelled:)]];
                _canceledHUD = NO;
                
                [hud showAnimated:YES whileExecutingBlock:^{
                    for (int i = 0; i < [_channelItem.images count] && !_canceledHUD; i++) {
                        hud.labelText = [NSString stringWithFormat:@"%@ (%i/%i)", NSLocalizedString(@"Loading images", nil), i + 1, (int)[_channelItem.images count]];
                        
                        ChannelItemImage *itemImage = [_channelItem.images objectAtIndex:i];
                        if (!itemImage.isOriginalFile) {
                            dispatch_semaphore_t sema = dispatch_semaphore_create(0);
                            
                            [[ServicesManager sharedInstance].channelsService loadItemImageWith:itemImage forceOriginalFile:YES completionBlock:^(UIImage *image) {
                                if (itemImage.image)
                                    [images addObject:[SKPhoto photoWithImage:itemImage.image]];
                                
                                dispatch_semaphore_signal(sema);
                            }];
                            
                            dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
                        }
                    }
                } completionBlock:^{
                    if (!_canceledHUD) {
                        // Display photos
                        [self setChannelItem:_channelItem]; // We need to update the cell with high resolution images
                        SKPhotoBrowser *photoBrowser = [[SKPhotoBrowser alloc] initWithOriginImage:imageView.image photos:images animatedFromView:imageView];
                        [photoBrowser initializePageIndex:pageIndex];
                        [activeViewController presentViewController:photoBrowser animated:YES completion:nil];
                    } else {
                        // Display "Canceled" message because the user has canceled the loading
                        MBProgressHUD *cancelHud = [UITools showHUDWithMessage:NSLocalizedString(@"Canceled", nil) forView:activeViewController.view mode:MBProgressHUDModeCustomView imageName:@"Failed" dismissCompletionBlock:nil];
                        [cancelHud showAnimated:YES whileExecutingBlock:^{
                            [NSThread sleepForTimeInterval:3];
                        } completionBlock:nil];
                    }
                }];
            } else {
                SKPhotoBrowser *photoBrowser = [[SKPhotoBrowser alloc] initWithOriginImage:imageView.image photos:images animatedFromView:imageView];
                [photoBrowser initializePageIndex:pageIndex];
                [activeViewController presentViewController:photoBrowser animated:YES completion:nil];
            }
        }
    }
}

-(void) hudWasCancelled:(UITapGestureRecognizer *) gestureRecognizer {
    if ([gestureRecognizer.view isKindOfClass:[MBProgressHUD class]]) {
        _canceledHUD = YES;
        [((MBProgressHUD *)gestureRecognizer.view) hide:YES];
    }
}

-(void) tapOnActionIcon:(UIGestureRecognizer *) recognizer {
    Channel *channel = [_channelsService getChannel:_channelItem.channelId];
    
    if (!channel)
        return;
    
    // If the user is not owner or publisher, do nothing because the button shouldn't be displayed (it can happen after a resume, when we are processing events)
    if (channel.userType != ChannelUserTypeOwner && channel.userType != ChannelUserTypePublisher)
        return;
    
    UIViewController *activeViewController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    
    if (_actionSheet) {
        [_actionSheet dismissViewControllerAnimated:NO completion:nil];
        _actionSheet = nil;
    }
    
    _actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    // Set the attributedTitle in bold
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:channel.name attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:30.0f]}];
    [_actionSheet setValue:attrString forKey:@"_attributedTitle"];
    
    // Publish a message is only available if the user is :
    //  * the owner
    //  * or a publisher of the channel
    if (!_selectedChannel) {
        UIAlertAction* publishMessage = [UIAlertAction actionWithTitle:NSLocalizedString(@"Add a post", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [_delegate publishMessageToChannelClicked:channel];
        }];
        [_actionSheet addAction:publishMessage];
    }
    
    // Modify a message is only available if the user is :
    //  * the publisher of the message
    //  * AND the message is type of "basic" (never modified or posted by the web client)
    if ([_channelItem.contact.rainbowID isEqualToString:[ServicesManager sharedInstance].myUser.contact.rainbowID] && _channelItem.type == ChannelItemTypeBasic) {
        UIAlertAction* modifyMessage = [UIAlertAction actionWithTitle:NSLocalizedString(@"Modify this post", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [_delegate modifyChannelItemClicked:_channelItem];
        }];
        [_actionSheet addAction:modifyMessage];
    }
    
    //  Suppress a message is only available if the user is :
    //  * the owner
    //  * or the publisher of the message
    if ([_channelItem.contact.rainbowID isEqualToString:[ServicesManager sharedInstance].myUser.contact.rainbowID] || channel.userType == ChannelUserTypeOwner) {
        UIAlertAction* deleteMessage = [UIAlertAction actionWithTitle:NSLocalizedString(@"Delete this post", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * action) {
            if (_actionSheet) {
                [_actionSheet dismissViewControllerAnimated:NO completion:nil];
                _actionSheet = nil;
            }
            
            _actionSheet = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Delete this post", nil) message:NSLocalizedString(@"You are about to delete this post from your channel. This operation cannot be undone.", nil) preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *confirmDelete = [UIAlertAction actionWithTitle:NSLocalizedString(@"Delete", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                [_channelsService deleteItem:_channelItem completionHandler:nil];
            }];
            [_actionSheet addAction:confirmDelete];
            
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
            [_actionSheet addAction:cancel];
            
            // show the menu.
            [_actionSheet.view setTintColor:[UITools defaultTintColor]];
            [activeViewController presentViewController:_actionSheet animated:YES completion:nil];
        }];
        [_actionSheet addAction:deleteMessage];
    }
    
    // Cancel action
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    [_actionSheet addAction:cancel];
    
    // show the menu.
    [_actionSheet.view setTintColor:[UITools defaultTintColor]];
    [activeViewController presentViewController:_actionSheet animated:YES completion:nil];
}

-(void) setBorderToImageView:(UIImageView *) imageView {
    imageView.layer.borderWidth = 0.5;
    imageView.layer.borderColor = [[UITools colorFromHexa:0xCCCCCCFF] CGColor];
}

-(void) prepareForReuse {
    [super prepareForReuse];
    [self resetGUI];
}

-(void) resetGUI {
    _channelName.hidden = NO;
    _channelName.text = nil;
    
    _channelItemDetails.hidden = NO;
    _channelItemDetails.text = nil;
    
    _avatar.image = nil;
    [_avatar setTintColor:nil];
    [_avatar setBackgroundColor:nil];
    
    _actionView.hidden = YES;
    
    _title.hidden = YES;
    _title.text = nil;
    
    _message.hidden = YES;
    _message.text = nil;
    
    _isNewItemIndicator.hidden = YES;
    
    _imagesButNoSubscribedHeightConstraint.constant = 0;
    
    // Remove all attachment views
    for (UIView *view in _cellStackView.arrangedSubviews) {
        if ([view isKindOfClass:[UIChannelItemAttachmentView class]]) {
            [_cellStackView removeArrangedSubview:view];
            [view removeFromSuperview];
        }
    }
    
    [self resetImageViews];
    
    // Youtube player
    _youtubePlayerView.hidden = YES;
}

-(void) resetImageViews {
    _horizontalStackView1.hidden = YES;
    _horizontalStackView2.hidden = YES;
    
    _horizontalSV1Image1.hidden = YES;
    _horizontalSV1Image2.hidden = YES;
    _horizontalSV2Image1.hidden = YES;
    _horizontalSV2Image2.hidden = YES;
    _horizontalSV2Image3.hidden = YES;
    
    _horizontalStackView1HeightConstraint.constant = 0;
    _horizontalStackView2HeightConstraint.constant = 0;
    
    _verticalParentStackView.hidden = YES;
    _verticalImage1.hidden = YES;
    _verticalImage2.hidden = YES;
    _verticalImage3.hidden = YES;
    _verticalImage4.hidden = YES;
    
    _verticalStackViewHeightConstraint.constant = 0;
    
    _moreImagesLabel.hidden = YES;
}

// Maybe workaround but force to not "select" the cell (the image view changes color..)
-(void) setSelected:(BOOL)selected animated:(BOOL)animated {
    if (!_selectedChannel) {
        UIColor *avatarSavedColor = _avatar.backgroundColor;
        UIColor *separatorSavedColor = _separatorView.backgroundColor;
        
        [super setSelected:selected animated:animated];
        
        _avatar.backgroundColor = avatarSavedColor;
        _separatorView.backgroundColor = separatorSavedColor;
    }
}

-(void) setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    if (!_selectedChannel) {
        UIColor *avatarSavedColor = _avatar.backgroundColor;
        UIColor *separatorSavedColor = _separatorView.backgroundColor;
        
        [super setHighlighted:highlighted animated:animated];
        
        _avatar.backgroundColor = avatarSavedColor;
        _separatorView.backgroundColor = separatorSavedColor;
    }
}

-(void) setSelectedChannel:(Channel *)selectedChannel {
    _selectedChannel = selectedChannel;
    
    // Rounded avatar
    if (_selectedChannel) {
        _avatar.layer.cornerRadius = _avatar.frame.size.width/2.0;
        _avatar.layer.masksToBounds = YES;
    }
}

-(void) setCategoryAvatarForChannel:(Channel *) channel {
    _avatar.image = [Channel imageForCategory:channel.categoryID];
    [_avatar setTintColor:[UIColor whiteColor]];
    [_avatar setBackgroundColor:[Channel colorForCategory:channel.categoryID]];
}

-(void) setChannelItem:(ChannelItem *) channelItem {
    if(_channelItem != channelItem){
        _channelItem = nil;
        _channelItem = channelItem;
    }
    
    Channel *channel = [_channelsService getChannel:channelItem.channelId];
    
    if (_selectedChannel) {
        [self setContactAvatar];
        _channelName.text = channelItem.contact.displayName;
    } else {
        if (channel) {
            if (channel.photoData)
                _avatar.image = [UIImage imageWithData:channel.photoData];
            else
                [self setCategoryAvatarForChannel:channel];
            
            _channelName.text = channel.name;
        } else {
            _avatar.image = [UIImage imageNamed:@"Newsfeed"];
        }
    }
    
    if (_selectedChannel || !_channelItem.contact.displayName)
        _channelItemDetails.text = [UITools optimizedFormatForDate:channelItem.date];
    else
        _channelItemDetails.text = [NSString stringWithFormat:@"%@ - %@", [UITools optimizedFormatForDate:channelItem.date], _channelItem.contact.displayName];
    
    if (_channelItem.editionDate) {
        NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ - ", _channelItemDetails.text]];
        NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"modified",nil) attributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:10.0f]}];
        [mutableAttributedString appendAttributedString:attrString];
        _channelItemDetails.attributedText = mutableAttributedString;
    }
    
    if ([_channelItem.message length] > 0) {
        // Workaround to manage all spaces sent by the web client
        NSString *text = [_channelItem.message stringByReplacingOccurrencesOfString:@"<p><br></p>" withString:@"<p></p>"];
        text = [text stringByReplacingOccurrencesOfString:@"</p><p>" withString:@"<br>"];
        text = [text stringByReplacingOccurrencesOfString:@"<p>" withString:@""];
        text = [text stringByReplacingOccurrencesOfString:@"</p>" withString:@""];
        text = [text stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];

        // Add the span to set the font name and size
        text = [NSString stringWithFormat:@"<span style=\"font-size: %f; font-family: '%@';\">%@</span>", 15.f, [UITools defaultFontName], text];

        // Because of the font we use, we need to replace all strong by a span with correct style
        if ([text containsString:@"<strong"]) {
            text = [text stringByReplacingOccurrencesOfString:@"<strong>" withString:[NSString stringWithFormat:@"<span style=\"font-family: '%@';\">", [UITools boldFontName]]];
            text = [text stringByReplacingOccurrencesOfString:@"<strong style=\"" withString:[NSString stringWithFormat:@"<span style=\"font-family: '%@'; ", [UITools boldFontName]]];
            text = [text stringByReplacingOccurrencesOfString:@"</strong>" withString:@"</span>"];
        }

        NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[text dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        
        _message.hidden = NO;
        _message.attributedText = attrStr;
    }
    
    if ([_channelItem.attachments count] > 0) {
        BOOL alreadyHasAttachments = NO;
        for (UIView *view in _cellStackView.arrangedSubviews) {
            if ([view isKindOfClass:[UIChannelItemAttachmentView class]]) {
                alreadyHasAttachments = YES;
                break;
            }
        }
        
        if (!alreadyHasAttachments) {
            for (File *file in _channelItem.attachments) {
                UIChannelItemAttachmentView *view = [UIChannelItemAttachmentView new];
                view.file = file;
                [view configureViewingMode];
                [_cellStackView addArrangedSubview:view];
            }
        }
    }
    
    _isNewItemIndicator.hidden = !_channelItem.isNew;
    
    // Update action icon (the 3 dots on the top right) if the user can publish, modify or delete the post
    [self updateActionIconWithChannel:channel];
    
    if (!channel.subscribed && [_channelItem.images count] > 0) {
        // If we are not subscribed (from search or discover), just display the message
        _imagesButNoSubscribedHeightConstraint.constant = 35;
    } else {
        // Images
        switch ([_channelItem.images count]) {
            case 0:
            {
                // Nothing to do
                break;
            }
            case 1:
            {
                _horizontalStackView1.hidden = NO;
                
                ChannelItemImage *itemImage = (ChannelItemImage *)channelItem.images.firstObject;
                [self setImageWithItemImage:itemImage forImageView:_horizontalSV1Image1];
                
                if (itemImage.image)
                    _horizontalStackView1HeightConstraint.constant = itemImage.image.size.height/itemImage.image.size.width*_horizontalStackView1.frame.size.width;
                else
                    _horizontalStackView1HeightConstraint.constant = _horizontalStackView1.frame.size.width/3;
                
                break;
            }
            case 2:
            {
                UIImage *image = ((ChannelItemImage *)channelItem.images.firstObject).image;
                CGFloat ratio = image.size.width / image.size.height;
                
                if (ratio > 1.2) {
                    _horizontalStackView1.hidden = NO;
                    _horizontalStackView2.hidden = NO;
                    
                    [self setImageWithItemImage:(ChannelItemImage *)[_channelItem.images objectAtIndex:0] forImageView:_horizontalSV1Image1];
                    [self setImageWithItemImage:(ChannelItemImage *)[_channelItem.images objectAtIndex:1] forImageView:_horizontalSV2Image2];
                    
                    _horizontalStackView1HeightConstraint.constant = _horizontalStackView1.frame.size.width/2;
                    _horizontalStackView2HeightConstraint.constant = _horizontalStackView2.frame.size.width/2;
                } else if (ratio < 0.8) {
                    _horizontalStackView1.hidden = NO;
                    
                    [self setImageWithItemImage:(ChannelItemImage *)[_channelItem.images objectAtIndex:0] forImageView:_horizontalSV1Image1];
                    [self setImageWithItemImage:(ChannelItemImage *)[_channelItem.images objectAtIndex:1] forImageView:_horizontalSV1Image2];
                    
                    _horizontalStackView1HeightConstraint.constant = _horizontalStackView1.frame.size.width;
                } else {
                    _horizontalStackView1.hidden = NO;
                    
                    [self setImageWithItemImage:(ChannelItemImage *)[_channelItem.images objectAtIndex:0] forImageView:_horizontalSV1Image1];
                    [self setImageWithItemImage:(ChannelItemImage *)[_channelItem.images objectAtIndex:1] forImageView:_horizontalSV1Image2];
                    
                    _horizontalStackView1HeightConstraint.constant = _horizontalStackView1.frame.size.width/2;
                }
                
                break;
            }
            case 3:
            {
                UIImage *image = ((ChannelItemImage *)channelItem.images.firstObject).image;
                CGFloat ratio = image.size.width / image.size.height;
                
                if (ratio > 1.2) {
                    _horizontalStackView1.hidden = NO;
                    _horizontalStackView2.hidden = NO;
                    
                    [self setImageWithItemImage:(ChannelItemImage *)[_channelItem.images objectAtIndex:0] forImageView:_horizontalSV1Image1];
                    [self setImageWithItemImage:(ChannelItemImage *)[_channelItem.images objectAtIndex:1] forImageView:_horizontalSV2Image1];
                    [self setImageWithItemImage:(ChannelItemImage *)[_channelItem.images objectAtIndex:2] forImageView:_horizontalSV2Image2];
                    
                    _horizontalStackView1HeightConstraint.constant = _horizontalStackView1.frame.size.width/2;
                    _horizontalStackView2HeightConstraint.constant = _horizontalStackView1.frame.size.width/2;
                } else if (ratio < 0.8) {
                    _verticalParentStackView.hidden = NO;
                    
                    [self setImageWithItemImage:(ChannelItemImage *)[_channelItem.images objectAtIndex:0] forImageView:_verticalImage1];
                    [self setImageWithItemImage:(ChannelItemImage *)[_channelItem.images objectAtIndex:1] forImageView:_verticalImage2];
                    [self setImageWithItemImage:(ChannelItemImage *)[_channelItem.images objectAtIndex:2] forImageView:_verticalImage3];
                    
                    _verticalStackViewHeightConstraint.constant = _verticalParentStackView.frame.size.width;
                    _verticalImageView1WidthConstraint.constant = _verticalParentStackView.frame.size.width/2;
                } else {
                    _horizontalStackView2.hidden = NO;
                    
                    [self setImageWithItemImage:(ChannelItemImage *)[_channelItem.images objectAtIndex:0] forImageView:_horizontalSV2Image1];
                    [self setImageWithItemImage:(ChannelItemImage *)[_channelItem.images objectAtIndex:1] forImageView:_horizontalSV2Image2];
                    [self setImageWithItemImage:(ChannelItemImage *)[_channelItem.images objectAtIndex:2] forImageView:_horizontalSV2Image3];
                    
                    _horizontalStackView2HeightConstraint.constant = _horizontalStackView1.frame.size.width/3;
                }
                break;
            }
            case 4:
            {
                UIImage *image = ((ChannelItemImage *)channelItem.images.firstObject).image;
                CGFloat ratio = image.size.width / image.size.height;
                
                if (ratio > 1.2) {
                    _horizontalStackView1.hidden = NO;
                    _horizontalStackView2.hidden = NO;
                    
                    [self setImageWithItemImage:(ChannelItemImage *)[_channelItem.images objectAtIndex:0] forImageView:_horizontalSV1Image1];
                    [self setImageWithItemImage:(ChannelItemImage *)[_channelItem.images objectAtIndex:1] forImageView:_horizontalSV2Image1];
                    [self setImageWithItemImage:(ChannelItemImage *)[_channelItem.images objectAtIndex:2] forImageView:_horizontalSV2Image2];
                    [self setImageWithItemImage:(ChannelItemImage *)[_channelItem.images objectAtIndex:3] forImageView:_horizontalSV2Image3];
                    
                    _horizontalStackView1HeightConstraint.constant = _horizontalStackView1.frame.size.width/3*2;
                    _horizontalStackView2HeightConstraint.constant = _horizontalStackView1.frame.size.width/3;
                } else if (ratio < 0.8) {
                    _verticalParentStackView.hidden = NO;
                    
                    [self setImageWithItemImage:(ChannelItemImage *)[_channelItem.images objectAtIndex:0]forImageView:_verticalImage1];
                    [self setImageWithItemImage:(ChannelItemImage *)[_channelItem.images objectAtIndex:1]forImageView:_verticalImage2];
                    [self setImageWithItemImage:(ChannelItemImage *)[_channelItem.images objectAtIndex:2]forImageView:_verticalImage3];
                    [self setImageWithItemImage:(ChannelItemImage *)[_channelItem.images objectAtIndex:3]forImageView:_verticalImage4];
                    
                    _verticalStackViewHeightConstraint.constant = _verticalParentStackView.frame.size.width;
                    _verticalImageView1WidthConstraint.constant = _verticalParentStackView.frame.size.width/3*2;
                } else {
                    _horizontalStackView1.hidden = NO;
                    _horizontalStackView2.hidden = NO;
                    
                    [self setImageWithItemImage:(ChannelItemImage *)[_channelItem.images objectAtIndex:0] forImageView:_horizontalSV1Image1];
                    [self setImageWithItemImage:(ChannelItemImage *)[_channelItem.images objectAtIndex:1] forImageView:_horizontalSV1Image2];
                    [self setImageWithItemImage:(ChannelItemImage *)[_channelItem.images objectAtIndex:2] forImageView:_horizontalSV2Image1];
                    [self setImageWithItemImage:(ChannelItemImage *)[_channelItem.images objectAtIndex:3] forImageView:_horizontalSV2Image2];
                    
                    _horizontalStackView1HeightConstraint.constant = _horizontalStackView1.frame.size.width/2;
                    _horizontalStackView2HeightConstraint.constant = _horizontalStackView1.frame.size.width/2;
                }
                break;
            }
            default: // 5 or more
            {
                _horizontalStackView1.hidden = NO;
                _horizontalStackView2.hidden = NO;
                
                [self setImageWithItemImage:(ChannelItemImage *)[_channelItem.images objectAtIndex:0] forImageView:_horizontalSV1Image1];
                [self setImageWithItemImage:(ChannelItemImage *)[_channelItem.images objectAtIndex:1] forImageView:_horizontalSV1Image2];
                [self setImageWithItemImage:(ChannelItemImage *)[_channelItem.images objectAtIndex:2] forImageView:_horizontalSV2Image1];
                [self setImageWithItemImage:(ChannelItemImage *)[_channelItem.images objectAtIndex:3] forImageView:_horizontalSV2Image2];
                [self setImageWithItemImage:(ChannelItemImage *)[_channelItem.images objectAtIndex:4] forImageView:_horizontalSV2Image3];
                
                _horizontalStackView1HeightConstraint.constant = _horizontalStackView1.frame.size.width/2;
                _horizontalStackView2HeightConstraint.constant = _horizontalStackView1.frame.size.width/3;
                
                // Display the label
                if ([_channelItem.images count] > 5) {
                    _moreImagesLabel.text = [NSString stringWithFormat:@"+ %@", @([_channelItem.images count] - 5)];
                    _moreImagesLabel.hidden = NO;
                }
                break;
            }
        }
    }
    
    if ([_channelItem.youtubeVideoId length] > 0) {
        _youtubePlayerView.hidden = NO;
        if (!_isYoutubePlayerViewInitialized) {
            NSDictionary *playerVars = @{ @"playsinline" : @1 };
            [_youtubePlayerView loadWithVideoId:_channelItem.youtubeVideoId playerVars:playerVars];
            _isYoutubePlayerViewInitialized = YES;
        } else {
             [_youtubePlayerView cueVideoById:_channelItem.youtubeVideoId startSeconds:0 suggestedQuality:kYTPlaybackQualityAuto];
        }
    }
}

-(void) setImageWithItemImage:(ChannelItemImage *) itemImage forImageView:(UIImageView *) imageView {
    imageView.image = itemImage.image;
    imageView.hidden = NO;
    
    if (!itemImage.image) {
        [[ServicesManager sharedInstance].channelsService loadItemImageWith:itemImage forceOriginalFile:NO completionBlock:^(UIImage *image) {
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    imageView.image = image;
                });
            }
        }];
    }
}

-(void) updateActionIconWithChannel:(Channel *) channel {
    BOOL hidden = YES;

    // If the user is not publisher, he is not able to publish modify or delete a post
    if (channel.userType == ChannelUserTypeOwner || channel.userType == ChannelUserTypePublisher) {
        BOOL canPublish = !_selectedChannel; // The user can only publish in the news tab (from the 3 dots action icon)
        BOOL canModify = [_channelItem.contact.rainbowID isEqualToString:[ServicesManager sharedInstance].myUser.contact.rainbowID] && _channelItem.type == ChannelItemTypeBasic;
        BOOL canDelete = [_channelItem.contact.rainbowID isEqualToString:[ServicesManager sharedInstance].myUser.contact.rainbowID] || channel.userType == ChannelUserTypeOwner;
        
        if (canPublish || canModify || canDelete)
            hidden = NO;
    }
    
    _actionView.hidden = hidden;
}

-(void) didUpdateChannel: (NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateChannel:notification];
        });
        return;
    }
    
    NSDictionary *userInfo = notification.object;
    NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedKeys];
    
    if (!changedKeys || [changedKeys count] == 0)
        return;
    
    Channel *channel = [userInfo objectForKey:kChannelKey];
    if ([channel.id isEqualToString:_channelItem.channelId]) {
        if (!_selectedChannel) { // We are in the "News" tab
            // Update the avatar if there is a new one
            if ([changedKeys containsObject:@"lastAvatarUpdateDate"]) {
                if (channel.photoData) {
                    _avatar.image = [UIImage imageWithData:channel.photoData];
                } else {
                    [self setCategoryAvatarForChannel:channel];
                }
            }
            
            // Update the name if updated
            if ([changedKeys containsObject:@"name"])
                _channelName.text = channel.name;
        }
        
        // The user type has been updated (publisher / member) so update the 3 dots action icon
        if ([changedKeys containsObject:@"type"])
            [self updateActionIconWithChannel:channel];
    }
}

-(void) didUpdateChannelItem: (NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateChannelItem:notification];
        });
        return;
    }
    
    NSDictionary *userInfo = notification.object;
    NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedKeys];
    
    if (!changedKeys || [changedKeys count] == 0)
        return;
    
    ChannelItem *channelItem = [userInfo objectForKey:kChannelItemKey];
    if ([channelItem.itemId isEqualToString:_channelItem.itemId]) {
        
        // Update the isNew indicator
        if([changedKeys count] == 1 && [changedKeys containsObject:@"isNew"])
            _isNewItemIndicator.hidden = !channelItem.isNew;
    }
}

-(void) didUpdateContact: (NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateContact:notification];
        });
        return;
    }
    
    // If there is no _selectedChannel, that means we are displaying a cell from a channel we are subscribed, so we don't need that information
    if (!_selectedChannel)
        return;
    
    NSDictionary *userInfo = notification.object;
    NSArray<NSString *> *attributes = [userInfo objectForKey:@"attributes"];
    Contact *contact = [userInfo objectForKey:@"contact"];

    if (!attributes || [attributes count] == 0)
        return;
    
    if (![contact.jid isEqualToString:_channelItem.contact.jid])
        return;
    
    if ([attributes containsObject:@"photoData"] || [attributes containsObject:@"displayName"]) {
        _channelName.text = contact.displayName;
        [self setContactAvatar];
    }
}

-(void) setContactAvatar {
    if (_channelItem.contact.photoData) {
        _avatar.image = [UIImage imageWithData:_channelItem.contact.photoData];
    } else {
        UIColor *color = [UIColor lightGrayColor];
        if (_channelItem.contact.displayName)
            color = [UITools colorForString:_channelItem.contact.displayName];
        
        NSString *initials = _channelItem.contact.initials;
        if(initials.length == 2) {
            [_avatar setImageWithString:_channelItem.contact.displayName color:color  circular:NO fontName:[UITools defaultFontName]];
        } else {
            [_avatar setImageWithString:initials color:color  circular:NO fontName:[UITools defaultFontName]];
        }
    }
}

@end
