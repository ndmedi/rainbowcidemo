//
//  UIChannelsAddMembersViewController.m
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 14/05/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import "UIChannelsAddMembersViewController.h"
#import "UITools.h"

@interface UIChannelsAddMembersViewController ()

@property (weak, nonatomic) IBOutlet UILabel *label;

@end

@implementation UIChannelsAddMembersViewController

- (void)awakeFromNib {
    [super awakeFromNib];
    self.hidesBottomBarWhenPushed = YES;
    self.searchRainbowUsersOnly = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (_channelMode == ChannelModeCompanyClosed)
        self.title = NSLocalizedString(@"Choose members", nil);
    else
        self.title = NSLocalizedString(@"Invite members", nil);
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Create", nil) style:UIBarButtonItemStylePlain target:self action:@selector(createButtonClicked)];
    
    [UITools applyCustomFontTo:_label andSize:13];
        
    self.tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.tokenInputView.drawBottomBorder = NO;
    
    // Set correct text label depending if the channel is private or not
    
    if (_channelMode == ChannelModeAllPrivate || _channelMode == ChannelModeCompanyPrivate)
        _label.text = NSLocalizedString(@"Channel can only be joined through an invitation. Only invited people can consult it, they can unsubscribe at any time.", nil);
    else if (_channelMode == ChannelModeAllPublic)
        _label.text = NSLocalizedString(@"Rainbow users can view the channel and subscribe. You can also notify some users and invite them to subscribe.", nil);
    else if (_channelMode == ChannelModeCompanyPublic)
        _label.text = NSLocalizedString(@"Any member of your company can view the channel and subscribe. You can also notify some members and invite them to subscribe.", nil);
    else if (_channelMode == ChannelModeCompanyClosed)
        _label.text = NSLocalizedString(@"People you will choose will automatically be subscribed to this channel. They will not bbe able to unsubscribe.", nil);
    
    // Set correct users (and boolean) depending if the channel is company or not
    NSString *userCompanyId = nil;
    if (_channelMode == ChannelModeCompanyClosed || _channelMode == ChannelModeCompanyPublic || _channelMode == ChannelModeCompanyPrivate) {
        userCompanyId = [ServicesManager sharedInstance].myUser.contact.companyId;
        self.searchSameCompanyOnly = YES;
    }
    
    [self.objects addObjectsFromArray:[self.servicesManager.contactsManagerService.myNetworkContacts filteredArrayUsingPredicate: [NSPredicate predicateWithBlock:^BOOL(Contact *contact, NSDictionary<NSString *,id> * bindings) {
        return contact.isInRoster && contact.canChatWith && (!self.searchSameCompanyOnly || [userCompanyId isEqualToString:contact.companyId]);
    }]]];
    
    [self sortUI];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.tableView reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UIRainbowGenericTableViewCell *cell = (UIRainbowGenericTableViewCell*)[tableView dequeueReusableCellWithIdentifier:kTableViewReusableKey forIndexPath:indexPath];
    if(indexPath.section == 0){
        if(!self.isTyping)
            cell.cellObject = [self.objects objectAtIndex:indexPath.row];
        else
            cell.cellObject = [self.filteredObjects objectAtIndex:indexPath.row];
    } else {
        cell.cellObject = [self.searchedObjects objectAtIndex:indexPath.row];
        // get the avatar.
        [[ServicesManager sharedInstance].contactsManagerService populateAvatarForContact:(Contact*)cell.cellObject];
    }
    cell.cellButtonTapHandler = nil;
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    
    Contact *selectedContact = nil;
    if(indexPath.section == 0){
        if(!self.isTyping)
            selectedContact = [self.objects objectAtIndex:indexPath.row];
        else
            selectedContact = [self.filteredObjects objectAtIndex:indexPath.row];
    } else {
        selectedContact = [self.searchedObjects objectAtIndex:indexPath.row];
    }
    
    CLToken *token = [[CLToken alloc] initWithDisplayText:selectedContact.displayName context:selectedContact];
    [self.tokenInputView addToken:token];
    
    if(indexPath.section == 0){
        if(self.isTyping)
            [self.filteredObjects removeObjectAtIndex:indexPath.row];
        
        [self.objects removeObject:selectedContact];
    } else {
        [self.searchedObjects removeObject:selectedContact];
    }
    
    [tableView reloadData];
}

-(NSPredicate *) predicateForText:(NSString *) text {
    return [NSPredicate predicateWithFormat:@"fullName contains[cd] %@", text];
}

-(void) sortUI {
    if(self.servicesManager.contactsManagerService.sortByFirstName){
        [self.objects sortUsingDescriptors:@[[UITools sortDescriptorForContactByFirstName], [UITools sortDescriptorForContactByLastName]]];
    } else {
        [self.objects sortUsingDescriptors:@[[UITools sortDescriptorForContactByLastName], [UITools sortDescriptorForContactByFirstName]]];
    }
}

-(void) createButtonClicked {
    [self.tokenInputView endEditing];
    
    [[ServicesManager sharedInstance].channelsService createChannelWithMode:_channelMode name:_channelNameString description:_channelDescriptionString category:_channelCategoryString maxItems:50 completionHandler:^(Channel *channel, NSError *error) {
        if (!error) {
            if (_avatar)
                [[ServicesManager sharedInstance].channelsService updateChannel:channel.id avatar:_avatar completionHandler:nil];

            if ([self.tokenInputView.allTokens count] > 0) {
                __block NSMutableArray *users = [NSMutableArray new];
                [self.tokenInputView.allTokens enumerateObjectsUsingBlock:^(CLToken * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    [users addObject:((Contact *)obj.context).rainbowID];
                }];
                
                [[ServicesManager sharedInstance].channelsService addMembersToChannel:channel members:users completionHandler:nil];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                NSInteger indexViewControllerToPop = [self.navigationController.viewControllers count] - 4;
                if (indexViewControllerToPop >= 0)
                    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:indexViewControllerToPop] animated:YES];
            });
        } else {
            NSString *message = @"";
            if (error.code == 409000)
                message = [NSString stringWithFormat:NSLocalizedString(@"The channel \"%@\" already exists. Please try again with another name.", nil), _channelNameString];
            else
                message = [NSString stringWithFormat:NSLocalizedString(@"The channel \"%@\" cannot be created.", nil), _channelNameString];
            [UITools showErrorPopupWithTitle:NSLocalizedString(@"An error has occurred", nil) message:message inViewController:self];
        }
    }];
}

@end
