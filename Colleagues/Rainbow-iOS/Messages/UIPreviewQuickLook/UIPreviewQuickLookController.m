/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */


#import "UIPreviewQuickLookController.h"
#import "UITools.h"

@interface UIPreviewQuickLookController () <QLPreviewControllerDelegate, QLPreviewControllerDataSource>
@property (nonatomic, strong) NSURL *urlToDisplay;

@property (nonatomic, strong) UIColor *previousTintColor;
@property (nonatomic, strong) NSDictionary<NSAttributedStringKey, id> *previousTextAttributes;
@end

@implementation UIPreviewQuickLookController

-(UIStatusBarStyle) preferredStatusBarStyle {
    if (@available(iOS 11, *)) {
        // iOS 11 (or newer)
        return UIStatusBarStyleDefault;
    } else {
        // iOS 10 or older code
        return UIStatusBarStyleLightContent;
    }
}

-(void) setAttachment:(File *)attachment {
    _attachment = attachment;
    // Save attachment data into application cache folder
    NSString *filePath = [NSString stringWithFormat:@"%@/%@",[UITools applicationDocumentsCache], _attachment.fileName];
    NSError *writeError = nil;
    if(![[NSFileManager defaultManager] fileExistsAtPath:filePath]){
        // Save the file only if it not already exist
        [_attachment.data writeToFile:filePath options:NSDataWritingAtomic error:&writeError];
    }
    
    if(!writeError){
        _urlToDisplay = [NSURL fileURLWithPath:filePath];
    
    }
    [self reloadData];
}

-(void) viewDidLoad {
    [super viewDidLoad];
    self.delegate = self;
    self.dataSource = self;
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (@available(iOS 11, *)) {
        // iOS 11 (or newer)
        NSObject<UIAppearance> *appearance = [UINavigationBar appearance];
        self.previousTintColor = [(UINavigationBar *)appearance tintColor];
        [(UINavigationBar *)appearance setTintColor:[UITools defaultTintColor]];
        
        self.previousTextAttributes =  [(UINavigationBar *)appearance titleTextAttributes];
        [(UINavigationBar *)appearance setTitleTextAttributes:@{NSForegroundColorAttributeName:[UITools defaultTintColor], NSFontAttributeName:[UIFont fontWithName:[UITools boldFontName] size:17.0]}];
    }
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (@available(iOS 11, *)) {
        NSObject<UIAppearance> *appearance = [UINavigationBar appearance];
        [(UINavigationBar *)appearance setTintColor:self.previousTintColor];
        
        [(UINavigationBar *)appearance setTitleTextAttributes:self.previousTextAttributes];
    }
}

-(void) dealloc {
    _urlToDisplay = nil;
    _attachment = nil;
}

- (NSInteger)numberOfPreviewItemsInPreviewController:(QLPreviewController *)controller {
    return 1;
}

- (id <QLPreviewItem>)previewController:(QLPreviewController *)controller previewItemAtIndex:(NSInteger)index {
    return _urlToDisplay;
}

- (void) previewControllerDidDismiss:(QLPreviewController *)controller {
    [[NSFileManager defaultManager] removeItemAtURL:_urlToDisplay error:nil];
}
@end
