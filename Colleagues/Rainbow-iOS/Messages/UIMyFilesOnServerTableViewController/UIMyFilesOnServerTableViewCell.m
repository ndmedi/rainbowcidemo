/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIMyFilesOnServerTableViewCell.h"
#import "File+DefaultImage.h"
#import <Rainbow/NSString+FileSize.h>
#import "UITools.h"

@interface UIMyFilesOnServerTableViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *fileImageView;
@property (weak, nonatomic) IBOutlet UILabel *fileName;
@property (weak, nonatomic) IBOutlet UILabel *fileSizeAndDate;
@property (nonatomic, strong) UITapGestureRecognizer *tapOnImage;
@end

@implementation UIMyFilesOnServerTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [UITools applyCustomFontTo:_fileSizeAndDate];
    [UITools applyCustomFontTo:_fileName];
    _fileSizeAndDate.textColor = [UIColor lightGrayColor];
    _fileImageView.tintColor = [UITools defaultTintColor];
    _tapOnImage = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnImage:)];
    
    self.backgroundColor = [UITools defaultBackgroundColor];
}

-(void) dealloc {
    [_fileImageView removeGestureRecognizer:_tapOnImage];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void) setFile:(File *)file {
    _file = file;
    _fileImageView.image = _file.thumbnailBig;
    _fileImageView.tintColor = _file.tintColor;
    if (_fileName.text) {
        _fileName.text = _file.fileName;
    }
    
    if(_file.uploadDate)
        _fileSizeAndDate.text = [NSString stringWithFormat:@"%@ - %@",[NSString formatFileSize:_file.size], [UITools optimizedFormatForDate:_file.uploadDate]];
    else if (_file.dateToSort)
        _fileSizeAndDate.text = [NSString stringWithFormat:@"%@ - %@",[NSString formatFileSize:_file.size], [UITools optimizedFormatForDate:_file.dateToSort]];
    
    [_fileImageView addGestureRecognizer:_tapOnImage];
}

-(void)didTapOnImage:(id)sender {
    [_delegate didTapOnImageForFile:_file forCell:self];
}
@end
