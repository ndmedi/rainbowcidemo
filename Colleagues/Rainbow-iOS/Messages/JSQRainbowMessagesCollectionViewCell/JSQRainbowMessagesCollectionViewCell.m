/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 *
 */

#import "JSQRainbowMessagesCollectionViewCell.h"
#import "UITools.h"
#import "File+DefaultImage.h"
#import <Rainbow/NSString+FileSize.h>
#import <MarqueeLabel/MarqueeLabel.h>
#import "JSQMessagesCollectionView.h"
#import "UIView+JSQMessagesConstant.h"
#import "File+JSQMessageMediaData.h"
#import "UIView+JSQMessages.h"
#import "RainbowMessageView.h"
#import "MarkdownParser.h"

@interface JSQMessagesCollectionView (tapOnImage)

-(void) didTapOnImageForAttachment:(File *) attachment atCell:(JSQRainbowMessagesCollectionViewCell *) cell;
-(void) didFailedSendAttachment:(File *) attachment atCell:(JSQRainbowMessagesCollectionViewCell *) cell;
-(void) didShareAttachment:(File *) attachment atCell:(JSQRainbowMessagesCollectionViewCell *) cell;
-(void) didLongPressOnImageForAttachment:(File *) attachment atCell:(JSQRainbowMessagesCollectionViewCell *) cell;
@end

@implementation JSQMessagesCollectionView (tapOnImage)

-(void) didTapOnImageForAttachment:(File *) attachment atCell:(JSQRainbowMessagesCollectionViewCell *) cell {
    [self.delegate performSelector:@selector(didTapOnImageForAttachment:atCell:) withObject:attachment withObject:cell];
}

-(void) didLongPressOnImageForAttachment:(File *) attachment atCell:(JSQRainbowMessagesCollectionViewCell *) cell {
    [self.delegate performSelector:@selector(didLongPressOnImageForAttachment:atCell:) withObject:attachment withObject:cell];
}

-(void) didFailedSendAttachment:(File *) attachment atCell:(JSQRainbowMessagesCollectionViewCell *) cell {
    [self.delegate performSelector:@selector(didFailedSendAttachment:atCell:) withObject:attachment withObject:cell];
}

-(void) didShareAttachment:(File *) attachment atCell:(JSQRainbowMessagesCollectionViewCell *) cell {
    [self.delegate performSelector:@selector(didShareAttachment:atCell:) withObject:attachment withObject:cell];
}

@end

@interface JSQRainbowMessagesCollectionViewCell ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *avatarContainerViewWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *avatarContainerViewHeightConstraint;
@property (nonatomic, weak) IBOutlet UILabel *fileName;
@property (nonatomic, weak) IBOutlet UILabel *fileSizeLabel;
- (void)jsq_updateConstraint:(NSLayoutConstraint *)constraint withConstant:(CGFloat)constant;
@property (nonatomic, strong) UIView *internalMediaView;
@property (nonatomic, strong) UIView *internalMaskView;
@property (nonatomic, strong) RainbowMessageView *internalAnsweredMessageView;
@end

@implementation JSQRainbowMessagesCollectionViewCell

-(void) awakeFromNib {
    [super awakeFromNib];
    [UITools applyCustomFontTo:_cellDateLabel];
    [UITools applyCustomFontTo:_fileName];
    [UITools applyCustomFontTo:_fileSizeLabel];
    _fileName.hidden = YES;
    _fileSizeLabel.hidden = YES;
    _progressView.clipsToBounds = YES;
    _progressView.layer.cornerRadius = 2.0;
    _progressView.hidden = YES;
    _shareButton.hidden = YES;
    _status.userInteractionEnabled = YES;
    _shareButton.tintColor = [UITools defaultTintColor];
    
    CGFloat borderWidth = 1.0f;
    
    _shareButton.frame = CGRectInset(self.frame, -borderWidth, -borderWidth);
    _shareButton.layer.borderColor = [UITools defaultTintColor].CGColor;
    _shareButton.layer.borderWidth = borderWidth;
    
    UIImage *image = [[UIImage imageNamed:@"menu_other"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_shareButton setImage:image forState:UIControlStateNormal];
    _shareButton.tintColor = [UITools defaultTintColor];
    _shareButton.imageEdgeInsets = UIEdgeInsetsMake(3, 3, 3, 3);
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shouldUpdateAttachment:) name:kFileSharingServiceDidUpdateFile object:nil];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnStatus:)];
    [_status addGestureRecognizer:tap];
    
}

-(void) addRepliedMessageSubView:(Message *)answeredMessage isOutgoing:(BOOL)isOutgoing withSenderLabel:(BOOL)withLabel {
    if(!self.messageBubbleContainerView)
        return;
    
    _internalAnsweredMessageView = [[[NSBundle mainBundle] loadNibNamed:@"MessageView" owner:self options:nil] objectAtIndex:0];
    _internalAnsweredMessageView.translatesAutoresizingMaskIntoConstraints = NO;
    _internalAnsweredMessageView.answeredMessage = answeredMessage;
    
    if(isOutgoing) {
        _internalAnsweredMessageView.textView.backgroundColor = [UITools colorFromHexa:0xFFFFFF60];
        _internalAnsweredMessageView.textView.textColor = [UIColor blackColor];
    } else {
        _internalAnsweredMessageView.textView.backgroundColor = [UITools colorFromHexa:0x00000040];
        _internalAnsweredMessageView.textView.textColor = [UIColor whiteColor];
    }
    
    [self.messageBubbleContainerView addSubview:_internalAnsweredMessageView];
    
    // Add constraints
    NSLayoutConstraint *leadingConstraint = [NSLayoutConstraint constraintWithItem:_internalAnsweredMessageView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.messageBubbleContainerView attribute:NSLayoutAttributeLeading multiplier:1.0 constant:(isOutgoing?10:15)];

    NSLayoutConstraint *trailingConstraint = [NSLayoutConstraint constraintWithItem:_internalAnsweredMessageView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.messageBubbleContainerView attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:(isOutgoing?-20:-10)];

    NSLayoutConstraint *topConstraint = [NSLayoutConstraint constraintWithItem:_internalAnsweredMessageView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.messageBubbleContainerView attribute:NSLayoutAttributeTop multiplier:1.0 constant:10+(withLabel?25:0)];

    NSLayoutConstraint *bottomConstraint = [NSLayoutConstraint constraintWithItem:_internalAnsweredMessageView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.messageBubbleContainerView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-23];

    [self.messageBubbleContainerView addConstraints:@[leadingConstraint, trailingConstraint, topConstraint, bottomConstraint]];
    
    [self jsq_updateConstraint:self.textViewTopVerticalSpaceConstraint withConstant: [self calculateTextviewHeight:answeredMessage textView:_internalAnsweredMessageView.textView].size.height + 15];
}

-(CGRect)calculateTextviewHeight:(Message *)message textView:(UITextView *)textview {
    CGRect quotedStringRect;
    
    if(message.bodyInMarkdown) {
        quotedStringRect = [[MarkdownParser.sharedInstance attributedStringFromMarkDown:message.bodyInMarkdown] boundingRectWithSize:CGSizeMake(self.messageBubbleContainerView.frame.size.width*0.8, 42) options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading | NSStringDrawingTruncatesLastVisibleLine) context:nil];
    } else {
        quotedStringRect = [textview.text boundingRectWithSize:CGSizeMake(self.messageBubbleContainerView.frame.size.width*0.8, 42) options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading | NSStringDrawingTruncatesLastVisibleLine) attributes:@{NSFontAttributeName: [UIFont fontWithName:[UITools defaultFontName] size:13]} context:nil];
    }
    
    return quotedStringRect;
}

- (id)initWithCoder:(NSCoder*)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if(self)
    {
         [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shouldUpdateAttachment:) name:kFileSharingServiceDidUpdateFile object:nil];
        
    }
    
    return self;
}
-(void) dealloc {
    _attachment = nil;
    _fileName = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kFileSharingServiceDidUpdateFile object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kFileSharingServiceDidRemoveFile object:nil];
}


- (void)setAvatarViewSize:(CGSize)avatarViewSize {
    [self jsq_updateConstraint:self.avatarContainerViewWidthConstraint withConstant:avatarViewSize.width];
    [self jsq_updateConstraint:self.avatarContainerViewHeightConstraint withConstant:avatarViewSize.height];
}

- (void)setTimestamp:(NSDate *)date {
    self.cellDateLabel.text = [UITools optimizedFormatForDate:date];
}

-(void) setTimestamp:(NSDate *)date isEdited:(BOOL)edited {
    self.cellDateLabel.text = edited ? [NSString stringWithFormat:@"%@ - %@", [UITools optimizedFormatForDate:date], NSLocalizedString(@"Edited", nil)] : [UITools optimizedFormatForDate:date];
}

-(void) prepareForReuse {
    [super prepareForReuse];
    _fileName.text = nil;
    _fileSizeLabel.text = nil;
    _fileName.hidden = YES;
    _fileSizeLabel.hidden = YES;
    _shareButton.hidden = YES;
    [_internalMediaView removeFromSuperview];
    [_internalMaskView removeFromSuperview];
    _internalMaskView = nil;
    [_internalAnsweredMessageView removeFromSuperview];
    _internalAnsweredMessageView = nil;
}

-(void)layoutSubviews {
    [super layoutSubviews];
}

-(void) shouldUpdateAttachment:(NSNotification *) notification {
    File *file = (File *) notification.object;
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([_attachment.url isEqual:file.url]) {
            [self setMediaView:file.mediaView];
            [self setAttachment:file];
        }
    });
}



-(void) setAttachment:(File *)attachment {
    _attachment = attachment;
    [_locationActivityIndicator startAnimating];
    _locationActivityIndicator.hidden = YES;    // remove && attachment.isDownloadAvailable because it cause error when try to download image not available the cell appears empty!
    if(attachment && (_attachment.isDownloadAvailable || _attachment.isLocationFile)){
        if (!_attachment.isOfflineAttachment && !_hasLocationValue) {
            _shareButton.hidden = NO;
        }

        // if the attachment type is image and have data or thumbnail data -> hide fileName and fileZiseLabel
        // if the attachment contains data but the type is not
        if (_attachment.hasThumbnailOnServer && attachment.thumbnailData) {
            _fileName.hidden = YES;
            _fileSizeLabel.hidden = YES;
        } else {
            _fileName.text = _attachment.fileName;
            _fileSizeLabel.text = [NSString formatFileSize:_attachment.size];
            _fileName.hidden = NO;
            _fileSizeLabel.hidden = NO;

            if (attachment.isLocationFile) {
                _fileSizeLabel.hidden = YES;
                _fileName.hidden = YES;
                _locationActivityIndicator.hidden = NO;
                [_locationActivityIndicator startAnimating];
            }
            
            
        }
        
    } else { // this means that the attachment parameter is nill
        _shareButton.hidden = YES;
        _fileName.hidden = YES;
        _fileSizeLabel.hidden = YES;
        _fileName.hidden = YES;
        [_internalMediaView removeFromSuperview];
        [_internalMaskView removeFromSuperview];
        _internalMediaView = nil;
        _internalMaskView = nil;
    }
    [self setNeedsUpdateConstraints];
}

- (void)didTapOnImage:(UIGestureRecognizer *)sender {
    [self.delegate performSelector:@selector(didTapOnImageForAttachment:atCell:) withObject:_attachment withObject:self];
}


- (void)didTapOnStatus:(UIGestureRecognizer *)sender {
    [self.delegate performSelector:@selector(didFailedSendAttachment:atCell:) withObject:_attachment withObject:self];
}

- (void)setMediaView:(UIView *)mediaView {
    [mediaView setTranslatesAutoresizingMaskIntoConstraints:NO];
    CGRect frame = self.messageBubbleContainerView.bounds;
    frame.size = self.attachment.mediaViewDisplaySize;
    frame.origin.y += self.textView.frame.size.height;
    mediaView.frame = frame;

    if(_internalMediaView){
        [_internalMediaView removeFromSuperview];
    }

    [self.messageBubbleContainerView addSubview:mediaView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnImage:)];
    [mediaView addGestureRecognizer:tap];
    mediaView.userInteractionEnabled = YES;

    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(didLongPressOnImage:)];
    longPress.minimumPressDuration = 1 ;
    longPress.numberOfTouchesRequired = 1 ;
    [longPress requireGestureRecognizerToFail:tap];
    [mediaView addGestureRecognizer:longPress];
    mediaView.userInteractionEnabled = YES;
    mediaView.tintColor = self.attachment.tintColor;

    _internalMediaView = mediaView;
}

- (void)addGifIcon:(BOOL) isRoom {
    if (_internalMaskView) {
        [_internalMaskView removeFromSuperview];
        _internalMaskView = nil;
    }
    
    UIImageView *maskImageView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"gif"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [maskImageView setTranslatesAutoresizingMaskIntoConstraints:YES];
    CGFloat size = 50;
    CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
    maskImageView.frame = CGRectMake((screenWidth*0.66 + 20 - size)/2, (screenWidth*0.66 + 20 - size)/2, size, size);
    [self.messageBubbleContainerView addSubview:maskImageView];
    
    _internalMaskView = maskImageView;
}

- (void)removeGifIcon {
    if (_internalMaskView) {
        [_internalMaskView removeFromSuperview];
        _internalMaskView = nil;
    }
}

-(UIView *) mediaView {
    return _internalMediaView;
}

-(UIView *) maskView {
    return _internalMaskView;
}

-(void) setProgressViewWithValue : (float) progressValue {
    self.progressView.hidden = NO;
    self.shareButton.hidden = YES;
    self.progressView.progress = progressValue;
    self.attachment.value = progressValue;
}

-(void) didLongPressOnImage:(UIGestureRecognizer *) gesture {
    [self.delegate performSelector:@selector(didLongPressOnImageForAttachment:atCell:) withObject:_attachment withObject:self];
}
- (IBAction)failedButtonAction:(id)sender {
    [self.delegate performSelector:@selector(didFailedSendAttachment:atCell:) withObject:_attachment withObject:self];
}

- (IBAction)shareButtonAction:(id)sender {
    [self.delegate performSelector:@selector(didShareAttachment:atCell:) withObject:_attachment withObject:self];
}
@end
