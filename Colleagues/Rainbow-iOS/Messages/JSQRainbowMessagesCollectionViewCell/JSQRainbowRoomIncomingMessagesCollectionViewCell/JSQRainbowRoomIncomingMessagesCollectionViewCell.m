/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "JSQRainbowRoomIncomingMessagesCollectionViewCell.h"

@interface JSQRainbowRoomIncomingMessagesCollectionViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *cellUserNameLabel;

@end

@implementation JSQRainbowRoomIncomingMessagesCollectionViewCell

+ (UINib *)nib {
    return [UINib nibWithNibName:NSStringFromClass([JSQRainbowRoomIncomingMessagesCollectionViewCell class])
                          bundle:[NSBundle bundleForClass:[JSQRainbowRoomIncomingMessagesCollectionViewCell class]]];
}

-(void) awakeFromNib {
    [super awakeFromNib];
    [UITools applyCustomFontTo:_cellUserNameLabel];
    self.cellDateLabel.textColor = [UIColor whiteColor];
}

- (void)setUserName:(NSString *)name {
    self.cellUserNameLabel.text = name;
}

-(void) setProgressViewWithValue : (float) progressValue {
    [super setProgressViewWithValue:progressValue];
}

@end
