/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */


#import "JSQGroupChatEventCell.h"
#import "UITools.h"

@interface JSQGroupChatEventCell()
@property (weak, nonatomic) IBOutlet UILabel *cellCompanyNameLabel;
@end

@implementation JSQGroupChatEventCell

#pragma mark - Class methods

+ (UINib *)nib {
    return [UINib nibWithNibName:NSStringFromClass([JSQGroupChatEventCell class])
                          bundle:[NSBundle bundleForClass:[JSQGroupChatEventCell class]]];
}

+ (NSString *)cellReuseIdentifier
{
    return NSStringFromClass([JSQGroupChatEventCell class]);
}

#pragma mark - Instance methods


-(void) awakeFromNib {
    [super awakeFromNib];
    [UITools applyCustomFontTo:_cellCompanyNameLabel];
}

- (void)setCompanyName:(NSString *)companyName {
    self.cellCompanyNameLabel.text = companyName;
}

- (void)setMessage:(NSString *)message {
    self.textView.text = message;
}

@end
