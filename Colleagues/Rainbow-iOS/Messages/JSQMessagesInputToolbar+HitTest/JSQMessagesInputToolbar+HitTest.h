/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>
#import "JSQMessagesInputToolbar.h"


/**
 When we add the attachment view to the input toolbar,
 We add it with a frame.y = -150 position.
 As it is out of the input toolbar frame, events are not received by the
 attachment view.
 Doing this, we force the propagation to subviews of events even if they are
 out of the view frame.
 */
@interface JSQMessagesInputToolbar (hitTest)
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event;
@end
