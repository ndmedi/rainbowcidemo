//
//  OtherNumberTableViewCell.m
//  Rainbow-iOS
//
//  Created by Alaa Bzour on 4/15/19.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import "OtherNumberTableViewCell.h"

@interface OtherNumberTableViewCell ()

@end

@implementation OtherNumberTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self setupTextFiled];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setupTextFiled {
    
    _textField.textColor = [UITools defaultTintColor];
    [UITools applyCustomFontToTextField:_textField];
    _textField.placeholder = NSLocalizedString(@"Other phone number", nil);
  
    // Add line
    UIView *lineView = [[UIView alloc] init];
    [lineView setBackgroundColor:[UITools defaultGrayColor]];
    [lineView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_textField addSubview:lineView];
    
    NSDictionary *metrics = @{@"width" : [NSNumber numberWithFloat:2.0]};
    NSDictionary *views = @{@"lineView" : lineView};
    [_textField addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[lineView]|" options: 0 metrics:metrics views:views]];
    _textField.font = [UIFont fontWithName:[UITools defaultFontName] size:16.0];
    [_textField addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[lineView(width)]|" options: 0 metrics:metrics views:views]];
}

#pragma mark - Textfield
- (IBAction)otherNumberTextFieldDidBeginEditing:(UITextField *)sender {
    _selectButton.selected = YES;
}

- (IBAction)otherNumberTextFieldDidEndEditing:(UITextField *)sender {
    NSLog(@"Did end editing %@", sender);
}

- (IBAction)otherNumberTextFieldDidChanged:(UITextField *)sender {
    
}

@end
