/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "JoinMeetingViewController.h"
#import "UITools.h"
#import <DZNEmptyDataSet/DZNEmptyDataSet.h>
#import <Rainbow/ServicesManager.h>
#import "JoinMeetingTableViewCell.h"
#import "OtherNumberTableViewCell.h"
#import "FilteredSortedSectionedArray.h"
#import <Rainbow/RainbowUserDefaults.h>
#import "DZNSegmentedControl.h"
#if (TARGET_OS_EMBEDDED || TARGET_OS_IPHONE)
#import <CoreText/CoreText.h>
#else
#import <AppKit/AppKit.h>
#endif
@interface PhoneNumber (readwrite)
@property (nonatomic, readwrite) NSString *number;
@end

#define kOtherNumberConferenceCallBackKey @"otherNumberConferenceCallBackSetting"
#define kSelectedConferenceCallBackNumberKey @"selectedConferenceCallBackNumber"

typedef NS_ENUM(NSInteger, SelectorType){
    SelectorTypeWillCall = 0,
    SelectorTypeCallMe
};

@interface JoinMeetingViewController () <DZNEmptyDataSetSource, DZNEmptyDataSetDelegate,DZNSegmentedControlDelegate, UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate,UIGestureRecognizerDelegate> {
    BOOL isShowMoreSelected;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *titleViewLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cancelButtonCenterConstraint;
@property (weak, nonatomic) IBOutlet UIButton *callButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *callButtonCenterConstraint;
@property (weak, nonatomic) IBOutlet DZNSegmentedControl *meetingFilter;
@property (weak, nonatomic)  UIButton *otherNumberRadioButton;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UITableView *phoneNumbersTableView;

@property (weak, nonatomic)  UITextField *otherPhoneNumberTextField;
@property (weak, nonatomic) IBOutlet UILabel *callLabel;

@property (nonatomic, strong) SectionNameComputationBlock sectionedByName;
@property (nonatomic, strong) NSPredicate *filterCallFromMobile;
@property (nonatomic, strong) NSPredicate *filterCallMeBack;
@property (nonatomic, strong) NSSortDescriptor *sortSectionAsc;

@property (nonatomic, strong) FilteredSortedSectionedArray<PhoneNumber*> *phoneNumbersDataSource;

@property (nonatomic, strong) PhoneNumber *selectedPhoneNumber;
@property (nonatomic, strong) NSIndexPath *selectedIndexPathWillCall;
@property (nonatomic, strong) NSIndexPath *selectedIndexPathCallMe;
@property (nonatomic, strong) PhoneNumber *defaultPhoneNumber;
@end

@implementation JoinMeetingViewController

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self){
        _phoneNumbersDataSource = [FilteredSortedSectionedArray new];
        
        _sectionedByName = ^NSString*(PhoneNumber *phoneNumber) {
            return @"";
        };
        _filterCallFromMobile = [NSPredicate predicateWithBlock:^BOOL(PhoneNumber *phoneNumber, NSDictionary<NSString *,id> * bindings) {
            if(phoneNumber.type == PhoneNumberTypeConference)
                return YES;
            return NO;
        }];
        _filterCallMeBack = [NSPredicate predicateWithBlock:^BOOL(PhoneNumber *phoneNumber, NSDictionary<NSString *,id> * bindings) {
            if(phoneNumber.type != PhoneNumberTypeConference)
                return YES;
            return NO;
        }];
        _sortSectionAsc = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES comparator:^NSComparisonResult(PhoneNumber* obj1, PhoneNumber* obj2) {
            // Make the default country phone number on top of list
            if([obj1 isEqual:_defaultPhoneNumber])
                return NSOrderedAscending;
            if([obj2 isEqual:_defaultPhoneNumber])
                return NSOrderedDescending;
            
            if (![obj1.label isEqualToString:@"Conference"]) {
               
                return [obj2.label compare:obj1.label options:NSCaseInsensitiveSearch];
            }
            // Return all others phone numbers ordered by contry name
            return [obj1.countryName compare:obj2.countryName options:NSCaseInsensitiveSearch];
        }];
        
        _phoneNumbersDataSource.sectionNameFromObjectComputationBlock = _sectionedByName;
        _phoneNumbersDataSource.globalFilteringPredicate = _filterCallFromMobile;
        _phoneNumbersDataSource.sectionSortDescriptor = _sortSectionAsc;
        _phoneNumbersDataSource.objectSortDescriptorForSection = @{@"__default__": @[_sortSectionAsc]};
    }
    return self;
}

-(void) viewDidLoad {
    [super viewDidLoad];
    
    isShowMoreSelected = NO;
  
    [UITools applyCustomFontTo:_titleViewLabel];
    
    NSMutableAttributedString *joinMeetingAtrributedString = [[NSMutableAttributedString alloc]initWithString:NSLocalizedString(@"Join the conference", nil)];
    NSDictionary *attributes = @{ NSForegroundColorAttributeName : [UITools defaultGrayColor],
                             NSFontAttributeName :[UIFont fontWithName:[UITools defaultFontName] size:13]
                             };
    NSAttributedString *attributesString = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"How do you want to join the meeting?",nil) attributes:attributes];
    [joinMeetingAtrributedString appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n\n"]];
    [joinMeetingAtrributedString appendAttributedString:attributesString];
    
    _titleViewLabel.attributedText = joinMeetingAtrributedString;
    
    [UITools applyCustomFontTo:_cancelButton.titleLabel];
    [_cancelButton setTitle:[NSLocalizedString(@"Cancel", nil) uppercaseString] forState:UIControlStateNormal];
    _cancelButton.tintColor = [UITools defaultTintColor];
    [_callButton setTitle:[NSLocalizedString(@"Call", nil) uppercaseString] forState:UIControlStateNormal];
    _callButton.tintColor = [UITools defaultTintColor];

    _phoneNumbersTableView.tableFooterView = [UIView new];
    _phoneNumbersTableView.emptyDataSetSource = self;
    _phoneNumbersTableView.emptyDataSetDelegate = self;
    
    [self adjustButtonsPositions];
   
//    NSString *savedOtherNumber = [[RainbowUserDefaults sharedInstance] objectForKey: kOtherNumberConferenceCallBackKey];
//    if(savedOtherNumber){
//        _otherPhoneNumberTextField.text = savedOtherNumber;
//        _otherNumberRadioButton.selected = YES;
//    }

    if([[RainbowUserDefaults sharedInstance] objectForKey: kSelectedConferenceCallBackNumberKey]){
        NSInteger selectedPhoneNumberIndex = [[[RainbowUserDefaults sharedInstance] objectForKey: kSelectedConferenceCallBackNumberKey] integerValue];
        if(selectedPhoneNumberIndex != NSNotFound && selectedPhoneNumberIndex != -1){
            _phoneNumbersDataSource.globalFilteringPredicate = _filterCallMeBack;
            NSString *key = [[_phoneNumbersDataSource sections] firstObject];
            if(key){
                NSArray * phoneNumbers = [_phoneNumbersDataSource objectsInSection:key];
                if(selectedPhoneNumberIndex < phoneNumbers.count)
                    _selectedIndexPathCallMe = [NSIndexPath indexPathForRow:selectedPhoneNumberIndex inSection:0];
            }
            _phoneNumbersDataSource.globalFilteringPredicate = _filterCallFromMobile;
        }
    }
    
  
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateMyContact:) name:kContactsManagerServiceDidUpdateMyContact object:nil];
    
    if ([ServicesManager sharedInstance].myUser.isUserDialInOnlyBundle || _room.conference.endpoint.isDialOutDisabled) {
         [_meetingFilter setHidden:YES];
    }
    else {
         [_meetingFilter setHidden:NO];
    }
    
    [_meetingFilter setItems:@[[NSLocalizedString(@"I will call in", nil) uppercaseString], [NSLocalizedString(@"Call me", nil) uppercaseString]]];
    [_meetingFilter setFrame:CGRectMake(0, 0, self.view.bounds.size.width - 20, self.view.bounds.size.height)];
    _meetingFilter.delegate = self;
    _meetingFilter.showsCount = NO;
    _meetingFilter.autoAdjustSelectionIndicatorWidth = NO;
    _meetingFilter.height = 60;
    _meetingFilter.selectionIndicatorHeight = 4.0f;
    [_meetingFilter setFont:[UIFont fontWithName:[UITools defaultFontName] size:15]];
    [_meetingFilter setHairlineColor:[UITools defaultTintColor]];
    [_meetingFilter setTintColor:[UITools defaultTintColor]];
    [_meetingFilter addTarget:self action:@selector(meetingFilterAction:) forControlEvents:UIControlEventValueChanged];
    
    _callLabel.text = NSLocalizedString(@"Call in to the meeting", nil);
    [_callLabel setFont:[UIFont fontWithName:[UITools defaultFontName] size:13.0]];
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(highlightLetter:)];
    tapRecognizer.delegate = self;
    [self.view addGestureRecognizer:tapRecognizer];
    
    [self.callButton setTitleColor:[UITools defaultTintColor] forState:UIControlStateNormal];
    [self.cancelButton setTitleColor:[UITools defaultTintColor] forState:UIControlStateNormal];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

- (void) viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];    
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidUpdateContact object:nil];
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
//     self.view.frame = CGRectMake(20,20,self.view.frame.size.width - 40, self.view.frame.size.height- 40 );
    [_phoneNumbersDataSource reloadData];
    [self reloadTableView];
}

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(BOOL)shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void) setPhoneNumbers {
    [_phoneNumbersDataSource removeAllObjects];
    
    NSMutableArray<PhoneNumber *> *phoneNumbers = [NSMutableArray arrayWithArray: [ServicesManager sharedInstance].conferencesManagerService.dialInNumbers];
    for(PhoneNumber *phoneNumber in [ServicesManager sharedInstance].conferencesManagerService.dialInNumbersShortList){
        if(![phoneNumbers containsObject:phoneNumber]){
            [phoneNumbers addObject:phoneNumber];
        }
    }
    
    // Add the default phone number from the user country
    [phoneNumbers enumerateObjectsUsingBlock:^(PhoneNumber * aPhoneNumber, NSUInteger idx, BOOL * stop) {
        // This default phone number is used as default selected country
        if([[ServicesManager sharedInstance].myUser.contact.countryCode isEqualToString:aPhoneNumber.countryCode]) {
            _defaultPhoneNumber = aPhoneNumber;
        }

        [_phoneNumbersDataSource addObject:aPhoneNumber];
    }];
    
    [[ServicesManager sharedInstance].myUser.contact.phoneNumbers enumerateObjectsUsingBlock:^(PhoneNumber * aPhoneNumber, NSUInteger idx, BOOL * stop) {
        [_phoneNumbersDataSource addObject:aPhoneNumber];
    }];

}

-(void) setRoom:(Room *)room {
    _room = room;

    [self setPhoneNumbers];
    
    // Fetch potentially missing informations about the contact
    [[ServicesManager sharedInstance].contactsManagerService fetchRemoteContactDetail:[ServicesManager sharedInstance].myUser.contact];
}

-(void) didUpdateMyContact:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateMyContact:notification];
        });
        return;
    }
    
    [self setPhoneNumbers];
    [_phoneNumbersDataSource reloadData];
    [self reloadTableView];
}

#pragma mark - Buttons actions
- (IBAction)didTapCancelButton:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)didTapCallButton:(UIButton *)sender {
    if(_meetingFilter.selectedSegmentIndex == SelectorTypeCallMe){
        [[RainbowUserDefaults sharedInstance] removeObjectForKey:kSelectedConferenceCallBackNumberKey];
        if(_selectedIndexPathCallMe)
            [[RainbowUserDefaults sharedInstance] setObject:[NSNumber numberWithInteger: _selectedIndexPathCallMe.row] forKey:kSelectedConferenceCallBackNumberKey];
        if(_otherNumberRadioButton.selected){
            [[RainbowUserDefaults sharedInstance] removeObjectForKey:kOtherNumberConferenceCallBackKey];
            [[RainbowUserDefaults sharedInstance] setObject:_otherPhoneNumberTextField.text forKey:kOtherNumberConferenceCallBackKey];
        }
    }
    if([_delegate respondsToSelector:@selector(didSelectPhoneNumber:callBack:)]){
        PhoneNumber *number = nil;
        if(_selectedPhoneNumber)
            number = _selectedPhoneNumber;
        else {
            number = [PhoneNumber new];
            number.number = _otherPhoneNumberTextField.text;
        }
        [_delegate didSelectPhoneNumber:number callBack:(_meetingFilter.selectedSegmentIndex == SelectorTypeWillCall) ? NO : YES];
    }
    [self didTapCancelButton:nil];
}

-(void) adjustViewHeight {
     CGRect screen = [[UIScreen mainScreen] bounds];
    void(^block)() = ^() {
        if (_meetingFilter.selectedSegmentIndex == SelectorTypeCallMe) {
            NSUInteger count = 0;
            NSArray *sectionsArray =  [_phoneNumbersDataSource sections];
            for (NSString *section in sectionsArray) {
                count += [_phoneNumbersDataSource objectsInSection:section].count;
            }
            
            CGFloat calculatedHeight = (count+1) * 44 + 300;
            CGFloat minHeight = (screen.size.height < calculatedHeight) ? screen.size.height : calculatedHeight;
            self.preferredContentSize = CGSizeMake(self.view.frame.size.width, minHeight);
        }
        else{
            if (isShowMoreSelected) {
                self.preferredContentSize = CGSizeMake(self.view.frame.size.width, screen.size.height);
            }
            else {
                CGFloat minHeight = (screen.size.height/2 < 380) ? 380 : screen.size.height/2;
                self.preferredContentSize = CGSizeMake(self.view.frame.size.width, minHeight);
            }
        }
        
        [self.view layoutIfNeeded];
    };
    
    [UIView animateWithDuration:0.2 delay:0 options: UIViewAnimationOptionCurveLinear animations:block completion:nil];
}

-(void) adjustButtonsPositions {
    CGRect screen = [[UIScreen mainScreen] bounds];
    CGFloat desiredCallButtonConstraintConstant = screen.size.width * 0.20;
    CGFloat desiredCancelButtonConstraintConstant = -(screen.size.width * 0.20);
    
    void(^block)() = ^() {
        if (_meetingFilter.selectedSegmentIndex == SelectorTypeCallMe) {
            if(_otherNumberRadioButton.isSelected && _otherPhoneNumberTextField.text.length == 0){
                _callButton.alpha = 0;
                _callButtonCenterConstraint.constant = 0;
                _cancelButtonCenterConstraint.constant = 0;
     
            } else {

                _callButton.alpha = 1.0f;
                _callButtonCenterConstraint.constant = desiredCallButtonConstraintConstant;
                _cancelButtonCenterConstraint.constant = desiredCancelButtonConstraintConstant;
            }
        }
        else{
            _callButton.alpha = 1.0f;
            _callButtonCenterConstraint.constant = desiredCallButtonConstraintConstant;
            _cancelButtonCenterConstraint.constant = desiredCancelButtonConstraintConstant;
        }
 
        [self.view layoutIfNeeded];
    };
    
    [UIView animateWithDuration:0.2 delay:0 options: UIViewAnimationOptionCurveLinear animations:block completion:nil];
}

#pragma mark - Radio button actions

- (void) didTapCallFromMyMobile {
    
    _selectedPhoneNumber = nil;
    _callLabel.text = NSLocalizedString(@"Call in to the meeting", nil);
    _phoneNumbersDataSource.globalFilteringPredicate = _filterCallFromMobile;
    [_phoneNumbersDataSource reloadData];
    [self reloadTableView];
    [self adjustViewHeight];
    [self adjustButtonsPositions];
    [_otherPhoneNumberTextField resignFirstResponder];
    [self updateCallButtonTitle:NSLocalizedString(@"Call", nil)];
}

- (void)didTapCallMeBack {
    _selectedPhoneNumber = nil;
    _callLabel.text = NSLocalizedString(@"Let Rainbow call you on the number of your choice", nil);
    _phoneNumbersDataSource.globalFilteringPredicate = _filterCallMeBack;
    [_phoneNumbersDataSource reloadData];
    [_phoneNumbersTableView reloadData];
    
    
    if (_otherPhoneNumberTextField.text.length == 0) {
        if (_otherNumberRadioButton.selected) {
            _otherNumberRadioButton.selected = NO;
        }
        if(!_selectedIndexPathCallMe && [ServicesManager sharedInstance].myUser.contact.phoneNumbers.count > 0){
            _selectedIndexPathCallMe = [NSIndexPath indexPathForRow:0 inSection:0];
        }
        
        if(_selectedIndexPathCallMe){
            [_phoneNumbersTableView selectRowAtIndexPath:_selectedIndexPathCallMe animated:YES scrollPosition:UITableViewScrollPositionMiddle];
            NSString *key = [[_phoneNumbersDataSource sections] objectAtIndex:_selectedIndexPathCallMe.section];
            PhoneNumber *phoneNumber = [[_phoneNumbersDataSource objectsInSection:key] objectAtIndex:_selectedIndexPathCallMe.row];
            _selectedPhoneNumber = phoneNumber;
        }
    }
    // check if the user has no phone number set in his profile
    if([ServicesManager sharedInstance].myUser.contact.phoneNumbers.count == 0){
        _otherNumberRadioButton.selected = YES;
        [_otherPhoneNumberTextField becomeFirstResponder];
        
    }
    
    [self adjustViewHeight];
    [self adjustButtonsPositions];
    [self updateCallButtonTitle:NSLocalizedString(@"Call me", nil)];
}

-(void) updateCallButtonTitle:(NSString *) title {
    [UIView setAnimationsEnabled:NO];
    [_callButton setTitle:[title uppercaseString] forState:UIControlStateNormal];
    [_callButton layoutIfNeeded];
    [UIView setAnimationsEnabled:YES];
}

- (IBAction)didTapOtherNumberRadioButton:(UIButton *)sender {
    sender.selected = YES;
    [_otherPhoneNumberTextField becomeFirstResponder];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[_phoneNumbersDataSource sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (isShowMoreSelected || _meetingFilter.selectedSegmentIndex == SelectorTypeCallMe) {
        NSString *key = [[_phoneNumbersDataSource sections] objectAtIndex:section];
        return [[_phoneNumbersDataSource objectsInSection:key] count] + 1;
    }
    return 2;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *sectionName = [[_phoneNumbersDataSource sections] objectAtIndex:section];
    if([_phoneNumbersDataSource objectsInSection:sectionName].count > 0){
        return NSLocalizedString(sectionName, nil);
    }
    return @"";
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *)view;
    tableViewHeaderFooterView.textLabel.font = [UIFont fontWithName:[UITools boldFontName] size:18.0f];
    tableViewHeaderFooterView.textLabel.textColor = [UITools defaultTintColor];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 1 && _meetingFilter.selectedSegmentIndex == SelectorTypeWillCall) {
        UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"cell"];
        if (!cell) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        }
        
        for (UIView *subUIView in cell.subviews) {
            [subUIView removeFromSuperview];
        }
        
        UILabel *textLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, cell.bounds.size.width - 44, cell.bounds.size.height)];
        
        if (!isShowMoreSelected) {
            textLabel.text = NSLocalizedString(@"Show all dial-in numbers", nil);
        }
        else {
            textLabel.text = NSLocalizedString(@"Hide all dial-in numbers",nil);
            
        }
        textLabel.textColor = [UITools defaultTintColor];
        textLabel.font = [UIFont fontWithName:[UITools defaultFontName] size:13.0];
        textLabel.textAlignment = NSTextAlignmentRight;
        [cell addSubview:textLabel];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(textLabel.bounds.size.width + 8, 15, 15, 15)];
        NSString *imageName = (isShowMoreSelected) ? @"dropup-menu" : @"dropdown-menu";
        imageView.image = [UIImage imageNamed:imageName];
        [cell addSubview:imageView];
        
        return cell;
    }
    
    NSString *key = [[_phoneNumbersDataSource sections] objectAtIndex:indexPath.section];
    if (indexPath.row == [_phoneNumbersDataSource objectsInSection:key].count && _meetingFilter.selectedSegmentIndex == SelectorTypeCallMe) {
        OtherNumberTableViewCell *cell = (OtherNumberTableViewCell *)[tableView dequeueReusableCellWithIdentifier:joinMeetingOtherPhoneNumberCellID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        _otherPhoneNumberTextField = cell.textField;
        _otherNumberRadioButton = cell.selectButton;
        _otherPhoneNumberTextField.delegate = self;
        return cell;
    }
    
    JoinMeetingTableViewCell *cell = (JoinMeetingTableViewCell *)[tableView dequeueReusableCellWithIdentifier:joinMeetingPhoneNumberCellID];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    PhoneNumber *phoneNumber;
    if (indexPath.row == 0 || _meetingFilter.selectedSegmentIndex == SelectorTypeCallMe) {
        phoneNumber = [[_phoneNumbersDataSource objectsInSection:key] objectAtIndex:indexPath.row];
    }
    else if (_meetingFilter.selectedSegmentIndex == SelectorTypeWillCall) {
         phoneNumber = [[_phoneNumbersDataSource objectsInSection:key] objectAtIndex:indexPath.row - 1];
    }
    
    cell.phoneNumber = phoneNumber;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *key = [[_phoneNumbersDataSource sections] objectAtIndex:indexPath.section];
    if (indexPath.row == [_phoneNumbersDataSource objectsInSection:key].count && _meetingFilter.selectedSegmentIndex == SelectorTypeCallMe) {
        if(_selectedIndexPathCallMe)
            [_phoneNumbersTableView deselectRowAtIndexPath:_selectedIndexPathCallMe animated:YES];
        _selectedIndexPathCallMe = nil;
        _selectedPhoneNumber = nil;
        [_otherPhoneNumberTextField becomeFirstResponder];
        _otherNumberRadioButton.selected = YES;
        _otherPhoneNumberTextField.text = @"";
        return;
    }
    
    PhoneNumber *phoneNumber;
    if(_meetingFilter.selectedSegmentIndex == SelectorTypeWillCall){
        if (indexPath.row == 1) {
            isShowMoreSelected = !isShowMoreSelected;
            [self adjustViewHeight];
            [self reloadTableView];
        }
        else{
            _selectedIndexPathWillCall = indexPath;
            if (indexPath.row == 0) {
                phoneNumber = [[_phoneNumbersDataSource objectsInSection:key] objectAtIndex:indexPath.row];
            }
            else {
                phoneNumber = [[_phoneNumbersDataSource objectsInSection:key] objectAtIndex:indexPath.row - 1];
            }
            _selectedPhoneNumber = phoneNumber;
        }
        return;
    }

    phoneNumber = [[_phoneNumbersDataSource objectsInSection:key] objectAtIndex:indexPath.row];
    _selectedPhoneNumber = phoneNumber;
    _selectedIndexPathCallMe = indexPath;
    [_otherPhoneNumberTextField resignFirstResponder];
    _otherNumberRadioButton.selected = NO;
    _otherPhoneNumberTextField.text = @"";
     [self adjustButtonsPositions];
}

- (void) reloadTableView {
    [_phoneNumbersTableView reloadData];
    if (_phoneNumbersTableView.numberOfSections) {
        if(!_selectedIndexPathWillCall){
            _selectedIndexPathWillCall = [NSIndexPath indexPathForRow:0 inSection:0];
        }
        
        if (isShowMoreSelected || _selectedIndexPathWillCall.row == 0) { // to avoid select row out of array range
            [_phoneNumbersTableView selectRowAtIndexPath:_selectedIndexPathWillCall animated:YES scrollPosition:UITableViewScrollPositionMiddle];
        }
        
        if (_selectedPhoneNumber == nil) {
            NSString *key = [[_phoneNumbersDataSource sections] objectAtIndex:_selectedIndexPathWillCall.section];
            PhoneNumber *phoneNumber;
            if (_selectedIndexPathWillCall.row == 0) {
                phoneNumber = [[_phoneNumbersDataSource objectsInSection:key] objectAtIndex:_selectedIndexPathWillCall.row];
            }
            else {
                phoneNumber = [[_phoneNumbersDataSource objectsInSection:key] objectAtIndex:_selectedIndexPathWillCall.row - 1];
            }
            _selectedPhoneNumber = phoneNumber;
        }
    }
}

#pragma mark - DZNEmptyDataSet

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    return nil;
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView {
    return nil;
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView {
    return [UITools defaultBackgroundColor];
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView {
    return 20.0f;
}

- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView {
    return NO;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView {
    return NO;
}

#pragma mark DZNSegmentedControl

- (IBAction)meetingFilterAction:(UISegmentedControl *)sender {
    if(sender.selectedSegmentIndex == SelectorTypeWillCall)
        [self didTapCallFromMyMobile];
    if(sender.selectedSegmentIndex == SelectorTypeCallMe)
        [self didTapCallMeBack];
}

#pragma mark textField Deleagtes
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if(_selectedIndexPathCallMe)
        [_phoneNumbersTableView deselectRowAtIndexPath:_selectedIndexPathCallMe animated:YES];
    _selectedIndexPathCallMe = nil;
    _selectedPhoneNumber = nil;
    [self adjustButtonsPositions];

}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    [self adjustButtonsPositions];

}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (string.length == 0 && range.location == 0) {
        _otherPhoneNumberTextField.text = @"";
    }
    [self adjustButtonsPositions];
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    return YES;
}

- (void)highlightLetter:(UITapGestureRecognizer*)sender {
    [_otherPhoneNumberTextField resignFirstResponder];
}

#pragma mark UIGestureRecognizerDelegate methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:self.tableView]) {
        
        // Don't let selections of auto-complete entries fire the
        // gesture recognizer
        return NO;
    }
    
    return YES;
}
@end
