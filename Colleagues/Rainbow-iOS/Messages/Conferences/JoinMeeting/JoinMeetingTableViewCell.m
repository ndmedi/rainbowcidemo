/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "JoinMeetingTableViewCell.h"
#import "UITools.h"

@interface JoinMeetingTableViewCell ()
@property (weak, nonatomic) IBOutlet UIButton *phoneNumberButton;

@end
@implementation JoinMeetingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [UITools applyCustomFontTo:_phoneNumberButton.titleLabel];
    _phoneNumberButton.tintColor = [UITools defaultTintColor];
}

-(void) setPhoneNumber:(PhoneNumber *)phoneNumber {
    _phoneNumber = phoneNumber;
    NSString *title = [NSString stringWithFormat:@"%@\n%@",(phoneNumber.type == PhoneNumberTypeConference)?(_phoneNumber.countryName)?_phoneNumber.countryName:@"":NSLocalizedString([self stringForPhoneNumberType:phoneNumber.type withDeviceType:phoneNumber.deviceType],nil), _phoneNumber.number];
    [_phoneNumberButton setTitle:title forState:UIControlStateNormal];
    [_phoneNumberButton setImage:[UIImage imageNamed:@"RadioButton"] forState:UIControlStateNormal];
    [_phoneNumberButton setImage:[UIImage imageNamed:@"RadioButton_selected"] forState:UIControlStateHighlighted];
    [_phoneNumberButton setImage:[UIImage imageNamed:@"RadioButton_selected"] forState:UIControlStateSelected];
    [_phoneNumberButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
    _phoneNumberButton.selected = NO;
    _phoneNumberButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _phoneNumberButton.titleLabel.numberOfLines = 2;
    _phoneNumberButton.titleLabel.font = [UIFont fontWithName:[UITools defaultFontName] size:16.0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    _phoneNumberButton.selected = selected;
}

- (NSString *) stringForPhoneNumberType:(PhoneNumberType) type withDeviceType:(PhoneNumberDeviceType) deviceType {
    switch (type) {
        case PhoneNumberTypeHome: {
            if (deviceType == PhoneNumberDeviceTypeMobile) {
                return @"Personal Mobile" ;
            }
            return @"Personal";
            break;
        }
        case PhoneNumberTypeWork:
        default:{
            if (deviceType == PhoneNumberDeviceTypeMobile) {
                return @"Professional Mobile";
            }
            return @"Professional";
            break;
        }
        case PhoneNumberTypeOther:{
            return @"Other";
            break;
        }
        case PhoneNumberTypeConference:{
            return @"Conference";
            break;
        }
    }
}
@end
