//
//  OtherNumberTableViewCell.h
//  Rainbow-iOS
//
//  Created by Alaa Bzour on 4/15/19.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Rainbow/PhoneNumber.h>
#import "UITools.h"

#define joinMeetingOtherPhoneNumberCellID @"OtherPhoneNumberCellID"

@interface OtherNumberTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIButton *selectButton;

@end
