/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ConferenceActiveTalkersViewController.h"
#import <Rainbow/ServicesManager.h>
#import <DZNEmptyDataSet/DZNEmptyDataSet.h>
#import "UITools.h"

#define kActiveTalkersShowHideTransitionDuration 0.5
#define kActiveTalkersPanelAlpha 1.0
#define kTalkerTableViewCell @"TalkerTableViewCell"

@interface ConferenceActiveTalkersViewController ()
@property (nonatomic, weak) IBOutlet UITableView *talkersTableView;

@property (nonatomic, strong) ConferencesManagerService *conferencesManagerService;
@property (nonatomic, strong) NSMutableArray<ConferenceParticipant *> *talkers;
@property (nonatomic, strong) UIView *seperatorView;
@end

@implementation ConferenceActiveTalkersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _conferencesManagerService = [ServicesManager sharedInstance].conferencesManagerService;
    _talkers = [NSMutableArray array];
    _seperatorView = nil;
    self.talkersTableView.backgroundColor = [UITools defaultTintColor];
    [self.talkersTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kTalkerTableViewCell];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateConference:) name:kConferencesManagerDidUpdateConference object:nil];
}

-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [_seperatorView removeFromSuperview];
}

- (void)dealloc {
    _talkers = nil;
    _conferencesManagerService = nil;
    _seperatorView = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kConferencesManagerDidUpdateConference object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void) didUpdateConference:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateConference:notification];
        });
        return;
    }
    
    NSDictionary *userInfo = (NSDictionary *)notification.object;
    NSArray<NSString *> *changedKeys = [userInfo objectForKey:kConferenceChangedAttributesKey];
    Conference *aConference = (Conference *)[userInfo objectForKey:kConferenceKey];
    
    if([aConference isEqual:_conference]){
        if([changedKeys containsObject:@"participants"] ){
            [_talkers removeAllObjects];
            [_talkers addObjectsFromArray:[_conference.participants filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"isTalking == 1"]]];
            [_talkersTableView reloadData];
            [self didActiveTalkersChanged];
        }
    }
}

#pragma mark - ActiveTalkersViewControllerDelegate protocol

-(void)didActiveTalkersChanged {
    NSInteger activeTalkersCount = _talkers.count;
    CGFloat offset = _conference.isMyConference ? 44 : 0;
    CGRect parentFrame = self.view.superview.frame;
    if(activeTalkersCount){
        self.view.frame = CGRectMake(0, parentFrame.size.height - (kActiveSpeakerCellHeight * activeTalkersCount) - offset, parentFrame.size.width, kActiveSpeakerCellHeight * activeTalkersCount);
    } else {
        self.view.frame = CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, kActiveSpeakerCellHeight * activeTalkersCount);
    }
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(_talkers.count > 0 && self.view.alpha == 0.0){
        [UIView animateWithDuration:kActiveTalkersShowHideTransitionDuration animations:^(void) {
            self.view.alpha = kActiveTalkersPanelAlpha;
        }];
    } else if(_talkers.count == 0 && self.view.alpha > 0){
        [UIView animateWithDuration:kActiveTalkersShowHideTransitionDuration animations:^(void) {
            self.view.alpha = 0;
        }];
    }
    
    return _talkers.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [self.talkersTableView dequeueReusableCellWithIdentifier:kTalkerTableViewCell forIndexPath:indexPath];
    if(!cell){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:kTalkerTableViewCell];
    }
    cell.backgroundColor = [UITools defaultTintColor];
    cell.textLabel.textColor = [UIColor whiteColor];
    
    ConferenceParticipant *participant = _talkers[indexPath.row];
    NSString *participantName = participant.contact?participant.contact.displayName:participant.phoneNumber;
    cell.textLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@ is talking", nil), participantName];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    [UITools applyCustomFontTo:cell.textLabel];
    
    return cell;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0 && indexPath.row == [tableView numberOfRowsInSection:0] - 1){
        if(self.seperatorView){
            [self.seperatorView removeFromSuperview];
        } else {
            CGRect sepFrame = CGRectMake(0, cell.frame.size.height-1, cell.frame.size.width , 1);
            self.seperatorView =[[UIView alloc] initWithFrame:sepFrame];
            self.seperatorView.backgroundColor = [UITools colorFromHexa:0xD5D5D5FF];
        }
        [cell.contentView addSubview:self.seperatorView];
        self.seperatorView.translatesAutoresizingMaskIntoConstraints = NO;
        [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.seperatorView
                                                                     attribute:NSLayoutAttributeLeft
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:cell.contentView
                                                                     attribute:NSLayoutAttributeLeft
                                                                    multiplier:1.0
                                                                      constant:0.0]];
        [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.seperatorView
                                                                       attribute:NSLayoutAttributeRight
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:cell.contentView
                                                                       attribute:NSLayoutAttributeRight
                                                                      multiplier:1.0
                                                                        constant:0.0]];
        [cell.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.seperatorView
                                                                       attribute:NSLayoutAttributeBottom
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:cell.contentView
                                                                       attribute:NSLayoutAttributeBottom
                                                                      multiplier:1.0
                                                                        constant:-1.0]];
        [self.seperatorView.heightAnchor constraintEqualToConstant: 1.0].active = YES;

    }
}

@end
