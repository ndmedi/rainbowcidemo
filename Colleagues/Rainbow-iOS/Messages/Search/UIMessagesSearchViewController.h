/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>
#import "JSQMessages.h"
#import <Rainbow/Contact.h>
#import <Rainbow/ContactsManagerService.h>
#import <Rainbow/ConversationsManagerService.h>
#import <Rainbow/Conversation.h>
#import "GroupedMessages.h"
#import "CustomNavigationController.h"

@interface UIMessagesSearchViewController : JSQMessagesViewController
@property (nonatomic, strong) Conversation *conversation;
@property (nonatomic, strong) NSMutableArray <GroupedMessages*> *messages;
@property (nonatomic, weak) id fromView;
@property (nonatomic, weak) CustomNavigationController *parentNavigationController;
@property (nonatomic, strong) NSString *searchMessageText;
@end
