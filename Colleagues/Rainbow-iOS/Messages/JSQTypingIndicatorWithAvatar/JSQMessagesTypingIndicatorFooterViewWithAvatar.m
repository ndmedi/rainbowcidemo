//
//  Created by Jesse Squires
//  http://www.jessesquires.com
//
//
//  Documentation
//  http://cocoadocs.org/docsets/JSQMessagesViewController
//
//
//  GitHub
//  https://github.com/jessesquires/JSQMessagesViewController
//
//
//  License
//  Copyright (c) 2014 Jesse Squires
//  Released under an MIT license: http://opensource.org/licenses/MIT
//

#import "JSQMessagesTypingIndicatorFooterViewWithAvatar.h"
#import "JSQMessagesBubbleImageFactory.h"
#import "UIImage+JSQMessages.h"


@interface JSQMessagesTypingIndicatorFooterViewWithAvatar ()
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *avatarImageViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *avatarImageViewWidthConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *bubbleImageView;
@property (weak, nonatomic) IBOutlet UIImageView *typingIndicatorImageView;
@end


@implementation JSQMessagesTypingIndicatorFooterViewWithAvatar

#pragma mark - Class methods

+ (UINib *)nib
{
    return [UINib nibWithNibName:NSStringFromClass([JSQMessagesTypingIndicatorFooterViewWithAvatar class])
                          bundle:[NSBundle bundleForClass:[JSQMessagesTypingIndicatorFooterViewWithAvatar class]]];
}

+ (NSString *)footerReuseIdentifier
{
    return NSStringFromClass([JSQMessagesTypingIndicatorFooterViewWithAvatar class]);
}

#pragma mark - Initialization

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.backgroundColor = [UIColor clearColor];
    self.userInteractionEnabled = NO;
    self.typingIndicatorImageView.contentMode = UIViewContentModeScaleAspectFit;
}

#pragma mark - Reusable view

- (void)setBackgroundColor:(UIColor *)backgroundColor
{
    [super setBackgroundColor:backgroundColor];
    self.bubbleImageView.backgroundColor = backgroundColor;
}

#pragma mark - Typing indicator

#define kMaxTypingAvatar 3
#define kMarginBetweenTypingAvatars 5

- (void)configureWithAvatar:(NSArray<UIImage *> *) avatars
              ellipsisColor:(UIColor *)ellipsisColor
                messageBubbleColor:(UIColor *)messageBubbleColor
                 forCollectionView:(UICollectionView *)collectionView
{
    NSParameterAssert(ellipsisColor != nil);
    NSParameterAssert(messageBubbleColor != nil);
    NSParameterAssert(collectionView != nil);
    
    // We concat the avatars (up to "kMaxTypingAvatar" avatars, not more to prevent from taking all screen size)
    CGSize size = CGSizeMake(0, 0);
    
    for (int i = 0; i < MIN([avatars count], kMaxTypingAvatar); i++) {
        UIImage *avatar = [avatars objectAtIndex:i];
        // add the avatar width to global width
        size.width += avatar.size.width;
        // Use the heightest avatar as height value
        if (avatar.size.height > size.height) {
            size.height = avatar.size.height;
        }
    }
    
    // Add margin between avatars
    size.width += (MIN([avatars count], kMaxTypingAvatar) - 1) * kMarginBetweenTypingAvatars;
    
    // Create image of computed size
    UIGraphicsBeginImageContextWithOptions(size, NO, [UIScreen mainScreen].scale);
    
    // Draw the avatars
    for (int i = 0; i < MIN([avatars count], kMaxTypingAvatar); i++) {
        UIImage *avatar = [avatars objectAtIndex:i];
        CGRect frame = CGRectMake((avatar.size.width * i) + (kMarginBetweenTypingAvatars * i),
                                  (size.height - avatar.size.height) / 2.0f,
                                  avatar.size.width,
                                  avatar.size.height);
        [avatar drawInRect:frame];
    }
    
    self.avatarImageView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.avatarImageViewWidthConstraint.constant = size.width;
    self.avatarImageViewHeightConstraint.constant = size.height;
    
    JSQMessagesBubbleImageFactory *bubbleImageFactory = [[JSQMessagesBubbleImageFactory alloc] init];
    self.bubbleImageView.image = [bubbleImageFactory incomingMessagesBubbleImageWithColor:messageBubbleColor].messageBubbleImage;
    
    [self setNeedsUpdateConstraints];
    
    self.typingIndicatorImageView.image = [[UIImage jsq_defaultTypingIndicatorImage] jsq_imageMaskedWithColor:ellipsisColor];
}

@end
