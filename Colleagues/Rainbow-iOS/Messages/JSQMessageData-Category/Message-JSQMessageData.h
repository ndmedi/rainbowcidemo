/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>
#import <Rainbow/Message.h>
#import "JSQMessageData.h"

@interface Message (JSQMessageData) <JSQMessageData>

@property (nonatomic, readonly) NSString *senderId;
@property (nonatomic, readonly) NSString *senderDisplayName;
@property (nonatomic, readonly) BOOL isMediaMessage;
@property (nonatomic, readonly) NSUInteger messageHash;
@property (nonatomic, readonly) NSString *text;

@end
