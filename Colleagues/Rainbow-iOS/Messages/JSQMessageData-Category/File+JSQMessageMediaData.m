/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "File+JSQMessageMediaData.h"
#import "JSQMessagesMediaViewBubbleImageMasker.h"
#import "File+DefaultImage.h"
#import "JSQMessagesMediaPlaceholderView.h"

@implementation File (JSQMessageMediaData)

-(BOOL) isOutgoing {
    return [ServicesManager sharedInstance].myUser.contact == self.owner;
}

- (UIView *)mediaPlaceholderView {
    CGSize size = [self mediaViewDisplaySize];
    UIView *view = [JSQMessagesMediaPlaceholderView viewWithActivityIndicator];
    view.frame = CGRectMake(0.0f, 0.0f, size.width, size.height);
    return view;
}

- (NSUInteger)mediaHash {
    return self.hash;
}

-(UIView *) mediaView {
    CGSize size = [self mediaViewDisplaySize];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:self.thumbnail];
    imageView.frame = CGRectMake(0.0f, 0.0f, size.width, size.height);
    
    if (self.type == FileTypePDF) {
        if(self.thumbnailData) {
            imageView = [[UIImageView alloc] initWithImage:[self thumbnailWithWhiteBG]];
        }
        imageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    else{
        imageView.contentMode = ((self.thumbnailData || self.data)&&(self.hasThumbnailOnServer))
        ?UIViewContentModeScaleAspectFill:UIViewContentModeScaleAspectFit;
    }
    
    imageView.clipsToBounds = YES;
    [imageView.heightAnchor constraintEqualToConstant:size.height].active = YES;
    imageView.layer.cornerRadius = 12.0f;
    imageView.layer.masksToBounds = YES;
    return imageView;
}

- (CGSize)mediaViewDisplaySize {
    if (self.hasThumbnailOnServer && self.thumbnailData) {
        return CGSizeMake(([[UIScreen mainScreen] bounds].size.width)* 0.66 + 20, ([[UIScreen mainScreen] bounds].size.width)* 0.66 );
    }
    else
        return CGSizeMake(([[UIScreen mainScreen] bounds].size.width)* 0.66 + 20, 32);
}

- (UIImage *)thumbnailWithWhiteBG {
    CGSize size = CGSizeMake(self.thumbnail.size.width, self.thumbnail.size.height);
    UIGraphicsBeginImageContext(size);
    CGRect imageRect = CGRectMake(0, 0, size.width, size.height);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
    CGContextFillRect(context, imageRect);
    imageRect = CGRectInset(imageRect, 0, 0);
    [self.thumbnail drawInRect:imageRect blendMode:kCGBlendModeNormal alpha:1.0];
    UIImage *newImage =  UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
