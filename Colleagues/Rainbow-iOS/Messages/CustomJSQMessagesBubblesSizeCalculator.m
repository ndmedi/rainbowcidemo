/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 *
 * Created by Vladimir Vyskocil on 01/12/16.
 */

#import "CustomJSQMessagesBubblesSizeCalculator.h"
#import "JSQMessagesCollectionView.h"
#import "JSQMessagesCollectionViewDataSource.h"
#import "JSQMessagesCollectionViewFlowLayout.h"
#import "JSQMessageData.h"
#import "GroupedMessages.h"
#import "UITools.h"
#import "UIImage+JSQMessages.h"
#import "JSQMessagesTimestampFormatter.h"
#import "File+JSQMessageMediaData.h"
#import "MarkdownParser.h"

@interface JSQMessagesBubblesSizeCalculator ()
@property (strong, nonatomic, readonly) NSCache *cache;
@end

@implementation CustomJSQMessagesBubblesSizeCalculator

- (id)init
{
    self = [super init];
    if (self != nil) {
        _isMUC = NO;
    }
    
    return self;
}

- (CGSize)messageBubbleSizeForMessageData:(id<JSQMessageData>)messageData
                              atIndexPath:(NSIndexPath *)indexPath
                               withLayout:(JSQMessagesCollectionViewFlowLayout *)layout
{

    CGSize size = [super messageBubbleSizeForMessageData:messageData atIndexPath:indexPath withLayout:layout];
    
    if([messageData isKindOfClass:[GroupedMessages class]]){
        GroupedMessages *groupedMessages = (GroupedMessages *)messageData;
        NSAttributedString *attrString = groupedMessages.attributedString;
        // Check if the message is a formatted message from markdown
        if (attrString){
            CGRect stringRect = [attrString boundingRectWithSize:CGSizeMake(size.width, CGFLOAT_MAX) options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading) context:nil];
            CGFloat verticalContainerInsets = 0.0;
            CGFloat additionalInset = 32.0;
            CGFloat verticalFrameInsets = layout.messageBubbleTextViewFrameInsets.top + layout.messageBubbleTextViewFrameInsets.bottom;
            CGFloat verticalInsets = verticalContainerInsets + verticalFrameInsets + additionalInset;
            size = CGSizeMake(size.width, stringRect.size.height + verticalInsets);
        }
        
        size.height = size.height + 26;
        // Add height for incoming messages to take account of the sender name label when in a MUC
        if (self.isMUC && ![[messageData senderId] isEqualToString:[layout.collectionView.dataSource senderId]]) {
            size.height += 25;
        }
        if(size.width < 200){
            size.width = 200;
        }
        
        if(groupedMessages.lastMessage.replacedDate != nil && groupedMessages.lastMessage.body == nil && groupedMessages.lastMessage.bodyInMarkdown == nil) {
            size.width = ([[UIScreen mainScreen] bounds].size.width)* 0.66 + 20;
        }
         NSString *msg = groupedMessages.lastMessage.body;
        // if the message has a thumbnail image account of its height
        if((groupedMessages.lastMessage.attachment && groupedMessages.lastMessage.attachment.isDownloadAvailable)){
            if((groupedMessages.lastMessage.attachment.thumbnailData &&
               (groupedMessages.lastMessage.attachment.hasThumbnailOnServer)) ){
               
                size.width = groupedMessages.lastMessage.attachment.mediaViewDisplaySize.width;
                size.height += groupedMessages.lastMessage.attachment.mediaViewDisplaySize.height +20;
    
                // check if image without text
                if (msg == nil) {
                    size.height -= 29;
                }
                else if (([msg rangeOfString:@"IMG_"].location != NSNotFound && [msg rangeOfString:@"."].location != NSNotFound) || !msg.length || [msg hasSuffix:@".jpg"] || [msg hasSuffix:@".png"]) {
                    size.height -= 29;
                }
                else{
                     size.height -= 4;
                }
            } else {
                // Icon 32 + filename label 30 + filesize label 30 + imageview top constraint 10
                size.height += 82;
                size.width += 60;
            }
            
            // For the file name
            if(_isMUC)
                size.height += 10;
        } else if (groupedMessages.lastMessage.imageUrl) {
            size.width = ([[UIScreen mainScreen] bounds].size.width)* 0.66 + 20;
            size.height = size.width + 17; // Date and checked mark
            
            // Add height for incoming messages to take account of the sender name label when in a MUC
            if (self.isMUC && ![[messageData senderId] isEqualToString:[layout.collectionView.dataSource senderId]])
                size.height += 27;
        } else {
            if ([msg length] > 0 && groupedMessages.lastMessage.attachment) {
                if (([msg rangeOfString:@"IMG_"].location != NSNotFound && [msg rangeOfString:@"."].location != NSNotFound) || !msg.length || [msg hasSuffix:@".jpg"] || [msg hasSuffix:@".png"]) {
                    size.height += 14;
                }
                else{
                    size.height += 44;
                }
            }
            else {
                size.height += 14;
                if (groupedMessages.lastMessage.location) {
                    size.height += 82;
                    size.width += 60;
                }
            }

        }
        
        if( size.width > ([[UIScreen mainScreen] bounds].size.width)* 0.66 + 20)
            size.width = ([[UIScreen mainScreen] bounds].size.width)* 0.66 + 20;
        
        if(!groupedMessages.isMediaMessage){
            if(groupedMessages.lastMessage && groupedMessages.lastMessage.type == MessageTypeGroupChatEvent){
                size.height = 74;
            }
            if(groupedMessages.lastMessage && groupedMessages.lastMessage.type == MessageTypeWebRTC)
                size.height = 60;
        }
        
        if(groupedMessages.answeredMessage) {
            NSString * bodyString = (groupedMessages.answeredMessage.body == nil && groupedMessages.answeredMessage.attachment != nil) ? groupedMessages.answeredMessage.attachment.fileName : groupedMessages.answeredMessage.body;
            
            if(bodyString || groupedMessages.answeredMessage.bodyInMarkdown) {
                CGRect quotedStringRect;
                
                if(groupedMessages.answeredMessage.bodyInMarkdown) {
                    quotedStringRect = [[MarkdownParser.sharedInstance attributedStringFromMarkDown:groupedMessages.answeredMessage.bodyInMarkdown] boundingRectWithSize:CGSizeMake(size.width*0.8, 42) options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading) context:nil];
                } else {
                    quotedStringRect = [bodyString boundingRectWithSize:CGSizeMake(size.width*0.8, 42) options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading) attributes:@{NSFontAttributeName: [UIFont fontWithName:[UITools defaultFontName] size:13]} context:nil];
                }

                // Message size (height + margin)
                size.height += 10 + (quotedStringRect.size.height);

                // Add height for incoming messages to take account of the sender name label when in a MUC
                if (self.isMUC) {
                    size.height += 15;
                }
            }
        }
    }
    
    [self.cache setObject:[NSValue valueWithCGSize:size] forKey:@([messageData messageHash])];
    return size;
}

@end
