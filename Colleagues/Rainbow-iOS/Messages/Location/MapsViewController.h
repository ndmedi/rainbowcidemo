//
//  MapsViewController.h
//  Rainbow-iOS
//
//  Created by Alaa Bzour on 2/11/19.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreLocation/CoreLocation.h"
#import <Rainbow/Message.h>

@interface MapsViewController : UIViewController

@property (nonatomic, strong) Message *selectedMessage;

@end
