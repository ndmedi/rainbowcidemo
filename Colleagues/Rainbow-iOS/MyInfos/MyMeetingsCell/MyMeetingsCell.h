//
//  MyMeetingsCell.h
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 08/02/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyMeetingsCell : UITableViewCell

@property (nonatomic, assign) int counterNewMeetings;
@property (weak, nonatomic) IBOutlet UILabel *counterNewMeetingsLabel;

@end
