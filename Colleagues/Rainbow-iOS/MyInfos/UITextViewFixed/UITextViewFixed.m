/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UITextViewFixed.h"
#import "UITools.h"

@implementation UITextViewFixed

-(void) layoutSubviews {
    [super layoutSubviews];
    [self setup];
}

-(void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    [self setup];
}

-(void) setup {
    self.textContainerInset = UIEdgeInsetsZero;
    self.contentInset = UIEdgeInsetsZero;
    self.textContainer.lineFragmentPadding = 0;
}

-(void)setContentInset:(UIEdgeInsets)contentInset {
    [super setContentInset:contentInset];
}

-(void)setContentOffset:(CGPoint)contentOffset animated:(BOOL)animated {
    [super setContentOffset:contentOffset animated:NO];
}

@end
