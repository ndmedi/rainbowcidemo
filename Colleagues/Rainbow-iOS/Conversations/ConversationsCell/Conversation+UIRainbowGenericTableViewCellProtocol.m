/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Conversation+UIRainbowGenericTableViewCellProtocol.h"
#import "NSDate+MediumFormat.h"
#import <Rainbow/Peer.h>
#import "Message+BodyForGroupChatEvent.h"
#import "UITools.h"

@implementation Conversation (UIRainbowGenericTableViewCellProtocol)
-(NSString *) mainLabel {
    return self.peer.displayName;
}

-(UIColor *) mainLabelTextColor {
    if([self.peer isKindOfClass:[Contact class]]){
        Contact *contact = (Contact *)self.peer;
        if( contact.calendarPresence.presence == CalendarPresenceOutOfOffice || contact.calendarPresence.automaticReply.isEnabled) {
            return [UITools defaultTintColor];
        } else if( contact.calendarPresence.presence == CalendarPresenceBusy ) {
            return [UITools defaultTintColor];
        } else {
            return [UIColor blackColor];
        }
    } else {
        return [UIColor blackColor];
    }
    return [UIColor blackColor];
}

-(UIFont *) mainLabelFont {
    if([self.peer isKindOfClass:[Room class]]){
        if(((Room*)self.peer).conference.myConferenceParticipant.state == ParticipantStateConnected)
            return [UIFont fontWithName:[UITools boldFontName] size:16];
    }
    if(self.hasActiveCall)
        return [UIFont fontWithName:[UITools boldFontName] size:16];
    return [UIFont fontWithName:[UITools defaultFontName] size:16];
}

-(NSString *) subLabel {
    NSString *text = self.lastMessage.body;
    NSString *fileText = self.lastMessage.attachment.fileName;
    

    if ([self.lastMessage replacedDate] != nil && (self.lastMessage.body == nil || ! self.lastMessage.body.length) && [self.lastMessage bodyInMarkdown] == nil) {
        if(self.lastMessage.isOutgoing) {
            text = NSLocalizedString(@"You deleted this message", nil);
        }
        else {
            text = NSLocalizedString(@"This message has been deleted", nil);
        }
    }
    
    if ([text length] == 0 && [fileText length] == 0 && self.lastMessage.location == nil)
        return nil;
    
    if(self.lastMessage.isOutgoing){
        if(self.lastMessage.attachment && text.length == 0 && !self.lastMessage.attachment.isDownloadAvailable) {
            text = NSLocalizedString(@"File deleted", nil);
        }
        if (self.lastMessage.location) {
            text = NSLocalizedString(@"You shared your current location", nil);
        }
        else if(self.lastMessage.attachment && text.length == 0){
            text = NSLocalizedString(@"You shared a file", nil);
        } else if ([self.lastMessage.staticGifUrl length] > 0) {
            text = NSLocalizedString(@"You have shared an animated GIF", nil);
        } else if ([self.lastMessage.imageUrl length] > 0) {
            text = NSLocalizedString(@"You have shared a picture", nil);
        } else if(!self.lastMessage.callLog) {
            text = [NSLocalizedString(@"You", nil) stringByAppendingFormat:@": %@", text];
        }
    } else {
        if(self.type == ConversationTypeRoom){
            if(self.lastMessage.peer.displayName) {
                if([self.lastMessage.peer isKindOfClass:[Contact class]] && ((Contact *)self.lastMessage.peer).firstName){

                    if(self.lastMessage.attachment && text.length == 0 && !self.lastMessage.attachment.isDownloadAvailable) {
                        text = NSLocalizedString(@"File deleted", nil);
                    }
                    if (self.lastMessage.location) {
                        text = [NSString stringWithFormat:NSLocalizedString(@"%@ has shared its current location with you", nil),((Contact *)self.lastMessage.peer).firstName];
                    }
                    else if(self.lastMessage.attachment && text.length == 0){
                        text = [NSString stringWithFormat:NSLocalizedString(@"%@ shared a file with you", nil),((Contact *)self.lastMessage.peer).firstName];
                    } else if ([self.lastMessage.staticGifUrl length] > 0) {
                        text = [NSString stringWithFormat:NSLocalizedString(@"%@ has shared an animated GIF", nil), ((Contact *)self.lastMessage.peer).firstName];
                    } else if ([self.lastMessage.imageUrl length] > 0) {
                        text = [NSString stringWithFormat:NSLocalizedString(@"%@ has shared a picture", nil), ((Contact *)self.lastMessage.peer).firstName];
                    } else {
                        text = [((Contact *)self.lastMessage.peer).firstName stringByAppendingFormat:@": %@", text];
                    }
                    
                }
            }
            if(self.lastMessage.groupChatEventType != MessageGroupChatEventNone && self.lastMessage.isDisplayable){
                text = self.lastMessage.bodyForGroupChatEvent;
            }
        } else {
            if(!self.lastMessage.isOutgoing){
                if (self.lastMessage.location) {
                    text = [NSString stringWithFormat:NSLocalizedString(@"%@ has shared its current location with you", nil),((Contact *)self.lastMessage.peer).firstName];
                }
                else if(self.lastMessage.attachment && text.length == 0){
                    text = [NSString stringWithFormat:NSLocalizedString(@"%@ shared a file with you", nil),((Contact *)self.lastMessage.peer).firstName];
                } else if ([self.lastMessage.staticGifUrl length] > 0) {
                    text = [NSString stringWithFormat:NSLocalizedString(@"%@ has shared an animated GIF", nil), ((Contact *)self.lastMessage.peer).firstName];
                } else if ([self.lastMessage.imageUrl length] > 0) {
                    text = [NSString stringWithFormat:NSLocalizedString(@"%@ has shared a picture", nil), ((Contact *)self.lastMessage.peer).firstName];
                }
            }
        }
    }
    
    return text;
}

-(NSDate *) date {
    return self.lastUpdateDate;
}

-(NSString *) bottomRightLabel {
    return nil;
}

-(Peer *) avatar {
    return self.peer;
}

-(NSInteger) badgeValue {
    return self.unreadMessagesCount;
}

-(NSArray *) observablesKeyPath {
    return @[kConversationUnreadMessagesCount, kConversationLastMessage];
}

-(NSString *) cellButtonTitle {
    return nil;
}

-(UIImage *) cellButtonImage {
    return nil;
}

-(UIImage *) rightIcon {
    return nil;
}
-(UIColor *) rightIconTintColor {
    return nil;
}


-(BOOL) showMaskedView {
    if([self.peer isKindOfClass:[Room class]]){
        Room *room = (Room *)self.peer;
        if(room.conference.mediaType == ConferenceMediaTypePSTN)
            return YES;
        if (room.conference.mediaType == ConferenceMediaTypeWebRTC && room.conference.endpoint)
            return YES;
    }
    
    return NO;
}

-(UIImage *) maskedImage {
    if([self.peer isKindOfClass:[Room class]]){
        Room *room = (Room *)self.peer;
        if (room.conference.mediaType == ConferenceMediaTypePSTN)
            return [UIImage imageNamed:@"meeting"];
        if (room.conference.mediaType == ConferenceMediaTypeWebRTC && room.conference.endpoint)
            return [UIImage imageNamed:@"video"];
    }
    
    return nil;
}

-(NSString *)midLabel {
    if([self.peer isKindOfClass:[Room class]]){
        Room *room = (Room *) self.peer;
        if(room.conference){
            if(room.conference.endpoint.mediaType == ConferenceEndPointMediaTypePSTNAudio){
                if(room.conference.type == ConferenceTypeInstant)
                    return NSLocalizedString(@"Instant meeting", nil);
                if(room.conference.type == ConferenceTypeScheduled){
                    NSString *dateStr = [UITools formatDateTimeForDate:room.conference.start];
                    if(room.conference.end && dateStr){
                        if([room.conference.end isEqualToDateIgnoringTime:room.conference.start])
                            dateStr = [NSString stringWithFormat:@"%@ - %@", dateStr, [UITools formatTimeForDate:room.conference.end]];
                        else
                            dateStr = [NSString stringWithFormat:@"%@ - %@", dateStr, [UITools formatDateTimeForDate:room.conference.end]];
                    }
                    
                    return dateStr;
                }
            } else if (room.conference.endpoint.mediaType == ConferenceEndPointMediaTypeWebRTC && room.conference.endpoint){
                return NSLocalizedString(@"Video conference", nil);
            }
        } else
            return nil;
    }
    return nil;
}
@end
