/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ConversationsTableViewCell.h"
#import "UIAvatarView.h"
#import "UITools.h"
#import "UIImageView+Letters.h"
    //This import is required to avoid error on class forward declaration
#import <Rainbow/Message.h>
#import "Contact+Extensions.h"
#import "SORelativeDateTransformer.h"
#import <Rainbow/NSDate+Utilities.h>
#import "UIImage+DrawText.h"
#import <Rainbow/NSDate+Distance.h>
#import <Rainbow/ContactsManagerService.h>
#import <Rainbow/RoomsService.h>
#import "NSDate+MediumFormat.h"
#import "UIView+MGBadgeView.h"
#import "Message+BodyForGroupChatEvent.h"

static int contactNameCenterConstraintDefaultValue = -15;
static int contactNameLeftConstraintDefaultValue = 14;
static int contactNameLeftConstraintWithIconValue = 34;

@interface ConversationsTableViewCell ()
@property (nonatomic, weak) IBOutlet UIAvatarView *avatar;
@property (nonatomic, weak) IBOutlet UILabel *contactName; // should be "conversation name"
@property (nonatomic, weak) IBOutlet UILabel *lastMessageLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contactNameCenterConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contactNameLeftConstraint;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (nonatomic, strong) NSTimer *refreshDateLabelTimer;
@property (nonatomic, strong) IBOutlet UIImageView *calendarPresenceIcon;

@end

@implementation ConversationsTableViewCell

-(void) awakeFromNib {
    [super awakeFromNib];
    _avatar.asCircle = YES;
    [UITools applyCustomFontTo:_contactName];
    [self setTintColor:[UITools defaultTintColor]];
    self.contentView.backgroundColor = [UITools defaultBackgroundColor];
    [UITools applyCustomFontTo:_dateLabel];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateContact:) name:kContactsManagerServiceDidUpdateContact object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateRoom:) name:kRoomsServiceDidUpdateRoom object:nil];
    
    _contactNameLeftConstraint.constant = contactNameLeftConstraintDefaultValue;
    _calendarPresenceIcon.hidden = YES;
    
    _avatar.badgeView.badgeColor = [UITools notificationBadgeColor];
    _avatar.badgeView.outlineWidth = 0.0;
    _avatar.badgeView.position = MGBadgePositionTopRight;
    _avatar.badgeView.textColor = [UIColor whiteColor];
    _avatar.badgeView.font = [UIFont fontWithName:[UITools defaultFontName] size:13];
    _avatar.badgeView.verticalOffset = 7;
    _avatar.badgeView.horizontalOffset = -7;
    _avatar.badgeView.displayIfZero = NO;
    _avatar.badgeView.bageStyle = MGBadgeStyleRoundRect;
    _avatar.badgeView.padding = 5;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidUpdateContact object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidUpdateRoom object:nil];
    
    if(_refreshDateLabelTimer){
        [_refreshDateLabelTimer invalidate];
        _refreshDateLabelTimer = nil;
    }
    
    [_conversation removeObserver:self forKeyPath:kConversationLastMessage context:nil];
    [_conversation removeObserver:self forKeyPath:kConversationUnreadMessagesCount context:nil];
    _conversation = nil;

}

-(void) didUpdateContact:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateContact:notification];
        });
        return;
    }
    
    NSDictionary *userInfo = (NSDictionary *)notification.object;
    Contact *contact = [userInfo objectForKey:kContactKey];
    NSArray<NSString *> *changedKeys = [userInfo objectForKey:kChangedAttributesKey];
    
    if([contact isEqual:_conversation.peer]) {
        /*if ([changedKeys containsObject:@"photoData"]) {
            _avatar.peer = _conversation.peer;
        }*/
        if ([changedKeys containsObject:@"displayName"]) {
            _contactName.text = _conversation.peer.displayName;
        }
        
        // Calendar presence
        if( contact.calendarPresence.presence == CalendarPresenceBusy ) {
            _contactNameLeftConstraint.constant = contactNameLeftConstraintWithIconValue;
            _calendarPresenceIcon.image = [UIImage imageNamed:@"inMeeting"];
            _calendarPresenceIcon.hidden = NO;
        } else if( contact.calendarPresence.presence == CalendarPresenceOutOfOffice ) {
            _contactNameLeftConstraint.constant = contactNameLeftConstraintWithIconValue;
            _calendarPresenceIcon.image = [UIImage imageNamed:@"outOfOffice"];
            _calendarPresenceIcon.hidden = NO;
        } else {
            _contactNameLeftConstraint.constant = contactNameLeftConstraintDefaultValue;
            _calendarPresenceIcon.image = nil;
            _calendarPresenceIcon.hidden = YES;
        }
    }
}

-(void) didUpdateRoom:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateRoom:notification];
        });
        return;
    }
    
    NSDictionary *userInfo = (NSDictionary *)notification.object;
    Room *room = [userInfo objectForKey:kRoomKey];
    NSArray<NSString *> *changedKeys = [userInfo objectForKey:kRoomChangedAttributesKey];

    if([_conversation.peer isEqual:room] && [changedKeys containsObject:@"name"]){
        _contactName.text = room.displayName;
    }
}

-(void) setConversation:(Conversation *) conversation {
    
    if (_conversation == conversation)
        return;
    
    // remove any KVO on the previous conversation
    [_conversation removeObserver:self forKeyPath:kConversationLastMessage context:nil];
    [_conversation removeObserver:self forKeyPath:kConversationUnreadMessagesCount context:nil];
    
    _conversation = nil;
    _conversation = conversation;
    
    [_conversation addObserver:self forKeyPath:kConversationLastMessage options:NSKeyValueObservingOptionNew context:nil];
    [_conversation addObserver:self forKeyPath:kConversationUnreadMessagesCount options:NSKeyValueObservingOptionNew context:nil];
    
    _avatar.peer = _conversation.peer;
    _contactName.text = _conversation.peer.displayName;
    
    [self setLastMessageLabelText:_conversation.lastMessage];
    [self setBadge];
}

-(void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self observeValueForKeyPath:keyPath ofObject:object change:change context:context];
        });
        return;
    }
    if([keyPath isEqualToString:kConversationUnreadMessagesCount]){
        [self setBadge];
    }
    if([keyPath isEqualToString:kConversationLastMessage]){
        [self setLastMessageLabelText:_conversation.lastMessage];
    }
}

-(void) setBadge {
    if(_conversation.unreadMessagesCount > 10) {
        _avatar.badgeView.bageStyle = MGBadgeStyleRoundRect;
        _avatar.badgeView.padding = 5;
    } else {
        _avatar.badgeView.bageStyle = MGBadgeStyleEllipse;
        _avatar.badgeView.padding = 0;
    }
    _avatar.badgeView.badgeValue = _conversation.unreadMessagesCount;
}

-(void) setLastMessageLabelText:(Message *) message {
    if(message.body.length > 0){
        _lastMessageLabel.hidden = NO;
        _contactNameCenterConstraint.constant = contactNameCenterConstraintDefaultValue;
        NSString *text = message.body;

        if(message.isOutgoing){
            text = [NSLocalizedString(@"You", nil) stringByAppendingFormat:@": %@", text];
        } else {
            if(_conversation.type == ConversationTypeRoom){
                if(message.peer.displayName) {
                    if([message.peer isKindOfClass:[Contact class]] && ((Contact *)message.peer).firstName){
                        text = [((Contact *)message.peer).firstName stringByAppendingFormat:@": %@", text];
                    }
                }
                if(message.groupChatEventType != MessageGroupChatEventNone){
                    text = message.bodyForGroupChatEvent;
                }
            }
        }
        _lastMessageLabel.text = text;
    } else {
        // We must rearrange name label position
        _lastMessageLabel.hidden = YES;
        _contactNameCenterConstraint.constant = 0;
    }
    [self setDateLabel];
}

-(void) setDateLabel {
    if(_conversation.lastUpdateDate > 0){
        _dateLabel.hidden = NO;
        if([_conversation.lastUpdateDate isToday]){
            _dateLabel.text = [_conversation.lastUpdateDate shortTimeString];
            _refreshDateLabelTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 + [[_conversation.lastUpdateDate dateAtEndOfDay] timeIntervalSinceNow] target:self selector:@selector(setDateLabel) userInfo:nil repeats:NO];
        } else if (_conversation.lastUpdateDate.year == [NSDate date].year){
            _dateLabel.text = [_conversation.lastUpdateDate mediumDateStringNoYear];
            _refreshDateLabelTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 + [[_conversation.lastUpdateDate dateAtEndOfYear] timeIntervalSinceNow] target:self selector:@selector(setDateLabel) userInfo:nil repeats:NO];
        } else {
            _dateLabel.text = [_conversation.lastUpdateDate mediumDateString];
        }
    } else {
        [_refreshDateLabelTimer invalidate];
        _dateLabel.hidden = YES;
    }
}

-(void) setSelected:(BOOL)selected animated:(BOOL)animated{
    [super setSelected:selected animated:animated];
    [_avatar setSelected:selected animated:animated];
}

-(void) setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    [_avatar setHighlighted:highlighted animated:animated];
}

@end
