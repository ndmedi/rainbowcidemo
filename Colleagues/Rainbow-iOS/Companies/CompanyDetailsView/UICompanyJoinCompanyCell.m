/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UICompanyJoinCompanyCell.h"
#import "UITools.h"
#import <Rainbow/Rainbow.h>

@interface UICompanyJoinCompanyCell ()
@property (weak, nonatomic) IBOutlet UILabel *cellTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *cellSubLabel;
@property (weak, nonatomic) IBOutlet UIButton *cellButton;
@end

@implementation UICompanyJoinCompanyCell

-(void) awakeFromNib {
    [super awakeFromNib];
    [UITools applyCustomFontTo:_cellTitleLabel];
    [UITools applyCustomFontTo:_cellSubLabel];
    [UITools applyCustomFontTo:_cellButton.titleLabel];
    _cellButton.tintColor = [UITools defaultTintColor];
    _cellTitleLabel.textColor = [UITools defaultTintColor];
    _cellButton.layer.borderColor = [UITools defaultTintColor].CGColor;
    _cellButton.layer.borderWidth = 2.0;
    _cellButton.layer.cornerRadius = 8.0f;
    _cellButton.layer.masksToBounds = YES;
}

-(void) setCompany:(Company *)company {
    _company = company;
    _cellTitleLabel.text = NSLocalizedString(@"Employees or affiliated individuals only", nil);
    if(!_company.alreadyRequestToJoin){
        _cellSubLabel.text = [NSString stringWithFormat:NSLocalizedString(@"If you are an employee of %@, you can join after being accepted by the company administrator.", nil),_company.name];
        [_cellButton setTitle:NSLocalizedString(@"Ask to join the company", nil) forState:UIControlStateNormal];
    } else {
        _cellSubLabel.text = [NSString stringWithFormat:NSLocalizedString(@"You have requested to become a member of the company %@\n.Your request is awaiting acceptance from the company administrator!", nil),_company.name];
        [_cellButton setTitle:NSLocalizedString(@"Cancel this request", nil) forState:UIControlStateNormal];
    }
}

- (IBAction)didTapCellButton:(UIButton *)sender {
    if(!_company.alreadyRequestToJoin){
        [self sendJoinRequestCompany];
    } else {
        [self cancelJoinRequest];
    }
}

-(CGFloat) computedHeight {
    return 200;
}

-(void) sendJoinRequestCompany {
    [[ServicesManager sharedInstance].companiesService requestJoinCompany:_company withCompletionHandler:^(Company *company, CompanyInvitation *companyInvitation, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if(error){
                UIAlertController *errorAlert = nil;
                if(error.code == 409){
                    errorAlert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"You already sent a request to join this company", nil) preferredStyle:UIAlertControllerStyleAlert];
                } else {
                    errorAlert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"Could not request joinning company", nil) preferredStyle:UIAlertControllerStyleAlert];
                }
                [errorAlert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:nil]];
                [_fromView presentViewController:errorAlert animated:YES completion:nil];
                return;
            }
        });
    }];
}

- (void)cancelJoinRequest {
    [[ServicesManager sharedInstance].companiesService cancelRequestedJoinCompany:_company withCompletionHandler:^(Company *company, CompanyInvitation *companyInvitation, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if(error){
                UIAlertController *errorAlert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"Could not cancel request to join company", nil) preferredStyle:UIAlertControllerStyleAlert];
                [errorAlert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:nil]];
                [_fromView presentViewController:errorAlert animated:YES completion:nil];
                return;
            }
        });
    }];
}
@end
