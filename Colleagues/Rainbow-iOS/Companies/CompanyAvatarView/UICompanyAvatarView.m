/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UICompanyAvatarView.h"

@interface UIAvatarView (protected)
@property (nonatomic, strong) UIImageView* contactPictureView;
@end

@implementation UICompanyAvatarView

-(void) setCompany:(Company *)company {
    _company = nil;
    _company = company;
    [self setNeedsLayout];
}

-(void) layoutSubviews {
    [super layoutSubviews];
    if(_company){
        UIImage *image = nil;
        if(_company.banner && _banner)
            image = [UIImage imageWithData:_company.banner];
        else if (_company.logo)
            image = [UIImage imageWithData:_company.logo];
        else
            image = [UIImage imageNamed:@"Company"];
        
        image.accessibilityValue = _company.name;
        self.contactPictureView.image = image;
    }
}

@end
