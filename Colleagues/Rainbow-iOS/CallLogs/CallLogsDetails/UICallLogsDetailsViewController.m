/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>
#import <Rainbow/CallLog.h>
#import "UITools.h"
#import "UICallLogsDetailsViewController.h"
#import "UICallLogsDetailsCell.h"
#import "UIContactDetailsViewController.h"
#import "UIStoryboardManager.h"

@implementation UICallLogsDetailsViewController

-(instancetype) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self){
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
 
    self.navigationController.navigationBar.translucent = NO;
    
    self.tableView.rowHeight = kCallLogsDetailsViewCellHeight;
    // A little trick for removing the cell separators
    self.tableView.tableFooterView = [UIView new];
    self.tableView.sectionIndexColor = [UITools defaultTintColor];
    // If we use clear color for this, the a-z index column will overlap with the section headers, etc.
    self.tableView.sectionIndexBackgroundColor = [UITools defaultBackgroundColor];
    self.tableView.backgroundColor = [UITools defaultBackgroundColor];

}

-(void) setCallLogs:(NSArray<CallLog *> *)callLogs {
    _callLogs = callLogs;
    
    // callLog.peer.displayName
    self.title = _callLogs.firstObject.peer.displayName;
    
    // Hide contact details button for anonymous
    if([self.title rangeOfString:@"^[*]*$" options:NSRegularExpressionSearch].location != NSNotFound)
        self.navigationItem.rightBarButtonItem = nil;

}

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(BOOL) shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_callLogs count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CallLog *callLog = [_callLogs objectAtIndex:indexPath.row];
    
    UICallLogsDetailsCell *cell = [tableView dequeueReusableCellWithIdentifier:kCallLogsDetailsCellTableViewReusableKey forIndexPath:indexPath];
    cell.callLog = callLog;
    
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (IBAction)didTapContactBarButton:(UIBarButtonItem *)sender {
    
    Contact *contact = (Contact *)_callLogs.firstObject.peer;
    
    UIContactDetailsViewController* controller = (UIContactDetailsViewController*)[[UIStoryboardManager sharedInstance].contactsDetailsStoryBoard instantiateViewControllerWithIdentifier:@"contactDetailsViewControllerID"];
    controller.contact = contact;
    controller.fromView = self;
    [self.navigationController pushViewController:controller animated:YES];

}
@end
