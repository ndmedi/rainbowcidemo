/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>
#import "WelcomePageCommonViewController.h"
#import "UITools.h"
#import <Rainbow/ServicesManager.h>
#import <Rainbow/defines.h>

@interface WelcomePage6ViewController : WelcomePageCommonViewController
@end

@interface WelcomePage6ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *presentationLabel2;
@property (weak, nonatomic) IBOutlet UILabel *bottomLabel;
@end

@implementation WelcomePage6ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [UITools applyCustomFontTo:_presentationLabel2];
    
    self.presentationLabel.text = NSLocalizedString(@"Communicate with all your contacts", nil);
    _presentationLabel2.text = NSLocalizedString(@"Rainbow needs access to your contacts to analyze them so you can see who is on Rainbow and thus communicate with them", nil);
    [UITools applyThinCustomFontTo:_bottomLabel];
    _bottomLabel.text = NSLocalizedString(@"This settings can be changed at any time in Rainbow settings", nil);
    self.continueButton.enabled = YES;
    self.continueButton.alpha = 1.0f;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localAccessGranted:) name:kContactsManagerServiceLocalAccessGrantedNotification object:nil];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceLocalAccessGrantedNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)continueButtonTapped:(UIButton *)sender {
    if(![ServicesManager sharedInstance].contactsManagerService.localDeviceAccessAlreadyRequired){
        [[ServicesManager sharedInstance].contactsManagerService requestAddressBookAccess];
    } else {
        [self.delegate nextPage];
    }
}

- (void)didTapInBackground:(UITapGestureRecognizer *)sender {
    // Nothing to do
}

-(void)localAccessGranted:(NSNotification *) notification {
    dispatch_async(dispatch_get_main_queue(), ^{
        if(![ServicesManager sharedInstance].contactsManagerService.localDeviceAccessGranted){
            _presentationLabel2.text = NSLocalizedString(@"We will not use your contacts", nil);
            _bottomLabel.textColor = [UIColor redColor];
            _bottomLabel.font = [UIFont fontWithName:[UITools defaultFontName] size:15];
        } else {
            [self.delegate nextPage];
        }
    });
}
// should display contact authorisation view
-(BOOL) shouldDisplay {
    if(self.delegate.errorDuringWizardSteps)
        return NO;
    if([ServicesManager sharedInstance].contactsManagerService.localDeviceAccessAlreadyRequired){
        NSLog(@"Contact access already required, skip this screen");
        return NO;
    }
    
    return YES;
}

@end
