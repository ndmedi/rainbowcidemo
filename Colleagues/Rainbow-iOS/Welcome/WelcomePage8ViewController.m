/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>
#import "WelcomePageCommonViewController.h"
#import "UITools.h"
#import <Rainbow/ServicesManager.h>
#import <Rainbow/defines.h>
#import <Photos/Photos.h>

@interface WelcomePage8ViewController : WelcomePageCommonViewController
@end

@interface WelcomePage8ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *presentationLabel2;
@property (weak, nonatomic) IBOutlet UILabel *bottomLabel;
@property (nonatomic) BOOL photoAccessPrompted;
@property (nonatomic) BOOL cameraAccessPrompted;
@end

@implementation WelcomePage8ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [UITools applyCustomFontTo:_presentationLabel2];
    
    self.presentationLabel.text = NSLocalizedString(@"Share images with all your contacts", nil);
    _presentationLabel2.text = NSLocalizedString(@"Rainbow needs access to your photo gallery and to your camera to allow you to share photo with your contacts", nil);
    [UITools applyThinCustomFontTo:_bottomLabel];
    _bottomLabel.text = NSLocalizedString(@"Thoses settings can be changed at any time in Rainbow settings", nil);
    self.continueButton.enabled = YES;
    self.continueButton.alpha = 1.0f;
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

-(void) dealloc {

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)continueButtonTapped:(UIButton *)sender {
    if(_photoAccessPrompted && _cameraAccessPrompted)
        [self.delegate nextPage];
    else {
        [self requestPhotoLibraryAccess];
        [self requestCameraDeviceAccess];
    }
}

-(void) requestPhotoLibraryAccess {
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        dispatch_async(dispatch_get_main_queue(), ^{
            _photoAccessPrompted = YES;
            if(status == PHAuthorizationStatusAuthorized){
                // Check if camera has been accepted / declined
                BOOL cameraAllowed = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusAuthorized;
                if(cameraAllowed)
                   [self.delegate nextPage];
            } else {
                BOOL cameraDenied = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusDenied;
                
                NSString *currentErrorText = nil;
                if(cameraDenied) {
                    currentErrorText  = [NSString stringWithFormat:@"%@\n%@",NSLocalizedString(@"We will not use your camera", nil),NSLocalizedString(@"We will not use your photo gallery", nil)];
                } else {
                    currentErrorText = NSLocalizedString(@"We will not use your photo gallery", nil);
                }
                
                _presentationLabel2.text = currentErrorText;
                _bottomLabel.textColor = [UIColor redColor];
                _bottomLabel.font = [UIFont fontWithName:[UITools defaultFontName] size:15];
            }
        });
    }];
}

-(void) requestCameraDeviceAccess {
    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
        _cameraAccessPrompted = YES;
        dispatch_async(dispatch_get_main_queue(), ^{
            if(granted){
                // Check if photo library has been accepted / declined
                BOOL photoLibraryAllowed = [PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusAuthorized;
                if(photoLibraryAllowed)
                    [self.delegate nextPage];
            } else {
                
                BOOL photoDenied = [PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusDenied;
                
                NSString *currentErrorText = nil;
                if(photoDenied) {
                    currentErrorText  = [NSString stringWithFormat:@"%@\n%@",NSLocalizedString(@"We will not use your photo gallery", nil), NSLocalizedString(@"We will not use your camera", nil)];
                } else {
                    currentErrorText = NSLocalizedString(@"We will not use your photo gallery", nil);
                }
                _presentationLabel2.text = currentErrorText;
                _bottomLabel.textColor = [UIColor redColor];
                _bottomLabel.font = [UIFont fontWithName:[UITools defaultFontName] size:15];
            }
        });
    }];
}

- (void)didTapInBackground:(UITapGestureRecognizer *)sender {
    // Nothing to do
}
// should display photo and video authorisation view
-(BOOL) shouldDisplay {
    if(self.delegate.errorDuringWizardSteps)
        return NO;
    BOOL photoLibraryAccessNotDetermined = [PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusNotDetermined;
    BOOL cameraAccessNotDetermined = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo] == AVAuthorizationStatusNotDetermined;
    
    return photoLibraryAccessNotDetermined || cameraAccessNotDetermined;
}

@end
