/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>
#import "WelcomePageCommonViewController.h"
#import "UITools.h"
#import <Rainbow/ServicesManager.h>
#import <Rainbow/OrderedDictionary.h>
#import <Rainbow/defines.h>
#import "ACFloatingTextField.h"

@interface WelcomePage3ViewController : WelcomePageCommonViewController
@end

@interface WelcomePage3ViewController () <UIPickerViewDelegate, UIPickerViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *presentationLabel2;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *countryTextField;
@property (weak, nonatomic) IBOutlet UIView *countryInputView;
@property (weak, nonatomic) IBOutlet UIPickerView *countryPickerView;
@property (weak, nonatomic) IBOutlet UIToolbar *countryInputViewToolBar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *countryInputViewDoneButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *countryInputViewTitleButton;
@property (nonatomic, strong) OrderedDictionary *countryList;
@property (nonatomic, strong) NSString *currentCountryCode;
@end

@implementation WelcomePage3ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [UITools applyCustomFontTo:_presentationLabel2];
    
    self.presentationLabel.text = NSLocalizedString(@"Welcome", nil);
    _presentationLabel2.text = NSLocalizedString(@"Rainbow need some informations about you", nil);
    
    [_firstNameTextField setTintColor:[UITools defaultTintColor]];
    _firstNameTextField.placeholder = NSLocalizedString(@"Firstname", nil);
    _firstNameTextField.borderStyle = UITextBorderStyleNone;
    _firstNameTextField.lineColor = [UITools foregroundGrayColor];
    _firstNameTextField.selectedLineColor = [UITools defaultTintColor];
    _firstNameTextField.placeHolderColor = [UITools foregroundGrayColor];
    _firstNameTextField.selectedPlaceHolderColor = [UITools defaultTintColor];
    
    [_lastNameTextField setTintColor:[UITools defaultTintColor]];
    _lastNameTextField.placeholder = NSLocalizedString(@"Lastname", nil);
    _lastNameTextField.borderStyle = UITextBorderStyleNone;
    _lastNameTextField.lineColor = [UITools foregroundGrayColor];
    _lastNameTextField.selectedLineColor = [UITools defaultTintColor];
    _lastNameTextField.placeHolderColor = [UITools foregroundGrayColor];
    _lastNameTextField.selectedPlaceHolderColor = [UITools defaultTintColor];
    
    [_countryTextField setTintColor:[UITools defaultTintColor]];
    _countryTextField.placeholder = NSLocalizedString(@"Country", nil);
    _countryTextField.borderStyle = UITextBorderStyleNone;
    _countryTextField.lineColor = [UITools foregroundGrayColor];
    _countryTextField.selectedLineColor = [UITools defaultTintColor];
    _countryTextField.placeHolderColor = [UITools foregroundGrayColor];
    _countryTextField.selectedPlaceHolderColor = [UITools defaultTintColor];

    [UITools applyCustomFontToTextField:_firstNameTextField];
    [UITools applyCustomFontToTextField:_lastNameTextField];
    [UITools applyCustomFontToTextField:_countryTextField];
    
    NSString *countryListFilePath = [[NSBundle bundleForClass:[self class]] pathForResource: @"country_list" ofType: @"plist"];
    _countryList = [OrderedDictionary dictionaryWithContentsOfFile:countryListFilePath];
    _countryTextField.inputView = _countryInputView;
    
    _currentCountryCode = [ContactsManagerService currentCountryCode];
    NSLog(@"CurrentCountryCode %@", _currentCountryCode);
    NSInteger pos = [_countryList indexOfKey:_currentCountryCode];
    if(pos == NSNotFound){
        pos = [_countryList indexOfKey:@"USA"];
    }
    [_countryPickerView selectRow:pos inComponent:0 animated:NO];
    _countryTextField.text = [_countryList objectAtIndex:pos];
    
    [_countryInputViewToolBar setTintColor:[UITools defaultTintColor]];
    [_countryInputViewToolBar setBarTintColor:[UITools defaultBackgroundColor]];
    [_countryInputViewDoneButton setTintColor:[UITools defaultTintColor]];
    [_countryInputViewDoneButton setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:[UITools boldFontName] size:16.0]} forState:UIControlStateNormal];
    [_countryInputViewDoneButton setTitle:NSLocalizedString(@"OK", nil)];
    [_countryInputViewTitleButton setTitleTextAttributes:@{NSFontAttributeName: [UIFont fontWithName:[UITools defaultFontName] size:16.0], NSForegroundColorAttributeName:[UITools defaultTintColor]} forState:UIControlStateNormal];
    _countryInputViewTitleButton.enabled = NO;
    
    [_countryPickerView setBackgroundColor:[UITools defaultBackgroundColor]];
    
    // Start in guest mode
    if (self.isGuestMode || [ServicesManager sharedInstance].myUser.isGuest) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didAddRoom:) name:kRoomsServiceDidAddRoom object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateRoom:) name:kRoomsServiceDidUpdateRoom object:nil];
        _countryTextField.hidden = YES;
        
        if ([self.invitationInformations[@"scenario"] isEqualToString:@"chat"]) {
            [self.continueButton setTitle:NSLocalizedString(@"Join the bubble",nil) forState:UIControlStateNormal];
        }
        if ([self.invitationInformations[@"scenario"] isEqualToString:@"pstn-conference"]) {
            [self.continueButton setTitle:NSLocalizedString(@"Join meeting",nil) forState:UIControlStateNormal];
        }
        
        NSString *roomJid = self.invitationInformations[@"joinRoomConfId"];
        Room *theRoom = [[ServicesManager sharedInstance].roomsService getRoomByJid:roomJid];
        if(theRoom){
            [self adaptUIForRoom:theRoom];
        }
    }
}

-(void) adaptUIForRoom:(Room *) room {
    self.presentationLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@ has invited you to join the Rainbow workspace %@",nil), room.creator.displayName, room.displayName];
    self.presentationLabel.numberOfLines = 2;
    self.presentationLabel.font = [UIFont fontWithName:[UITools defaultFontName] size:16];
    self.presentationLabel2.text = NSLocalizedString(@"To connect, please enter your first name and last name", nil);
    self.presentationLabel2.font = [UIFont fontWithName:[UITools defaultFontName] size:15];
}

-(void) didAddRoom:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didAddRoom:notification];
        });
        return;
    }
    
    Room *theRoom = (Room *)notification.object;
    if([theRoom.jid isEqualToString:self.invitationInformations[@"joinRoomConfId"]]){
        [self adaptUIForRoom:theRoom];
    }
}

-(void) didUpdateRoom:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didUpdateRoom:notification];
        });
        return;
    }
    
    NSDictionary *userInfo = (NSDictionary *)notification.object;
    Room *theRoom = [userInfo objectForKey:kRoomKey];
    if([theRoom.jid isEqualToString:self.invitationInformations[@"joinRoomConfId"]]){
        [self adaptUIForRoom:theRoom];
    }
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [_firstNameTextField layoutIfNeeded];
    [_lastNameTextField layoutIfNeeded];
    [_countryTextField layoutIfNeeded];
    
    _firstNameTextField.text = [ServicesManager sharedInstance].myUser.contact.firstName;
    _lastNameTextField.text = [ServicesManager sharedInstance].myUser.contact.lastName;
    [self checkContinueButtonState];
}

-(void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    _countryList = nil;
    _currentCountryCode = nil;
    if(self.isGuestMode){
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidAddRoom object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:kRoomsServiceDidUpdateRoom object:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)didTapInBackground:(UITapGestureRecognizer *)sender {
    [super didTapInBackground:sender];
    if(_firstNameTextField.isFirstResponder)
        [_firstNameTextField resignFirstResponder];
    if(_lastNameTextField.isFirstResponder)
        [_lastNameTextField resignFirstResponder];
    if(_countryTextField.isFirstResponder)
        [_countryTextField resignFirstResponder];
}

-(void) checkContinueButtonState {
    if(_firstNameTextField.text.length > 0 && _lastNameTextField.text.length > 0){
        self.continueButton.enabled = YES;
        self.continueButton.alpha = 1.0f;
    } else {
        self.continueButton.enabled = NO;
        self.continueButton.alpha = 0.5f;
    }
}

- (IBAction)textFieldValueChanged:(UITextField *)sender {
    [self checkContinueButtonState];
}

- (IBAction)continueButtonTapped:(UIButton *)sender {
    [_firstNameTextField resignFirstResponder];
    [_lastNameTextField resignFirstResponder];
    NSMutableDictionary *givenInfo = [NSMutableDictionary dictionary];
    [givenInfo setObject:_firstNameTextField.text forKey:@"firstname"];
    [givenInfo setObject:_lastNameTextField.text forKey:@"lastname"];
    if(_countryTextField.text.length > 0){
        // Find countryCode for selected country
        NSString *countryCode = [_countryList keyAtIndex:[_countryPickerView selectedRowInComponent:0]];
        if(countryCode.length == 0){
            countryCode = @"USA";
        }
        [givenInfo setObject:countryCode forKey:@"country"];
    }
    
    if (self.isGuestMode || [ServicesManager sharedInstance].myUser.isGuest) {
        // We have to update the user vcard with isInitialized to false
        [givenInfo setObject:@NO forKey:@"isInitialized"];
    }

    [[NSUserDefaults standardUserDefaults] setObject:givenInfo forKey:@"userInfos"];
    
    if (self.isGuestMode || [ServicesManager sharedInstance].myUser.isGuest) {
        if(self.invitationInformations){
            // We are comming from an invitation to join a room, so we must update our name right now
            if([self.invitationInformations objectForKey:@"joinRoomConfId"]){
                [[ServicesManager sharedInstance].contactsManagerService updateUserWithFields:givenInfo withCompletionBlock:^(NSError *error) {
                    // accept to join the bubble
                    if(self.invitationInformations){
                        NSString *roomJid = self.invitationInformations[@"joinRoomConfId"];
                        Room *theRoom = [[ServicesManager sharedInstance].roomsService getRoomByJid:roomJid];
                       
                        [[ServicesManager sharedInstance].roomsService acceptInvitation:theRoom completionBlock:nil];
                        
                        [[ServicesManager sharedInstance].conversationsManagerService startConversationWithPeer:theRoom withCompletionHandler:nil];
                        
                        // We have to remove the invitation informations in the loginViewController, so ask to the delegate
                        [self.delegate resetInvitationInformations];
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self hideHUD];
                        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userInfos"];
                        [self.delegate nextPage];
                    });
                }];
            }
        } else {
            // We don't have the user informations so we must update them
            [[ServicesManager sharedInstance].contactsManagerService updateUserWithFields:givenInfo withCompletionBlock:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self hideHUD];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userInfos"];
                    [self.delegate nextPage];
                });
            }];
        }
    } else
        [self.delegate nextPage];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(textField == _firstNameTextField){
        [_lastNameTextField becomeFirstResponder];
        return NO;
    } else if (textField == _lastNameTextField) {
        if(!self.isGuestMode)
            [_countryTextField becomeFirstResponder];
        else {
            if(self.continueButton.enabled)
                [self continueButtonTapped:self.continueButton];
            return YES;
        }
        return NO;
    } else if (textField == _countryTextField) {
        [_countryTextField resignFirstResponder];
        if(self.continueButton.enabled)
            [self continueButtonTapped:self.continueButton];
        
        return YES;
    }
    return YES;
}

-(BOOL) shouldDisplay {
    if ([ServicesManager sharedInstance].myUser.isGuest){
        if(![ServicesManager sharedInstance].myUser.contact.firstName && ![ServicesManager sharedInstance].myUser.contact.lastName)
            return YES;
        else
            return NO;
    }
    return ![ServicesManager sharedInstance].myUser.isInitialized;
}


#pragma mark - Picker delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return _countryList.allKeys.count;
}

-(NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString *text = [_countryList objectAtIndex:row];
    UIColor *color = [UIColor darkTextColor];
    
    if(row == [pickerView selectedRowInComponent:0])
        color = [UITools defaultTintColor];
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:[UITools defaultFontName] size:18.0f], NSForegroundColorAttributeName: color};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    _countryTextField.text = [_countryList objectAtIndex:row];
    [_countryPickerView reloadComponent:0];
}

- (IBAction)countryInputViewDone:(UIBarButtonItem *)sender {
    NSInteger row = [_countryPickerView selectedRowInComponent:0];
    _countryTextField.text = [_countryList objectAtIndex:row];
    
    [self textFieldShouldReturn:_countryTextField];
}
@end
