/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "GoogleAnalytics.h"
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import <Rainbow/Rainbow.h>

static NSString *const kTrackerName = @"Rainbow-iOS";
static NSString *const kProdGATrackingId = @"UA-51336686-8";
static NSString *const kDevGATrackingId = @"UA-51336686-7";
static NSString *const kAllowGATracking = @"allowGATracking";
static NSString *const kAllowGATrackingRequested = @"allowGATrackingRequested";

@interface  GoogleAnalytics ()

@property (nonatomic, readwrite) NSString *id;
@property (nonatomic, strong) id<GAITracker> tracker;

@end

static GoogleAnalytics *instance = nil;

@implementation GoogleAnalytics

-(instancetype) initWithTrackerId:(NSString *) id {
    self = [super init];
    if(self){
        _id = id;
        NSDictionary *appDefaults = @{kAllowGATracking: @(NO), kAllowGATrackingRequested: @(NO)};
        [[NSUserDefaults standardUserDefaults] registerDefaults:appDefaults];

        // Dispatch interval in seconds, -1 to dispatch only when quitting
        [GAI sharedInstance].dispatchInterval = 120;
        [GAI sharedInstance].trackUncaughtExceptions = NO;
        // User must be able to opt out of tracking
        [GAI sharedInstance].optOut = ![[NSUserDefaults standardUserDefaults] boolForKey:kAllowGATracking];
        self.tracker = [[GAI sharedInstance] trackerWithName:kTrackerName
                                                  trackingId:id];
    }
    return self;
}

+(instancetype) sharedInstance {
    if(!instance){
        NSString *trackingId = [ServicesManager sharedInstance].myUser.server.defaultServer ? kProdGATrackingId : kDevGATrackingId;
        instance = [[GoogleAnalytics alloc] initWithTrackerId:trackingId];
    }
    return instance;
}

-(BOOL) authorizationAlreadyRequested {
    return [[NSUserDefaults standardUserDefaults] boolForKey:kAllowGATrackingRequested];
}

-(void) setAuthorizationAlreadyRequested:(BOOL)authorizationRequested {
    [[NSUserDefaults standardUserDefaults] setBool:authorizationRequested forKey:kAllowGATrackingRequested];
}

-(BOOL) authorizationGranted {
    return [[NSUserDefaults standardUserDefaults] boolForKey:kAllowGATracking];
}

-(void) setAuthorizationGranted:(BOOL)authorizationGranted {
    [[NSUserDefaults standardUserDefaults] setBool:authorizationGranted forKey:kAllowGATracking];
}

-(void) dispatchEventWithCategory:(NSString *)category action:(NSString *)action label:(NSString *)label value:(NSNumber *)value {
    if(self.authorizationGranted){
        if([GAI sharedInstance].optOut){
            [GAI sharedInstance].optOut = NO;
        }
        NSMutableDictionary *event =
        [[GAIDictionaryBuilder createEventWithCategory:category
                                                action:action
                                                 label:label
                                                 value:value] build];
        [self.tracker send:event];
        [[GAI sharedInstance] dispatch];
    }
}

@end
