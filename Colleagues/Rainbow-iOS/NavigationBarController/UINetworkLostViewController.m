/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UINetworkLostViewController.h"
#import "UITools.h"


@interface UINetworkLostViewController ()
@property (weak, nonatomic) IBOutlet UILabel *networkLostLabel;
@end

@implementation UINetworkLostViewController

-(void) viewDidLoad {
    [super viewDidLoad];
    [UITools applyCustomFontTo:_networkLostLabel];
    _networkLostLabel.textColor = [UIColor whiteColor];
    _networkLostLabel.numberOfLines = 1;
    
    if (self.noNetworkConnectionIssue) {
        [self noNetworkConnection];
    }
    else {
        [self noXMPPConnection];
    }
}

-(void) noNetworkConnection {
    _networkLostLabel.text = NSLocalizedString(@"No network connection", nil);
    self.view.backgroundColor = [UIColor redColor];
}

-(void) noXMPPConnection {
    _networkLostLabel.text = NSLocalizedString(@"Connecting", nil);
    self.view.backgroundColor = [UIColor orangeColor];
}

@end
