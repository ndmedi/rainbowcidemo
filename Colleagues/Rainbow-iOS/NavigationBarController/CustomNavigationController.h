/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <UIKit/UIKit.h>

#define CustomNavigationControllerDidHideNetworkLostView @"didHideNetworkLostView"
#define CustomNavigationControllerWillShowNetworkLostView @"willShowNetworkLostView"
#define CustomNavigationControllerDidChangeNetworkLostViewFrame @"didChangeNetworkLostViewFrame"
#define CustomNavigationControllerDidHideGuestModeView @"didHideGuestModeView"
#define CustomNavigationControllerWillShowGuestModeView @"willShowGuestModeView"

@interface CustomNavigationController : UINavigationController
@property (nonatomic, readonly) BOOL isDisplayingNetworkLostView;
@property (nonatomic, readonly) BOOL isDisplayingGuestModeView;
@property (nonatomic, readonly) CGFloat lostNetwotkViewSize;
@property (nonatomic, readonly) CGFloat guestBannerViewSize;
@end
