/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIGuestModeViewController.h"
#import "UITools.h"

@interface UIGuestModeViewController ()

@property (weak, nonatomic) IBOutlet UILabel *guestModeLabel;
@property (weak, nonatomic) IBOutlet UIButton *guestFinalizeButton;
- (IBAction)finalizeGuestAccount:(id)sender;

@end

@implementation UIGuestModeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_guestModeLabel setText:NSLocalizedString(@"Welcome. Create your account for free and join the Rainbow community.", nil)];
    [UITools applyCustomFontTo:_guestModeLabel];
    
    [_guestFinalizeButton setTitle:NSLocalizedString(@"CREATE", nil) forState:UIControlStateNormal];
    _guestFinalizeButton.backgroundColor = [UITools defaultTintColor];
    [UITools applyCustomFontTo:_guestFinalizeButton.titleLabel];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)finalizeGuestAccount:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"willFinalizeGuestAccount" object:nil];
}
@end
