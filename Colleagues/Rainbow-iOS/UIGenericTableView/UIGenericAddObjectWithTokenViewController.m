/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIGenericAddObjectWithTokenViewController.h"
#import "ACFloatingTextField.h"
#import "UITools.h"
#import <Rainbow/Tools.h>
#import "UIContactDetailsViewController.h"
#import "UIPreviewingRoomAvatarView.h"
#import <Photos/Photos.h>
#import "GoogleAnalytics.h"
#import "UIRainbowGenericTableViewCell.h"
#import "UIStoryboardManager.h"
#import <Rainbow/defines.h>


@interface UIGenericAddObjectWithTokenViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *leftBarButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *rightBarButton;
@property (strong, nonatomic) IBOutlet UIView *limitParticipantReachedView;
@property (weak, nonatomic) IBOutlet UILabel *limitParticipantReachedLabel;
@property (strong, nonatomic) UIImage *customAvatarImage;
@property (weak, nonatomic) IBOutlet UIButton *customAvatarEditButton;
@property (strong, nonatomic) IBOutlet UIButton *addGuestButton;

@property (nonatomic) int hasFoundRainbowTVsOnServer;

@end

@implementation UIGenericAddObjectWithTokenViewController

-(void) awakeFromNib {
    [super awakeFromNib];
    
    _servicesManager = [ServicesManager sharedInstance];
    _objects = [NSMutableArray array];
    
    _selectedObjects = [NSMutableArray array];
    _filteredObjects = [NSMutableArray array];
    _searchedObjects = [NSMutableArray array];
    _invitedEmails = [NSMutableArray array];
    _searchOperationQueue = [NSOperationQueue new];
    _searchOperationQueue.maxConcurrentOperationCount = 1;
    
    _searchOnServer = YES;
    
    _mainLabel = nil;
    _subLabel = nil;
    _customAvatarImage = nil;
    _meetingStartDate = nil;
    _meetingEndDate = nil;
}

-(void) dealloc {
    _servicesManager = nil;
    [_filteredObjects removeAllObjects];
    [_selectedObjects removeAllObjects];
    [_searchedObjects removeAllObjects];
    [_invitedEmails removeAllObjects];
    _filteredObjects = nil;
    _selectedObjects = nil;
    _searchedObjects = nil;
    _invitedEmails = nil;
    [_searchOperationQueue cancelAllOperations];
    _searchOperationQueue = nil;
}

-(UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(BOOL) shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"UIRainbowGenericTableViewCell" bundle:nil] forCellReuseIdentifier:kTableViewReusableKey];
    
    self.title = _isAddingTVs ? NSLocalizedString(@"Rainbow TVs", nil) : NSLocalizedString(@"Attendees", nil);
    self.navigationController.navigationBar.translucent = NO;
    [self.view setBackgroundColor:[UITools defaultBackgroundColor]];
    
    
    _isSearchingOnServer = _isAddingTVs;
    
    if (_isAddingTVs) {
        _searchDone = NO;
        [[ServicesManager sharedInstance].contactsManagerService fetchAllTVs:^(NSArray *rainbowTVs, NSError *error) {
            _hasFoundRainbowTVsOnServer = [rainbowTVs count] > 0;
            [self.objects addObjectsFromArray:rainbowTVs];
            [self removeAlreadyAddedContactsInList];
            
            // Hide the "Add" part if there is no rainbow tv
            if ([self.objects count] == 0) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    _tokenInputViewTopConstraint.constant = -_tokenInputView.frame.size.height;
                    _tokenInputView.hidden = YES;
                });
            }
            
            _searchDone = YES;
        }];
    } else {
        // Add to suggested list only contacts the can be invited
        [self.objects addObjectsFromArray:[self.servicesManager.contactsManagerService.myNetworkContacts filteredArrayUsingPredicate: [NSPredicate predicateWithBlock:^BOOL(Contact *contact, NSDictionary<NSString *,id> * bindings) {
            return contact.isInRoster && contact.canChatWith && !contact.rainbowTV;
        }]]];
        
        [self removeAlreadyAddedContactsInList];
        [self sortUI];
    }
    
    _tableView.emptyDataSetSource = self;
    _tableView.emptyDataSetDelegate = self;
    
    _tokenInputView.fieldName = NSLocalizedString(@"Add :", nil);
    _tokenInputView.placeholderText = _isAddingTVs ? nil : NSLocalizedString(@"Enter a name", nil);
    _tokenInputView.tintColor = [UITools defaultTintColor];
    _tokenInputView.fieldColor = [UIColor darkGrayColor];
    _tokenInputView.drawBottomBorder = YES;
    _tokenInputView.backgroundColor = [UIColor whiteColor];
    _tokenInputView.keyboardType = UIKeyboardTypeDefault;
    
    _addGuestButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [_addGuestButton setTitle:NSLocalizedString(@"Invite", nil) forState:UIControlStateNormal];
    [_addGuestButton sizeToFit];
    [_addGuestButton addTarget:self action:@selector(addGuestEmailToken:) forControlEvents:UIControlEventTouchUpInside];
    _tokenInputView.accessoryView = _addGuestButton;
    _addGuestButton.hidden = YES;
    
    _tokenInputView.autocapitalizationType = UITextAutocapitalizationTypeNone;
    _tokenInputView.delegate = self;
    
    _tableView.tableFooterView = [UIView new];
    _tableView.backgroundColor = [UITools defaultBackgroundColor];
    self.tabBarController.tabBar.translucent = NO;
    _isTyping = NO;
    
    _previewingAvatarView.asCircle = YES;
    _previewingAvatarView.showPresence = NO;
    [self updatePreviewingAvatarView: [[NSMutableArray alloc] init]];
    
    if(!_mainLabel && !_subLabel) {
        // Updating participants for an existing bubble
        [_leftBarButton setTitle:NSLocalizedString(@"Cancel", nil)];
        [_rightBarButton setTitle:NSLocalizedString(@"Add", nil)];
        _tokenInputViewTopConstraint.constant = 0;
        _headerView.hidden = YES;
    } else {
        // Adding participants during creation of bubble
        [_leftBarButton setTitle:NSLocalizedString(@"Back", nil)];
        NSString *rightButtonText = NSLocalizedString([[ServicesManager sharedInstance].myUser isAllowedToUseAndroidTv] && !_isAddingTVs ? @"Continue" : @"Create", nil);
        [_rightBarButton setTitle:rightButtonText];
        _tokenInputViewTopConstraint.constant = _isAddingTVs ? 0 : _headerView.frame.size.height +5;
        _headerView.hidden = _isAddingTVs;
    }
    
    
    [UITools applyCustomFontTo:_limitParticipantReachedLabel];
    _limitParticipantReachedLabel.textColor = [UIColor redColor];
    _limitParticipantReachedLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Limit of number of participant reached (%ld)", nil), [ServicesManager sharedInstance].myUser.maxNumberOfParticipantPerRoom];
    
    [UITools applyCustomFontTo:_customAvatarEditButton.titleLabel];
    _customAvatarEditButton.tintColor = [UITools defaultTintColor];
    [_customAvatarEditButton setTitle:NSLocalizedString(@"Change avatar", nil) forState:UIControlStateNormal];
    
    for (Contact *participant in _initialParticipants) {
        CLToken *token = [[CLToken alloc] initWithDisplayText:participant.displayName context:participant];
        [self.objects removeObject:participant];
        [self.tokenInputView addToken:token];
        [self tokenInputView:_tokenInputView didAddToken:token];
    }
}

-(void) setRoom:(Room *)room {
    _room = room;
    self.object = _room;
}

-(void) removeAlreadyAddedContactsInList {
    [_room.participants enumerateObjectsUsingBlock:^(Participant * aParticipant, NSUInteger idx, BOOL * stop) {
        if(aParticipant.status == ParticipantStatusInvited || aParticipant.status == ParticipantStatusAccepted){
            if(![self.selectedObjects containsObject:aParticipant.contact])
                [self.selectedObjects addObject:aParticipant.contact];
            
            if([self.objects containsObject:aParticipant.contact])
                [self.objects removeObject:aParticipant.contact];
        }
    }];
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self doneButtonState:[self validateUIContent]];
}

- (void)viewDidAppear:(BOOL)animated {
    if (!_isAddingTVs && !_tokenInputView.editing && _object) {
        [_tokenInputView beginEditing];
    }
    [super viewDidAppear:animated];
    [self.tableView reloadData];
}

-(void) doneButtonState:(BOOL) enabled {
    _rightBarButton.enabled = enabled;
}

-(BOOL) validateUIContent {
    if (_room && _tokenInputView.allTokens.count == 0)
        return NO;
    
    return YES;
}

-(MBProgressHUD *) showHUDWithMessage:(NSString *) message mode:(MBProgressHUDMode) mode dismissCompletionBlock:(void (^)()) dismissCompletion {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = mode;
    hud.removeFromSuperViewOnHide = YES;
    if( message ) {
        hud.labelText = message;
    }
    [hud setLabelFont:[UIFont fontWithName:[UITools defaultFontName] size:14.0f]];
    if(mode == MBProgressHUDModeCustomView){
        hud.mode = MBProgressHUDModeCustomView;
        hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark"]];
        [hud showAnimated:YES whileExecutingBlock:^{sleep(1);} completionBlock:dismissCompletion];
    }
    return hud;
}

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = [self textForEmptyView];
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:[UITools defaultFontName] size:16.0f], NSForegroundColorAttributeName: [UIColor darkGrayColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView {
    return [UITools defaultBackgroundColor];
}

- (void)tokenInputView:(CLTokenInputView *)view didChangeText:(NSString *)text {
    if (_isAddingTVs) {
        view.text = nil;
        return;
    }
    [_filteredObjects removeAllObjects];
    
    if ([text isEqualToString:@""]){
        _isTyping = NO;
        _isSearchingOnServer = NO;
        [self.tableView reloadData];
    } else {
        _isTyping = YES;
        
        NSPredicate *predicate = [self predicateForText:text];
        [_filteredObjects addObjectsFromArray:[_objects filteredArrayUsingPredicate:predicate]];
        // Used to exclude contact contained in filteredObjects
        NSPredicate *predicateNotFiltered = [NSPredicate predicateWithBlock:^BOOL(Contact *contact, NSDictionary<NSString *,id> * bindings) {
            return ([_filteredObjects containsObject:contact] || [_selectedObjects containsObject:contact]) ? NO : YES;
        }];
        
        [_searchOperationQueue cancelAllOperations];
        
        _isSearchingOnServer = YES;
        _searchDone = NO;
        [self.tableView reloadData];
        NSString *originalText = [NSString stringWithString:text];
        if(_searchOnServer && _tokenInputView.text.length >= 2){
            [_searchOperationQueue addOperationWithBlock:^{
                NSString *textBeforeStartSearch = [NSString stringWithString:originalText];
                [NSThread sleepForTimeInterval:0.5];
                NSLog(@"BP");
                dispatch_sync(dispatch_get_main_queue(), ^{
                    if(![textBeforeStartSearch isEqualToString:_tokenInputView.text]){
                    } else {
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
                            [[ServicesManager sharedInstance].contactsManagerService searchRemoteContactsWithPattern:text withCompletionHandler:^(NSString *searchPattern, NSArray<Contact *> *foundContacts) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [_searchedObjects removeAllObjects];
                                    if(foundContacts.count > 0 && [searchPattern isEqualToString:_tokenInputView.text])
                                        // Filter contact not already contained in filteredObjects
                                        [_searchedObjects addObjectsFromArray:[foundContacts filteredArrayUsingPredicate:predicateNotFiltered]];
                                    
                                    _searchDone = YES;
                                    [self.tableView reloadData];
                                });
                            }];
                        });
                    }
                });
            }];
        }
        if ([Tools isValidEmailAddress:text]) {
            // Display addGuestButton only if the given email address in not in my contact
            Contact *contact = [[ServicesManager sharedInstance].contactsManagerService searchRainbowContactWithEmailString:text];
            if (!contact) {
                _addGuestButton.hidden = NO;
            } else{
                _addGuestButton.hidden = YES;
                [_filteredObjects addObject:contact];
                [self.tableView reloadData];
            }
        } else {
            _addGuestButton.hidden = YES;
        }
    }
}

- (void)tokenInputView:(CLTokenInputView *)view didAddToken:(CLToken *)token {
    // Check if token is a invitation email
    if ([Tools isValidEmailAddress:token.displayText]) {
        [_invitedEmails addObject:token.displayText];
    } else {
        [_selectedObjects addObject:token.context];
        [self updatePreviewingAvatarView: _selectedObjects];
    }
    [self doneButtonState:[self validateUIContent]];
    
    NSInteger count = view.allTokens.count;
    if(_room)
        count += _room.participants.count;
    if(count > [ServicesManager sharedInstance].myUser.maxNumberOfParticipantPerRoom){
        self.tableView.tableHeaderView = _limitParticipantReachedView;
    } else {
        self.tableView.tableHeaderView = nil;
    }
    
}

- (void)tokenInputView:(CLTokenInputView *)view didRemoveToken:(CLToken *)token {
    // Check if token is a invitation email
    if ([Tools isValidEmailAddress:token.displayText]) {
        [_invitedEmails removeObject:token.displayText];
    } else {
        id anObject = token.context;
        [_selectedObjects removeObject:anObject];
        [self updatePreviewingAvatarView: _selectedObjects];
        
        if(![_objects containsObject:anObject] && ((Contact *)anObject).canChatWith)
            [_objects addObject:anObject];
    }
    [self sortUI];
    [self doneButtonState:[self validateUIContent]];
    [_tableView reloadData];
    
    if(view.allTokens.count <= [ServicesManager sharedInstance].myUser.maxNumberOfParticipantPerRoom){
        self.tableView.tableHeaderView = nil;
    } else {
        self.tableView.tableHeaderView = _limitParticipantReachedView;
    }
}

- (CLToken *)tokenInputView:(CLTokenInputView *)view tokenForText:(NSString *)text {
    // Return nil to avoid creating a token only with the entered text
    return nil;
}

- (void)tokenInputViewDidEndEditing:(CLTokenInputView *)view {
    [self doneButtonState:[self validateUIContent]];
}

- (void)tokenInputViewDidBeginEditing:(CLTokenInputView *)view {
    [self.view removeConstraint:_tableViewTopConstraint];
    _tableViewTopConstraint = [NSLayoutConstraint constraintWithItem:_tableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:5];
    [self.view addConstraint:_tableViewTopConstraint];
    [self.view layoutIfNeeded];
    [self doneButtonState:[self validateUIContent]];
}

- (BOOL)tokenInputViewShouldReturn:(CLTokenInputView *)view {
    return NO;
}

- (void)addGuestEmailToken:(UIButton *)sender {
    NSLog(@"Add guest email address in the list of participants");
    
    CLToken *token = [[CLToken alloc] initWithDisplayText:_tokenInputView.text context:_tokenInputView.text];
    [self.tokenInputView addToken:token];

    _addGuestButton.hidden = YES;
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(_isSearchingOnServer && _searchOnServer && [_searchedObjects count] > 0)
        return 2;
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0){
        if(!_isTyping)
            return [_objects count];
        else
            return [_filteredObjects count];
    } else {
        return [_searchedObjects count];
    }
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if(_isSearchingOnServer && section == 1)
        return NSLocalizedString(@"More contacts", nil);
    
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UIRainbowGenericTableViewCell *cell = (UIRainbowGenericTableViewCell*)[tableView dequeueReusableCellWithIdentifier:kTableViewReusableKey forIndexPath:indexPath];
    
    if(indexPath.section == 0){
        if(!self.isTyping)
            cell.cellObject = [self.objects objectAtIndex:indexPath.row];
        else
            cell.cellObject = [self.filteredObjects objectAtIndex:indexPath.row];
    } else {
        cell.cellObject= [self.searchedObjects objectAtIndex:indexPath.row];
        // get the avatar.
        [[ServicesManager sharedInstance].contactsManagerService populateAvatarForContact:(Contact *)cell.cellObject];
    }
    
    if (_isAddingTVs) {
        cell.cellButtonTapHandler = nil;
    } else {
        __weak typeof(cell) weakCell = cell;
        cell.cellButtonTapHandler = ^(UIButton *sender) {
            [self showContactDetails:(Contact *)weakCell.cellObject];
        };
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *)view;
    tableViewHeaderFooterView.textLabel.font = [UIFont fontWithName:[UITools boldFontName] size:16.0f];
    tableViewHeaderFooterView.textLabel.textColor = [UITools defaultTintColor];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kRainbowGenericTableViewCellHeight;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [_tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Contact *selectedContact = nil;
    if(indexPath.section == 0){
        if(!self.isTyping)
            selectedContact = [self.objects objectAtIndex:indexPath.row];
        else
            selectedContact = [self.filteredObjects objectAtIndex:indexPath.row];
    } else {
        selectedContact = [self.searchedObjects objectAtIndex:indexPath.row];
    }
    
    NSString * displayTextString = selectedContact.displayName;
    if (displayTextString == nil && selectedContact.emailAddresses.count) {
         displayTextString = [[selectedContact.emailAddresses objectAtIndex:0] address];
    }
    else if (displayTextString == nil)
        displayTextString = @"";
    
    CLToken *token = [[CLToken alloc] initWithDisplayText:displayTextString context:selectedContact];
    [self.tokenInputView addToken:token];
    
    if(indexPath.section == 0){
        if(self.isTyping)
            [self.filteredObjects removeObjectAtIndex:indexPath.row];
        
        [self.objects removeObject:selectedContact];
    } else {
        [self.searchedObjects removeObject:selectedContact];
    }
    
    [tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)didTapOnCancelButton:(UIBarButtonItem *)sender {
    [_tokenInputView endEditing];
    if(_mainLabel) // Creation
        [self.navigationController popViewControllerAnimated:YES];
    else // Adding participant
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)didTapOnRightBarButtonItem:(UIBarButtonItem *)sender {
    [_tokenInputView endEditing];
    
    if(!_room) {
        if (![[ServicesManager sharedInstance].myUser isAllowedToUseAndroidTv] || _isAddingTVs) {
            // During creation, the user is not allowed to use rainbow tv so just directly create the room
            NSArray *allTokens = [self.tokenInputView.allTokens arrayByAddingObjectsFromArray:_contactsToken];
            [self createRoomWithName:_mainLabel topic:_subLabel participants:allTokens];
        } else {
            // Push the controller to add a rainbow tv
            UIGenericAddObjectWithTokenViewController* controller = (UIGenericAddObjectWithTokenViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"objectWithTokenViewController"];
            controller.mainLabel = _mainLabel;
            controller.subLabel = _subLabel;
            controller.isMeeting = _isMeeting;
            controller.initialParticipants = _initialParticipants;
            controller.contactsToken = self.tokenInputView.allTokens;
            controller.room = _room;
            controller.isAddingTVs = YES;
            
            [self.navigationController pushViewController:controller animated:YES];
        }
    } else {
        [self updateRoomWithName:_mainLabel topic:_subLabel participants:self.tokenInputView.allTokens];
    }
}

- (IBAction)didTapOnEditAvatarButton:(UIButton *) sender {
    if( !_customAvatarImage ) {
        [self showUIAlertSourceSelector];
        return;
    }
    
    UIAlertController *editActionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* changeAvatarAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Change avatar", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        [self showUIAlertSourceSelector];
    }];
    
    [editActionSheet addAction:changeAvatarAction];
    
    UIAlertAction* deleteAvatarAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Delete bubble avatar", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        
        [self setCustomAvatarImageFromData: nil];
    }];
    
    [editActionSheet addAction:deleteAvatarAction];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil) style:UIAlertActionStyleCancel handler:nil];
    [editActionSheet addAction:cancel];
    
    // show the menu.
    [editActionSheet.view setTintColor:[UITools defaultTintColor]];
    [self presentViewController:editActionSheet animated:YES completion:nil];
}

-(void)showUIAlertSourceSelector {
    
    UIAlertController *chooseSource = [UIAlertController alertControllerWithTitle: NSLocalizedString(@"Choose your source", nil) message: nil preferredStyle: UIAlertControllerStyleActionSheet];
    
    [chooseSource addAction:[UIAlertAction actionWithTitle: NSLocalizedString(@"Choose From Library", nil) style: UIAlertActionStyleDefault handler: ^(UIAlertAction * action) {
        [UITools openImagePicker:UIImagePickerControllerSourceTypePhotoLibrary delegate:self presentingViewController:self allowsEditing:YES];
    }]];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [chooseSource addAction:[UIAlertAction actionWithTitle: NSLocalizedString(@"Take Photo", nil) style: UIAlertActionStyleDefault handler: ^(UIAlertAction * _Nonnull action) {
            [UITools openImagePicker:UIImagePickerControllerSourceTypeCamera delegate:self presentingViewController:self allowsEditing:YES];
        }]];
    }
    
    // Custom 'cancel' button
    [chooseSource addAction:[UIAlertAction actionWithTitle: NSLocalizedString(@"Cancel", nil)
                                                     style: UIAlertActionStyleCancel
                                                   handler: ^(UIAlertAction * _Nonnull action) {
                                                       [chooseSource dismissViewControllerAnimated:YES completion:nil];
                                                   }]];
    [chooseSource.view setTintColor:[UITools defaultTintColor]];
    [self presentViewController:chooseSource animated:YES completion:nil];
}

#pragma mark -  UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    UIImage *editedImage = info[UIImagePickerControllerEditedImage];
    UIImage *originalImage = info[UIImagePickerControllerOriginalImage];
    NSData *imageData;
    
    if (editedImage) {
        NSLog(@"Edited image %@", editedImage);
        imageData = [NSData dataWithData:UIImageJPEGRepresentation(editedImage, kJPEGCompressionQuality)];
    } else {
        NSLog(@"Original image %@", originalImage);
        imageData = [NSData dataWithData:UIImageJPEGRepresentation(originalImage, kJPEGCompressionQuality)];
    }
    
    [self setCustomAvatarImageFromData: imageData];
    
    [picker dismissViewControllerAnimated: YES completion: nil];
}

-(void) setCustomAvatarImageFromData:(NSData *) data {
    if( data == nil) {
        _customAvatarImage = nil;
        [self updatePreviewingAvatarView: _selectedObjects];
    } else {
        _customAvatarImage = [UIImage imageWithData:data];
        [_previewingAvatarView setAvatarFromData: data];
    }
}

-(NSPredicate *) predicateForText:(NSString *) text {
    return [NSPredicate predicateWithFormat:@"fullName contains[cd] %@", text];
}

-(NSString *) textForEmptyView {
    NSString *endOfSearchStringToDisplay = NSLocalizedString(@"No contact found", nil);
    
    if (_isAddingTVs)
        endOfSearchStringToDisplay = _hasFoundRainbowTVsOnServer ? @"There are no more Rainbow TV equipment to add in the bubble." : @"There are no Rainbow TV equipment yet declared in your company.";

    return NSLocalizedString(!_searchDone && _isSearchingOnServer ? @"Searching ..." : endOfSearchStringToDisplay, nil);
}

-(void) sortUI {
    if(self.servicesManager.contactsManagerService.sortByFirstName){
        [self.objects sortUsingDescriptors:@[[UITools sortDescriptorForContactByFirstName], [UITools sortDescriptorForContactByLastName]]];
    } else {
        [self.objects sortUsingDescriptors:@[[UITools sortDescriptorForContactByLastName], [UITools sortDescriptorForContactByFirstName]]];
    }
}

-(void) updatePreviewingAvatarView:(NSMutableArray <id> *) participants {
    if( _customAvatarImage ) {
        return;
    }
    
    Contact *myContact = [ServicesManager sharedInstance].myUser.contact;
    
    if(myContact && ![participants containsObject:myContact]){
        [participants insertObject:myContact atIndex:0];
    }
    
    [_previewingAvatarView setParticipantList: participants];
}

// ROOMS CREATION
-(void) createRoomWithName:(NSString *) name topic:(NSString *) topic participants:(NSArray *) participants {

    
    MBProgressHUD *hud = [self showHUDWithMessage:NSLocalizedString((_isMeeting?@"Creating new meeting ...":@"Creating new bubble ..."), nil) mode:MBProgressHUDModeIndeterminate dismissCompletionBlock:nil];
    
    // Create room
    [hud show:YES];
    
    NSError __block *error = nil;
    NSError __block *errorAddParticipant = nil;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        _room = [[ServicesManager sharedInstance].roomsService createRoom:name withTopic:topic error:&error];
        [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:_isMeeting?@"meeting":@"bubble" action:@"create" label:nil value:nil];
        if(error){
            NSLog(@"Error while tryng to create room");
            dispatch_async(dispatch_get_main_queue(), ^{
                [hud hide:NO];
                NSString *msg = error.localizedDescription;
                if(error.code == 403620){
                    if(_isMeeting)
                        msg = NSLocalizedString(@"You cannot create a new meeting, the maximum number of collaboration spaces (bubbles or meetings) has been reached", nil);
                    else
                        msg = NSLocalizedString(@"You cannot create a new bubble, the maximum number of collaboration spaces (bubbles or meetings) has been reached", nil);
                }
                else{
                    NSString * msgString = _isMeeting?NSLocalizedString(@"meeting",nil):NSLocalizedString(@"bubble",nil);
                    msg = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"Error while creating a", nil),msgString];
                }
                
                 [UITools showErrorPopupWithTitle:_isMeeting?NSLocalizedString(@"Create a meeting", nil):NSLocalizedString(@"Create a bubble", nil) message:msg inViewController:self];
            });
            return;
        }
        dispatch_group_t createGroup = dispatch_group_create();
        
        NSLog(@"THE ROOM %@ AND PARTICIPANTS %@", _room, participants);
        
        // Upload custom avatar
        if( _customAvatarImage ) {
            dispatch_group_enter(createGroup);
            [self uploadCustomAvatar:_room completionBlock:^{
                dispatch_group_leave(createGroup);
            }];
        }
        
        // Meeting
        if( _isMeeting ) {
            dispatch_group_enter(createGroup);
            [[ServicesManager sharedInstance].conferencesManagerService createConferenceForRoom:_room startDate:_meetingStartDate endDate:_meetingEndDate completionHandler:^(NSError *error) {
                if(error){
                    // Request a delete of the room and display an error
                    [[ServicesManager sharedInstance].roomsService deleteRoom:_room];

                    dispatch_async(dispatch_get_main_queue(), ^{
                        [UITools showErrorPopupWithTitle:NSLocalizedString(@"Error while creating meeting", nil) message:NSLocalizedString(error.localizedDescription, nil) inViewController:self];
                    });
                    dispatch_group_leave(createGroup);
                    return;
                } else {
                    [self inviteParticipants:participants toRoom:_room completionBlock:nil];
                    [[ServicesManager sharedInstance].conferencesManagerService inviteParticipants:_room.participants toJoinConference:_room.conference inRoom:_room completionBlock:^(NSError *error) {
                        if (error) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [UITools showErrorPopupWithTitle:NSLocalizedString(@"Error while creating meeting", nil) message:NSLocalizedString(error.localizedDescription, nil) inViewController:self];
                            });
                        }
                        
                        dispatch_group_leave(createGroup);
                    }];
                }
            }];
        } else {
            dispatch_group_enter(createGroup);
            [self inviteParticipants:participants toRoom:_room completionBlock:^(NSError *aError) {
                errorAddParticipant = aError;
                dispatch_group_leave(createGroup);
            }];
        }
        
        // Create operations done. leave view
        dispatch_group_notify(createGroup, dispatch_get_main_queue(), ^{
            [hud hide:YES];
            if (!error) {
                MBProgressHUD *hudOK;
                if (_room) {
                    hudOK = [self showHUDWithMessage:NSLocalizedString(_isMeeting?@"Meeting created":@"Bubble created", nil) mode:MBProgressHUDModeCustomView dismissCompletionBlock:nil];
                    hudOK.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark"]];
                }
                else{
                    hudOK = [self showHUDWithMessage:NSLocalizedString(_isMeeting?@"Failed to create meeting \n Check your network connection":@"Failed create Bubble \n Check your network connection", nil) mode:MBProgressHUDModeCustomView dismissCompletionBlock:nil];
                    hudOK.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Failed"]];
                }
                
                [hudOK showAnimated:YES whileExecutingBlock:^{sleep(1);} completionBlock:^{
                    if (!errorAddParticipant) {
                        if(_initialParticipants)
                            [[ServicesManager sharedInstance].conversationsManagerService startConversationWithPeer:_room withCompletionHandler:nil];
                        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
                    } else {
                        // show error alert
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [UITools showErrorPopupWithTitle:NSLocalizedString(@"Error while updating the bubble", nil) message:NSLocalizedString(errorAddParticipant.localizedDescription, nil) inViewController:self];
                        });
                    }
                }];
            }
        });
    });
}

-(void) updateRoomWithName:(NSString *) name topic:(NSString *) topic participants:(NSArray *) participants {
    MBProgressHUD *hud = [self showHUDWithMessage:NSLocalizedString((_isMeeting?@"Updating meeting ...":@"Updating bubble ..."), nil) mode:MBProgressHUDModeIndeterminate dismissCompletionBlock:nil];
    [hud show:YES];
    
    NSError __block *error = nil;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if(name)
            [[ServicesManager sharedInstance].roomsService updateRoom:_room withName:name];
        if(topic)
            [[ServicesManager sharedInstance].roomsService updateRoom:_room withTopic:topic];
        
        [[GoogleAnalytics sharedInstance] dispatchEventWithCategory:_isMeeting?@"meeting":@"bubble" action:@"update" label:nil value:nil];
        
        dispatch_group_t createGroup = dispatch_group_create();
        
        NSLog(@"THE ROOM %@ AND PARTICIPANTS %@", _room, participants);
        
        // Invite participants
        dispatch_group_enter(createGroup);
        [self inviteParticipants:participants toRoom:_room completionBlock:^(NSError *aError) {
            error = aError;
            dispatch_group_leave(createGroup);
        }];
        
        // Upload custom avatar
        if( _customAvatarImage ) {
            dispatch_group_enter(createGroup);
            [self uploadCustomAvatar:_room completionBlock:^{
                dispatch_group_leave(createGroup);
            }];
        }
        
        // Meeting
        if( _isMeeting ) {
            dispatch_group_enter(createGroup);
            [[ServicesManager sharedInstance].conferencesManagerService updateConferenceForRoom:_room startDate:_meetingStartDate endDate:_meetingEndDate completionHandler:^(NSError *error) {
                if(error){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [UITools showErrorPopupWithTitle:NSLocalizedString(@"Error while creating meeting", nil) message:NSLocalizedString(error.localizedDescription, nil) inViewController:self];
                    });
                    dispatch_group_leave(createGroup);
                    return;
                } else {
                    [[ServicesManager sharedInstance].conferencesManagerService inviteParticipants:_room.participants toJoinConference:_room.conference inRoom:_room completionBlock:^(NSError *error) {
                        dispatch_group_leave(createGroup);
                    }];
                }
            }];
        }
        
        // Create operations done. leave view
        dispatch_group_notify(createGroup, dispatch_get_main_queue(), ^{
            [hud hide:YES];
            
            if (!error) {
                MBProgressHUD *hudOK = [self showHUDWithMessage:NSLocalizedString(_isMeeting?@"Meeting updated":@"Bubble updated", nil) mode:MBProgressHUDModeCustomView dismissCompletionBlock:nil];
                hudOK.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark"]];
                [hudOK showAnimated:YES whileExecutingBlock:^{sleep(1);} completionBlock:^{
                    if(_initialParticipants && !_isMeeting)
                        [[ServicesManager sharedInstance].conversationsManagerService startConversationWithPeer:_room withCompletionHandler:nil];
                    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
                }];
            } else {
                // show error alert
                dispatch_async(dispatch_get_main_queue(), ^{
                    [UITools showErrorPopupWithTitle:NSLocalizedString(@"Error while updating the bubble", nil) message:NSLocalizedString(error.localizedDescription, nil) inViewController:self];
                });
            }
        });
    });
}

-(void) inviteParticipants:(NSArray *) contactsAndGuests toRoom:(Room *) room completionBlock:(void (^)(NSError* error))completion {
    
    NSMutableArray *contacts = [NSMutableArray new];
    NSMutableArray *guests = [NSMutableArray new];
    
    // Filter Contact object and guest email NSString object
    for (CLToken *token in contactsAndGuests) {
        if ([token.context isKindOfClass:[Contact class]]) {
            [contacts addObject:token];
        }
        else if ([token.context isKindOfClass:[NSString class]]){
            [guests addObject:(NSString*)token.context];
        }
    }

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
        NSError __block * error = nil;
        [contacts enumerateObjectsUsingBlock:^(CLToken * aToken, NSUInteger idx, BOOL * stop) {
            
            Participant *participant = [_room participantFromContact:(Contact*)aToken.context];
            NSLog(@"invite participant %@ to join room",participant);
            if(participant.status == ParticipantStatusRejected){
                NSLog(@"this participant is rejected so we will delete it");
                [[ServicesManager sharedInstance].roomsService deleteParticipant:participant fromRoom:room];
            }
            
            [[ServicesManager sharedInstance].roomsService inviteContact:(Contact*)aToken.context inRoom:room error:&error];
        }];
        
        if (!error && guests.count > 0) {
            [[ServicesManager sharedInstance].roomsService notifyGuestUsers:guests forRoom:room withCompletionHandler:^(NSDictionary *response, NSError *error) {
                if(error)
                    completion(error);
            }];
        }
        
        if(completion)
            completion(error);
    });
    
}

-(void)uploadCustomAvatar: (Room *) room completionBlock:(void (^)())completion {
    NSData *imageData;
    
    if( _customAvatarImage ) {
        imageData = [NSData dataWithData:UIImageJPEGRepresentation(_customAvatarImage, kJPEGCompressionQuality)];
    }
    
    if(imageData) {
        [[ServicesManager sharedInstance].roomsService updateAvatar:room withPhotoData: imageData withCompletionBlock:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if(error) {
                    NSLog(@"An error occured while updating the room avatar %@", error);
                }
                
                completion();
            });
        }];
    }
    
    else {
        completion();
    }
}

// CONTACT DETAILS
-(void) showContactDetails:(Contact *) contact {
    UIContactDetailsViewController* controller = (UIContactDetailsViewController*)[[UIStoryboardManager sharedInstance].contactsDetailsStoryBoard instantiateViewControllerWithIdentifier:@"contactDetailsViewControllerID"];
    controller.contact = contact;
    controller.fromView = self;
    [self.navigationController pushViewController:controller animated:YES];
}

@end
