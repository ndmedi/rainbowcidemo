/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIWithToggleTableViewCell.h"
#import "UITools.h"

@interface UIWithToggleTableViewCell()
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UISwitch *meetingSwitch;
@end

@implementation UIWithToggleTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [UITools applyCustomFontTo:_label];
    _label.textColor = [UITools defaultTintColor];
}

-(void)setLabelText:(NSString *)label {
    _label.text = label;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
