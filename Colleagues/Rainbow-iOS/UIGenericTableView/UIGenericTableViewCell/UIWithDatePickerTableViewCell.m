/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "UIWithDatePickerTableViewCell.h"
#import "UITools.h"

@interface UIWithDatePickerTableViewCell()
@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (strong, nonatomic) NSDateFormatter *dateFormater;
@end

@implementation UIWithDatePickerTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [UITools applyCustomFontTo:_label];
    _label.textColor = [UITools defaultTintColor];
    _icon.image = [UIImage imageNamed:@"Duration"];
    [_icon setTintColor:[UITools defaultTintColor]];
    
    _dateFormater = [[NSDateFormatter alloc] init];
    [_dateFormater setTimeZone:[NSTimeZone systemTimeZone]];
    _dateFormater.dateStyle = NSDateFormatterMediumStyle;
}

-(void)setExpanded:(BOOL)expanded {
    _expanded = expanded;
    _datePicker.hidden = !expanded;
}

-(void)setLabelText:(NSString *)label {
    _label.text = label;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if(selected){
        dispatch_async(dispatch_get_main_queue(), ^{
            _datePicker.countDownDuration = _datePicker.countDownDuration;
        });
    }
}

- (IBAction)datePickerValueChanged:(UIDatePicker *)sender {
    if( sender.datePickerMode == UIDatePickerModeDateAndTime ) {
        _label.text = [UITools formatDateTimeForDate:sender.date];
    } else {
        [_dateFormater setDateFormat:@"HH:mm"];
        _label.text = [_dateFormater stringFromDate:sender.date];
    }
}

- (void)setDatePickerMode: (UIDatePickerMode) mode {
    _datePicker.datePickerMode = mode;
    
    if(mode == UIDatePickerModeCountDownTimer ) {
        _datePicker.minuteInterval = 30; // Minutes interval
        [_datePicker setCountDownDuration:(NSTimeInterval)3600];
        _icon.image = [UIImage imageNamed:@"Duration"];
    }
    
    else {
        _datePicker.minuteInterval = 15; // Minutes interval
        NSDate *curDate = [self nearestDate:[NSDate date] inIntervall: (int)_datePicker.minuteInterval];
        [_datePicker setMinimumDate: curDate ];
        [_datePicker setDate:[NSDate date] animated:YES];
        [_datePicker setDate:curDate animated:YES];
        _icon.image = [UIImage imageNamed:@"meeting"];
    }
    
    [self datePickerValueChanged:_datePicker];
}

-(NSDate *)nearestDate:(NSDate *)curDate inIntervall: (int)interval {

    int referenceTimeInterval = (int)[curDate timeIntervalSinceReferenceDate];
    int remainingSeconds = referenceTimeInterval % (interval*60);
    int timeRoundedToMinutes = referenceTimeInterval +((interval*60)-remainingSeconds);
    
    return [NSDate dateWithTimeIntervalSinceReferenceDate:(NSTimeInterval)timeRoundedToMinutes];
}

-(NSDate *)datePickerDate {
    return _datePicker.date;
}

-(NSTimeInterval)datePickerDuration {
    return _datePicker.countDownDuration;
}

@end
