//
//  UITableView+LoadingFooter.m
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 26/02/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import "UITableView+LoadingFooter.h"

@implementation UITableView (LoadingFooter)

-(void) showLoadingFooter {
    UIActivityIndicatorView *loadingFooter = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    loadingFooter.frame = CGRectMake(loadingFooter.frame.origin.x, loadingFooter.frame.origin.y, loadingFooter.frame.size.width, 50);
    loadingFooter.hidesWhenStopped = YES;
    [loadingFooter startAnimating];
    [self setTableFooterView:loadingFooter];
    self.tableFooterView = loadingFooter;
}

-(void) hideLoadingFooter {
    dispatch_async(dispatch_get_main_queue(), ^{
        BOOL tableContentSufficentlyTall = self.contentSize.height > self.frame.size.height;
        BOOL atBottomOfTable = self.contentOffset.y >= self.contentSize.height - self.frame.size.height;
        if (atBottomOfTable && tableContentSufficentlyTall) {
            [UIView animateWithDuration:0.2 animations:^{
                self.contentOffset = CGPointMake(self.contentOffset.x, self.contentOffset.y - 50);
            } completion:^(BOOL finished) {
                self.tableFooterView = [UIView new];
            }];
        } else {
            self.tableFooterView = [UIView new];
        }
    });
}

-(BOOL) isLoadingFooterShowing {
    return [self.tableFooterView isKindOfClass:[UIActivityIndicatorView class]];
}

@end
