//
//  UIMyInfoAvatarView.h
//  Rainbow-iOS
//
//  Created by Le Trong Nghia Huynh on 08/02/2019.
//  Copyright © 2019 ALE International. All rights reserved.
//

#import "UIAvatarView.h"

@interface UIMyInfoAvatarView : UIAvatarView
-(void) displayNotificationBadgeWithCount:(int) count;
@end
