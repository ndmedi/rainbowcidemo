/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Rainbow/Rainbow.h>

@interface ContactsManagerService (Private)
-(Contact *) createOrUpdateRainbowContactWithJid:(NSString *) jid;
-(void) updateRainbowContact:(Contact *) contact withJSONDictionary:(NSDictionary *) jsonDictionary;
-(void) updateRainbowContact:(Contact *)contact withPhotoData:(NSData *) photoData;
@end
@interface Conversation (Private)
@property (nonatomic, readwrite, strong) NSString *conversationId;
@property (nonatomic, readwrite, strong) Peer *peer;
@property (nonatomic, readwrite, strong) Message *lastMessage;
@end

@interface Peer (Private)
@property (nonatomic, readwrite, strong) NSString *rainbowID;
@property (nonatomic, readwrite, strong) NSString *jid;
@property (nonatomic, readwrite, strong) NSString *displayName;
@end

@interface Message (Private)
@property (nonatomic, readwrite, strong) Peer *peer;
@property (nonatomic, readwrite, strong) NSDate *timestamp;
@end

@interface UIRainbowGenericTableViewCell (Private)
@property (strong, nonatomic) IBOutlet UIAvatarView *avatarView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@end
