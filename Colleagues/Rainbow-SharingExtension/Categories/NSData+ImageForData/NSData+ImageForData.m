/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "NSData+ImageForData.h"
#import <Rainbow/NSData+MimeType.h>

@implementation NSData (ImageForData)

-(UIImage *) imageForData {
    NSString *imageName = @"unknownBig";
    NSString *mimeType = [self mimeTypeByGuessing];
    
    if ([mimeType isEqualToString:@"image/jpeg"] ||
        [mimeType isEqualToString:@"image/png"] ||
        [mimeType isEqualToString:@"image/jp2"] ||
        [mimeType isEqualToString:@"image/heic"]
        ) {
        return [UIImage imageWithData:self];
    } else if ([mimeType isEqualToString:@"application/pdf"]){
        imageName = @"pdfBig";
    } else if ([mimeType isEqualToString:@"application/msword"] ||
               [mimeType isEqualToString:@"application/vnd.openxmlformats-officedocument.wordprocessingml.document"] ||
               [mimeType isEqualToString:@"application/vnd.oasis.opendocument.text"]){
        imageName = @"docBig";
    } else if ([mimeType isEqualToString:@"application/vnd.ms-powerpoint"] ||
               [mimeType isEqualToString:@"application/vnd.openxmlformats-officedocument.presentationml.presentation"]){
        imageName = @"pptBig";
    } else if ([mimeType isEqualToString:@"application/vnd.ms-excel"] ||
               [mimeType isEqualToString:@"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"]){
        imageName = @"xlsBig";
    } else if([mimeType containsString:@"audio/"]){
        imageName = @"audioVideoBig";
    } else if ([mimeType containsString:@"video/"]){
        imageName = @"audioVideoBig";
    } else
        imageName = @"unknownBig";
    
    
    return [UIImage imageNamed:imageName];
}
@end
