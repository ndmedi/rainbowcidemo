//
//  ChartView.m
//  HelloWorld
//
//  Created by Thibaut Ackermann on 19/01/2017.
//  Copyright © 2017 Personal Cloud. All rights reserved.
//

#import "UIChartView.h"

@implementation UIChartView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        _data = [NSMutableArray new];
        
        // Default values of rendering parameters
        _topMargin = 20;
        _bottomMargin = 20;
        _leftMargin = 20;
        _rightMargin = 20;
        
        _yAxisWidth = 50;
        _xAxisHeight = 25;
        _axisLineWidth = 2;
        _axisLineColor = [UIColor redColor];
        
        _minValue = nil;
        _maxValue = nil;
        _maxDataCount = 120;
        _differential = NO;
        
        _title = @"";
        _titleColor = [UIColor cyanColor];
        _titleFont = [UIFont systemFontOfSize:20.0];
        
        _gridLineWidth = 1;
        _gridLineColor = [UIColor darkGrayColor];
        
        _curveLineWidth = 1;
        _curveLineColor = [UIColor greenColor];
        _textColor = [UIColor cyanColor];
        _textFont = [UIFont systemFontOfSize:15.0];
    }
    return self;
}

-(void) dealloc {
    [_data removeAllObjects];
    _data = nil;
}

- (void)drawRect:(CGRect)rect {
    // Not enough data
    if ([_data count] < 2) {
        return;
    }
    // Need 3 for diff data (the first one is not displayed)
    if (_differential && [_data count] < 3) {
        return;
    }
    // Remove if we have too much elements
    if ([_data count] > _maxDataCount)
        [_data removeObjectsInRange:NSMakeRange(0, [_data count] - _maxDataCount)];
    
    // Let's draw the chart.
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    UIGraphicsPushContext(ctx);
    //CGContextSetRGBFillColor(ctx, 1.0f, 1.0f, 1.0f, 1.0f);
    
    // The title
    NSDictionary *titleAttributes = @{NSForegroundColorAttributeName:_titleColor, NSFontAttributeName:_titleFont};
    CGSize titleSize = [_title sizeWithAttributes:titleAttributes];
    titleSize = CGSizeMake(titleSize.width, titleSize.height+5);
    CGRect titleFrame = CGRectMake(_leftMargin, _topMargin, titleSize.width, titleSize.height);
    [_title drawInRect:titleFrame withAttributes:titleAttributes];
    
    // Draw the Axis lines
    CGContextSetLineWidth(ctx, _axisLineWidth);
    CGContextSetStrokeColorWithColor(ctx, [_axisLineColor CGColor]);
    
    // The X (horizontal) axis
    CGContextMoveToPoint(ctx, _yAxisWidth+_leftMargin, self.frame.size.height-_bottomMargin-_xAxisHeight);
    CGContextAddLineToPoint(ctx, self.frame.size.width-_rightMargin, self.frame.size.height-_bottomMargin-_xAxisHeight);
    CGContextStrokePath(ctx);
    
    // The Y (vertical) axis
    CGContextMoveToPoint(ctx, _yAxisWidth+_leftMargin, _topMargin+titleSize.height);
    CGContextAddLineToPoint(ctx, _yAxisWidth+_leftMargin, self.frame.size.height-_bottomMargin-_xAxisHeight);
    CGContextStrokePath(ctx);
    
    
    
    // We start at this index in our data.
    // When we have diff display, we dont display element 0, but we use it
    // to compute diff(current, previous) of element 1
    unsigned long startElement = _differential ? 1 : 0;
    
    // get min and max values of our data
    float minValue = 0;
    float maxValue = 0;
    float previousValue = [[[_data objectAtIndex:0] objectForKey:@"value"] floatValue];
    for (unsigned long idx = startElement; idx < [_data count]; idx++) {
        NSDictionary *point = [_data objectAtIndex:idx];
        float value = [[point objectForKey:@"value"] floatValue];
        if (_differential) {
            float tmp = value;
            value = value - previousValue;
            previousValue = tmp;
        }
        if (idx == startElement) {
            maxValue = value;
            minValue = value;
        }
        maxValue = MAX(maxValue, value);
        minValue = MIN(minValue, value);
    }
    
    if (_minValue)
        minValue = [_minValue floatValue];
    if (_maxValue)
        maxValue = [_maxValue floatValue];
    
    // The real size of our graph (without margins and axis)
    float graphWidth = self.frame.size.width - _leftMargin - _rightMargin - _yAxisWidth;
    float graphHeight = self.frame.size.height - _topMargin - _bottomMargin - _xAxisHeight - titleSize.height;
    
    // scale depending on our data
    float numberOfElement = _differential ? ([_data count] - 2) : ([_data count] - 1);
    float xScale = graphWidth / numberOfElement;
    float yScale = graphHeight / (maxValue - minValue);
    
    
    // Draw the grid horizontal lines
    int gridCount = 5;
    float yGridScale = graphHeight / (float) (gridCount-1);
    for (int idx = 0; idx < gridCount; idx++) {
        float y = (self.frame.size.height - _bottomMargin - _xAxisHeight) - (idx * yGridScale);
        
        // No line if 0 or it will be over the x axis, but we still want the text
        if (idx > 0) {
            CGContextSetLineWidth(ctx, _gridLineWidth);
            CGContextSetStrokeColorWithColor(ctx, [_gridLineColor CGColor]);
            CGContextMoveToPoint(ctx, _yAxisWidth+_leftMargin, y);
            CGContextAddLineToPoint(ctx, self.frame.size.width-_rightMargin, y);
            CGContextStrokePath(ctx);
        }
        
        // Draw value text in y axis
        NSString *value = [NSString stringWithFormat:@"%.1f", ((idx * yGridScale)/yScale)+minValue];
        NSDictionary *attributes = @{NSForegroundColorAttributeName:_textColor, NSFontAttributeName:_textFont};
        CGSize size = [value sizeWithAttributes:attributes];
        
        CGRect textFrame = CGRectMake(_leftMargin+(_yAxisWidth/2.0f)-(size.width/2.0f), y-(size.height/2.0f), size.width, size.height);
        [value drawInRect:textFrame withAttributes:attributes];
    }
    
    
    // Draw the curve and timestamp values in x axis
    __block float last_x = 0;
    __block float last_y = 0;
    previousValue = [[[_data objectAtIndex:0] objectForKey:@"value"] floatValue];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm"];
    __block NSString *lastTime = @"";
    for (unsigned long idx = startElement; idx < [_data count]; idx++) {
        NSDictionary *point = [_data objectAtIndex:idx];
        float value = [[point objectForKey:@"value"] floatValue];
        if (_differential) {
            float tmp = value;
            value = value - previousValue;
            previousValue = tmp;
        }
        
        float x = _leftMargin + _yAxisWidth + (xScale * (idx-startElement));
        float y = (self.frame.size.height - _bottomMargin - _xAxisHeight) - ((value - minValue) * yScale);
        
        // Starting at the second point
        // Draw a line to the last point
        if (idx > startElement) {
            CGContextSetLineWidth(ctx, _curveLineWidth);
            CGContextSetStrokeColorWithColor(ctx, [_curveLineColor CGColor]);
            CGContextMoveToPoint(ctx, x, y);
            CGContextAddLineToPoint(ctx, last_x, last_y);
            CGContextStrokePath(ctx);
        }
        
        last_x = x;
        last_y = y;
        
        // Draw time text in x axis
        NSNumber *time = [point objectForKey:@"time"];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:[time doubleValue]];
        NSString *timeFormatted = [formatter stringFromDate:date];
        
        NSDictionary *attributes = @{NSForegroundColorAttributeName:_textColor, NSFontAttributeName:_textFont};
        CGSize size = [timeFormatted sizeWithAttributes:attributes];
        
        // Different from last time or last element
        if (![timeFormatted isEqualToString:lastTime]) {
            CGRect textFrame = CGRectMake(x-(size.width/2.0f), self.frame.size.height - _bottomMargin - (_xAxisHeight/2.0f), size.width, size.height);
            [timeFormatted drawInRect:textFrame withAttributes:attributes];
            lastTime = timeFormatted;
        }
    }
}

@end
