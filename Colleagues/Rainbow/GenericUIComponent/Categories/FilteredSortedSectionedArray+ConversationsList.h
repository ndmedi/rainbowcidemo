//
//  FilteredSortedSectionedArray+ConversationsList.h
//  Rainbow-iOS
//
//  Created by Asal Tech on 7/24/18.
//  Copyright © 2018 ALE International. All rights reserved.
//

#import "FilteredSortedSectionedArray.h"

@class Conversation;
@interface FilteredSortedSectionedArray (ConversationsList)

-(BOOL)containsConversation:(Conversation *)converastion;

@end
