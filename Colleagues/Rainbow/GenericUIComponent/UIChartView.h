//
//  ChartView.h
//  HelloWorld
//
//  Created by Thibaut Ackermann on 19/01/2017.
//  Copyright © 2017 Personal Cloud. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIChartView : UIView

// data has form of {@"value": 1, @"time": 1484746093.142090}
@property (nonatomic, strong) NSMutableArray<NSDictionary *> *data;

// Rendering parameters
@property (nonatomic) float topMargin;
@property (nonatomic) float bottomMargin;
@property (nonatomic) float leftMargin;
@property (nonatomic) float rightMargin;

@property (nonatomic) float yAxisWidth;
@property (nonatomic) float xAxisHeight;
@property (nonatomic) float axisLineWidth;
@property (nonatomic, strong) UIColor *axisLineColor;

@property (nonatomic, strong) NSNumber *minValue;
@property (nonatomic, strong) NSNumber *maxValue;
@property (nonatomic        ) unsigned int maxDataCount;
@property (nonatomic        ) BOOL differential; // The displayed value is the data value or the diff(current, previous)

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) UIColor *titleColor;
@property (nonatomic, strong) UIFont *titleFont;

@property (nonatomic) float gridLineWidth;
@property (nonatomic, strong) UIColor *gridLineColor;

@property (nonatomic) float curveLineWidth;
@property (nonatomic, strong) UIColor *curveLineColor;
@property (nonatomic, strong) UIColor *textColor;
@property (nonatomic, strong) UIFont *textFont;

@end
