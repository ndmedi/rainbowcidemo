/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "RainbowUITests.h"

#import <RainbowUnitTestFakeServer/RainbowUnitTestFakeServer.h>
#include <netdb.h>
#include <arpa/inet.h>

/*
 * Use some private Apple API to start any application from UI tests
 */
@implementation XCUIApplication(FromBundleID)
+ (instancetype)applicationWithBundleID:(NSString *) bundleID {
    XCUIApplication * application  = [[XCUIApplication alloc] performSelector:@selector(initPrivateWithPath:bundleID:)
                                                                   withObject:nil
                                                                   withObject:bundleID];
    [application launch];
    return application;
}
+ (instancetype)springBoard {
    XCUIApplication * springboard  = [[XCUIApplication alloc] performSelector:@selector(initPrivateWithPath:bundleID:)
                                                                   withObject:nil
                                                                   withObject:@"com.apple.springboard"];
    
    
    
    
    [springboard performSelector:@selector(resolve)];
    return springboard;
}
- (void)tapApplicationWithIdentifier:(NSString *)identifier {
    XCUIElement *appElement = [[self descendantsMatchingType:XCUIElementTypeAny]
                               elementMatchingPredicate:[NSPredicate predicateWithFormat:@"identifier = %@", identifier]
                               ];
    [appElement tap];
}


-(void) tapElementAndWaitForKeyboardToAppear:(XCUIElement *) element {
    XCUIElement *keyboardElement = self.keyboards.element;
    while (true) {
        [element tap];
        if ([keyboardElement exists]) {
            break;
        }
        [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
    }
}
@end


/*
 * Our super class for all Rainbow UI Tests
 */
@implementation RainbowUITests {
    HTTPServer *httpServer;
}

-(Class) server {
    return [RESTAPIServer class];
}

-(void) launchApplication {
    // launch the app with parameters
    self.app = [[XCUIApplication alloc] init];
    NSMutableDictionary *env = [NSMutableDictionary dictionaryWithDictionary:self.app.launchEnvironment];
    [env setObject:[self.server myLoginEmail] forKey:@"username"];
    [env setObject:[self.server myLoginPassword] forKey:@"password"];
    [env setObject:[NSString stringWithFormat:@"rainbow://%@:%u/unsecure", FAKE_SERVER_HOST, FAKE_SERVER_PORT] forKey:@"server"];
    self.app.launchEnvironment = env;
    [self.app launch];
    
    // The app should autologin with our creds, on the right server directly
    
    // Go through spashscreen, if visible
    if (self.app.buttons[@"Already a member"].exists) {
        [self.app.buttons[@"Already a member"] tap];
    }
    
    // Go through welcome screens, if visible
    if (self.app.buttons[@"GIVE ACCESS"].exists) {

//        [self.app.alerts[@"\u201cRainbow\u201d Would Like to Access Your Contacts"].buttons[@"OK"] tap];
//        [self.app.alerts[@"\U201cRainbow\U201d Would Like to Access the Camera"].buttons[@"OK"] tap];
//        [self.app.alerts[@"\U201cRainbow\U201d Would Like to Access Your Photos"].buttons[@"OK"] tap];
//        [self.app.alerts[@"Usage data"].buttons[@"Don't allow"] tap];
        
    
        
        // Magic answer to accept system dialogs
        // http://stackoverflow.com/a/33700623
        [self addUIInterruptionMonitorWithDescription:@"\u201cRainbow\u201d Would Like to Access Your Contacts" handler:^BOOL(XCUIElement * _Nonnull interruptingElement) {
            if(interruptingElement.buttons[@"OK"].exists){
                [interruptingElement.buttons[@"OK"] tap];
                return YES;
            }
            return NO;
        }];
        [self.app tap];
        [NSThread sleepForTimeInterval:1];
        [self.app/*@START_MENU_TOKEN@*/.buttons[@"GIVE ACCESS"]/*[[".scrollViews.buttons[@\"GIVE ACCESS\"]",".buttons[@\"GIVE ACCESS\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/ tap];
//        [self.app/*@START_MENU_TOKEN@*/.buttons[@"CONTINUE"]/*[[".scrollViews.buttons[@\"CONTINUE\"]",".buttons[@\"CONTINUE\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/ tap];
        //[[self.app coordinateWithNormalizedOffset:CGVectorMake(0, 0)] tap];
    }
}


-(void) launchApplicationWithoutAutoLogin {
    
    // launch the app with parameters -> nil login/pwd but correct server
    self.app = [[XCUIApplication alloc] init];
    NSMutableDictionary *env = [NSMutableDictionary dictionaryWithDictionary:self.app.launchEnvironment];
    [env setObject:@"" forKey:@"username"];
    [env setObject:@"" forKey:@"password"];
    [env setObject:[NSString stringWithFormat:@"rainbow://%@:%u/unsecure", FAKE_SERVER_HOST, FAKE_SERVER_PORT] forKey:@"server"];
    self.app.launchEnvironment = env;
    [self.app launch];
    
    
    // Go through spashscreen, if visible
    if (self.app.buttons[@"Already a member"].exists) {
        [self.app.buttons[@"Already a member"] tap];
    }
}


- (void)setUp {
    [super setUp];
    
    // Check if the hostname we use in correctly 127.0.0.1, or tests will not work
    struct hostent *host_entry = gethostbyname(FAKE_SERVER_HOST.UTF8String);
    NSAssert(host_entry != NULL, @"Error: you have to add entry '127.0.0.1 %@' to your /etc/hosts or UI tests will not work.", FAKE_SERVER_HOST);
    char *buff;
    buff = inet_ntoa(*((struct in_addr *)host_entry->h_addr_list[0]));
    NSAssert(strncmp("127.0.0.1", buff, strlen("127.0.0.1")) == 0, @"Error: you have to add entry '127.0.0.1 %@' to your /etc/hosts or UI tests will not work.", FAKE_SERVER_HOST);
    
    // Add myself to as user in server, so search will work
    ContactAPI *contact = [ContactAPI new];
    contact.ID = [self.server myRainbowID];
    contact.jid = [self.server myJID];
    contact.loginEmail = [self.server myLoginEmail];
    contact.firstname = @"Alice";
    contact.lastname = @"Tester";
    contact.companyId = [self.server myCompanyID];
    contact.companyName = [self.server myCompanyName];
    [self.server addContact:contact];
    
    
    // This server will fake both the REST API *and* the XMPP websocket servers.
    httpServer = [[HTTPServer alloc] init];
    [httpServer setPort:FAKE_SERVER_PORT];
    [httpServer setConnectionClass:self.server];
    
    [self startServer];
    
    // In UI tests it is usually best to stop immediately when a failure occurs.
    self.continueAfterFailure = NO;
}

- (void)tearDown {
    [self.app terminate];
    [self stopServer];
    [self.server flushData];
    [[XMPPWebSocketServer stubDescriptors] removeAllObjects];
    [[RESTAPIServer stubDescriptors] removeAllObjects];
    [super tearDown];
}

-(void) putAppToBackground {
    [XCUIDevice.sharedDevice pressButton:XCUIDeviceButtonHome];
}

-(void) putAppToForeground {
    [NSThread sleepForTimeInterval:0.2f];
    [[XCUIApplication springBoard] tapApplicationWithIdentifier:@"Rainbow"];
}

-(void) startServer {
    NSError *error = nil;
    if([httpServer start:&error]) {
        NSLog(@"Started HTTP Server on port %hu", [httpServer listeningPort]);
    } else {
        NSLog(@"Error starting HTTP Server: %@", error);
    }
}

-(void) stopServer {
    NSLog(@"Stopping HTTP Server on port %hu", [httpServer listeningPort]);
    [self.server closeWebsocket];
    [self.server clean];
    [httpServer stop];
}

-(void) addLocalContactFirstname:(NSString *) first lastname:(NSString *) last email:(NSString *) email {
    XCUIApplication *contacts = [XCUIApplication applicationWithBundleID:@"com.apple.MobileAddressBook"];
    
    [contacts.navigationBars[@"Contacts"].buttons[@"Add"] tap];
    
    [contacts.tables.cells.textFields[@"First name"] tap];
    [contacts typeText:first];
    
    [contacts.tables.cells.textFields[@"Last name"] tap];
    [contacts typeText:last];
    
    [contacts.tables.cells.staticTexts[@"add email"] tap];
    [contacts typeText:email];
    
    [contacts.navigationBars[@"New Contact"].buttons[@"Done"] tap];
    
    // Go back to contact list
    [contacts.navigationBars.buttons[@"Contacts"] tap];
    
    [contacts terminate];
}

-(void) linkLocalContactName:(NSString *) firstlast with:(NSString *) withfirstlast {
    XCUIApplication *contacts = [XCUIApplication applicationWithBundleID:@"com.apple.MobileAddressBook"];
    
    [contacts.tables.staticTexts[firstlast] tap];
    
    [contacts.navigationBars[@"CNContactView"].buttons[@"Edit"] tap];
    
    [contacts swipeUp];
    
    // Attention to non-std character for '...' at the end of 'link contacts'
    [contacts.tables.staticTexts[@"link contacts…"] tap];
    
    [contacts.tables.staticTexts[withfirstlast] tap];
    
    [contacts.navigationBars.buttons[@"Link"] tap];
    
    [contacts.navigationBars.buttons[@"Done"] tap];
    
    [contacts.navigationBars.buttons[@"Contacts"] tap];
    
    [contacts terminate];
}

-(void) removeLocalContacts {
    // prevent catastrophe.
    
    #if TARGET_IPHONE_SIMULATOR
    XCUIApplication *contacts = [XCUIApplication applicationWithBundleID:@"com.apple.MobileAddressBook"];
    
    while (!contacts.tables[@"No Contacts"].exists) {
        [[contacts.tables.cells elementBoundByIndex:0] tap];
        
        [contacts.navigationBars[@"CNContactView"].buttons[@"Edit"] tap];
        
        [contacts swipeUp];
        
        [contacts.tables.staticTexts[@"Delete Contact"] tap];
       
        [contacts.sheets.buttons[@"Delete Contact"] tap];
    }
    
    [contacts terminate];
#endif
}

@end
