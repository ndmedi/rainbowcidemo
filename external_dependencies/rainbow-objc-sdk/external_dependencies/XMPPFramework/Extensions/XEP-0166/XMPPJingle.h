//
//  XMPPJingle.h
//
//  Created by Ganvir, Manish  on 2/5/15.
//

#import <Foundation/Foundation.h>
#import "XMPPModule.h"

#import "Peer.h"


@interface XMPPJingle : XMPPModule

- (BOOL)sendSessionInitiateSDP:(NSString *)sdp to:(XMPPJID *)to sessionID:(NSString *)sessionID peer:(Peer *) peer;

-(BOOL) sendMediaPillarSessionInitiateSDP:(NSString *)sdp to:(XMPPJID *)to sessionID:(NSString *)sessionID phoneNumber:(NSString *) number;

- (BOOL)sendSessionAcceptSDP:(NSString *)sdp to:(XMPPJID *)to sessionID:(NSString *)sessionID peer:(Peer *) peer;

- (BOOL)sendSessionTerminateReason:(NSString *)reason to:(XMPPJID *)to sessionID:(NSString *)sessionID peer:(Peer *) peer;

- (BOOL)sendTransportInfoCandidate:(NSDictionary *)candidate to:(XMPPJID *)to sessionID:(NSString *)sessionID peer:(Peer *) peer;
-(BOOL) sendTransportReplaceWithSDP:(NSString *)sdp to:(XMPPJID *) to sessionID:(NSString *) sessionID peer:(Peer *) peer;
-(BOOL) sendTransportAcceptWithSDP:(NSString *) sdp to:(XMPPJID *) to sessionID:(NSString *) sessionID peer:(Peer *) peer;
-(BOOL) sendContentAddWithSDP:(NSString *) sdp to:(XMPPJID *) to sessionID:(NSString *) sessionID peer:(Peer *) peer;
-(BOOL) sendContentModifyWithSDP:(NSString *) sdp to:(XMPPJID *) to sessionID:(NSString *) sessionID peer:(Peer *) peer;
-(BOOL) sendContentRemoveWithSDP:(NSString *) sdp to:(XMPPJID *) to sessionID:(NSString *) sessionID peer:(Peer *) peer;
-(BOOL) sendContentAcceptWithSDP:(NSString *) sdp to:(XMPPJID *) to sessionID:(NSString *) sessionID peer:(Peer *) peer;
@end


@protocol XMPPJingleDelegate <NSObject>

- (void)xmppJingle:(XMPPJingle *) xmppJingle didReceiveSessionInitiate:(NSString *) sessionID sdp:(NSString *)sdp from:(XMPPJID *)from mediaType:(NSString *) mediaType publisherID:(NSString *)publisher;
- (void)xmppJingle:(XMPPJingle *) xmppJingle didReceiveSessionAccept:(NSString *) sessionID sdp:(NSString *)sdp from:(XMPPJID *)from mediaType:(NSString *) mediaType publisherID:(NSString *)publisher;
- (void)xmppJingle:(XMPPJingle *) xmppJingle didReceiveSessionTerminate:(NSString *) sessionID from:(XMPPJID *)from mediaType:(NSString *) mediaType publisherID:(NSString *)publisher;
- (void)xmppJingle:(XMPPJingle *) xmppJingle didReceiveTransportInfo:(NSString *) sessionID candidate:(NSDictionary *)candidate from:(XMPPJID *)from mediaType:(NSString *) mediaType publisherID:(NSString *)publisher;
- (void)xmppJingle:(XMPPJingle *) xmppJingle didReceiveTransportAccept:(NSString *) sessionID sdp:(NSString *) sdp from:(XMPPJID *)from mediaType:(NSString *) mediaType publisherID:(NSString *)publisher;
- (void)xmppJingle:(XMPPJingle *) xmppJingle didReceiveTransportReplace:(NSString *) sessionID sdp:(NSString *) sdp from:(XMPPJID *)from mediaType:(NSString *) mediaType publisherID:(NSString *)publisher;
- (void)xmppJingle:(XMPPJingle *) xmppJingle didReceiveContentAdd:(NSString *) sessionID sdp:(NSString *)sdp from:(XMPPJID *)from mediaType:(NSString *) mediaType publisherID:(NSString *)publisher;

- (void)xmppJingle:(XMPPJingle *) xmppJingle didReceiveContentModify:(NSString *) sessionID sdp:(NSString *)sdp from:(XMPPJID *)from mediaType:(NSString *) mediaType publisherID:(NSString *)publisher;
- (void)xmppJingle:(XMPPJingle *) xmppJingle didReceiveContentRemove:(NSString *) sessionID sdp:(NSString *)sdp from:(XMPPJID *)from mediaType:(NSString *) mediaType publisherID:(NSString *)publisher;
- (void)xmppJingle:(XMPPJingle *) xmppJingle didReceiveContentAccept:(NSString *) sessionID sdp:(NSString *)sdp from:(XMPPJID *)from mediaType:(NSString *) mediaType publisherID:(NSString *)publisher;
@end
