//
//  XMPPJingle.m
//
//  Created by Ganvir, Manish  on 2/5/15.
//

/* The purpose of this extension is to use jingle protocol as described in
 * http://xmpp.org/extensions/xep-0166.html and handle the urn urn:xmpp:jingle
 * and manage the messages such as session-initiate, session-accept
 * session-terminate.
 * This extension will also extract SDP and candidates so that a SIP/Webrtc
 * call can work.
 * It will provide an extension which will do translation of XMPP based message
 * to \r\n based SDP messages.
 * The delegate will send the SDP messages to the application later and API
 * calls will allow to send SDP message to XMPP
 */
#import "XMPP.h"
#import "XMPPFramework.h"
#import "XMPPJingle.h"
#import "XMPPJingleSDP.h"


@interface XMPPJingle() {
    XMPPJingleSDPUtil *sdpUtil;
}
@end


@implementation XMPPJingle

#pragma mark - XMPP module related methods

// init
- (id)init {
    return [self initWithDispatchQueue:nil];
}

// init with queue
- (id)initWithDispatchQueue:(dispatch_queue_t)queue {
    if ((self = [super initWithDispatchQueue:queue])) {
        sdpUtil = [[XMPPJingleSDPUtil alloc] init];
    }
    return self;
}

// Activate module
- (BOOL)activate:(XMPPStream *)aXmppStream {
    if ([super activate:aXmppStream]) {
#ifdef _XMPP_CAPABILITIES_H
        [xmppStream autoAddDelegate:self delegateQueue:moduleQueue toModulesOfClass:[XMPPCapabilities class]];
#endif
        return YES;
    }
    return NO;
}

// Deactivate module
- (void)deactivate {
#ifdef _XMPP_CAPABILITIES_H
    [xmppStream removeAutoDelegate:self delegateQueue:moduleQueue fromModulesOfClass:[XMPPCapabilities class]];
#endif
    [super deactivate];
}

#ifdef _XMPP_CAPABILITIES_H
/**
 * If an XMPPCapabilites instance is used we want to advertise our support for jingle.
 **/
- (void)xmppCapabilities:(XMPPCapabilities *)sender collectingMyCapabilities:(NSXMLElement *)query {
    // This method is invoked on the moduleQueue.
    // <query xmlns="http://jabber.org/protocol/disco#info">
    //   ...
    //   <feature var="urn:xmpp:jingle:1"/>
    //   ...
    // </query>
    
    NSString *jingleNS = @"urn:xmpp:jingle";
    NSArray *listOfSupportedFeatures = @[XEP_0166_XMLNS,
                                         [NSString stringWithFormat:@"%@:apps:rtp:1",jingleNS],
                                         [NSString stringWithFormat:@"%@:apps:rtp:audio",jingleNS],
                                         [NSString stringWithFormat:@"%@:apps:rtp:video",jingleNS],
                                         [NSString stringWithFormat:@"%@:transports:raw-udp:1", jingleNS],
                                         [NSString stringWithFormat:@"%@:transports:ice-udp:1", jingleNS],
                                         @"urn:ietf:rfc:3264",
                                         @"urn:ietf:rfc:5576",
                                         @"urn:ietf:rfc:5888",
                                         [NSString stringWithFormat:@"%@:apps:rtp:rtcp-fb:0", jingleNS],
                                         [NSString stringWithFormat:@"%@:apps:rtp:rtp-hdrext:0", jingleNS],
                                         @"urn:xmpp:tmp:jingle:apps:dtls:0",
                                         [NSString stringWithFormat:@"%@:apps:rtp:zrtp:1", jingleNS],
                                         ];
    /*
    <feature var="urn:xmpp:jingle:1"/>
    <feature var="urn:xmpp:jingle:apps:rtp:1"/>
    <feature var="urn:xmpp:jingle:apps:rtp:audio"/>
    <feature var="urn:xmpp:jingle:apps:rtp:video"/>
    <feature var="urn:xmpp:jingle:transports:raw-udp:1"/>
    <feature var="urn:xmpp:jingle:transports:ice-udp:1"/>
    <feature var="urn:ietf:rfc:3264"/>
    <feature var="urn:ietf:rfc:5576"/>
    <feature var="urn:ietf:rfc:5888"/>
    <feature var="urn:xmpp:jingle:apps:rtp:rtcp-fb:0"/>
    <feature var="urn:xmpp:jingle:apps:rtp:rtp-hdrext:0"/>
    <feature var="urn:xmpp:tmp:jingle:apps:dtls:0"/>
    <feature var="urn:xmpp:jingle:apps:rtp:zrtp:1"/>
    <feature var="http://jabber.org/protocol/jinglenodes"/>
     */
    
    for (NSString *theFeature in listOfSupportedFeatures) {
        NSXMLElement *feature = [NSXMLElement elementWithName:@"feature"];
        [feature addAttributeWithName:@"var" stringValue:theFeature];
        [query addChild:feature];
    }
}
#endif


#pragma mark - Public methods

- (BOOL)sendSessionInitiateSDP:(NSString *)sdp to:(XMPPJID *)to sessionID:(NSString *)sessionID peer:(Peer *) peer {
    XMPPIQ *iq = [sdpUtil SDPToXMPP:sdp action:@"session-initiate" initiator:[xmppStream myJID] target:to UID:[xmppStream generateUUID] SID:sessionID peer:peer];
    if (!iq) {
        NSLog(@"Error: cannot send jingle session-initiate IQ");
        return NO;
    }
    [xmppStream sendElement:iq];
    return YES;
}

- (BOOL)sendSessionAcceptSDP:(NSString *)sdp to:(XMPPJID *)to sessionID:(NSString *)sessionID peer:(Peer *) peer {
    XMPPIQ *iq = [sdpUtil SDPToXMPP:sdp action:@"session-accept" initiator:[xmppStream myJID] target:to UID:[xmppStream generateUUID] SID:sessionID peer:peer];
    if (!iq) {
        NSLog(@"Error: cannot send jingle session-accept IQ");
        return NO;
    }
    [xmppStream sendElement:iq];
    return YES;
}

- (BOOL)sendSessionTerminateReason:(NSString *)reason to:(XMPPJID *)to sessionID:(NSString *)sessionID peer:(Peer *)peer {
    NSXMLElement *jingleElement = [NSXMLElement elementWithName:@"jingle"];
    [jingleElement addAttributeWithName:@"xmlns" stringValue:@"urn:xmpp:jingle:1"];
    [jingleElement addAttributeWithName:@"sid" stringValue:sessionID];
    [jingleElement addAttributeWithName:@"action" stringValue:@"session-terminate"];
    [jingleElement addAttributeWithName:@"initiator" stringValue:[[xmppStream myJID] full]];
    
    NSXMLElement *reasonElement = [NSXMLElement elementWithName:@"reason"];
    NSXMLElement *successElement = [NSXMLElement elementWithName:reason];
    
    [reasonElement addChild:successElement];
    [jingleElement addChild:reasonElement];
    
    XMPPIQ *iq  = [[XMPPIQ alloc]initWithType:@"set" to:to elementID:[xmppStream generateUUID] child:[jingleElement copy]];
    if (!iq) {
        NSLog(@"Error: cannot send jingle session-terminate IQ");
        return NO;
    }
    [xmppStream sendElement:iq];
    return YES;
}

- (BOOL)sendTransportInfoCandidate:(NSDictionary *)candidate to:(XMPPJID *)to sessionID:(NSString *)sessionID peer:(Peer *)peer {
    XMPPIQ *iq = [sdpUtil CandidateToXMPP:candidate action:@"transport-info" initiator:to/*[myStream myJID] ?*/ target:to UID:[xmppStream generateUUID] SID:sessionID];
    if (!iq) {
        NSLog(@"Error: cannot send jingle transport-info IQ");
        return NO;
    }
    [xmppStream sendElement:iq];
    return YES;
}

-(BOOL) sendTransportReplaceWithSDP:(NSString *)sdp to:(XMPPJID *) to sessionID:(NSString *) sessionID peer:(Peer *) peer {
    XMPPIQ *iq = [sdpUtil SDPToXMPP:sdp action:@"transport-replace" initiator:[xmppStream myJID] target:to UID:[xmppStream generateUUID] SID:sessionID peer:peer];
    if (!iq) {
        NSLog(@"Error: cannot send jingle transport-replace IQ");
        return NO;
    }
    [xmppStream sendElement:iq];
    return YES;
}

-(BOOL) sendTransportAcceptWithSDP:(NSString *) sdp to:(XMPPJID *) to sessionID:(NSString *) sessionID peer:(Peer *) peer {
    XMPPIQ *iq = [sdpUtil SDPToXMPP:sdp action:@"transport-accept" initiator:[xmppStream myJID] target:to UID:[xmppStream generateUUID] SID:sessionID peer:peer];
    if (!iq) {
        NSLog(@"Error: cannot send jingle transport-accept IQ");
        return NO;
    }
    [xmppStream sendElement:iq];
    return YES;
}

-(BOOL) sendContentAddWithSDP:(NSString *) sdp to:(XMPPJID *) to sessionID:(NSString *) sessionID peer:(Peer *) peer {
    XMPPIQ *iq = [sdpUtil SDPToXMPP:sdp action:@"content-add" initiator:[xmppStream myJID] target:to UID:[xmppStream generateUUID] SID:sessionID peer:peer];
    if (!iq) {
        NSLog(@"Error: cannot send jingle content-accept IQ");
        return NO;
    }
    [xmppStream sendElement:iq];
    return YES;
}

-(BOOL) sendContentModifyWithSDP:(NSString *) sdp to:(XMPPJID *) to sessionID:(NSString *) sessionID peer:(Peer *) peer{
    XMPPIQ *iq = [sdpUtil SDPToXMPP:sdp action:@"content-modify" initiator:[xmppStream myJID] target:to UID:[xmppStream generateUUID] SID:sessionID peer:peer];
    if (!iq) {
        NSLog(@"Error: cannot send jingle content-accept IQ");
        return NO;
    }
    [xmppStream sendElement:iq];
    return YES;
}

-(BOOL) sendContentRemoveWithSDP:(NSString *) sdp to:(XMPPJID *) to sessionID:(NSString *) sessionID peer:(Peer *) peer{
    XMPPIQ *iq = [sdpUtil SDPToXMPP:sdp action:@"content-remove" initiator:[xmppStream myJID] target:to UID:[xmppStream generateUUID] SID:sessionID peer:peer];
    if (!iq) {
        NSLog(@"Error: cannot send jingle content-accept IQ");
        return NO;
    }
    [xmppStream sendElement:iq];
    return YES;
}


-(BOOL) sendContentAcceptWithSDP:(NSString *) sdp to:(XMPPJID *) to sessionID:(NSString *) sessionID peer:(Peer *) peer {
    XMPPIQ *iq = [sdpUtil SDPToXMPP:sdp action:@"content-accept" initiator:[xmppStream myJID] target:to UID:[xmppStream generateUUID] SID:sessionID peer:peer];
    if (!iq) {
        NSLog(@"Error: cannot send jingle content-accept IQ");
        return NO;
    }
    [xmppStream sendElement:iq];
    return YES;
}

- (BOOL) sendMediaPillarSessionInitiateSDP:(NSString *)sdp to:(XMPPJID *)to sessionID:(NSString *)sessionID phoneNumber:(NSString *)number {
    
    NSXMLElement *mediaPillarElement = [NSXMLElement elementWithName:@"mediapillar" xmlns:@"urn:xmpp:janus:1"];
    NSXMLElement *calledNumberElement = [NSXMLElement elementWithName:@"callednumber" stringValue:number];
    
    [mediaPillarElement addChild:calledNumberElement];
    
    XMPPIQ *iq = [sdpUtil SDPToXMPP:sdp action:@"session-initiate" initiator:[xmppStream myJID] target:to UID:[xmppStream generateUUID] SID:sessionID peer:nil];
    
    if (!iq) {
        NSLog(@"Error: cannot send jingle session-initiate IQ");
        return NO;
    }
    
    NSXMLElement *jingleElement = [iq elementForName:@"jingle"];
    [jingleElement addChild:mediaPillarElement];
    
    NSLog(@"[MEDIAPILLAR] Sending session-initiate IQ %@", iq);
    
    [xmppStream sendElement:iq];
    return YES;
    
}


#pragma mark - Internal methods

- (void)onSessionInitiate:(XMPPIQ *)iq sessionID:(NSString *)sessionID {
    NSString *sdp = [sdpUtil XMPPToSDP:iq];
    NSXMLElement *jingle = [iq elementForName:@"jingle"];
    NSString *localType = [jingle attributeStringValueForName:@"localType"];
    NSLog(@"onSessionInitiate : FOUND LOCAL TYPE %@", localType);
    NSString *publisherID = nil;
    
    NSXMLElement *conference = [jingle elementForName:@"conference" xmlns:@"urn:xmpp:janus:1"];
    if(conference){
        publisherID = [conference attributeForName:@"publisherId"].stringValue;
    }
    [multicastDelegate xmppJingle:self didReceiveSessionInitiate:sessionID sdp:sdp from:[iq from] mediaType:localType publisherID:publisherID];
}

- (void)onSessionAccept:(XMPPIQ *)iq sessionID:(NSString *)sessionID {
    NSString *sdp = [sdpUtil XMPPToSDP:iq];
    NSString *localType = [[iq elementForName:@"jingle"] attributeStringValueForName:@"localType"];
    NSLog(@"onSessionAccept : FOUND LOCAL TYPE %@", localType);
    NSString *publisherID = nil;
    NSXMLElement *conference = [iq elementForName:@"conference" xmlns:@"urn:xmpp:janus:1"];
    if(conference){
        publisherID = [conference attributeForName:@"publisherId"].stringValue;
    }
    [multicastDelegate xmppJingle:self didReceiveSessionAccept:sessionID sdp:sdp from:[iq from] mediaType:localType publisherID:publisherID];
}

- (void)onSessionTerminate:(XMPPIQ *)iq sessionID:(NSString *)sessionID {
    NSString *localType = [[iq elementForName:@"jingle"] attributeStringValueForName:@"localType"];
    NSLog(@"onSessionTerminate : FOUND LOCAL TYPE %@", localType);
    NSString *publisherID = nil;
    NSXMLElement *conference = [iq elementForName:@"conference" xmlns:@"urn:xmpp:janus:1"];
    if(conference){
        publisherID = [conference attributeForName:@"publisherId"].stringValue;
    }
    [multicastDelegate xmppJingle:self didReceiveSessionTerminate:sessionID from:[iq from] mediaType:localType publisherID:publisherID];
}

- (void)onTransportReplace:(XMPPIQ *)iq sessionID:(NSString *)sessionID {
    NSString *sdp = [sdpUtil XMPPToSDP:iq];
    NSString *localType = [[iq elementForName:@"jingle"] attributeStringValueForName:@"localType"];
    NSLog(@"onTransportReplace : FOUND LOCAL TYPE %@", localType);
    NSString *publisherID = nil;
    NSXMLElement *conference = [iq elementForName:@"conference" xmlns:@"urn:xmpp:janus:1"];
    if(conference){
        publisherID = [conference attributeForName:@"publisherId"].stringValue;
    }
    [multicastDelegate xmppJingle:self didReceiveTransportReplace:sessionID sdp:sdp from:[iq from] mediaType:localType publisherID:publisherID];
}

- (void)onTransportInfo:(XMPPIQ *)iq sessionID:(NSString *)sessionID {
    NSDictionary *candidate = [sdpUtil XMPPToCandidate:iq];
    NSString *localType = [[iq elementForName:@"jingle"] attributeStringValueForName:@"localType"];
    NSLog(@"onTransportInfo : FOUND LOCAL TYPE %@", localType);
    NSString *publisherID = nil;
    NSXMLElement *conference = [iq elementForName:@"conference" xmlns:@"urn:xmpp:janus:1"];
    if(conference){
        publisherID = [conference attributeForName:@"publisherId"].stringValue;
    }
    [multicastDelegate xmppJingle:self didReceiveTransportInfo:sessionID candidate:candidate from:[iq from] mediaType:localType publisherID:publisherID];
}

-(void) onTransportAccept:(XMPPIQ *) iq sessionID:(NSString *) sessionID {
    NSString *sdp = [sdpUtil XMPPToSDP:iq];
    NSString *localType = [[iq elementForName:@"jingle"] attributeStringValueForName:@"localType"];
    NSLog(@"onTransportAccept : FOUND LOCAL TYPE %@", localType);
    NSString *publisherID = nil;
    NSXMLElement *conference = [iq elementForName:@"conference" xmlns:@"urn:xmpp:janus:1"];
    if(conference){
        publisherID = [conference attributeForName:@"publisherId"].stringValue;
    }
    [multicastDelegate xmppJingle:self didReceiveTransportAccept:sessionID sdp:sdp from:[iq from] mediaType:localType publisherID:publisherID];
}

- (void)onContentAdd:(XMPPIQ *)iq sessionID:(NSString *)sessionID {
    NSString *sdp = [sdpUtil XMPPToSDP:iq];
    NSString *localType = [[iq elementForName:@"jingle"] attributeStringValueForName:@"localType"];
    NSLog(@"onContentAdd : FOUND LOCAL TYPE %@", localType);
    NSString *mediaType = [[iq elementForName:@"jingle"] attributeStringValueForName:@"mediaType"];
    NSLog(@"onContentAdd : FOUND MEDIA TYPE %@", mediaType);
    NSString *publisherID = nil;
    NSXMLElement *conference = [iq elementForName:@"conference" xmlns:@"urn:xmpp:janus:1"];
    if(conference){
        publisherID = [conference attributeForName:@"publisherId"].stringValue;
    }
    [multicastDelegate xmppJingle:self didReceiveContentAdd:sessionID sdp:sdp from:[iq from] mediaType:mediaType publisherID:publisherID];
}

-(void) onContentModify:(XMPPIQ *) iq sessionID:(NSString *) sessionID {
    NSString *sdp = [sdpUtil XMPPToSDP:iq];
    NSString *localType = [[iq elementForName:@"jingle"] attributeStringValueForName:@"localType"];
    NSLog(@"onContentModify : FOUND LOCAL TYPE %@", localType);
    NSString *mediaType = [[iq elementForName:@"jingle"] attributeStringValueForName:@"mediaType"];
    NSLog(@"onContentAdd : FOUND MEDIA TYPE %@", mediaType);
    NSString *publisherID = nil;
    NSXMLElement *conference = [iq elementForName:@"conference" xmlns:@"urn:xmpp:janus:1"];
    if(conference){
        publisherID = [conference attributeForName:@"publisherId"].stringValue;
    }
    [multicastDelegate xmppJingle:self didReceiveContentModify:sessionID sdp:sdp from:[iq from] mediaType:mediaType publisherID:publisherID];
}

-(void) onContentRemove:(XMPPIQ *) iq sessionID:(NSString *) sessionID {
    NSString *sdp = [sdpUtil XMPPToSDP:iq];
    NSString *localType = [[iq elementForName:@"jingle"] attributeStringValueForName:@"localType"];
    NSLog(@"onContentRemove : FOUND LOCAL TYPE %@", localType);
    NSString *mediaType = [[iq elementForName:@"jingle"] attributeStringValueForName:@"mediaType"];
    NSLog(@"onContentAdd : FOUND MEDIA TYPE %@", mediaType);
    NSString *publisherID = nil;
    NSXMLElement *conference = [iq elementForName:@"conference" xmlns:@"urn:xmpp:janus:1"];
    if(conference){
        publisherID = [conference attributeForName:@"publisherId"].stringValue;
    }
    [multicastDelegate xmppJingle:self didReceiveContentRemove:sessionID sdp:sdp from:[iq from] mediaType:mediaType publisherID:publisherID];
}

-(void) onContentAccept:(XMPPIQ *) iq sessionID:(NSString *) sessionID {
    NSString *sdp = [sdpUtil XMPPToSDP:iq];
    NSString *localType = [[iq elementForName:@"jingle"] attributeStringValueForName:@"localType"];
    NSLog(@"onContentAccept : FOUND LOCAL TYPE %@", localType);
    NSString *mediaType = [[iq elementForName:@"jingle"] attributeStringValueForName:@"mediaType"];
    NSLog(@"onContentAdd : FOUND MEDIA TYPE %@", mediaType);
    NSString *publisherID = nil;
    NSXMLElement *conference = [iq elementForName:@"conference" xmlns:@"urn:xmpp:janus:1"];
    if(conference){
        publisherID = [conference attributeForName:@"publisherId"].stringValue;
    }
    [multicastDelegate xmppJingle:self didReceiveContentAccept:sessionID sdp:sdp from:[iq from] mediaType:mediaType publisherID:publisherID];
}

-(void) onSessionInfo:(XMPPIQ *)iq sessionID:(NSString *)sessionID {
}


# pragma mark - XMPP stream methods

// Called when a iq message is received
- (BOOL)xmppStream:(XMPPStream *)sender didReceiveIQ:(XMPPIQ *)iq {

    // Check if it is a jingle message
    NSXMLElement *jingleElement = [iq elementForName:@"jingle" xmlns:XEP_0166_XMLNS];
    
    // We are only looking for jingle related messages
    if (jingleElement == nil)
        return NO;
    
    // Check the type of the message
    NSString *type = [jingleElement attributeStringValueForName:@"action"];
    
    // If the message doesnt have a type, dont parser
    if (type == nil)
        return NO;
    
    NSString *sessionID = [jingleElement attributeStringValueForName:@"sid"];
    
    if (!sessionID) {
        NSLog(@"Error: <jingle> element MUST have a sid attribute %@", iq);
        return NO;
    }
    
    // We handle only the ones we need.
    if ([type isEqualToString:@"session-initiate"]) {
        [self onSessionInitiate:iq sessionID:sessionID];
    }
    else if ([type isEqualToString:@"session-accept"]) {
        [self onSessionAccept:iq sessionID:sessionID];
    }
    else if ([type isEqualToString:@"session-terminate"]) {
        [self onSessionTerminate:iq sessionID:sessionID];
    }
    else if ([type isEqualToString:@"transport-replace"])
        [self onTransportReplace:iq sessionID:sessionID];
    else if ([type isEqualToString:@"transport-info"])
        [self onTransportInfo:iq sessionID:sessionID];
    else if ([type isEqualToString:@"transport-accept"])
        [self onTransportAccept:iq sessionID:sessionID];
    else if ([type isEqualToString:@"content-add"]) {
        // Rainbow web-client uses this action for escalade
        [self onContentAdd:iq sessionID:sessionID];
    } else if([type isEqualToString:@"content-modify"]) {
        [self onContentModify:iq sessionID:sessionID];
    } else if([type isEqualToString:@"content-remove"]) {
        [self onContentRemove:iq sessionID:sessionID];
    } else if([type isEqualToString:@"content-accept"]) {
        [self onContentAccept:iq sessionID:sessionID];
    } else if([type isEqualToString:@"session-info"]){
        // Deal with session-info
        NSLog(@"Receive session-info");
        [self onSessionInfo:iq sessionID:sessionID];
    }
    else {
        NSLog(@"Jingle: unhandled IQ %@", iq);
        return NO;
    }

    XMPPIQ *iqResponse = [XMPPIQ iqWithType:@"result" to:[iq from] elementID:[iq elementID]];
    [xmppStream sendElement:iqResponse];

    return YES;
}

@end
