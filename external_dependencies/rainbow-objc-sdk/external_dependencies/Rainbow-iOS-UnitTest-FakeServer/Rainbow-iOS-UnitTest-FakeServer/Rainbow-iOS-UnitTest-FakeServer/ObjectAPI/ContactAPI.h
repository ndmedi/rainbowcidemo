/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ObjectAPI.h"
#import <UIKit/UIKit.h>

@interface ContactAPI : ObjectAPI

/* Easy to copy/paste :

*/

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *jid;
@property (nonatomic, strong) NSString *loginEmail;
@property (nonatomic, strong) NSString *firstname;
@property (nonatomic, strong) NSString *lastname;
@property (nonatomic) BOOL inRoster;
@property (nonatomic) BOOL isPending;
@property (nonatomic) BOOL hasSubscribe;
@property (nonatomic) BOOL isBot;
@property (nonatomic, strong) NSString *presence;
@property (nonatomic, strong) NSMutableArray<NSString *> *emails;
@property (nonatomic, strong) NSMutableArray<NSString *> *phoneNumbers;
@property (nonatomic, strong) UIColor *avatarColor;

@property (nonatomic, strong) NSString *companyId;
@property (nonatomic, strong) NSString *companyName;

@end
