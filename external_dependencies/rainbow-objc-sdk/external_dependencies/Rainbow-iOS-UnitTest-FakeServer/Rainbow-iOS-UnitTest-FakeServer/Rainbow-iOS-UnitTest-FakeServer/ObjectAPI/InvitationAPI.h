/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ObjectAPI.h"

@interface InvitationAPI : ObjectAPI

/* Easy to copy/paste :

 "id": "57cd5922d341df5812bbcb72",
 "invitedUserId": "573b46a305a4c22a19b216ce",
 "invitedUserEmail": "bob@company.com",
 "invitingUserId": "5703d4829ccf39843c7ef89b",
 "invitingUserEmail": "alice@my-company.com",
 "requestedNotificationLanguage": "fr",
 "invitingDate": "2016-09-28T16:31:36.881Z",
 "lastNotificationDate": "2016-09-28T16:31:36.879Z",
 "acceptationDate": "2016-09-29T15:18:31.342Z",
 "status": "pending",
 "type": "visibility",

*/

@property (nonatomic        ) BOOL received;
@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *invitedUserId;
@property (nonatomic, strong) NSString *invitedUserEmail;
@property (nonatomic, strong) NSString *invitingUserId;
@property (nonatomic, strong) NSString *invitingUserEmail;
@property (nonatomic, strong) NSString *invitingDate;
@property (nonatomic, strong) NSString *status;

@end
