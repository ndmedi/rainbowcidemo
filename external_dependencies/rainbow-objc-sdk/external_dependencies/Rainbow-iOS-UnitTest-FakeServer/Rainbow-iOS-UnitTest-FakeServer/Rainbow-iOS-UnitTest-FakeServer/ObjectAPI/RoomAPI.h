/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ObjectAPI.h"

@interface RoomAPI : ObjectAPI

/* Easy to copy/paste :

*/

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *jid;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *topic;
@property (nonatomic, strong) NSString *visibility;
@property (nonatomic, strong) NSString *creationDate;
@property (nonatomic, strong) NSString *creator;
@property (nonatomic, strong) NSMutableArray<NSMutableDictionary *> *users;


-(NSDictionary *) addUser:(NSString *) userID privilege:(NSString *) privilege status:(NSString *) status;

@end
