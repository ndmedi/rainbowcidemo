/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "WebSocket.h"


typedef BOOL(^XMPPTestBlock)(NSDictionary * request);
typedef NSArray<NSString*> *(^XMPPResponseBlock)(NSDictionary * request);

@interface XMPPStubDescriptor : NSObject
@property(atomic, copy) XMPPTestBlock testBlock;
@property(atomic, copy) XMPPResponseBlock responseBlock;

+(XMPPStubDescriptor*)xmppStubPassing:(XMPPTestBlock)testBlock withResponse:(XMPPResponseBlock)responseBlock;
@end

/*
 * This will be our XMPP websocket server
 * Obviously we'll be able to drive this server
 * from the tests, to make them predictible
 */
@interface XMPPWebSocketServer : WebSocket

// Set only on creation :
@property (nonatomic, strong) NSString *barejid;

// Do not modify :
@property (nonatomic, strong) NSString *fulljid;

@property (nonatomic, strong) NSString *rainbowID;

+(NSMutableArray<XMPPStubDescriptor *> *) stubDescriptors;

@end

