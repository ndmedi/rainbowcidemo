//
//  APNS.h
//  APNS Pusher
//
//  Created by Simon Blommegård on 2011-10-13.
//  Copyright (c) 2011 Simon Blommegård. All rights reserved.
//

// COPIED FROM https://github.com/KnuffApp/Knuff

#import <UIKit/UIKit.h>

@interface SBAPNS : NSObject
@property (nonatomic, assign, readonly, getter = isReady) BOOL ready;
@property (nonatomic, assign) SecIdentityRef identity;
@property (nonatomic, assign, getter = isSandbox) BOOL sandbox;
@property (nonatomic, copy) void(^errorBlock)(uint8_t status, NSString *description, uint32_t identifier);

- (void)pushPayload:(NSDictionary *)payload withToken:(NSString *)token;
@end
