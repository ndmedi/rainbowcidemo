/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>

@interface NSDate (JSONString)

// Return date following RFC 3339 format
/**
 *  Return a date for the given string following RFC3339
 *
 *  @param string the string to tranform
 *
 *  @return the date following RFC3339, return `nil` if the given string doesn't follow RFC3339
 */
+ (NSDate *)dateFromJSONString:(NSString *)string;

/**
 *  Return a string for the given date following RFC3339
 *
 *  @param date the date to transform
 *
 *  @return the string following RFC3339.
 */
+(NSString *) jsonStringFromDate:(NSDate*) date;
@end
