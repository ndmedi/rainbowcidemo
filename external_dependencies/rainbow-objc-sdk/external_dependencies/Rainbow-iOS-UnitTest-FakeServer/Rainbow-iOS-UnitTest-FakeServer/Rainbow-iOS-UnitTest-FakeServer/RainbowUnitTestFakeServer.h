/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */


#import <Foundation/Foundation.h>

FOUNDATION_EXPORT double RainbowUnitTestFakeServerVersionNumber;

FOUNDATION_EXPORT const unsigned char RainbowUnitTestFakeServerVersionString[];

// Import from CocoaHTTPServer
#import <RainbowUnitTestFakeServer/HTTPAuthenticationRequest.h>
#import <RainbowUnitTestFakeServer/HTTPConnection.h>
#import <RainbowUnitTestFakeServer/HTTPLogging.h>
#import <RainbowUnitTestFakeServer/HTTPMessage.h>
#import <RainbowUnitTestFakeServer/HTTPResponse.h>
#import <RainbowUnitTestFakeServer/HTTPServer.h>
#import <RainbowUnitTestFakeServer/MultipartFormDataParser.h>
#import <RainbowUnitTestFakeServer/MultipartMessageHeader.h>
#import <RainbowUnitTestFakeServer/MultiPartMessageHeaderField.h>
#import <RainbowUnitTestFakeServer/HTTPAsyncFileResponse.h>
#import <RainbowUnitTestFakeServer/HTTPDataResponse.h>
#import <RainbowUnitTestFakeServer/HTTPDynamicFileResponse.h>
#import <RainbowUnitTestFakeServer/HTTPErrorResponse.h>
#import <RainbowUnitTestFakeServer/HTTPRedirectResponse.h>
#import <RainbowUnitTestFakeServer/WebSocket.h>
#import <RainbowUnitTestFakeServer/DAVConnection.h>
#import <RainbowUnitTestFakeServer/DAVResponse.h>
#import <RainbowUnitTestFakeServer/DELETEResponse.h>
#import <RainbowUnitTestFakeServer/PUTResponse.h>
#import <RainbowUnitTestFakeServer/GCDAsyncSocket.h>
#import <RainbowUnitTestFakeServer/DDAbstractDatabaseLogger.h>
#import <RainbowUnitTestFakeServer/DDASLLogger.h>
#import <RainbowUnitTestFakeServer/DDFileLogger.h>
#import <RainbowUnitTestFakeServer/DDLog.h>
#import <RainbowUnitTestFakeServer/DDTTYLogger.h>
#import <RainbowUnitTestFakeServer/ContextFilterLogFormatter.h>
#import <RainbowUnitTestFakeServer/DispatchQueueLogFormatter.h>

#import <RainbowUnitTestFakeServer/ObjectAPI.h>

#import <RainbowUnitTestFakeServer/ContactAPI.h>
#import <RainbowUnitTestFakeServer/ConversationAPI.h>
#import <RainbowUnitTestFakeServer/InvitationAPI.h>
#import <RainbowUnitTestFakeServer/MessageAPI.h>
#import <RainbowUnitTestFakeServer/ObjectAPIStore.h>
#import <RainbowUnitTestFakeServer/RoomAPI.h>
#import <RainbowUnitTestFakeServer/SettingsAPI.h>
#import <RainbowUnitTestFakeServer/NSDictionary+JSON.h>
#import <RainbowUnitTestFakeServer/NSDictionary+JSONString.h>
#import <RainbowUnitTestFakeServer/NSDate+JSONString.h>
#import <RainbowUnitTestFakeServer/NSArray+JSONString.h>
#import <RainbowUnitTestFakeServer/NSData+JSON.h>
#import <RainbowUnitTestFakeServer/RESTAPIServer.h>
#import <RainbowUnitTestFakeServer/SBAPNS.h>
#import <RainbowUnitTestFakeServer/XMPPWebSocketServer.h>

