//
//  LinqToObjectiveC-OSX-dynamic.h
//  LinqToObjectiveC-OSX-dynamic
//
//  Created by Jerome Heymonet on 23/11/2015.
//  Copyright © 2015 scottlogic. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for LinqToObjectiveC-OSX-dynamic.
FOUNDATION_EXPORT double LinqToObjectiveC_OSX_dynamicVersionNumber;

//! Project version string for LinqToObjectiveC-OSX-dynamic.
FOUNDATION_EXPORT const unsigned char LinqToObjectiveC_OSX_dynamicVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <LinqToObjectiveC_OSX_dynamic/PublicHeader.h>


