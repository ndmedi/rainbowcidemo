//
//  NSString+Emoji.m
//
//
//  Created by Valerio Mazzeo on 24/04/13.
//  Copyright (c) 2013 Valerio Mazzeo. All rights reserved.
//

#import "NSString+Emoji.h"

@implementation NSString (Emoji)

static NSDictionary * s_emoticonToUnicode = nil;
static NSString * s_supportedCharset = @":;=8B>3O^-o<(";

+(void) load {
    [super load];
    if (!s_emoticonToUnicode) {
        [NSString initializeEmojiCheatCodes];
    }
}


+ (void)initializeEmojiCheatCodes
{
    NSDictionary *emoticonMap = @{
                                  // Smile
                                  @":)": @"😄",
                                  @":]": @"😄",
                                  @"=)": @"😄",
                                  @":-)": @"😄",
                                  // Frown
                                  @":(" : @"😦",
                                  @":[" : @"😦",
                                  @"=(" : @"😦",
                                  @":-(" : @"😦",
                                  // Tongue
                                  @":p" : @"😛",
                                  @":P" : @"😛",
                                  @"=P" : @"😛",
                                  @":-p" : @"😛",
                                  @":-P" : @"😛",
                                  // Grin
                                  @":D" : @"😀",
                                  @"=D" : @"😀",
                                  @":-D" : @"😀",
                                  // Gasp
                                  @":o" : @"😮",
                                  @":O" : @"😮",
                                  @":-o" : @"😮",
                                  @":-O" : @"😮",
                                  // Wink
                                  @";)" : @"😉",
                                  @";-)" : @"😉",
                                  // Glasses
                                  @"8-)" : @"🤓",
                                  @"B-)" : @"🤓",
                                  // Sunglasses
                                  @"8|" : @"😎",
                                  @"B|" : @"😎",
                                  @"8-|" : @"😎",
                                  @"B-|" : @"😎",
                                  // Unsure
                                  @":/" : @"😕",
                                  @":\\": @"😕",
                                  @":-/" : @"😕",
                                  @":-\\" : @"😕",
                                  // Cry
                                  @":'(" : @"😢",
                                  // Kiss
                                  @":*" : @"😘",
                                  @":-*" : @"😘",
                                  // Kiki
                                  @"^_^" : @"😊",
                                  // Squint
                                  @"-_-" : @"😑",
                                  // Confused
                                  @"o.O" : @"😕",
                                  @"O.o" : @"😕",
                                  // Curly lips
                                  @":3" : @"😘",
                                  // Heart
                                  @"<3" : @"😍",
                                  // Robot
                                  @":|]" : @"🤖",
                                  // Penguin
                                  @"<(\")" : @"🐧",
                                  // Like
                                  @"(y)" : @"👍",
                                  @"(Y)" : @"👍",
                                  // Poop
                                  @":poop:" : @"💩"
                                  };

    @synchronized(self) {
        s_emoticonToUnicode = emoticonMap;
    }
}

- (NSString *)stringByReplacingEmojiCheatCodesWithUnicode
{
    NSInteger loc = [self rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:s_supportedCharset]].location;
    if (loc != NSNotFound) {
        __block NSMutableString *newText = [NSMutableString stringWithString:self];
        
        [s_emoticonToUnicode enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *obj, BOOL *stop) {
            NSRange searchRange = NSMakeRange(0, newText.length);
            NSRange foundRange;
            while (searchRange.location < newText.length) {
                searchRange.length = newText.length-searchRange.location;
                foundRange = [newText rangeOfString:key options:NSLiteralSearch range:searchRange];
                if (foundRange.location != NSNotFound) {
                    // We allow the emoticon if :
                    // - is first-char of the string OR previous char is a whitespace
                    NSUInteger offset = 0;
                    if (foundRange.location == 0 || [[NSCharacterSet whitespaceAndNewlineCharacterSet] characterIsMember:[newText characterAtIndex:foundRange.location-1]]) {
                        // AND
                        // - is last-char of the string OR next char is a (whitespace , . ? !)
                        NSMutableCharacterSet *charsAllowedAfter = [NSMutableCharacterSet characterSetWithCharactersInString:@".,?!"];
                        [charsAllowedAfter formUnionWithCharacterSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                        
                        if (foundRange.location+foundRange.length == newText.length || [charsAllowedAfter characterIsMember:[newText characterAtIndex:foundRange.location+foundRange.length]]) {
                            [newText replaceOccurrencesOfString:key withString:obj options:NSLiteralSearch range:foundRange];
                            offset = [obj length] - [key length];
                        }
                    }
                    
                    searchRange.location = foundRange.location+foundRange.length + offset;
                } else {
                    // no more substring to find
                    break;
                }
            }
        }];
        
        return newText;
    }
    
    return self;
}

@end
