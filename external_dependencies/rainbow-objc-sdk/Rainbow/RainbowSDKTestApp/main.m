//
//  main.m
//  RainbowSDKTestApp
//
//  Created by Jerome Heymonet on 05/04/2016.
//  Copyright © 2016 ALE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
