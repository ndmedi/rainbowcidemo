/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "RainbowSDKTestAbstract.h"

@interface BotsServiceTests : RainbowSDKTestAbstract

@end

@implementation BotsServiceTests

- (void)setUp {
    [super setUp];

    ContactAPI *bot = [ContactAPI new];
    bot.ID = @"bot_123";
    bot.jid = @"emily.jerome-all-in-one-dev-1.opentouch.cloud";
    bot.lastname = @"Emily";
    bot.inRoster = YES;
    bot.isPending = NO;
    bot.presence = @"Online";
    bot.isBot = YES;
    [self.server addContact:bot];
}

- (void)tearDown {
    [super tearDown];
}

-(void) testEmily {
    [self expectationForNotification:kContactsManagerServiceDidUpdateContact object:nil handler:^BOOL(NSNotification * _Nonnull notification) {
        NSDictionary *userInfo = (NSDictionary *)notification.object;
        Contact *contact = [userInfo objectForKey:kContactKey];
        NSLog(@"NOTIFICATION %@", contact);
        if(contact.isBot){
            XCTAssertTrue([contact.rainbowID isEqualToString:@"bot_123"], @"Rainbow ID is not the excepted one");
            XCTAssertTrue([contact.jid isEqualToString:@"emily.jerome-all-in-one-dev-1.opentouch.cloud"], @"JID is not the excepted one");
            return YES;
        }
        return NO;
    }];
    
    [self doLoginAndStartXMPP];
    
    [self waitForExpectationsWithTimeout:5.0 handler:^(NSError * _Nullable error) {
        if(error)
            NSLog(@"Timeout Error %@", error);
    }];
}

@end
