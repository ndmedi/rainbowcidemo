/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ConferenceInfo.h"
#import "ConferenceInfo+Internal.h"
#import "ConferenceParticipant.h"
#import "ConferenceParticipant+Internal.h"
#import "Tools.h"

@implementation ConferenceInfo

-(instancetype) init {
    self = [super init];
    if(self){
        _eventType = ConferenceEventTypeUnknown;
        _confId = nil;
        _addedParticipants = [NSMutableArray new];
        _updatedParticipants = [NSMutableArray new];
        _removedParticipants = [NSMutableArray new];
        _addedPublishers = [NSMutableArray new];
        _removedPublishers = [NSMutableArray new];
        _talkers = [NSMutableArray new];
    }
    return self;
}

-(NSString *) description {
    @synchronized(self){
        return [NSString stringWithFormat:@"Conference info %p <EventType %@, confId %@, isActive %@ isRecording %@ addedParticipants %@, updatedParticipant %@, removedParticipants %@, talkers %@, addedPublishers %@, removedPublishers %@", self, [ConferenceInfo stringForConferenceEventType:_eventType], _confId, NSStringFromBOOL(_isActive), NSStringFromBOOL(_isRecording), _addedParticipants, _updatedParticipants, _removedParticipants, _talkers, _addedPublishers, _removedPublishers];
    }
}

+(NSString *) stringForConferenceEventType:(ConferenceEventType) eventType {
    switch (eventType) {
        case ConferenceEventTypeState:
            return @"state";
            break;
        case ConferenceEventTypeParticipantsUpdate:
            return @"participants-update";
            break;
        case ConferenceEventTypeParticipantsRemoved:
            return @"participants-removed";
            break;
        case ConferenceEventTypeTalkers:
            return @"talkers";
            break;
        case ConferenceEventPublishersAdded:
            return @"added-publishers";
            break;
        case ConferenceEventPublishersRemoved:
            return @"removed-publishers";
            break;
        case ConferenceEventTypeUnknown:
        default:
            return @"unknown";
            break;
    }
}

@end
