/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "CompanyInvitation.h"
#import "CompanyInvitation+Internal.h"
#import "Company+Internal.h"
#import "NSDate+JSONString.h"

@implementation CompanyInvitation

-(NSString *) description {
    @synchronized(self){
        return [NSString stringWithFormat:@"Company Invitation %p <id: %@ company: %@ direction: %@ status: %@",self, self.invitationID, _company, [Invitation stringFromInvitationDirection:self.direction], [Invitation stringFromInvitationStatus:self.status]];
    }
}

-(NSDictionary *) dictionaryRepresentation {
    NSMutableDictionary *dic = [NSMutableDictionary new];
    NSDictionary *superDic = [super dictionaryRepresentation];
    [dic addEntriesFromDictionary:superDic];
    /*
     {
     companyId = 589dc8f9037a173b5ff967e9;
     companyName = Company;
     invitingAdminId = 589dc919037a173b5ff967eb;
     invitingAdminLoginEmail = "jerome@company.com";
     }
     )
     */
    if(self.company.companyId)
        [dic setObject:self.company.companyId forKey:@"companyId"];
    
    if(self.company.name)
        [dic setObject:self.company.name forKey:@"companyName"];
    
    if(self.peer.rainbowID)
        [dic setObject:self.peer.rainbowID forKey:@"invitingAdminId"];
    if(self.lastNotificationDate)
        [dic setObject:[NSDate jsonStringFromDate:self.lastNotificationDate] forKey:@"lastNotificationDate"];
    return dic;
}

@end
