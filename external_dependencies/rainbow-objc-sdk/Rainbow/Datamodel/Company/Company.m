/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Company.h"
#import "Company+Internal.h"
#import "Tools.h"

@implementation Company

-(NSString *) description {
    @synchronized(self){
        return [NSString stringWithFormat:@"Company %p <ID: %@ Name: %@, adminEmail: %@ website: %@ requetToJoin %@>", self, _companyId, _name, _adminEmail, _websiteURL, NSStringFromBOOL(_alreadyRequestToJoin)];
    }
}
@end
