/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */
#import "ConferencePublisher.h"
#import "NSXMLElement+XMPP.h"

@interface ConferencePublisher ()
@property (nonatomic, readwrite, strong) Contact *contact;
@property (nonatomic, readwrite, strong) NSString *publisherID;
@property (nonatomic, readwrite) PublisherMediaType mediaType;
@property (nonatomic, readwrite, strong) NSString *jidIM;
@property (nonatomic, readwrite) BOOL subscribed;
+(NSString *) stringFromMediaType:(PublisherMediaType) mediaType;
+(PublisherMediaType) mediaTypeFromNSString:(NSString *) mediaType;
+(ConferencePublisher*) publisherFromXMLElement:(NSXMLElement *) element;
-(NSDictionary *) dictionaryRepresentation;
@end
