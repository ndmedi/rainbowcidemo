/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ConferencePublisher.h"
#import "NSString+NSXMLElement.h"
#import "ConferencePublisher+Internal.h"

@implementation ConferencePublisher

-(BOOL) isEqual:(ConferencePublisher *)object {
    return [_contact isEqual:object.contact];
}

-(NSString *) description {
    return [NSString stringWithFormat:@"Conference publisher %p : <ID %@, MediaType %@, Contact %@",self, _publisherID, [ConferencePublisher stringFromMediaType:_mediaType], _contact];
}

-(NSDictionary *) dictionaryRepresentation {
    NSMutableDictionary *dic = [NSMutableDictionary new];
    if(_publisherID)
        [dic setObject:_publisherID forKey:@"publisherID"];
    if(_jidIM)
        [dic setObject:_jidIM forKey:@"jidIM"];
    
    [dic setObject:[ConferencePublisher stringFromMediaType:_mediaType] forKey:@"mediaType"];
    
    return dic;
}

+(PublisherMediaType) mediaTypeFromNSString:(NSString *) mediaType {
    if([mediaType isEqualToString:@"sharing"]){
        return PublisherMediaTypeSharing;
    }
    if([mediaType isEqualToString:@"video"]){
        return PublisherMediaTypeVideo;
    }
    return PublisherMediaTypeUnknown;
}

+(NSString *) stringFromMediaType:(PublisherMediaType) mediaType {
    switch (mediaType) {
        case PublisherMediaTypeSharing:
            return @"sharing";
            break;
        case PublisherMediaTypeVideo:
            return @"video";
            break;
        case PublisherMediaTypeUnknown:
        default:
            return @"unknown";
            break;
    }
}

+(ConferencePublisher *) publisherFromXMLElement:(NSXMLElement *) element {
    ConferencePublisher *publisher = [ConferencePublisher new];
    publisher.publisherID = [NSString valueForElement:element withName:@"publisher-id"];
    if(!publisher.publisherID)
        publisher.publisherID = [NSString valueForElement:element withName:@"participant-id"];
    publisher.mediaType = [ConferencePublisher mediaTypeFromNSString:[NSString valueForElement:element withName:@"media-type"]];
    publisher.jidIM = [NSString valueForElement:element withName:@"jid-im"];
    return publisher;
}

@end
