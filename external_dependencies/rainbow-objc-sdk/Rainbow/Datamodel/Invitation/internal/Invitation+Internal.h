/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Invitation.h"


@interface Invitation ()

@property (nonatomic, readwrite, strong) NSString *invitationID;

@property (nonatomic, readwrite) InvitationDirection direction;

@property (nonatomic, readwrite, strong) NSString *email;

@property (nonatomic, readwrite, strong) NSString *phoneNumber;

@property (nonatomic, readwrite, strong) Peer *peer;

@property (nonatomic, readwrite) InvitationStatus status;

@property (nonatomic, readwrite, strong) NSDate *date;

/* Rainbow ID of the user that sent this invitation */
@property (nonatomic, strong, readwrite) NSString *invitingUserId;

-(NSDictionary *) dictionaryRepresentation;

@end
