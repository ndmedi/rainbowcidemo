/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "File.h"
#import "File+Internal.h"
#import "Tools.h"
#import "OrderedDictionary.h"

static NSDictionary *mimeTypeList = nil;

@implementation File

+(void) load {
    [super load];
    if(!mimeTypeList){
        NSString *mimeTypeFilePath = [[NSBundle bundleForClass:[self class]] pathForResource: @"file_mime_types" ofType: @"plist"];
        mimeTypeList = [OrderedDictionary dictionaryWithContentsOfFile:mimeTypeFilePath];
    }
}

-(instancetype) init {
    self = [super init];
    if(self){
        _viewers = [NSMutableArray array];
        _isDownloadAvailable = YES;
        _isOfflineAttachment = NO;
        _canDownloadThumbnail = NO;
        _canDownloadFile = YES;
    }
    return self;
}

-(BOOL) isEqual:(File *) otherFile {
    return [_rainbowID isEqual:otherFile.rainbowID];
}

-(NSString *) description {
    @synchronized(self){
        return [NSString stringWithFormat:@"File <%p> : id %@ url %@ filename %@ size %ld hasData %@ viewers %@ onwer %@", self, _rainbowID, _url, _fileName, _size, NSStringFromBOOL(_data.length>0), _viewers, _owner];
    }
}

-(FileType) type {
    return [File fileTypeForMimeType:_mimeType];
}

-(BOOL) hasThumbnailOnServer {
    FileType type = [File fileTypeForMimeType:_mimeType];
    
    if (type == FileTypeImage || (type == FileTypePDF))
        return YES;
    return NO;
}

-(void) addPeerInViewers:(Peer *) peer {
    if(![_viewers containsObject:peer])
        [_viewers addObject:peer];
}

-(void) removePeerFromViewers:(Peer *) peer {
    if([_viewers containsObject:peer])
        [_viewers removeObject:peer];
}

-(void) removeAllPeerFromViewers {
    [_viewers removeAllObjects];
        
}

-(NSDate *) uploadDate {
    if(_uploadDate)
        return _uploadDate;
    else
        return _registrationDate;
}

-(NSDate *) dateToSort {
    if(_dateToSort)
        return _dateToSort;
    else
        return _registrationDate;
}

+(FileType) fileTypeForMimeType:(NSString *) mimeType {
    if ([mimeType isKindOfClass:[NSNull class]]) { // sometimes mimeType passed as NSNULL object!
        return FileTypeOther;
    }
    if ([mimeType containsString:@"image/"]) {
        return FileTypeImage;
    } else if ([mimeType isEqualToString:@"application/pdf"]){
        return FileTypePDF;
    } else if ([mimeType isEqualToString:@"application/msword"] ||
               [mimeType isEqualToString:@"application/vnd.openxmlformats-officedocument.wordprocessingml.document"] ||
               [mimeType isEqualToString:@"application/vnd.oasis.opendocument.text"]){
        return FileTypeDoc;
    } else if ([mimeType isEqualToString:@"application/vnd.ms-powerpoint"] ||
               [mimeType isEqualToString:@"application/vnd.openxmlformats-officedocument.presentationml.presentation"]){
        return FileTypePPT;
    } else if ([mimeType isEqualToString:@"application/vnd.ms-excel"] ||
               [mimeType isEqualToString:@"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"]){
        return FileTypeXLS;
    } else if([mimeType containsString:@"audio/"]){
        return FileTypeAudio;
    } else if ([mimeType containsString:@"video/"]){
        return FileTypeVideo;
    }
    else
        return FileTypeOther;
}

+(NSString *) extensionForMimeType:(NSString *) mimeType {
    return [mimeTypeList valueForKey:mimeType];
}

-(NSDictionary *) dictionaryRepresentation {
    NSMutableDictionary *dic = [NSMutableDictionary new];
    // Maybe complete if needed
    if (_fileName)
        [dic setObject:_fileName forKey:@"fileName"];
    if (_size)
        [dic setObject:[NSNumber numberWithUnsignedInteger:_size] forKey:@"size"];
    if (_mimeType)
        [dic setObject:_mimeType forKey:@"mimeType"];
    if (_url)
        [dic setObject:_url forKey:@"url"];
    if (_owner)
        [dic setObject:_owner.rainbowID forKey:@"ownerId"];
    if (_isDownloadAvailable)
        [dic setObject:[NSNumber numberWithBool:_isDownloadAvailable] forKey:@"downloadAvailable"];
    
    return dic;
}

@end
