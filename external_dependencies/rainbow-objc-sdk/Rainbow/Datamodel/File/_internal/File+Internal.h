/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>
#import "File.h"

@interface File ()
@property (nonatomic, readwrite, strong) NSString *rainbowID;
@property (nonatomic, readwrite, strong) NSURL *url;
@property (nonatomic, readwrite, strong) NSString *fileName;
@property (nonatomic, readwrite, strong) NSString *mimeType;
@property (nonatomic, readwrite) NSUInteger size;
@property (nonatomic, readwrite) NSData *data;
@property (nonatomic, readwrite) NSData *thumbnailData;
@property (nonatomic, readwrite) NSMutableArray<Peer*> *viewers;
@property (nonatomic, readwrite) NSDate *uploadDate;
@property (nonatomic, readwrite) NSDate *registrationDate;
@property (nonatomic, readwrite) NSDate *dateToSort;
@property (nonatomic, readwrite) Contact *owner;
@property (readwrite) BOOL isDownloadAvailable;
@property (readwrite) BOOL canDownloadThumbnail;

+(FileType) fileTypeForMimeType:(NSString *) mimeType;
+(NSString *) extensionForMimeType:(NSString *) mimeType;

-(void) addPeerInViewers:(Peer *) peer;
-(void) removePeerFromViewers:(Peer *) peer;
-(void) removeAllPeerFromViewers;
@end
