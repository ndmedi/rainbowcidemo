/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "CKBrowsingOperation.h"
#import "CKBrowsableItem.h"

@implementation CKBrowsingOperation

@synthesize itemsCount = _itemsCount, startIndex = _startIndex;
@synthesize jobResultError = _jobResultError;
@synthesize jobResultPage = _jobResultPage;
@synthesize outdated = _outdated;

-(void) dealloc {
    [_jobResultError release];
    _jobResultError = nil;
    [_jobResultPage release];
    _jobResultPage = nil;
    [super dealloc];
}

-(void) main {
    NSArray* page = [self performJobWithError:&_jobResultError];
    if (!_jobResultError) {
        // ensure the page items are sorted by date (freshest at index 0).
        NSMutableArray * sortedArray = [NSMutableArray arrayWithArray:page];
        [sortedArray sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            return [((id<CKBrowsableItem>)obj2).date compare:((id<CKBrowsableItem>)obj1).date]; 
        }];
        _jobResultPage = [sortedArray retain];
    } else 
        [_jobResultError retain];
    // otherwise, keep it with nil.
}

-(NSDate*) timeFrameStart {
    return [_jobResultPage count]>0?((NSObject<CKBrowsableItem>*)[_jobResultPage lastObject]).date:nil;
}

-(NSDate*) timeFrameEnd {
    // if this page starts at the index 0, this is the first page (the FRESHEST).
    // as a way of consequence, it means that is represent info until "NOW".
    if (_startIndex==0) {
        return nil;
    }else {
        return [_jobResultPage count]>0?((NSObject<CKBrowsableItem>*)[_jobResultPage firstObject]).date:nil;
    }
}

-(NSArray*) performJobWithError:(NSError **)error {
    if (error)
        *error = [NSError errorWithDomain:@"Browsing" code:-1 userInfo:@{NSLocalizedDescriptionKey:@"Operation not implemented."}];
    return nil;
}

-(NSString*) description {
    return [NSString stringWithFormat:@"CKBrowsingOperation %@ (from %ld for %ld items)", (_outdated)?@"[!OUTDATED!]":@"", (unsigned long)_startIndex, (unsigned long)_itemsCount];
}

@end
