/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "RoomInvitationScenario.h"

@implementation RoomInvitationScenario

+(NSString *) stringFromRoomInvitationScenario:(InvitationScenario) scenario {
    
    switch (scenario) {
        case InvitationChat:
            return @"chat";
        case InvitationPstnConference:
            return @"pstn-conference";
        case InvitationVideoConference:
            return @"video-conference";
        default:
            return @"unknown";
    }
}

@end
