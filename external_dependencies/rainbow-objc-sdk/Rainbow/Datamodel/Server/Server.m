/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Server.h"
#import "Server+Internal.h"

@implementation Server

-(instancetype) initWithServerName:(NSString *) serverName {
    self = [super init];
    if(self){
        _serverHostname = serverName;
        _useExternalAuthentication = YES;
        _externalAuthenticationPath = @"/api/authenticate";
        _requireWebsockets = YES;
        _websocketPath = @"websocket";
        _defaultServer = NO;
        _isSecure = YES;
    }
    return self;
}

-(NSString *) serverDisplayedName {
    return _serverHostname;
}

-(BOOL) isEqual:(Server *) serverToCompare {
    if([serverToCompare isKindOfClass:[Server class]]){
        return [self.serverHostname isEqualToString:serverToCompare.serverHostname];
    }
    return NO;
}

-(NSString *) description {
    return [NSString stringWithFormat:@"%@", _serverHostname];
}

-(BOOL) isAllInOne {
#if DEBUG
    // Used for unit tests
    if([_serverHostname isEqualToString:@"localhost:6670"])
        return YES;
#endif
    NSArray *splitted = [_serverHostname componentsSeparatedByString:@"."];
    if(splitted.count >= 3)
        return YES;
    
    return NO;
}

+(Server *) opentouchRainbowServer {
    Server *server = [[Server alloc] initWithServerName:@"openrainbow.com"];
    server.defaultServer = YES;
    return server;
}

+(Server *) serverWithServerName:(NSString *) serverName {
    return [[Server alloc] initWithServerName:serverName];
}
@end
