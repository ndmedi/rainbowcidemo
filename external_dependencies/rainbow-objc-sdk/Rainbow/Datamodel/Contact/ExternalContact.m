/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ExternalContact.h"
#import "NSObject+NotNull.h"
#import "ContactsManagerServiceToolKit.h"
#import "PhoneNumberInternal.h"
#import "EmailAddressInternal.h"
#import "PostalAddressInternal.h"
#import "LocalContactsToolkit.h"

@implementation ExternalContact

+(void) fillExternalContact:(ExternalContact*) contact withJsonDictionary:(NSDictionary *) dictionary {
    
    if([dictionary[@"companyName"] notNull])
        contact.companyName = [dictionary[@"companyName"] notNull];
    if ([dictionary[@"firstName"] notNull])
        contact.firstName = [dictionary[@"firstName"] notNull];
    if ([dictionary[@"lastName"] notNull])
        contact.lastName = [dictionary[@"lastName"] notNull];
    if([dictionary[@"nickName"] notNull])
        contact.nickName = [dictionary[@"nickName"] notNull];
    if([dictionary[@"title"] notNull])
        contact.title = [dictionary[@"title"] notNull];
    if([dictionary[@"jobTitle"] notNull])
        contact.jobTitle = [dictionary[@"jobTitle"] notNull];
    if([dictionary[@"timezone"] notNull])
        contact.timeZone = [NSTimeZone timeZoneWithName:dictionary[@"timezone"]];
    
    if (dictionary[@"businessPhonesE164"]){
        NSArray *businessPhonesE164 = dictionary[@"businessPhonesE164"];
        for (NSString *businessPhoneE164 in businessPhonesE164) {
            PhoneNumber *thePhoneNumber = [ContactsManagerServiceToolKit phoneNumberFromNumber:businessPhoneE164 numberE164:businessPhoneE164 type:[PhoneNumber typeFromPhoneNumberLabel:@"work"] deviceType:[PhoneNumber deviceTypeFromDeviceTypeLabel:@"landline"] isMonitored:NO];
            if(thePhoneNumber)
                [contact addPhoneNumberObject:thePhoneNumber];
        }
    }
    
    if (dictionary[@"businessPhones"]){
        NSArray *businessPhonesE164 = dictionary[@"businessPhones"];
        for (NSString *businessPhoneE164 in businessPhonesE164) {
            PhoneNumber *thePhoneNumber = [ContactsManagerServiceToolKit phoneNumberFromNumber:businessPhoneE164 numberE164:businessPhoneE164 type:[PhoneNumber typeFromPhoneNumberLabel:@"work"] deviceType:[PhoneNumber deviceTypeFromDeviceTypeLabel:@"landline"] isMonitored:NO];
            if(thePhoneNumber)
                [contact addPhoneNumberObject:thePhoneNumber];
        }
    }
    
    if (dictionary[@"mobilePhonesE164"]){
        NSArray *mobilePhonesE164 = dictionary[@"mobilePhonesE164"];
        for (NSString *mobilePhoneE164 in mobilePhonesE164) {
            PhoneNumber *thePhoneNumber = [ContactsManagerServiceToolKit phoneNumberFromNumber:mobilePhoneE164 numberE164:mobilePhoneE164 type:[PhoneNumber typeFromPhoneNumberLabel:@"work"] deviceType:[PhoneNumber deviceTypeFromDeviceTypeLabel:@"mobile"] isMonitored:NO];
            if(thePhoneNumber)
                [contact addPhoneNumberObject:thePhoneNumber];
        }
    }
    
    if (dictionary[@"mobilePhone"]){
        NSString *mobilePhone = [dictionary[@"mobilePhone"] notNull];
        PhoneNumber *thePhoneNumber = [ContactsManagerServiceToolKit phoneNumberFromNumber:mobilePhone numberE164:mobilePhone type:[PhoneNumber typeFromPhoneNumberLabel:@"work"] deviceType:[PhoneNumber deviceTypeFromDeviceTypeLabel:@"mobile"] isMonitored:NO];
        if(thePhoneNumber)
            [contact addPhoneNumberObject:thePhoneNumber];
    }
    
    if (dictionary[@"number"]){
        NSString *PBXPhoneNumber = [dictionary[@"number"] notNull];
        PhoneNumber *thePhoneNumber = [ContactsManagerServiceToolKit phoneNumberFromNumber:PBXPhoneNumber type:[PhoneNumber typeFromPhoneNumberLabel:@"work"] deviceType:[PhoneNumber deviceTypeFromDeviceTypeLabel:@"landline"]];
        if(thePhoneNumber)
            [contact addPhoneNumberObject:thePhoneNumber];
    }
    
    if ([dictionary[@"mail"] notNull]){
        NSString *mail = [dictionary[@"mail"] notNull];
        if(mail.length>0){
            EmailAddress *emailAddress = [ContactsManagerServiceToolKit emailAddressFromEmailAddress:mail type:[EmailAddress typeFromEmailAddressLabel:@"work"]];
            emailAddress.isVisible = YES;
            if(emailAddress)
                [contact addEmailAddressObject:emailAddress];
        }
    }
    
    if([dictionary[@"oid"] notNull])
       contact.externalObjectID = [dictionary[@"oid"] notNull];
    
    if(dictionary[@"picture"]){
        NSString *base64PictureData = [dictionary[@"picture"] notNull];
        NSData* data = [[NSData alloc] initWithBase64EncodedString:base64PictureData options:0];
        UserPhoto *photo = [[UserPhoto alloc] initWithPhotoData:data];
        contact.userPhoto = photo;
    }
    
    NSMutableDictionary *addressInfos = [NSMutableDictionary new];
    if([dictionary[@"streetAddress"] notNull])
        [addressInfos setObject:dictionary[@"streetAddress"] forKey:kPostalAddressStreetKey];
    if([dictionary[@"city"] notNull])
        [addressInfos setObject:dictionary[@"city"] forKey:kPostalAddressCityKey];
    if([dictionary[@"state"] notNull])
        [addressInfos setObject:dictionary[@"state"] forKey:kPostalAddressStateKey];
    if([dictionary[@"country"] notNull])
        [addressInfos setObject:dictionary[@"country"] forKey:kPostalAddressCountryKey];
    if([dictionary[@"zip"] notNull])
        [addressInfos setObject:dictionary[@"zip"] forKey:kPostalAddressZipKey];
    if([dictionary[@"usageLocation"] notNull]){
        NSString *alpha3CountryCode = [LocalContactsToolkit getISO3611Alpha3CodeFor:[dictionary[@"usageLocation"] uppercaseString]];
        if(alpha3CountryCode)
            [addressInfos setObject:alpha3CountryCode forKey:kPostalAddressCountryCodeKey];
    }
    if(addressInfos.count > 0){
        PostalAddress *address = [PostalAddress postalAddressFromDictionary:addressInfos];
        if(address){
            [contact.addresses addObject:address];
        }
    }
    
    if([dictionary[@"mySite"] notNull]){
        [contact.webSitesURL addObject:dictionary[@"mySite"]];
    }
}
@end
