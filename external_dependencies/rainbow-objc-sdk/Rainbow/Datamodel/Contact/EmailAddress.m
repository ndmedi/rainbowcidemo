/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "EmailAddress.h"
#import "EmailAddressInternal.h"
#import "Tools.h"
#import "NSDictionary+JSONString.h"

@implementation EmailAddress

-(instancetype) initWithEmailAddress:(EmailAddress *) emailAddress {
    self = [super init];
    if(self) {
        [self updateWith:emailAddress];
    }
    return self;
}

-(void) updateWith:(EmailAddress *) emailAddress {
    self.address = emailAddress.address?[NSString stringWithString:emailAddress.address]:nil;
    self.type = emailAddress.type?emailAddress.type:EmailAddressTypeHome;
    self.isPrefered = emailAddress.isPrefered?emailAddress.isPrefered:NO;
}

+(NSString *) stringForEmailAddressType:(EmailAddressType) type {
    switch (type) {
        case EmailAddressTypeHome: {
            return @"Home";
            break;
        }
        default:
        case EmailAddressTypeWork: {
            return @"Work";
            break;
        }
        case EmailAddressTypeOther: {
            return @"Other";
            break;
        }
    }
}

+(EmailAddressType) typeFromEmailAddressLabel:(NSString *) label {
    if([label isEqualToString:@"home"])
        return EmailAddressTypeHome;
    if([label isEqualToString:@"other"])
        return EmailAddressTypeOther;
    
    return EmailAddressTypeWork;
}

-(NSString *) label {
    return [EmailAddress stringForEmailAddressType:_type];
}

-(void) setAddress:(NSString *)address {
    _address = nil;
    _address = [address lowercaseString];
}

-(NSString *) description {
    @synchronized(self){
        return [NSString stringWithFormat:@" Email Address : <%@ - %@ (Prefered: %@)>", _address, [EmailAddress stringForEmailAddressType:_type], NSStringFromBOOL(_isPrefered)];
    }
}

-(BOOL) isEqual:(EmailAddress *) emailAddress {
    if([emailAddress isKindOfClass:[EmailAddress class]]){
        return [_address isEqualToString:emailAddress.address];
    }
    return NO;
}

-(NSUInteger)hash {
    return [_address hash];
}

- (id)copyWithZone:(nullable NSZone *)zone {
    return [[EmailAddress allocWithZone:zone] initWithEmailAddress:self];
}

-(NSString *) jsonRepresentation {
    return [[self dictionaryRepresentation] jsonStringWithPrettyPrint:NO];
}

-(NSDictionary *) dictionaryRepresentation {
    NSMutableDictionary *jsonDic = [NSMutableDictionary dictionary];
    if(_address)
        [jsonDic setObject:_address forKey:@"email"];

    [jsonDic setObject:[[EmailAddress stringForEmailAddressType:_type] lowercaseString] forKey:@"type"];
    
    return jsonDic;
}
@end
