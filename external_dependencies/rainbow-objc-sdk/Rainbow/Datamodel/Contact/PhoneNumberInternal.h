/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "PhoneNumber.h"

@interface PhoneNumber () <NSCopying>
@property (nonatomic, strong, readwrite) NSString *number;
@property (nonatomic, strong, readwrite) NSString *numberE164;
@property (nonatomic, readwrite) PhoneNumberType type;
@property (nonatomic, readwrite) PhoneNumberDeviceType deviceType;
@property (nonatomic, readwrite) BOOL isPrefered;
@property (nonatomic, strong, readwrite) NSString *countryName;
@property (nonatomic, strong, readwrite) NSString *countryCode;
@property (nonatomic, readwrite) BOOL isMonitored;
@property (nonatomic, readwrite) BOOL needLanguageSelection;

+(PhoneNumberType) typeFromPhoneNumberLabel:(NSString *) label;
+(PhoneNumberDeviceType) deviceTypeFromDeviceTypeLabel:(NSString *) label;

@end
