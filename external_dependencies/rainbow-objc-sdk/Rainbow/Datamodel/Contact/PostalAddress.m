/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "PostalAddress.h"
#import "PostalAddressInternal.h"
#import "NSDictionary+JSONString.h"

@implementation PostalAddress

-(NSString *) description {
    @synchronized(self){
        return [NSString stringWithFormat:@"Postal Address : pobox %@ street %@ city %@ state %@ postalCode %@ country %@",_pobox, _street, _city, _state, _postalCode, _country];
    }
}

+(PostalAddress *) postalAddressFromDictionary:(NSDictionary *) dictionary {
    PostalAddress *address = [PostalAddress new];
    if([dictionary objectForKey:kPostalAddressStreetKey])
        address.street = dictionary[kPostalAddressStreetKey];
    if([dictionary objectForKey:kPostalAddressCityKey])
        address.city = dictionary[kPostalAddressCityKey];
    if([dictionary objectForKey:kPostalAddressStateKey])
        address.state = dictionary[kPostalAddressStateKey];
    if([dictionary objectForKey:kPostalAddressCountryKey])
        address.country = dictionary[kPostalAddressCountryKey];
    if([dictionary objectForKey:kPostalAddressCountryCodeKey])
        address.countryCode = dictionary[kPostalAddressCountryCodeKey];
    if([dictionary objectForKey:kPostalAddressZipKey])
        address.postalCode = dictionary[kPostalAddressZipKey];
    return address;
}

-(NSString *) stringRepresentation {
    NSMutableArray *postalAddressComponents = [NSMutableArray array];
    if(_street)
        [postalAddressComponents addObject:_street];
    if(_pobox)
        [postalAddressComponents addObject:_pobox];
    if(_state)
        [postalAddressComponents addObject:_state];
    if(_postalCode)
        [postalAddressComponents addObject:_postalCode];
    if(_city)
        [postalAddressComponents addObject:[NSString stringWithFormat:@"%@\n",_city]];
    if(_countryCode && _country.length == 0)
        [postalAddressComponents addObject:_countryCode];
    if(_country)
        [postalAddressComponents addObject:_country];
    
    return [postalAddressComponents componentsJoinedByString:@" "];
}

-(NSDictionary *) dictionaryRepresentation {
    return [self fullDictionaryRepresentation:YES];
}

-(NSDictionary *) fullDictionaryRepresentation:(BOOL) full {
    NSMutableDictionary *postalAddressDictionary = [NSMutableDictionary dictionary];
    if(_street)
        [postalAddressDictionary setObject:_street forKey:kPostalAddressStreetKey];
    if(_pobox)
        [postalAddressDictionary setObject:_pobox forKey:kPostalAddressPoBoxKey];
    if(_city)
        [postalAddressDictionary setObject:_city forKey:kPostalAddressCityKey];
    if(_state)
        [postalAddressDictionary setObject:_state forKey:kPostalAddressStateKey];
    if(_postalCode)
        [postalAddressDictionary setObject:_postalCode forKey:kPostalAddressZipKey];
    if(full){
        if(_countryCode && _country.length == 0)
            [postalAddressDictionary setObject:_countryCode forKey:kPostalAddressCountryCodeKey];
        if(_country)
            [postalAddressDictionary setObject:_country forKey:kPostalAddressCountryKey];
    } else
        if(_countryCode)
            [postalAddressDictionary setObject:_countryCode forKey:kPostalAddressCountryKey];
    if(_type)
        [postalAddressDictionary setObject:[[PostalAddress stringForPostalAddressType:_type] lowercaseString] forKey:kPostalAddressTypeKey];
    
    return postalAddressDictionary;
}

-(NSDictionary *) jsonDictionaryRepresentation {
    return [self fullDictionaryRepresentation:NO];
}


+(PostalAddressType) typeFromPostalAddressLabel:(NSString *) label {
    if([label isEqualToString:@"Home"])
        return PostalAddressTypeHome;
    
    return PostalAddressTypeWork;
}

+(NSString *) stringForPostalAddressType:(PostalAddressType) type {
    switch (type) {
        case PostalAddressTypeHome: {
            return @"Home";
            break;
        }
        default:
        case PostalAddressTypeWork: {
            return @"Work";
            break;
        }
    }
}

-(NSString *) jsonRepresentation {
    return [[self jsonDictionaryRepresentation] jsonStringWithPrettyPrint:NO];
}
@end
