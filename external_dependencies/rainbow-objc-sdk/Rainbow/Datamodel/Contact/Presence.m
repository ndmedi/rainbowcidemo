/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Presence.h"
#import "PresenceInternal.h"

@implementation Presence

-(instancetype) init {
    self = [super init];
    if(self){
        _presence = ContactPresenceUnavailable;
    }
    return self;
}

+(NSString *) stringForContactPresence:(Presence*) presence {
    switch (presence.presence) {
        default:
        case ContactPresenceUnavailable: {
            return @"Offline";
            break;
        }
        case ContactPresenceAvailable: {
            return @"Online";
            break;
        }
        case ContactPresenceDoNotDisturb: {
            return @"Do not disturb";
            break;
        }
        case ContactPresenceAway: {
            return @"Away";
            break;
        }
        case ContactPresenceBusy: {
            return @"Busy";
            break;
        }
        case ContactPresenceInvisible: {
            return @"Invisible";
            break;
        }
    }
}

-(NSString *) description {
    @synchronized(self){
        return [NSString stringWithFormat:@"%@", [Presence stringForContactPresence:self]];
    }
}

-(BOOL) isEqual:(Presence *) presence {
    if([presence isKindOfClass:[Presence class]]){
        return (self.presence == presence.presence /*&& [self.status isEqualToString:presence.status]*/);
    }
    return NO;
}

+(Presence *) presenceAvailable {
    Presence *available = [Presence new];
    available.presence = ContactPresenceAvailable;
    return available;
}

+(Presence *) presenceDoNotDisturb {
    Presence *dnd = [Presence new];
    dnd.presence = ContactPresenceDoNotDisturb;
    return dnd;
}

+(Presence *) presenceAway {
    Presence *away = [Presence new];
    away.presence = ContactPresenceAway;
    return away;
}

+(Presence *) presenceExtendedAway {
    Presence *extendedAway = [Presence new];
    extendedAway.presence = ContactPresenceInvisible;
    return extendedAway;
}
+(Presence *) presenceUnavailable {
    Presence *unavailable = [Presence new];
    unavailable.presence = ContactPresenceUnavailable;
    return unavailable;
}
@end
