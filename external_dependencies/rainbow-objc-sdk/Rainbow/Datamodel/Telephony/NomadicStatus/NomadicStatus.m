/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "NomadicStatus+Internal.h"

@implementation NomadicStatus

-(NSString *) description {
    @synchronized(self){
        return [NSString stringWithFormat:@"NomadicStatus - activated: %@ feature: %@", (self.activated?@"true":@"false"), (self.featureActivated?@"true":@"false")];
    }
}

-(BOOL) isEqual:(NomadicStatus *) status {
    if([status isKindOfClass:[NomadicStatus class]]){
        return (self == status);
    }
    return NO;
}

@end
