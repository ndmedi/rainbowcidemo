/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "NomadicStatus.h"
#import "PhoneNumber.h"

@interface NomadicStatus ()
@property (nonatomic, readwrite, strong) PhoneNumber *destination;
@property (nonatomic, readwrite) BOOL featureActivated;
@property (nonatomic, readwrite) BOOL activated;
@end
