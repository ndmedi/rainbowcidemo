/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "CallParticipant+Internal.h"

@implementation CallParticipant
-(NSString *) description {
    return [NSString stringWithFormat:@"CallParticipant %p <Jid %@, number %@, firstName %@, lastName %@ >",self, _jid, _number, _firstname, _lastname];
}
@end
