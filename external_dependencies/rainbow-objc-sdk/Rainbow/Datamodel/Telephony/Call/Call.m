/*
 * Rainbow
 *
 * Copyright (c) 2016-2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Call+Internal.h"
#import "Tools.h"


@implementation Call

-(instancetype) init {
    self = [super init];
    if (self) {
        _isIncoming = NO;
        _capabilities = CallCapabilitiesHangup | CallCapabilitiesMute | CallCapabilitiesSpeaker;
    }
    return self;
}


-(NSString*) shortCallRef
{
    return [[self.callRef componentsSeparatedByString:@"#"] firstObject];   // the 2nd part can be the peer.
}


-(BOOL) isCellularCall
{
    if (![self isKindOfClass:[RTCCall class]] && !self.isMediaPillarCall)
        return YES;
    return NO;
}


-(BOOL) isRtcCall
{
    if ([self isKindOfClass:[RTCCall class]])
        return YES;
    return NO;
}


+(NSString *) stringForStatus:(CallStatus) status {
    switch (status) {
        case CallStatusRinging: {
            return @"ringing";
        }
        case CallStatusConnecting: {
            return @"accepted";
        }
        case CallStatusDeclined: {
            return @"declined";
        }
        case CallStatusTimeout: {
            return @"timeout";
        }
        case CallStatusCanceled: {
            return @"canceled";
        }
        case CallStatusEstablished: {
            return @"established";
        }
        case CallStatusHangup: {
            return @"hangup";
        }
        case CallStatusHeldByUser: {
            return @"heldByUser";
        }
        case CallStatusHeldByRemote: {
            return @"heldByRemote";
        }
    }
    return @"";
}
    

-(NSString *) description {
    return [NSString stringWithFormat:@"Call %p <CallReference '%@', state '%@', Peer '%@', isIncoming %@, connectionDate %@, capabilities %ld>", self, _callRef, [Call stringForStatus:_status], _peer, NSStringFromBOOL(_isIncoming), _connectionDate, (long)_capabilities];
}


-(BOOL) isEqual:(Call *)object {
     if ([[self class] isKindOfClass:[object class]]) {
        if (self == object)
            return YES;
        if ([self.shortCallRef isEqualToString:object.shortCallRef])
            return YES;
    }
    
    return NO;
}

@end
