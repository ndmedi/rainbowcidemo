/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "CallForwardStatus+Internal.h"

@implementation CallForwardStatus

-(instancetype) init {
    self = [super init];
    if(self){
        self.type = CallForwardTypeNotForwarded;
    }
    return self;
}

-(NSString *) description {
    @synchronized(self){
        return [NSString stringWithFormat:@"CallForwardStatus - type: %ld", (long)(self.type)];
    }
}

-(BOOL) isEqual:(CallForwardStatus *) status {
    if([status isKindOfClass:[CallForwardStatus class]]){
        return (self == status);
    }
    return NO;
}

@end
