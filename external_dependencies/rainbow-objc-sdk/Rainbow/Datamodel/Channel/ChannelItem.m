/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ChannelItem+Internal.h"
#import "ChannelItemImage.h"
#import "NSDate+JSONString.h"

@implementation ChannelItem

-(instancetype) init {
    if(self = [super init]){
        _images = [NSMutableArray new];
    }
    return self;
}

-(NSString *) description {
    return [NSString stringWithFormat:@"<ChannelItem %p: itemId: %@ channelId: %@ contact: %@ type: %@ message: %@>", self, _itemId, _channelId, _contact.displayName, _type, _message];
}

-(NSDictionary *) dictionaryRepresentation {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    
    if (_itemId)
        [dic setObject:_itemId forKey:@"id"];
    if (_channelId)
        [dic setObject:_channelId forKey:@"channelId"];
    if (_contact)
        [dic setObject:_contact.jid forKey:@"from"];
    if (_type)
        [dic setObject:_type forKey:@"type"];
    if (_editionDate && _date) {
        [dic setObject:[NSDate jsonStringFromDate:_editionDate] forKey:@"timestamp"];
        [dic setObject:[NSDate jsonStringFromDate:_date] forKey:@"creation"];
    } else if (_date) {
        [dic setObject:[NSDate jsonStringFromDate:_date] forKey:@"timestamp"];
    }

    if (_message)
        [dic setObject:_message forKey:@"message"];
    if (_title)
        [dic setObject:_title forKey:@"title"];
    if (_url)
        [dic setObject:_url forKey:@"url"];
    if (_images) {
        NSMutableArray *array = [NSMutableArray new];
        for (ChannelItemImage *itemImage in _images) {
            [array addObject:itemImage.rainbowId];
        }
        [dic setObject:array forKey:@"images"];
    }
    if (_youtubeVideoId)
        [dic setObject:@{@"id": _youtubeVideoId} forKey:@"video"];
    
    return dic;
}

@end
