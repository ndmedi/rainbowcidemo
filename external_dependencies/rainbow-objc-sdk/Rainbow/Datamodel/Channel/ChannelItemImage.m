//
//  ChannelImage.m
//  Rainbow
//
//  Created by Le Trong Nghia Huynh on 13/02/2019.
//  Copyright © 2019 ALE. All rights reserved.
//

#import "ChannelItemImage.h"
#import "ServicesManager.h"
#import "ChannelsService+Internal.h"

@interface ChannelItemImage ()

@property (nonatomic, strong, readwrite) NSString *rainbowId;
//@property (nonatomic, strong, readwrite) UIImage *image;
@property (nonatomic, strong, readwrite) NSString *channelItemId;
@property (nonatomic, assign) BOOL isLoadingImage;

@end

@implementation ChannelItemImage

-(instancetype) initWithRainbowId:(NSString *) rainbowId andChannelItemId:(NSString *) channelItemId {
    if(self = [super init]){
        _rainbowId = rainbowId;
        _channelItemId = channelItemId;
        _isLoadingImage = NO;
        _isOriginalFile = NO;
    }
    return self;
}

-(UIImage *) image {
    return _image;
}

@end
