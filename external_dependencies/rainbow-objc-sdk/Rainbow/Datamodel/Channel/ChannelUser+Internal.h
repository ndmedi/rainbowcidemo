/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ChannelUser.h"

@interface ChannelUser ()
@property (nonatomic, strong, readwrite) NSString *rainbowId;
@property (nonatomic, readwrite) ChannelUserType type;
@property (nonatomic, strong, readwrite) NSDate *additionDate;
@property (nonatomic, strong, readwrite) NSString *loginEmail;
@property (nonatomic, strong, readwrite) NSString *displayName;
@property (nonatomic, strong, readwrite) NSString *companyId;
@property (nonatomic, strong, readwrite) NSString *companyName;
@end
