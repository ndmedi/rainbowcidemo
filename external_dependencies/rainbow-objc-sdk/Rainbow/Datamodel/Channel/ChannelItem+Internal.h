/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ChannelItem.h"

@interface ChannelItem ()

@property (nonatomic, readwrite, strong) NSString *itemId;
@property (nonatomic, readwrite, strong) NSString *channelId;
@property (nonatomic, readwrite, strong) Contact *contact;
@property (nonatomic, readwrite, strong) NSString *type;
@property (nonatomic, readwrite, strong) NSDate *date;
@property (nonatomic, readwrite, strong) NSDate *editionDate;
@property (nonatomic, readwrite, strong) NSString *message;
@property (nonatomic, readwrite, strong) NSString *title;
@property (nonatomic, readwrite, strong) NSString *url;
@property (nonatomic, readwrite, strong) NSMutableArray *images;
@property (nonatomic, readwrite, strong) NSString *youtubeVideoId;

-(NSDictionary *) dictionaryRepresentation;

@end

