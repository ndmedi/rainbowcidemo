//
//  ConferenceParticipantsTable.m
//  Rainbow
//
//  Created by Vladimir Vyskocil on 30/05/2017.
//  Copyright © 2017 ALE. All rights reserved.
//

#import "ConferenceParticipantsTable.h"
#import "ConferenceParticipantsTable+Internal.h"
#import "ConferenceParticipant+Internal.h"

@implementation ConferenceParticipantsTable

-(instancetype)init {
    self = [super init];
    if(self){
        _participants = [NSMutableDictionary dictionary];
    }
    return self;
}

-(void)dealloc {
    [(NSMutableDictionary *)_participants removeAllObjects];
    _participants = nil;
}

- (NSUInteger)countByEnumeratingWithState:(NSFastEnumerationState *)state objects:(id __unsafe_unretained _Nullable [_Nonnull])buffer count:(NSUInteger)len{
    NSUInteger count = [[_participants copy] countByEnumeratingWithState:state objects:buffer count:len];
    return count;
}

-(BOOL) addOrUpdateParticipant:(ConferenceParticipant *)participant {
    BOOL isUpdated = NO;
    ConferenceParticipant *existingParticipant = [self.participants objectForKey:participant.participantId];
    if(!existingParticipant){
        [(NSMutableDictionary *)self.participants setObject:participant forKey:participant.participantId];
    } else {
        isUpdated = YES;
        if(existingParticipant.role != participant.role){
            existingParticipant.role = participant.role;
        }
        if(![existingParticipant.phoneNumber isEqualToString:participant.phoneNumber]){
            existingParticipant.phoneNumber = participant.phoneNumber;
        }
        if(existingParticipant.state != participant.state){
            existingParticipant.state = participant.state;
        }
        if(existingParticipant.muted != participant.muted){
            existingParticipant.muted = participant.muted;
        }
        if(existingParticipant.hold != participant.hold){
            existingParticipant.hold = participant.hold;
        }
    }
    return isUpdated;
}

-(BOOL) removeParticipant:(ConferenceParticipantId *)participantId {
    BOOL isRemoved = [self.participants objectForKey:participantId] != nil;
    if(isRemoved){
        [(NSMutableDictionary *)self.participants removeObjectForKey:participantId];
    }
    return isRemoved;
}

-(NSUInteger) count {
    return _participants.count;
}

- (ConferenceParticipant *)objectForKey:(ConferenceParticipantId *)aKey {
    return [_participants objectForKey:aKey];
}

-(NSArray<ConferenceParticipant *> *)allValues {
    return [_participants allValues];
}

@end
