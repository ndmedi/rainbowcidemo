/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

@interface ConferenceParticipant ()
@property (strong, readwrite) ConferenceParticipantId *participantId;
@property (strong, readwrite) NSString *jidIM;
@property (strong, readwrite) NSString *phoneNumber;
@property (readwrite) ParticipantRole role;
@property (readwrite) BOOL muted;
@property (readwrite) BOOL hold;
@property (readwrite) ParticipantState state;
@property (strong, readwrite) Contact *contact;
@end
