/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Participant.h"

@interface Participant ()

@property (nonatomic, strong, readwrite) Contact *contact;
@property (nonatomic, readwrite) ParticipantPrivilege privilege;
@property (nonatomic, readwrite) ParticipantStatus status;
@property (nonatomic, strong, readwrite) NSDate *addedDate;

+(ParticipantPrivilege) stringToPrivilege:(NSString *) privilege;
+(ParticipantStatus) stringToStatus:(NSString *) status;
+(NSString *) stringForPrivilege:(ParticipantPrivilege) privilege;
+(NSString *) stringForStatus:(ParticipantStatus) status;

-(NSDictionary *) dictionaryRepresentation;

@end
