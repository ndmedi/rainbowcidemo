/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>

//! Project version number for SDK.
FOUNDATION_EXPORT double RainbowVersionNumber;

//! Project version string for SDK.
FOUNDATION_EXPORT const unsigned char RainbowVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Rainbow/PublicHeader.h>

#import "LoginManager.h"
#import "ServicesManager.h"
#import "LocalContacts.h"
#import "MessagesBrowser.h"
#import "CKBrowsingOperation.h"
#import "CKItemsBrowser+protected.h"
#import "Message+Browsing.h"
#import "MessagesBrowser.h"
#import "ContactsManagerService.h"
#import "ConversationsManagerService.h"
#import "RoomsService.h"
#import "LogsRecorder.h"
#import "defines.h"
#import "Tools.h"
#import "NSDate+Utilities.h"
#import "Peer.h"
#import "Room.h"
#import "Contact.h"
#import "EmailAddress.h"
#import "PhoneNumber.h"
#import "PostalAddress.h"
#import "Presence.h"
#import "Message.h"
#import "Server.h"
#import "Conversation.h"
#import "Room.h"
