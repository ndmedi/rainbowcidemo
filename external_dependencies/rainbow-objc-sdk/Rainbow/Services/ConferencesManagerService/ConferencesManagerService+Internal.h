/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ApiUrlManagerService.h"
#import "XMPPService.h"

typedef void (^ConferenceManagerFetchConfEndpointDetailsCompletionHandler) (NSError *error);
typedef void (^ConferenceManagerDetachMyOpenInviteCompletionHandler) (NSError *error);
typedef void (^ConferenceManagerAttachMyOpenInviteCompletionHandler) (NSError *error);

@interface ConferencesManagerService ()
@property (nonatomic, readwrite, strong) NSString *myOpenInviteId;
@property (nonatomic, readwrite, strong) Room *myOpenInviteRoom;

-(instancetype) initWithDownloadManager:(DownloadManager *) downloadManager apiUrlManagerService:(ApiUrlManagerService *) apiUrlManagerService contactsManagerService:(ContactsManagerService *) contactsManagerService roomsService:(RoomsService *) roomsService xmppService:(XMPPService *) xmppService myUser:(MyUser *)myUser rtcService:(RTCService *) rtcService;

-(Conference *) createOrUpdateConferenceFromJson:(NSDictionary *) jsonDictionary;
-(Conference *) getConferenceByRainbowID:(NSString *) conferenceRainbowID;
-(void) fetchConfEndpointDetails:(ConfEndpoint *) confEndpoint completionBlock:(ConferenceManagerFetchConfEndpointDetailsCompletionHandler) completionHandler;
-(void) addConference:(Conference *)conference;
-(void) removeConferenceWithId:(NSString *)confId;

/**
 *  Create a instant conference
 *  @param  mediatype           PSTN or WebRTC media type
 *  @param  confId              The conference endpoint ID
 *  @param  ownerId             The RainbowID of the owner of the conference
 *  @param  startDate           The creation date of the conference
 */
-(Conference *) createInstantConferenceWithMediaType:(ConferenceEndPointMediaType)mediaType confId:(NSString *)confId ownerId:(NSString *)ownerId startDate:(NSDate *)start;

-(void) attachMyOpenInviteToRoomId:(NSString *)roomId withCompletionHandler: (ConferenceManagerAttachMyOpenInviteCompletionHandler)completionHandler;
-(void) detachMyOpenInviteWithCompletionHandler:(ConferenceManagerDetachMyOpenInviteCompletionHandler)completionHandler;
@end

