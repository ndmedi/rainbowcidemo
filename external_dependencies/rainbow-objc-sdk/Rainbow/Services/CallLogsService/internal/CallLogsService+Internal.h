/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */
#import "CallLogsService.h"
#import "XMPPService.h"
#import "CallLog.h"
#import "MyUser.h"

@interface CallLogsService () <XMPPServiceCallLogsDelegate>
-(instancetype) initWithXMPPService:(XMPPService *) xmppService myUser:(MyUser *) myUser;
@property (nonatomic, strong) XMPPService *xmppService;
@property (nonatomic, strong) MyUser *myUser;
@property (nonatomic, readwrite, strong) NSMutableArray <CallLog*> * callLogs;
@property (nonatomic, strong) NSMutableArray <Peer *> *peers;
@property (nonatomic, strong) NSObject *callLogsMutex;
@property (nonatomic) BOOL callLogFullyLoaded;
@property (nonatomic) BOOL cacheLoaded;
@end
