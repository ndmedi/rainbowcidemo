/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "LocalContactsToolkit.h"
#import "ContactInternal.h"
#import "Tools.h"
#import "GenericMaciOS.h"
#import "ContactInternal.h"
#import "PhoneNumberInternal.h"
#import "EmailAddressInternal.h"
#import "PhoneNumberInternal.h"
#import "PostalAddressInternal.h"
#import "NSObject+NotNull.h"
#import "ContactsManagerServiceToolKit.h"

@implementation LocalContactsToolkit

/**
 *  Quick functions to get the emails of an CNContact without filling a Contact structure.
 *
 *  @param record Extract the emails of this CNContact
 *
 *  @return return an array of all the CNContact emails
 */
+(NSArray<EmailAddress*>*) getEmailAddressFromContact:(CNContact *)contact {
    NSMutableArray<EmailAddress*>* emails = [NSMutableArray array];
    
    for(CNLabeledValue<NSString *> *email in contact.emailAddresses){
        EmailAddress *myEmail = [EmailAddress new];
        myEmail.address = email.value;
        myEmail.type = [EmailAddress typeFromEmailAddressLabel:email.label];
        [emails addObject:myEmail];
    }

    return emails;
}

/**
 *  Quick functions to get the phone numbers of an CNContact without filling a Contact structure.
 *
 *  @param record Extract the phone numbers of this CNContact
 *
 *  @return return an array of all the CNContact phone numbers
 */
+(NSArray<PhoneNumber*>*) getPhoneNumbersFromContact:(CNContact *)contact {
    NSMutableArray<PhoneNumber*>* numbers = [NSMutableArray array];
    
    for(CNLabeledValue<NSString *> *phone in contact.phoneNumbers){
        NSString *phoneNumber = phone.value;
        NSString *phoneNumberLabel = phone.label;
        NSString *trimmedPhoneNb = [phoneNumber stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSMutableString *stripped = [[NSMutableString alloc] initWithString:trimmedPhoneNb];
        // Replace MIDDLE DOT character return in iPhone contact phone number by a whitespace to allow numbering lib to work correctly
        [stripped replaceOccurrencesOfString:@"\xc2\xa0" withString:@" " options:0 range:NSMakeRange(0, [stripped length])];
        
        PhoneNumber *pn = [LocalContactsToolkit createPhoneNumberWithNumber:stripped andLabel:phoneNumberLabel];
        [numbers addObject:pn];
    }
    
    return numbers;
}

+(void) fillLocalContact:(LocalContact*)localContact withContact:(CNContact *)contact {
    localContact.addressBookRecordID = contact.identifier;
    
    [LocalContactsToolkit fillLocalContact:localContact withNameAndFirstNameFromContact:contact];
    [LocalContactsToolkit fillLocalContact:localContact withJobTitleFromContact:contact];
    [LocalContactsToolkit fillLocalContact:localContact withCompanyFromContact:contact];
    [LocalContactsToolkit fillLocalContact:localContact withTelephoneNumbersFromContact:contact];
    [LocalContactsToolkit fillLocalContact:localContact withEmailsFromContact:contact];
    [LocalContactsToolkit fillLocalContact:localContact withWebSitesFromContact:contact];
    [LocalContactsToolkit fillLocalContact:localContact withPhotoFromContact:contact];
    [LocalContactsToolkit fillLocalContact:localContact withPostalAddressFromContact:contact];
}


#pragma mark - Fill contact with ABRecord

+(void) fillLocalContact:(LocalContact*)localContact withNameAndFirstNameFromContact:(CNContact *)contact {
    localContact.lastName = contact.familyName;
    localContact.firstName = contact.givenName;
}

+(void) fillLocalContact:(LocalContact*)localContact withJobTitleFromContact:(CNContact *)contact {
    localContact.jobTitle = contact.jobTitle;
}

+(void) fillLocalContact:(LocalContact*)localContact withCompanyFromContact:(CNContact *)contact {
    localContact.companyName = contact.organizationName;
}

+(void) fillLocalContact:(LocalContact*)localContact withTelephoneNumbersFromContact:(CNContact *)contact {
    [localContact removeAllPhoneNumbers];
    for(CNLabeledValue<CNPhoneNumber *> *phone in contact.phoneNumbers){
        CNPhoneNumber *phoneNumber = phone.value;
        NSString *phoneNumberLabel = phone.label;
        NSString *trimmedPhoneNb = [phoneNumber.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSMutableString *stripped = [[NSMutableString alloc] initWithString:trimmedPhoneNb];
        // Replace MIDDLE DOT character return in iPhone contact phone number by a whitespace to allow numbering lib to work correctly
        [stripped replaceOccurrencesOfString:@"\xc2\xa0" withString:@" " options:0 range:NSMakeRange(0, [stripped length])];
        
        PhoneNumber *pn = [LocalContactsToolkit createPhoneNumberWithNumber:stripped andLabel:phoneNumberLabel];
        [localContact addPhoneNumberObject:pn];
    }
}

+(PhoneNumber *) createPhoneNumberWithNumber:(NSString *) number andLabel:(NSString *) label {
    PhoneNumber *pn = [PhoneNumber new];
    pn.number = number;
    
    // Landline home
    if([label isEqualToString:CNLabelPhoneNumberMain]) {
        pn.type = PhoneNumberTypeHome;
        pn.deviceType = PhoneNumberDeviceTypeLandline;
    }
    // Mobile, consider them as work.
    else if([label isEqualToString:CNLabelPhoneNumberMobile] ||
            [label isEqualToString:CNLabelPhoneNumberiPhone]) {
        pn.type = PhoneNumberTypeWork;
        pn.deviceType = PhoneNumberDeviceTypeMobile;
    }
    // Pager
    else if([label isEqualToString:CNLabelPhoneNumberPager]) {
        pn.type = PhoneNumberTypeOther;
        pn.deviceType = PhoneNumberDeviceTypeOther;
    }
    // Faxes
    else if([label isEqualToString:CNLabelPhoneNumberWorkFax]){
        pn.type = PhoneNumberTypeWork;
        pn.deviceType = PhoneNumberDeviceTypeFax;
    } else if([label isEqualToString:CNLabelPhoneNumberHomeFax]) {
        pn.type = PhoneNumberTypeHome;
        pn.deviceType = PhoneNumberDeviceTypeFax;
    } else if([label isEqualToString:CNLabelPhoneNumberOtherFax]) {
        pn.type = PhoneNumberTypeOther;
        pn.deviceType = PhoneNumberDeviceTypeFax;
    }
    // Default : Landline work
    else {
        pn.type = PhoneNumberTypeWork;
        pn.deviceType = PhoneNumberDeviceTypeLandline;
    }
    
    return pn;
}

+(void) fillLocalContact:(LocalContact*)localContact withEmailsFromContact:(CNContact *)contact {
    for(CNLabeledValue<NSString *> *email in contact.emailAddresses){
        if([Tools isValidEmailAddress:email.value]){
            EmailAddress *myEmail = [LocalContactsToolkit createEmailAddressWithAddress:email.value andLabel:email.label];
            [localContact addEmailAddressObject:myEmail];
        }
    }
}


+(EmailAddress *) createEmailAddressWithAddress:(NSString *) address andLabel:(NSString *) label {
    EmailAddress *email = [EmailAddress new];
    email.address = address;
    
    // Home address
    if([label isEqualToString:CNLabelHome]) {
        email.type = EmailAddressTypeHome;
    }
    // Default : Work address
    else {
        // label = kABWorkLabel
        email.type = EmailAddressTypeWork;
    }
    
    return email;
}

+(void) fillLocalContact:(LocalContact*)localContact withWebSitesFromContact:(CNContact *)contact {
    [localContact.webSitesURL removeAllObjects];
    for(CNLabeledValue<CNContact *> *webSite in contact.urlAddresses){
        [localContact.webSitesURL addObject:(NSString *)webSite.value];
    }
}

+(void) fillLocalContact:(LocalContact*)localContact withPhotoFromContact:(CNContact *)contact {
    if(contact.imageDataAvailable){
        localContact.userPhoto = [[UserPhoto alloc] initWithPhotoData:contact.thumbnailImageData];
    }
}

+(void) fillLocalContact:(LocalContact*)localContact withPostalAddressFromContact:(CNContact *)contact {
    [localContact.addresses removeAllObjects];
    for(CNLabeledValue<CNPostalAddress *> *address in contact.postalAddresses){
        NSString *postalAddressLabel = address.label;
        PostalAddress *postalAddress = [PostalAddress postalAddressFromDictionary:[self createAddressFromDict:address.value]];
        postalAddress.type = [PostalAddress typeFromPostalAddressLabel:postalAddressLabel];
        [localContact.addresses addObject: postalAddress];
        
    }
}

#pragma mark - Fill CNContact with Contact

+ (void) setWorkPhoneNumberInPersonContact:(CNMutableContact *)newPerson fromContact:(Contact*) contact {
    if ([contact.phoneNumbers count] > 0){
        NSMutableArray<CNLabeledValue<CNPhoneNumber *> *> *multiPhone = [newPerson.phoneNumbers copy];

        for (PhoneNumber* phone in contact.phoneNumbers){
            if ([phone.label isEqualToString:CNLabelWork]){
                
                if(![LocalContactsToolkit isPhoneNumber:phone matchingAnyPhoneNumberFromContact:multiPhone]){
                    CNLabeledValue *newPhone = [CNLabeledValue labeledValueWithLabel:CNLabelWork value:phone.number];
                    [multiPhone addObject:newPhone];
                }
            }
        }
        newPerson.phoneNumbers = multiPhone;
    }
}


+(BOOL) isPhoneNumber:(PhoneNumber*)number matchingAnyPhoneNumberFromContact:(NSArray<CNLabeledValue<CNPhoneNumber *> *> *) multiPhone {
    BOOL matching = NO;
    
    for (CNPhoneNumber* phone in multiPhone){
        PhoneNumber* phoneNumber = [PhoneNumber new];
        phoneNumber.number = phone.stringValue;
        matching = [number isEqual:phoneNumber];
        if (matching)
            break;
    }

    return matching;
}

+ (void) setEmailInPersonContact:(CNMutableContact *)newPerson fromContact:(Contact*) contact {
    if( [contact.emailAddresses count] > 0 ){
        NSMutableArray<CNLabeledValue<NSString *> *> *multiEmail = [newPerson.emailAddresses copy];
        for (CNLabeledValue* email in contact.emailAddresses){
            if(![LocalContactsToolkit isEmailAddress:email.value matchingAnyEmailFromContact:multiEmail]){
                CNLabeledValue *newEmail = [CNLabeledValue labeledValueWithLabel:CNLabelWork value:email.value];
                [multiEmail addObject:newEmail];
            }
        }

        newPerson.emailAddresses = multiEmail;
    }
}

+ (BOOL) isEmailAddress:(NSString*) email matchingAnyEmailFromContact:(NSArray<CNLabeledValue<NSString *> *> *) multiEmail {
    BOOL matching = NO;
    
    for (CNLabeledValue* email in multiEmail){
        EmailAddress* emailAddress = [EmailAddress new];
        emailAddress.address = email.value;
        matching = [email isEqual:emailAddress];
        if (matching)
            break;
    }
    
    return matching;
}

+ (void) setAddressInPersonContact:(CNMutableContact *)newPerson fromContact:(Contact*) contact {
        NSMutableArray<CNLabeledValue<CNPostalAddress *> *> *multiAddress = [newPerson.postalAddresses copy];
        for (PostalAddress* address in contact.addresses){
            CNMutablePostalAddress *postalAddress = [[CNMutablePostalAddress alloc] init];
            postalAddress.street = address.street;
            postalAddress.city = address.city;
            postalAddress.state = address.state;
            postalAddress.postalCode = address.postalCode;
            postalAddress.country = address.country;
            postalAddress.ISOCountryCode = address.countryCode;
            NSString *label = nil;
            if(address.type == PostalAddressTypeHome){
                label = CNLabelHome;
            } else {
                label = CNLabelWork;
            }
 
            CNLabeledValue<CNPostalAddress *> *labeledAddress = [[CNLabeledValue alloc] initWithLabel:label value:postalAddress];
            [multiAddress addObject: labeledAddress];
        }

        newPerson.postalAddresses = multiAddress;

}

#pragma mark -

+(NSDictionary*) createAddressFromDict:(CNPostalAddress *) address {
    NSMutableDictionary *addressInfos = [NSMutableDictionary new];

    NSString *street = address.street;
    NSString *zip = address.postalCode;
    NSString *city = address.city;
    NSString *state = address.state;
    NSString *country = address.country;
    NSString *alpha3CountryCode = address.ISOCountryCode;
    
    if(street){
        [addressInfos setObject:street forKey:kPostalAddressStreetKey];
    }
    if(zip){
        [addressInfos setObject:zip forKey:kPostalAddressZipKey];
    }
    if(city){
        [addressInfos setObject:city forKey:kPostalAddressCityKey];
    }
    if(state){
        [addressInfos setObject:state forKey:kPostalAddressStateKey];
    }
    if(country){
        [addressInfos setObject:country forKey:kPostalAddressCountryKey];
    }
    if(alpha3CountryCode){
        [addressInfos setObject:alpha3CountryCode forKey:kPostalAddressCountryCodeKey];
    }
    return addressInfos;
}

+(NSString *) getISO3611Alpha3CodeFor:(NSString *) alpha2Code {
    NSString *myPlistFilePath = [[NSBundle bundleForClass:[LocalContactsToolkit class]] pathForResource: @"iso3166_1_2_to_iso3166_1_3" ofType: @"plist"];
    NSDictionary *cities = [NSDictionary dictionaryWithContentsOfFile: myPlistFilePath];
    return [cities objectForKey:alpha2Code];
}

@end
