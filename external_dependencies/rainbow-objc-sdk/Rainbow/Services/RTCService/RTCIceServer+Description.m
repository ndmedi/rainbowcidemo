/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <WebRTC/WebRTC.h>

@implementation RTCIceServer (Description)

-(NSString *)description {
    return [NSString stringWithFormat:@"RTCIceServer %p <urls %@, tlsCertPolicy %lu, hostName %@", self, self.urlStrings, (unsigned long)self.tlsCertPolicy, self.hostname];
}
@end
