/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "SourcesManagerService.h"
#import "LoginManager.h"
#import "ContactsManagerService+Internal.h"
#import "NSDictionary+JSONString.h"
#import "NSData+JSON.h"
#import "Source+Internal.h"
#import "MyUser+internal.h"
#import "LocalContact.h"

@interface SourcesManagerService ()
@property (nonatomic, strong) DownloadManager *downloadManager;
@property (nonatomic, strong) MyUser *myUser;
@property (nonatomic, strong) ApiUrlManagerService * apiUrlManagerService;
@property (nonatomic, strong) NSOperationQueue *sourcesOperationQueue;

@end

@implementation SourcesManagerService

-(instancetype) initWithDownloadManager:(DownloadManager *) downloadManager myUser:(MyUser *) myUser apiUrlManagerService:(ApiUrlManagerService *) apiUrlManagerService {
    self = [super init];
    if(self){
        _downloadManager = downloadManager;
        _myUser = myUser;
        _apiUrlManagerService = apiUrlManagerService;
        
        [_myUser.source addObserver:self forKeyPath:kServerSourceIdKey options:NSKeyValueObservingOptionNew context:nil];
        
        // Login / Logout notifications
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerInternalDidLoginSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:kLoginManagerDidLogoutSucceeded object:nil];
        
        // Local contacts notifications
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localContactAccessGranted:) name:kContactsManagerServiceLocalAccessGrantedNotification object:nil];
        _sourcesOperationQueue = [NSOperationQueue new];
    }
    return self;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceLocalAccessGrantedNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLogoutSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerInternalDidLoginSucceeded object:nil];
    _sourcesOperationQueue = nil;
    [_myUser.source removeObserver:self forKeyPath:kServerSourceIdKey context:nil];
    _apiUrlManagerService = nil;
    _myUser = nil;
    _downloadManager = nil;
}

#pragma mark - Notifications and observers

-(void) didLogin:(NSNotification *) notification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didAddLocalContact:) name:kContactsManagerServiceDidAddLocalContact object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateLocalContact:) name:kContactsManagerServiceDidUpdateLocalContact object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveLocalContact:) name:kContactsManagerServiceDidRemoveLocalContact object:nil];
    [self createContactsSource];
}

-(void) didLogout:(NSNotification *) notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidAddLocalContact object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidUpdateLocalContact object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidRemoveLocalContact object:nil];
}

-(void) localContactAccessGranted:(NSNotification *) notification {
    
}

-(void) didAddLocalContact:(NSNotification *) notification {
    LocalContact *contact = (LocalContact *)notification.object;
    NSLog(@"DID ADD LOCAL CONTACT %@",[contact jsonRepresentation]);
    NSDictionary *contactDic = [_myUser.source getContactServerInformationForAddressBookRecordId:[contact contactLocalID]];
    if(contactDic){
        // We already have uploaded this contact, so check if we must update it.
        NSString *contactJsonRepr = [contactDic objectForKey:contactDic.allKeys[0]];
        if(![[contact jsonRepresentation] isEqualToString:contactJsonRepr]){
            // We must update it.
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW,0), ^{
                [self sendUpdatedContactsToServer:contact];
            });
        }
    } else {
        // It's a new contact upload it.
    }
}

-(void) didUpdateLocalContact:(NSNotification *) notification {
    LocalContact *contact = (LocalContact *)notification.object;
    NSLog(@"DID UPDATE LOCAL CONTACT %@",[contact jsonRepresentation]);
    NSDictionary *contactDic = [_myUser.source getContactServerInformationForAddressBookRecordId:[contact contactLocalID]];
    if(contactDic){
        // We already have uploaded this contact, so check if we must update it.
    }
}

-(void) didRemoveLocalContact:(NSNotification *) notification {
    LocalContact *contact = (LocalContact *)notification.object;
    NSLog(@"DID REMOVE LOCAL CONTACT %@",[contact jsonRepresentation]);
    NSDictionary *contactDic = [_myUser.source getContactServerInformationForAddressBookRecordId:[contact contactLocalID]];
    if(contactDic){
        // We already have uploaded this contact, so check if we must remove it.
    } else {
        // It's contact upload was not uploaded ... nothing todo.
    }
}
/*
-(void) deviceContactsChanged:(NSNotification *) notification {
    NSArray<Contact*> *contactsBeforeUpdate = (NSArray *)notification.object;
    @synchronized(_contactsMutex) {
        NSMutableArray<Contact*> *addedContacts = [NSMutableArray array];
        NSMutableArray<Contact*> *updatedContacts = [NSMutableArray array];
        NSMutableArray<Contact*> *removedContacts = [NSMutableArray array];
        
        [_localContacts.contacts enumerateObjectsUsingBlock:^(Contact *contact, NSUInteger idx, BOOL * stop) {
            if(![_contacts containsObject:contact] && ![_contactsPoolManager findRememberedContactThatMatchContact:contact])
                [addedContacts addObject:contact];
        }];
        
        // Look for updated contacts
        if(contactsBeforeUpdate.count > 0){
            [contactsBeforeUpdate enumerateObjectsUsingBlock:^(Contact * oldContact, NSUInteger idx, BOOL * stop) {
                Contact * newContact = [_localContacts getContactWithAddressBookRecordID:oldContact.addressBookRecordID];
                if(newContact){
                    if(![[oldContact jsonRepresentationIncludingContactId:YES] isEqualToString:[newContact jsonRepresentationIncludingContactId:YES]]){
                        [updatedContacts addObject:newContact];
                    }
                }
            }];
        } else {
            // This is certainly a startup of an application so we must check if there is a difference between already uploaded contacts and local contacts.
            [_localContacts.contacts enumerateObjectsUsingBlock:^(Contact * aContact, NSUInteger idx, BOOL * stop) {
                NSDictionary *contactDic = [_myUser.source getContactServerInformationForAddressBookRecordId:[aContact contactLocalID]];
                if(contactDic){
                    NSString *contactJsonRepr = [contactDic objectForKey:contactDic.allKeys[0]];
                    if(![[aContact jsonRepresentationIncludingContactId:YES] isEqualToString:contactJsonRepr]){
                        // This contact has been updated and we don't update it on the server.
                        [updatedContacts addObject:aContact];
                    }
                }
            }];
        }
        
        [_contacts enumerateObjectsUsingBlock:^(Contact *contact, NSUInteger idx, BOOL * stop) {
            if(contact.addressBookRecordID != -1 && ![_localContacts.contacts containsObject:contact] && ![_contactsPoolManager findRememberedContactThatMatchContact:contact])
                [removedContacts addObject:contact];
        }];
        
        if(addedContacts.count > 0){
            [((NSMutableArray*)_contacts) addObjectsFromArray:addedContacts];
            [self didAddContacts:addedContacts];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW,0), ^{
                //                [self sendNewContactsToServer:addedContacts];
                [self searchForRainbowUserInContacts:addedContacts];
            });
        }
        
        if(updatedContacts.count > 0){
            //            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW,0), ^{
            //                [self sendUpdatedContactsToServer:updatedContacts];
            //            });
            [updatedContacts enumerateObjectsUsingBlock:^(Contact * updatedContact, NSUInteger idx, BOOL * stop) {
                NSInteger idxOfContact = [_contacts indexOfObject:updatedContact];
                if(idxOfContact != NSNotFound){
                    // We must update the contact
                    Contact *contactToUpdate = [_contacts objectAtIndex:idxOfContact];
                    Contact *theContact = nil;
                    if([contactToUpdate isKindOfClass:[ResolvedContact class]])
                        theContact = ((ResolvedContact *)contactToUpdate).localContact;
                    if(theContact){
                        [theContact updateWith:updatedContact];
                        [self didUpdateContact:theContact];
                    } else {
                        [contactToUpdate updateWith:updatedContact];
                        [self didUpdateContact:contactToUpdate];
                    }
                }
                
                
                NSDictionary *persistedContactsInfos = [_myUser getContactServerInfoForAddressBookRecordId:[updatedContact contactLocalID]];
                if(persistedContactsInfos.count == 2){
                    // We already search for this user, check if it doesn't changed
                    [_myUser removeContactWithAddressBookRecordId:[updatedContact contactLocalID]];
                    [_myUser writeContactSearchMatchingToDisk];
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW,0), ^{
                        [self searchForRainbowUserInContacts:@[updatedContact]];
                    });
                }
            }];
        }
        
        if(removedContacts.count > 0){
            //            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW,0), ^{
            //                [self sendDeletedContactsToServer:removedContacts];
            //            });
            [((NSMutableArray*)_contacts) removeObjectsInArray:removedContacts];
            [self didRemoveContacts:removedContacts];
        }
    }
}
*/
-(void) observeValueForKeyPath:(NSString *)keyPath ofObject:(Contact *) contact change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if([keyPath isEqualToString:@"serverSourceId"]){
        //        NSLog(@"New server source ID, we could start upload contacts");
        //        [self sendNewContactsToServer:_localContacts.contacts];
        //[self searchForRainbowUserInLocalContacts/*:_localContacts.contacts*/];
        return;
    }
}

#pragma mark - Source creation

-(void) createContactsSource {
    // create a source on server if needed
    if(_myUser.source.needToUpdateSource){
        NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesSources];
        NSDictionary *headersParameters = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesSources];
        
        NSDictionary *bodyContent = @{@"sourceId":_myUser.source.deviceSourceId, @"os":_myUser.source.osInformation};
        
        [_downloadManager postRequestWithURL:url body:[bodyContent jsonStringWithPrettyPrint:YES] requestHeadersParameter:headersParameters completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
            NSDictionary *jsonResponse = [receivedData objectFromJSONData];
            if(!error && [jsonResponse objectForKey:@"errorMsg"] == nil){
                NSString *serverSourceID = jsonResponse[@"data"][@"id"];
                [_myUser setServerSourceID:serverSourceID];
                NSLog(@"Source created with ID %@", serverSourceID);
                [_myUser writeSourceOnDisk];
            } else {
                NSLog(@"Could not create source raison : %@", jsonResponse[@"errorDetails"]);
            }
        }];
    } else {
        NSLog(@"Source is already existing no need to create a new one.");
    }
}

-(void) sendNewContactsToServer:(LocalContact *) contact {
    if(_myUser.source.serverSourceId.length > 0){
        NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesContacts];
        NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesContacts];
        
        if(![_myUser.source getContactServerInformationForAddressBookRecordId:[contact contactLocalID]]){
            NSData *receivedData = nil;
            NSError *requestError = nil;
            NSURLResponse *response = nil;
            BOOL requestSucceed = [_downloadManager postSynchronousRequestWithURL:url body:[contact jsonRepresentation] requestHeadersParameter:headers returningResponseData:&receivedData returningNSURLResponse:&response returningError:&requestError];
            if(requestSucceed && !requestError){
                NSDictionary *response = [receivedData objectFromJSONData];
                if(!requestError && [response objectForKey:@"errorMsg"] == nil){
                    // Saving sourceId has valid, and saving received id
                    NSString *contactServerID = response[@"data"][@"id"];
                    NSString *addressBookRecordID = response[@"data"][@"contactId"];
                    [_myUser.source insertContactServerId:contactServerID contactInfo:[contact jsonRepresentation] forAddressBookRecordId:addressBookRecordID];
                    [_myUser writeSourceOnDisk];
                    NSLog(@"Contact created %@, uploaded to server with ID %@",[contact jsonRepresentation], contactServerID);
                } else {
                    NSLog(@"Could not create contact raison : %@", response[@"errorDetails"]);
                }
            }
        } else
            NSLog(@"Don't upload this contact, we already upload it");
    } else
        NSLog(@"No source found");
}

-(void) sendUpdatedContactsToServer:(LocalContact *) contact {
    if(_myUser.source.serverSourceId.length > 0){
        NSURL *serviceUrl = [_apiUrlManagerService getURLForService:ApiServicesContacts];
        NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesContacts];
        
        NSDictionary *contactsDic = [_myUser.source getContactServerInformationForAddressBookRecordId:[contact contactLocalID]];
        NSString *contactServerID = contactsDic.allKeys[0];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",serviceUrl, contactServerID]];
        NSData *receivedData = nil;
        NSError *requestError = nil;
        NSURLResponse *response = nil;
        BOOL requestSucceed = [_downloadManager putSynchronousRequestWithURL:url body:[contact jsonRepresentation] requestHeadersParameter:headers returningResponseData:&receivedData returningNSURLResponse:&response returningError:&requestError];
        if(requestSucceed && !requestError){
            NSDictionary *response = [receivedData objectFromJSONData];
            if(!requestError && [response objectForKey:@"errorMsg"] == nil){
                // Saving sourceId has valid, and saving received id
                NSString *contactServerID = response[@"data"][@"id"];
                NSString *addressBookRecordID = response[@"data"][@"contactId"];
                [_myUser.source removeContactAddressBookRecordId:[contact contactLocalID]];
                [_myUser.source insertContactServerId:contactServerID contactInfo:[contact jsonRepresentation] forAddressBookRecordId:addressBookRecordID];
                [_myUser writeSourceOnDisk];
                NSLog(@"Upload updated contact %@ to server with ID %@",[contact jsonRepresentation], contactServerID);
            } else {
                NSLog(@"Could not update contact, raison : %@", response[@"errorDetails"]);
            }
        }
    } else
        NSLog(@"No source found");
}

-(void) sendDeletedContactsToServer:(LocalContact *) contact {
    if(_myUser.source.serverSourceId.length > 0){
        NSURL *serviceUrl = [_apiUrlManagerService getURLForService:ApiServicesContacts];
        NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesContacts];
        
        NSDictionary *contactsDic = [_myUser.source getContactServerInformationForAddressBookRecordId:[contact contactLocalID]];
        NSString *contactServerID = contactsDic.allKeys[0];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@",serviceUrl, contactServerID]];
        NSData *receivedData = nil;
        NSError *requestError = nil;
        NSURLResponse *response = nil;
        BOOL requestSucceed = [_downloadManager deleteSynchronousRequestWithURL:url requestHeadersParameter:headers returningResponseData:&receivedData returningNSURLResponse:&response returningError:&requestError];
        if(requestSucceed && !requestError){
            NSDictionary *response = [receivedData objectFromJSONData];
            if(!requestError && [response objectForKey:@"errorMsg"] == nil){
                [_myUser.source removeContactAddressBookRecordId:[contact contactLocalID]];
                [_myUser writeSourceOnDisk];
                NSLog(@"Deleted contact %@ with server ID %@",[contact jsonRepresentation], contactServerID);
            } else {
                NSLog(@"Could not delete contact, raison : %@", response[@"errorDetails"]);
            }
        }
    } else
        NSLog(@"No source found");
}

@end
