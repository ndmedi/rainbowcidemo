/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "MessagesPageOperation.h"

@interface MessagesPageOperation ()
@property (nonatomic, strong) Peer *peer;
@property (nonatomic, strong) XMPPService *xmppService;
@property (nonatomic, strong) NSCondition *operationLock;
@property (nonatomic, copy) MessagePageOperationCompletionHandler completionHandler;
@end

@implementation MessagesPageOperation

-(instancetype) initWithPeer:(Peer *) peer withXMPPService:(XMPPService *) xmppService withCompletionHandler:(MessagePageOperationCompletionHandler) completionHandler {
    self = [super init];
    if(self) {
        _peer = peer;
        _xmppService = xmppService;
        _operationLock = [NSCondition new];
        _completionHandler = completionHandler;
    }
    return self;
}

-(void) dealloc {
    _peer = nil;
    _xmppService = nil;
    _operationLock = nil;
    _completionHandler = nil;
}

-(NSArray*) performJobWithError:(NSError**)error {
    __block NSArray* pageMessages = nil;
    __block BOOL hasPerformedRequest = NO;
    [_xmppService loadArchivedMessagesWith:_peer maxSize:[NSNumber numberWithInteger:_itemsCount] beforeDate:_beforeDate lastMessageID:_lastMessageID offset:[NSNumber numberWithInteger:self.startIndex] withCompletionHandler:^(NSArray *archive, NSError *error, NSString *firstElementRetreived, NSString *lastElementRetreived, BOOL isCompleted, NSNumber *totalCount) {
        if(!error){
            pageMessages = [NSArray arrayWithArray:archive];            
            self.hasMorePages = !isCompleted;
        }
        [_operationLock lock];
        hasPerformedRequest = YES;
        [_operationLock signal];
        [_operationLock unlock];
        
        if(_completionHandler)
            _completionHandler(firstElementRetreived, lastElementRetreived, isCompleted, [NSNumber numberWithLong:[pageMessages count]]);
    }];
    
    [_operationLock lock];
    while (!hasPerformedRequest) {
        [_operationLock wait];
    }
    [_operationLock unlock];
    
    return pageMessages;
}

-(NSString *) description {
    return [NSString stringWithFormat:@"MessagesPageOperation - %@",[super description]];
}

@end
