/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "MessagesCachedPageOperation.h"

@interface MessagesCachedPageOperation ()
@property (nonatomic, strong) Peer *peer;
@property (nonatomic, strong) XMPPService *xmppService;
@end

@implementation MessagesCachedPageOperation

-(instancetype) initWithPeer:(Peer *) peer withXMPPService:(XMPPService *) xmppService {
    self = [super init];
    if(self) {
        _peer = peer;
        _xmppService = xmppService;
    }
    return self;
}

-(void) dealloc {
    _peer = nil;
    _xmppService = nil;
}

-(NSArray*) performJobWithError:(NSError**)error {
     NSArray *pageMessages = [_xmppService getArchivedMessageWithContactJid:_peer.jid maxSize:[NSNumber numberWithInteger:_itemsCount] offset:[NSNumber numberWithInteger:self.startIndex]];
    [pageMessages enumerateObjectsUsingBlock:^(Message *obj, NSUInteger idx, BOOL * stop) {
        [_xmppService.conversationDelegate xmppService:_xmppService didReceiveArchivedMessage:obj];
    }];
    
    return pageMessages;
}
-(NSString *) description {
    return [NSString stringWithFormat:@"MessagesCachedPageOperation - %@",[super description]];
}

@end
