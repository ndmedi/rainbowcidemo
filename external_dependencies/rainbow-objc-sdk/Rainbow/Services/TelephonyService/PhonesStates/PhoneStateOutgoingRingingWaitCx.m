//
//  PhoneStateOutgoingRingingWaitCx.m
//  Rainbow
//
//  Created by Jean-Luc on 21/05/2018.
//  Copyright © 2018 ALE. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhoneStateOutgoingRingingWaitCx.h"
#import "AllPhoneStates.h"
#import "RTCService+Internal.h"


@implementation PhoneStateOutgoingRingingWaitCx

- (void) abortMakeCall
{
    OTCLog (@"abortMakeCall : change to Idle");
    [self.telService setState:[PhoneStateIdle class]];
}


- (void) timerOff
{
    [self gsmIdle:nil];
}


- (void) gsmRinging:(CXCall *) cxCall
{
    if (cxCall)
    {
        OTCLog (@"PhoneState no change on GsmRinging (stop timer)");
        [self.telService stopTimer];
        //    [self.telService setState:[PhoneStateMakeCallGsmRinging class]];
    }
    else   // rtcWeb call
    {
        OTCLog (@"PhoneState no change : InitMakeCall -> GsmRinging for rtcCall, should auto-answer...");
        [self.telService addRtcCallAsOutgoing:YES];
        [NSThread sleepForTimeInterval:0.2f];
        RTCCall* rtCall = [self.telService rtcCallFromCxCallOne:cxCall];
        if (rtCall)
            [self.rtcService acceptIncomingCall:rtCall withFeatures:RTCCallFeatureAudio];
    }
}


- (void) gsmConnected:(CXCall *) call
{
    OTCLog (@"gsmConnected : change to OutgoingRinging");
    [self.telService notifyCallToDisplay:nil];
    [self.telService setState:[PhoneStateOutgoingRinging class]];
}

@end
