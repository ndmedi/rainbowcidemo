//
//  PhoneStateInitMakeCall.m
//  Rainbow
//
//  Created by Jean-Luc on 21/05/2018.
//  Copyright © 2018 ALE. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhoneStateInitMakeCall.h"
#import "AllPhoneStates.h"
#import "RTCService+Internal.h"


@implementation PhoneStateInitMakeCall

- (void) timerOff
{
    [self gsmIdle:nil];
}


- (void) startMakeCall
{
    OTCLog (@"PhoneState event : startMakeCall -> new business call ?");
}


- (void) abortMakeCall
{
    OTCLog (@"abortMakeCall : change to Idle");
    [self.telService setState:[PhoneStateIdle class]];
}


- (void) gsmRinging:(CXCall*) cxCall
{
    if (cxCall)
    {
        OTCLog (@"PhoneState no change on GsmRinging (stop timer)");
        [self.telService stopTimer];
        //    [self.telService setState:[PhoneStateMakeCallGsmRinging class]];
    }
    else   // rtcWeb call
    {
        OTCLog (@"PhoneState no change : InitMakeCall -> GsmRinging for rtcCall, should auto-answer...");
        [self.telService addRtcCallAsOutgoing:YES];
        [NSThread sleepForTimeInterval:0.2f];
        RTCCall* rtCall = [self.telService rtcCallFromCxCallOne:cxCall];
        if (rtCall)
            [self.rtcService acceptIncomingCall:rtCall withFeatures:RTCCallFeatureAudio];
    }
}


- (void) gsmConnected:(CXCall*) call
{
    OTCLog (@"gsmConnected : change to OutgoingRinging");
    [self.telService setState:[PhoneStateOutgoingRinging class]];
}


-(void) releaseEvent:(Call*) aCall
{
    // Android considers that a release in this state is an error
    NSString* errMsg =  @"ReleaseDuringMakeCall";
    if ([aCall.callCause isEqualToString:@"NORMALCLEARING"])
    {
        aCall.status = CallStatusHangup;
        aCall.callCause = errMsg;
    }
    OTCLog (@"releaseEvent : change to Idle, and indicate the cause '%@'", aCall.callCause);
    [self.telService removeCall:aCall];
    [self.telService setState:[PhoneStateIdle class]];
}


-(void) outgoingRingEvent:(Call*) aCall callCause:(NSString*) callCause
{
    OTCLog (@"outgoingRingEvent : change to OutgoingRingingWaitCx");
    [self.telService setState:[PhoneStateOutgoingRingingWaitCx class]];
}

@end
