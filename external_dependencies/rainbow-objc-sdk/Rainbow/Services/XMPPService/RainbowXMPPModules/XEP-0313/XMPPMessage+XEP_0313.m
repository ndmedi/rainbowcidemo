/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "XMPPMessage+XEP_0313.h"
#import "NSXMLElement+XMPP.h"
#import "NSXMLElement+XEP_0297.h"
#import "XMPPDateTimeProfiles.h"

static NSString *const xmlns_mam = @"urn:xmpp:mam:tmp";
static NSString *const xmlns_receipts = @"urn:xmpp:receipts";
#define NAME_XMPP_STANZA_RESULT @"result"
#define XMLNS_XMPP_STANZA_RESULT @"urn:xmpp:mam:1"

@implementation XMPPMessage (XEP_0313)
-(BOOL) isArchivedMessage {
    return [self elementForName:@"archived" xmlns:xmlns_mam];
}

-(NSString *) archivedMamMessageID {
    return [[self elementForName:@"archived" xmlns:xmlns_mam] attributeForName:@"id"].stringValue ;
}

-(BOOL) isResultStanza {
    return [self isResultStanzaWithNamespace:XMLNS_XMPP_STANZA_RESULT];
}

-(BOOL) isResultStanzaWithNamespace:(NSString *) aNamespace {
    if([self elementForName:@"result" xmlns:aNamespace])
        return YES;
    else
        return NO;
}

-(NSXMLElement *) resultStanzaWithNamespace:(NSString *) aNamespace {
    if([self isResultStanzaWithNamespace:aNamespace]) {
        return [self elementForName:@"result" xmlns:aNamespace];
    }
    return nil;
}

-(NSXMLElement *) resultStanza {
    return [self resultStanzaWithNamespace:XMLNS_XMPP_STANZA_RESULT];
}

-(XMPPMessage *) resultForwardedMessage {
    if([self isResultStanza] && [[self resultStanza] hasForwardedStanza]){
        return [[[self resultStanza] forwardedStanza] forwardedMessage];
    }
    return nil;
}


#pragma mark - Custom Rainbow <ack/> markup

/**
 *  This returns NO if we have the attribute "received"="false" in the <ack/>
 *  any other case is YES. (markup missing, attribute missing, attribute = true)
 *  because by default (if <ack> is missing), the message is considered as received.
 *
 *  @return Whether the message is marked as received or not.
 */
-(BOOL) isArchivedMessageReceived {
    if ([[[self elementForName:@"ack"] attributeStringValueForName:@"received"] isEqualToString:@"true"])
        return YES;
    return NO;
}

-(BOOL) isArchivedMessageSent {
    return (!([self isArchivedMessageDelivered] && [self isArchivedMessageRead] && [self isArchivedMessageReceived]));
}

-(BOOL) isArchivedMessageDelivered {
    if ([[[self elementForName:@"ack"] attributeStringValueForName:@"delivered"] isEqualToString:@"true"])
        return YES;
    return NO;
}

-(NSDate *) archivedMessageDeliveredDate {
    NSDate *deliveredDate = [XMPPDateTimeProfiles parseDateTime:[[self elementForName:@"ack"] attributeStringValueForName:@"delivered_timestamp"]];
    if(!deliveredDate)
        deliveredDate = [NSDate date];
    return deliveredDate;
}

/**
 *  This returns the datetime when the message was received, or nil.
 *
 *  @return The datetime when message was received
 */
-(NSDate *) archivedMessageReceivedDate {
    return [XMPPDateTimeProfiles parseDateTime:[[self elementForName:@"ack"] attributeStringValueForName:@"recv_timestamp"]];
}

/**
 *  This returns NO if we have the attribute "read"="false" in the <ack/>
 *  any other case is YES. (markup missing, attribute missing, attribute = true)
 *  because by default (if <ack> is missing), the message is considered as read.
 *
 *  @return Whether the message is marked as read or not.
 */
-(BOOL) isArchivedMessageRead {
    if ([[[self elementForName:@"ack"] attributeStringValueForName:@"read"] isEqualToString:@"true"])
        return YES;
    return NO;
}

/**
 *  This returns the datetime when the message was read, or nil.
 *
 *  @return The datetime when message was read
 */
-(NSDate *) archivedMessageReadDate {
    return [XMPPDateTimeProfiles parseDateTime:[[self elementForName:@"ack"] attributeStringValueForName:@"read_timestamp"]];
}

/**
 *  This returns the datetime when the message was archived, or nil.
 *
 *  @return The datetime when message was archived
 */
-(NSDate *) archivedMessageDate {
    return [XMPPDateTimeProfiles parseDateTime:[[self elementForName:@"archived" xmlns:xmlns_mam] attributeStringValueForName:@"stamp"]];
}

/**
 *  This returns the timestamp or nil.
 *
 *  @return The timestamp 
 */
-(NSDate *) timestampMessageDate {
    return [XMPPDateTimeProfiles parseDateTime:[[self elementForName:@"timestamp" xmlns:xmlns_receipts] attributeStringValueForName:@"value"]];
}

/**
 * This return the time or nil
 * @parm NSString date
 * @return the time from date
 */
-(NSDate *) getTimeFromString :(NSString *) date{
    return [XMPPDateTimeProfiles parseDateTime:date];
}
@end
