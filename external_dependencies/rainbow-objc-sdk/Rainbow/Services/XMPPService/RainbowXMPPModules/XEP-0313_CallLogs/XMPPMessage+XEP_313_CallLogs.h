/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "XMPPMessage.h"
#import "XMPPMessage+XEP_0313.h"

#define XMLNS_XMPP_MAM_IQ_CALLLOGS @"jabber:iq:telephony:call_log"
#define XMLNS_XMPP_MAM_CALLLOGS @"jabber:iq:notification:telephony:call_log"
#define XMLNS_XMPP_CALLLOGS_RECEIPTS @"urn:xmpp:telephony:call_log:receipts"

@interface XMPPMessage (XEP_313_CallLogs)
-(BOOL) isCallLogMessage;
-(NSXMLElement *) callLogResultStanza;
-(NSXMLElement *) callLogStanza;
-(NSDate *) callLogForwardedDate;

-(BOOL) isUpdatedCallLogMessage;
-(NSXMLElement *) updatedCallLogStanza;
-(NSXMLElement *) updatedCallLogForwardedStanza;
-(BOOL) isDeletedCallLogMessage;
-(BOOL) isReadCallLogNotificationMessage;
@end
