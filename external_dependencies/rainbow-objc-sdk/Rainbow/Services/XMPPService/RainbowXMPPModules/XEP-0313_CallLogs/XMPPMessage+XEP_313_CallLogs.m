/*
 * Rainbow
 *
 * Copyright (c) 2017, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "XMPPMessage+XEP_313_CallLogs.h"
#import "NSXMLElement+XMPP.h"
#import "NSXMLElement+XEP_0297.h"

@implementation XMPPMessage (XEP_313_CallLogs)
-(BOOL) isCallLogMessage {
    if([self isResultStanzaWithNamespace:XMLNS_XMPP_MAM_IQ_CALLLOGS]){
        NSXMLElement *forwardedStanza = [[self resultStanzaWithNamespace:XMLNS_XMPP_MAM_IQ_CALLLOGS] forwardedStanza];
        if([forwardedStanza elementForName:@"call_log" xmlns:XMLNS_XMPP_MAM_CALLLOGS]){
            return YES;
        }
    }
    return NO;
}

-(NSXMLElement *) callLogResultStanza {
    if([self isCallLogMessage])
        return [self resultStanzaWithNamespace:XMLNS_XMPP_MAM_IQ_CALLLOGS];
    return nil;
}

-(NSXMLElement *) callLogStanza {
    if([self isCallLogMessage])
        return [[[self callLogResultStanza] forwardedStanza] elementForName:@"call_log" xmlns:XMLNS_XMPP_MAM_CALLLOGS];
    return nil;
}

-(NSDate *) callLogForwardedDate {
    if([self isCallLogMessage]) {
        return [[[self callLogResultStanza] forwardedStanza] forwardedStanzaDelayedDeliveryDate];
    }
    return nil;
}

-(BOOL) isUpdatedCallLogMessage {
    return [self updatedCallLogStanza] != nil;
}

-(NSXMLElement * ) updatedCallLogStanza {
    NSXMLElement *updatedCallLogElement = [self elementForName:@"updated_call_log" xmlns:XMLNS_XMPP_MAM_CALLLOGS];
    if(updatedCallLogElement){
        NSXMLElement *forwarded = [updatedCallLogElement forwardedStanza];
        if(forwarded){
            NSXMLElement *callLog = [forwarded elementForName:@"call_log" xmlns:XMLNS_XMPP_MAM_CALLLOGS];
            if(callLog)
                return callLog;
        }
    }
    return nil;
}

-(NSXMLElement *) updatedCallLogForwardedStanza {
    NSXMLElement *updatedCallLogElement = [self elementForName:@"updated_call_log" xmlns:XMLNS_XMPP_MAM_CALLLOGS];
    if(updatedCallLogElement){
        NSXMLElement *forwarded = [updatedCallLogElement forwardedStanza];
        if(forwarded)
            return forwarded;
    }
    return nil;
}

-(BOOL) isDeletedCallLogMessage {
    NSXMLElement *deletedCallLogElement = [self elementForName:@"deleted_call_log" xmlns:XMLNS_XMPP_MAM_CALLLOGS];
    if(deletedCallLogElement)
        return YES;
    
    return NO;
}

-(BOOL) isReadCallLogNotificationMessage {
    NSXMLElement *readElement = [self elementForName:@"read" xmlns:XMLNS_XMPP_CALLLOGS_RECEIPTS];
    if(readElement)
        return YES;
    return NO;
}

@end
