/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "XMPPMessage+Retransmission.h"
#import "NSXMLElement+XMPP.h"

@implementation XMPPMessage (Retransmission)
// Deal with balise <retransmission xmlns="jabber:iq:notification" source="push"/>
/**
 <message xmlns="jabber:client" from="c2673e1952ef40178b68cb4ae2c6c2ed@jerome-all-in-one-dev-1.opentouch.cloud/web_win_1.25.7_670RBIS0" to="e1a3c0b9bab449bbbf4968421b751b32@jerome-all-in-one-dev-1.opentouch.cloud/mobile_ios_F9F4BF6A-D63A-40AB-BD19-B6BDFC90CA19" type="chat" id="web_71ddb391-148f-4613-aef5-2d6c3a105c5631" default:lang="fr"><archived xmlns="urn:xmpp:mam:tmp" by="jerome-all-in-one-dev-1.opentouch.cloud" stamp="2017-06-05T12:02:41.222744Z"/><archived xmlns="urn:xmpp:mam:tmp" by="jerome-all-in-one-dev-1.opentouch.cloud" stamp="2017-06-05T12:02:30.312385Z"/><body xmlns="jabber:client" lang="fr">salut ca va bien ?</body><request xmlns="urn:xmpp:receipts"/><active xmlns="http://jabber.org/protocol/chatstates"/><delay xmlns="urn:xmpp:delay" from="jerome-all-in-one-dev-1.opentouch.cloud" stamp="2017-06-05T12:02:30.313Z"/><retransmission xmlns="jabber:iq:notification" source="push"/></message>
 */
-(BOOL) isRetransmissionMessage {
    NSXMLElement *retransmissionElement = [self elementForName:@"retransmission" xmlns:@"jabber:iq:notification"];
    if(retransmissionElement)
        return YES;
    
    return NO;
}
@end
