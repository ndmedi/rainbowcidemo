/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */


#import "XMPPRoom+ServerShutdown.h"

enum XMPPRoomState
{
    kXMPPRoomStateNone        = 0,
    kXMPPRoomStateCreated     = 1 << 1,
    kXMPPRoomStateJoining     = 1 << 3,
    kXMPPRoomStateJoined      = 1 << 4,
    kXMPPRoomStateLeaving     = 1 << 5,
};

@implementation XMPPRoom (ServerShutdown)
-(void) changeLeaveState {
    dispatch_block_t block = ^{ @autoreleasepool {
        
        state &= ~kXMPPRoomStateJoining;
        state &= ~kXMPPRoomStateJoined;
        state |=  kXMPPRoomStateLeaving;
        
    }};
    
    if (dispatch_get_specific(moduleQueueTag))
        block();
    else
        dispatch_async(moduleQueue, block);
}
@end
