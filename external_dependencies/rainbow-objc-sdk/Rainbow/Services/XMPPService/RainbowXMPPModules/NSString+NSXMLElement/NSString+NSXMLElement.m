/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "NSString+NSXMLElement.h"
#import "NSXMLElement+XMPP.h"
#import "DDXMLElement.h"

@implementation NSString (NSXMLElementFacilitor)
+(NSString *) valueForElement:(NSXMLElement *) element withName:(NSString *) name {
    NSArray *subElement = [element elementsForName:name];
    if(subElement){
        return ((NSXMLElement*)[subElement firstObject]).stringValue;
    } else {
        return nil;
    }
}
@end
