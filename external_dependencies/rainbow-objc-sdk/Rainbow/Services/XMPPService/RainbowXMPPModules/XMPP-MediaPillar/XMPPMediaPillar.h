//
//  XMPPMediaPillar.h
//  Rainbow
//
//  Created by Joseph on 23/11/2017.
//  Copyright © 2017 ALE. All rights reserved.
//

#import "XMPPModule.h"
#import "Peer.h"
#import "NSXMLElement+XMPP.h"
#import "XMPPIQ.h"
#import "XMPPStream.h"
#import "XMPPJingle.h"
#import "XMPPJingleSDP.h"

#define XMLNSMediaPillar @"urn:xmpp:janus:1"

@interface XMPPMediaPillar : XMPPModule

-(void) registerMediaPillar:(NSString *)number displayName:(NSString *)name secret:(NSString *)secret toURLAddress:(NSString *)urlAddress jid:(NSString *)jid jidTel:(NSString *)jidTel;
-(void) unregisterMediaPillar:(NSString *)number toURLAddress:(NSString *)urlAddress;

@end
