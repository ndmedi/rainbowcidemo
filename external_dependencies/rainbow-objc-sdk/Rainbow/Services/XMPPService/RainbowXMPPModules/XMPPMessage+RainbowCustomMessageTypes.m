/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "XMPPMessage+RainbowCustomMessageTypes.h"
#import "NSXMLElement+XMPP.h"

@implementation XMPPMessage (RainbowCustomMessageTypes)

-(BOOL) isFileTranfer {
    if ([[self type] isEqualToString:@"file"])
        return YES;

    return NO;
}

-(BOOL) isWebRTCCall {
    if ([[self type] isEqualToString:@"webrtc"] || [[self type] isEqualToString:@"webrtc-start"] || [[self type] isEqualToString:@"webrtc-ringing"]) {
        return YES;
    }
    return NO;
}

-(BOOL) isWebRTCCallWithSupportedBody {
    if([self isWebRTCCall]){
        if([[[self body] componentsSeparatedByString:@"||"] count] >= 2){
            return YES;
        }
    }
    return NO;
}

-(BOOL) isManagementMessage {
    if ([[self type] isEqualToString:@"management"])
        return YES;
    
    return NO;
}

-(BOOL) isWebRTCCallWithCallLogInfo {
    if([self isWebRTCCall]){
        NSArray *callLog = [self elementsForName:@"call_log"];
        return ([callLog count] > 0);
    }
    return NO;
}

-(BOOL) isCallServiceMessage {
    if ([self elementForName:@"callservice" xmlns:@"urn:xmpp:pbxagent:callservice:1"])
        return YES;
    
    return NO;
}

-(BOOL) isCallServiceWithNomadicStatus {
    if ([self isCallServiceMessage]) {
        NSXMLElement *xElem = [self elementForName:@"callservice" xmlns:@"urn:xmpp:pbxagent:callservice:1"];
        if(xElem && [xElem elementForName:@"nomadicStatus"]) {
            return YES;
        }
    }
    return NO;
}

-(BOOL) isConferenceManagementMessage {
    if(self.children.count == 1 && [((XMPPMessage *)self.children.firstObject).name isEqualToString:@"conference-info"]){
        return YES;
    }
    return NO;
}

-(BOOL) isConferenceReminder {
    NSXMLElement *xElem = [self elementForName:@"x" xmlns:kXMPPJabberXAudioConferenceNS];
    if(xElem){
        NSString *type = [xElem attributeForName:@"type"].stringValue;
        if([type isEqualToString:@"reminder"]){
            return YES;
        }
    }
    return NO;
}

-(BOOL) isChatMessageWithChangedNotification {
    if ([self isChatMessage]){
        return ([self elementForName:@"changed"] != nil);
    }
    return NO;
}

-(BOOL) isChatMessageWithMarkAllAsReadNotification {
    if ([self isChatMessage]){
        return ([self elementForName:@"mark_as_read"] != nil);
    }
    return NO;
}

-(BOOL) isChatMessageWithRecordingState {
    if([self isChatMessage]){
        return ([self elementForName:@"recording"] != nil);
    }
    return NO;
}
@end
