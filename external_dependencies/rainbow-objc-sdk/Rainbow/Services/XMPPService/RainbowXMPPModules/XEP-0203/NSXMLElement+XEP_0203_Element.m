/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "NSXMLElement+XEP_0203_Element.h"
#import "NSXMLElement+XMPP.h"

@implementation NSXMLElement (XEP_0203_Element)
-(NSXMLElement *) delayElement {
    
    NSXMLElement *delay;
    
    delay = [self elementForName:@"delay" xmlns:@"urn:xmpp:delay"];
    
    return delay;
}
@end
