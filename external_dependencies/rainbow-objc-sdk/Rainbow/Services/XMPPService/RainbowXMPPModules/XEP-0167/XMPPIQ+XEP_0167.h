/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "XMPPIQ.h"

@interface XMPPIQ (XEP_0167)

+ (XMPPIQ *)iqWithHoldJingleFrom:(NSString *)fromJid to:(NSString *)toJid elementID:(NSString *)eid sessionID:(NSString*) sid;
+ (XMPPIQ *)iqWithUnholdJingleFrom:(NSString *)fromJid to:(NSString *)toJid elementID:(NSString *)eid sessionID:(NSString*) sid;
+ (XMPPIQ *)iqWithJingle:(NSXMLElement *)jingle from:(NSString *)fromJid to:(NSString *)toJid elementID:(NSString *)eid;

/*
 Send hold message
 -- Refer to https://xmpp.org/extensions/xep-0167.html#info-hold
 */
+(NSXMLElement *) holdJingleFromJid:(NSString*) fromJid toJid:(NSString *) toJid sessionID:(NSString*) sid;

/*
 Ends the hold state
 -- Refer to https://xmpp.org/extensions/xep-0167.html#info-hold
 */
+(NSXMLElement *) unholdJingleFromJid:(NSString*) fromJid toJid:(NSString *) toJid sessionID:(NSString*) sid;

-(BOOL) isHoldJingle;
-(BOOL) isUnholdJingle;
-(NSString *) sessionIDFromJingle;
@end
