/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "XMPPIQ+XEP_ClickToCallMobile.h"


@implementation XMPPIQ (XEP_ClickToCallMobile)

// Example of C2C IQ
// <iq xmlns="jabber:client" from="998e8955e3da477f98946bf7272e231b@jerome-all-in-one-dev-1.opentouch.cloud/web_win_1.16.0_lK3kx4WM" to="998e8955e3da477f98946bf7272e231b@jerome-all-in-one-dev-1.opentouch.cloud/mobile_ios_DEBUG_1.15.221_E764B8FD-B093-4C3E-A0F6-43D2EC85E8C9" type="set" id="911821a2-0caf-4063-ba90-a248d557cbd9:sendIQ"><call xmlns="urn:xmpp:call" phoneNumber="0123456789" directCall="false"/></iq>

-(BOOL) isClickToCallMobileIQ {
    return [self elementForName:@"call" xmlns:XMLNSClickToCallMobile] ? YES : NO;
}

-(NSString *) getC2CPhoneNumber {
    NSXMLElement *call = [self elementForName:@"call" xmlns:XMLNSClickToCallMobile];
    if (!call) {
        return nil;
    }
    return [call attributeStringValueForName:@"phoneNumber"];
}

-(BOOL) isC2CDirectCall {
    NSXMLElement *call = [self elementForName:@"call" xmlns:XMLNSClickToCallMobile];
    if (!call) {
        return NO;
    }
    return [call attributeBoolValueForName:@"directCall"];
}

@end
