/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "XMPPPresence.h"

#define kXMPPJabberConfigurationNS @"jabber:iq:configuration"
#define kXMPPvCardAvatarElement @"x"
#define kXMPPvCardAvatarNS @"vcard-temp:x:update"
#define kXMPPAvatarDelay @"urn:xmpp:delay"
#define kXMPPvCardAvatarPhotoElement @"avatar"
#define kXMPPvCardDataElement @"data"
#define kXMPPvCardDateElement @"stamp"
#define KXMPPvCardDelayElement @"delay"
#define kXMPPvCardActorElement @"actor"
#define kXMPPJabberConferenceNS @"jabber:iq:conference"
#define kXMPPJabberNotificationNS @"jabber:iq:notification"


@interface XMPPPresence (RainbowCustomType)
-(BOOL) isPresenceWithActor;
@end
