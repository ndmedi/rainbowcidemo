/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>
#import "XMPPStream.h"
#import "Server.h"
#import "DownloadManager.h"
#import "MyUser.h"

#define kForceCloseSocketErrorCode -987

@interface XMPPStreamWebSockets : XMPPStream
@property (nonatomic, strong) DownloadManager *certificateManager;
@property (nonatomic, strong) Server *server;
@property (nonatomic, strong) MyUser *myUser;

-(void) simulateDidReceiveMessage:(XMPPMessage *) message;
-(void) forceCloseSocket;
-(void) setResourceName:(NSString *)name;
@end
