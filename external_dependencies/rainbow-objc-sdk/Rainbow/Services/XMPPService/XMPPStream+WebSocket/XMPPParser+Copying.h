/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "XMPPParser.h"

@interface XMPPParser () {
    @protected
        #if __has_feature(objc_arc_weak)
        __weak id delegate;
        #else
        __unsafe_unretained id delegate;
        #endif
        dispatch_queue_t delegateQueue;
    @public
        xmlParserCtxt *parserCtxt;
}
@end

@interface XMPPParser (Copying) <NSCopying>

@end
