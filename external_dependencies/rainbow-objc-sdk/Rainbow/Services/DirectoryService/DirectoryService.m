/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "DirectoryService.h"
#import "NSString+URLEncode.h"
#import "ContactsManagerServiceToolKit.h"
#import "NSDictionary+JSONString.h"
#import "NSData+JSON.h"

@interface DirectoryService ()

@property (nonatomic, strong) ApiUrlManagerService* apiUrlManagerService;
@property (nonatomic, strong) DownloadManager* downloadManager;
@end

@implementation DirectoryService

-(instancetype) initWithDownloadManager:(DownloadManager *) downloadManager apiUrlManagerService:(ApiUrlManagerService *) apiUrlManagerService {
    self = [super init];
    if(self){
        _apiUrlManagerService = apiUrlManagerService;
        _downloadManager = downloadManager;
    }
    return self;
}

-(void) dealloc {
    _apiUrlManagerService = nil;
    _downloadManager = nil;
}

-(void) searchRequestWithPattern:(NSString *) pattern withCompletionBlock:(SearchCompletionHandler) completionBlock {
    NSString *trimmedPattern = [pattern stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

    NSURL* serviceUrl = [_apiUrlManagerService getURLForService:ApiServicesSearch];
    NSDictionary *headersParameters = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesSearch];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?displayName=%@",serviceUrl, [trimmedPattern urlEncode]]];
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headersParameters completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        NSDictionary *returnedResponse = nil;
        if(!error) {
            returnedResponse = [receivedData objectFromJSONData];
        }
        if(completionBlock) {
            completionBlock(trimmedPattern, returnedResponse[@"data"], error);
        }
    }];
}


-(void) searchRequestOnActiveDirectoryWithPattern:(NSString *) pattern withCompletionBlock:(SearchCompletionHandler) completionBlock {
    NSString *trimmedPattern = [pattern stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSURL* url = [_apiUrlManagerService getURLForService:ApiServicesSearchActiveDirectory,@"search"];
    NSDictionary *headersParameters = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesSearchActiveDirectory];
    NSDictionary *bodyContent = @{@"name": pattern};
    
    [_downloadManager postRequestWithURL:url body:[bodyContent jsonStringWithPrettyPrint:YES] requestHeadersParameter:headersParameters completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        NSDictionary *returnedResponse = nil;
        if(!error) {
            returnedResponse = [receivedData objectFromJSONData];
        } else {
            NSLog(@"Error during search in AD %@, response %@, received data %@", error, response, [receivedData objectFromJSONData]);
        }
        if(completionBlock) {
            id responseData = [returnedResponse objectForKey:@"data"];
            if([responseData isKindOfClass:[NSDictionary class]] && [responseData objectForKey:@"docs"]){
                // We are on a old version of the server !! that is not good
                // we force nil response
                completionBlock(trimmedPattern, nil, error);
            } else {
                
                completionBlock(trimmedPattern, returnedResponse[@"data"], error);
            }
        }
    }];
}

-(void) searchRequestOnPBXPhonebookWithPattern:(NSString *) pattern withCompletionBlock:(SearchCompletionHandler) completionBlock {
    NSString *trimmedPattern = [pattern stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSURL* serviceUrl = [_apiUrlManagerService getURLForService:ApiServicesSearchPBXPhonebook];
    NSDictionary *headersParameters = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesSearchPBXPhonebook];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?name=%@",serviceUrl, [trimmedPattern urlEncode]]];
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headersParameters completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        NSDictionary *returnedResponse = nil;
        if(!error) {
            returnedResponse = [receivedData objectFromJSONData];
        }
        if(completionBlock) {
            completionBlock(trimmedPattern, returnedResponse[@"phoneBooks"], error);
        }
    }];
}

-(void) searchExternalContactDetailsWithObjectID:(NSString *) objectID withCompletionBlock:(SearchCompletionHandler) completionBlock {
    
    NSURL* url = [_apiUrlManagerService getURLForService:ApiServicesSearchActiveDirectory,[NSString stringWithFormat:@"details/%@",objectID]];
    NSDictionary *headersParameters = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesSearchActiveDirectory];
    
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headersParameters completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            NSLog(@"Error: unable to get the user details by RainbowID %@ in REST.", objectID);
            if(completionBlock)
                completionBlock(@"",@[], error);
        }
        
        NSDictionary * returnedResponse = [receivedData objectFromJSONData];
        if(completionBlock) {
            if(returnedResponse){
                completionBlock(@"", @[returnedResponse], error);
            } else
                completionBlock(@"", @[], error);
            
        }
    }];
}

-(void) searchRainbowContactWithRainbowID:(NSString *)rainbowID withCompletionBlock:(SearchCompletionHandler)completionBlock {
    NSURL *baseUrl = [_apiUrlManagerService getURLForService:ApiServicesUsers, rainbowID];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/?format=full",baseUrl]];
    NSDictionary *headersParameters = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesUsers];
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headersParameters completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            NSLog(@"Error: unable to get the user details by RainbowID %@ in REST.", rainbowID);
            if(completionBlock)
                completionBlock(@"",@[], error);
        }
        
        NSDictionary * returnedResponse = [receivedData objectFromJSONData];
        if(completionBlock) {
            if([returnedResponse objectForKey:@"data"]){
                completionBlock(@"", @[returnedResponse[@"data"]], error);
            } else
                completionBlock(@"", @[], error);
            
        }
    }];
}

-(NSDictionary *) searchRainbowContactWithRainbowID:(NSString *) rainbowID {
    NSAssert(![[NSThread currentThread] isEqual:[NSThread mainThread]], @"This method must NOT be executed on main thread");
    NSURL* baseUrl = [_apiUrlManagerService getURLForService:ApiServicesUsers, rainbowID];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/?format=full",baseUrl]];
    NSDictionary *headersParameters = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesUsers];
    
    NSData *response = nil;
    NSURLResponse *urlResponse = nil;
    NSError *error = nil;
    [_downloadManager getSynchronousRequestWithURL:url requestHeadersParameter:headersParameters returningResponseData:&response returningNSURLResponse:&urlResponse returningError:&error];
    
    if (error) {
        NSLog(@"Error: unable to get the user %@ details in REST.", rainbowID);
        return nil;
    }
    
    NSDictionary *dict = [response objectFromJSONData];
    
    if (error) {
        NSLog(@"Error: unable to parse JSON for get the user %@ details in REST.", rainbowID);
        return nil;
    }
    
    return dict[@"data"];
}

-(void) searchRainbowContactsWithJids:(NSArray<NSString *> *) jids fromOffset:(NSUInteger) offset withLimit:(NSUInteger) limit withCompletionBlock:(SearchCompletionHandler) completionBlock {
    NSURL *baseUrl = [_apiUrlManagerService getURLForService:ApiServicesUsers,@"jids"];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/?format=medium",baseUrl]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesUsers];
    NSDictionary *bodyDict = @{@"jid_im": jids, @"limit":[NSNumber numberWithUnsignedInteger:limit]};
    [_downloadManager postRequestWithURL:url body:[bodyDict jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot get vcard by jids in REST : %@ receivedData %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse) {
            NSLog(@"Error: Cannot get vcard by jids in REST due to JSON parsing failure");
            return;
        }

        if(completionBlock) {
            completionBlock(@"", jsonResponse[@"data"], error);
        }
    }];
}

-(void) searchRainbowContactWithJid:(NSString *) jid withCompletionBlock:(SearchCompletionHandler) completionBlock {
    NSURL *baseUrl = [_apiUrlManagerService getURLForService:ApiServicesUsers,@"jids"];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/?format=medium",baseUrl,jid]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesUsers];

    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            NSLog(@"Error: Unable to get the user with jid %@ in REST.", jid);
            if(completionBlock)
                completionBlock(jid,@[], error);
            return;
        }
        
        NSDictionary * returnedResponse = [receivedData objectFromJSONData];
        if(completionBlock) {
            if([returnedResponse objectForKey:@"data"]){
                completionBlock(jid, @[returnedResponse[@"data"]], error);
            } else
                completionBlock(jid, @[], error);
        }
    }];
}

-(void) searchRainbowContactsWithLoginEmails:(NSArray<NSString *> *) loginEmails fromOffset:(NSUInteger) offset withLimit:(NSUInteger) limit withCompletionBlock:(SearchCompletionHandler) completionBlock {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesUsers,@"loginemails"];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesUsers];
    NSDictionary *bodyDict = @{@"loginEmail": loginEmails, @"limit": [NSNumber numberWithUnsignedInteger:limit]};
    [_downloadManager postRequestWithURL:url body:[bodyDict jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error) {
            NSLog(@"Error: Cannot get vcard by loginEmail in REST : %@ %@", error, [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding]);
            return;
        }
        
        NSDictionary *jsonResponse = [receivedData objectFromJSONData];
        if(!jsonResponse) {
            NSLog(@"Error: Cannot get vcard by loginEmail in REST due to JSON parsing failure");
            return;
        }
        
        if(completionBlock) {
            completionBlock(@"", jsonResponse[@"data"], error);
        }
    }];
}

-(NSArray *) searchRainbowContactsWithLoginEmails:(NSArray<NSString *> *) loginEmails fromOffset:(NSUInteger) offset withLimit:(NSUInteger) limit {
    NSAssert(![[NSThread currentThread] isEqual:[NSThread mainThread]], @"This method must NOT be executed on main thread");
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesUsers,@"loginemails"];
    NSDictionary *headersParameters = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesUsers];
    NSDictionary *bodyDict = @{@"loginEmail": loginEmails, @"limit":[NSNumber numberWithUnsignedInteger:limit]};
    
    NSData *response = nil;
    NSURLResponse *urlResponse = nil;
    NSError *error = nil;
    
    [_downloadManager postSynchronousRequestWithURL:url body:[bodyDict jsonStringWithPrettyPrint:NO] requestHeadersParameter:headersParameters returningResponseData:&response returningNSURLResponse:&urlResponse returningError:&error];
    
    if (error) {
        NSLog(@"Error: unable to get the user details in REST.");
        return nil;
    }
    
    NSDictionary *jsonResponse = [response objectFromJSONData];
    
    if (!jsonResponse) {
        NSLog(@"Error: unable to parse JSON for get the user details in REST.");
        return nil;
    }
    
    return jsonResponse[@"data"];
}
@end
