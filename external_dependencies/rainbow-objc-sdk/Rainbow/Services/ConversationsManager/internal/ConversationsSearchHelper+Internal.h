/*
 * Rainbow
 *
 * Copyright (c) 2018, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "XMPPService.h"
#import "ConversationsSearchHelper.h"
#import "ConversationsManagerService.h"
#import "RoomsService.h"

typedef NS_ENUM(NSInteger, ConversationSearchHelperQueryType) {
    ConversationSearchHelperQueryTypeNone = 0,
    ConversationSearchHelperQueryTypeGlobal,
    ConversationSearchHelperQueryTypeSingle,
    ConversationSearchHelperQueryTypeForOccurences
};

@interface ConversationsSearchHelper ()
-(instancetype)initWithXMPPService:(XMPPService *) xmppService;
@end
