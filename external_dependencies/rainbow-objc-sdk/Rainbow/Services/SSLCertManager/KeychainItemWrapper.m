/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "KeychainItemWrapper.h"
#import <Security/Security.h>


#define kRPIdentifierRef @"RPIdentityRef"


static KeychainItemWrapper *singleton = NULL;

@interface KeychainItemWrapper ()
@property (nonatomic, retain) NSString *overridedAppName;
@end

@implementation KeychainItemWrapper

-(id) init {
    self = [super init];
    if(self){
        _overridedAppName = nil;
    }
    return self;
}

+(KeychainItemWrapper *) defaultKeychain {
    if (singleton == NULL) {
        singleton = [[KeychainItemWrapper alloc] init];
    }
    return singleton;
}

-(void) addItemWithInfo:(NSMutableDictionary *)info {
    if (![[info objectForKey: (id)kSecReturnRef] boolValue]){
        [info setObject:(id)kCFBooleanTrue forKey:(id)kSecReturnPersistentRef];
    }
    
    CFDataRef itemPersistentRef = NULL;
    CFTypeRef item = NULL;
    OSStatus err = SecItemAdd((CFDictionaryRef)info, (CFTypeRef*)&itemPersistentRef);
    if(err == errSecDuplicateItem){
        NSLog(@"[KEYCHAIN] Keychain claims it's a dup, so look for existing item");
        [info removeObjectForKey:(id)kSecReturnPersistentRef];
        [info setObject:(id)kCFBooleanTrue forKey:(id)kSecReturnRef];
        if(SecItemCopyMatching((CFDictionaryRef)info, (CFTypeRef *)&item)){
            if(!item){
                NSLog(@"[KEYCHAIN] Couldn't find supposedly-duplicate item, info=%@",info);
            }
        }
    } else if(err) {
        NSLog(@"[KEYCHAIN] ERR = %d",(int)err);
    } else {
        NSLog(@"[KEYCHAIN] SecItem added item; ref=%@",itemPersistentRef);
    }
    if(itemPersistentRef != NULL){
        CFRelease(itemPersistentRef);
    }
    if(item != NULL){
        CFRelease(item);
    }
}

- (void)dealloc {
    [singleton release];
    singleton = nil;
    [_overridedAppName release];
    _overridedAppName = nil;
    [super dealloc];
}

- (BOOL) removeAllCertificates {
    NSLog(@"[KEYCHAIN] Removing certificates");
    NSMutableDictionary *query = [self getBaseQuery];
    [query retain];
    OSStatus err = SecItemDelete((CFDictionaryRef)query);
    [query release];
    if(err){
        return NO;
    } else {
        return YES;
    }
    
}

-(NSMutableDictionary *)getBaseQuery {
    NSMutableDictionary *baseQuery = [[NSMutableDictionary alloc] init];
    [baseQuery setObject:(id)kSecClassCertificate forKey:(id)kSecClass];
    //    [baseQuery setObject:@"YOUR_APP_ID_HERE.com.yourcompany.GenericKeychainSuite" forKey:(id)kSecAttrAccessGroup];
    return [baseQuery autorelease];
}

#pragma mark -
#pragma mark UserName and Password managment

- (NSString *)appName {
    if(_overridedAppName)
        return _overridedAppName;
    
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    
    // Attempt to find a name for this application
    NSString *appName = [bundle objectForInfoDictionaryKey:@"CFBundleDisplayName"];
    if (!appName) {
        appName = [bundle objectForInfoDictionaryKey:@"CFBundleName"];
    }
    return appName;
}

- (void) overrideAppNameWithName:(NSString *) newAppName {
    [newAppName retain];
    [_overridedAppName release];
    _overridedAppName = newAppName;
}

- (BOOL)setString:(NSString *)string forKey:(NSString *)key forService:(NSString *)service withType:(CFTypeRef) type {
    if (key == nil) {
        return NO;
    }
    
    key = [NSString stringWithFormat:@"%@ - %@ - %@", [self appName], key, service];
    
    // First check if it already exists, by creating a search dictionary and requesting that
    // nothing be returned, and performing the search anyway.
    
    OSStatus res;
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
#if (TARGET_OS_IPHONE || TARGET_OS_EMBEDDED)
    
    NSMutableDictionary *query = [NSMutableDictionary dictionary];
    
    [query setObject:(id)kSecClassGenericPassword forKey:(id)kSecClass];
    
    // Add the keys to the search dict
    [query setObject:service forKey:(id)kSecAttrService];
    [query setObject:key forKey:(id)type];
    if(!string){
        NSLog(@"We try to remove the object at key %@", key);
        OSStatus status = SecItemDelete((CFDictionaryRef)query);
        if (status != errSecSuccess) {
            NSLog(@"[KEYCHAIN] SecItemDelete failed: %d for key %@", (int)status, key);
        } else {
            NSLog(@"[KEYCHAIN] Entry (%@) deleted", key);
        }
        return YES;
    }
    res = SecItemCopyMatching((CFDictionaryRef)query, NULL);
    
    if (res == errSecItemNotFound) {
        if (string != nil) {
            NSMutableDictionary *addDict = query;
            [addDict setObject:data forKey:(id)kSecValueData];
            [addDict setObject:(id)kSecAttrAccessibleAlwaysThisDeviceOnly forKey:(id)kSecAttrAccessible];
            
            res = SecItemAdd((CFDictionaryRef)addDict, NULL);
            switch (res) {
                case errSecDuplicateItem:{
                    NSLog(@"[KEYCHAIN] Duplicate item found, update the existing one");
                    NSDictionary *attributeDict = [NSDictionary dictionaryWithObjectsAndKeys:(id)kSecAttrAccessibleAlwaysThisDeviceOnly, (id)kSecAttrAccessible, (CFDataRef)data, (id)kSecValueData, nil];
                    res = SecItemUpdate((CFDictionaryRef)query, (CFDictionaryRef)attributeDict);
                    if(res != errSecSuccess){
                        NSLog(@"[KEYCHAIN] SecItemUpdated returned %d! when setting %@", (int)res, key);
                        return NO;
                    }
                    break;
                }
                case errSecSuccess:
                    return YES;
                    break;
                default:{
                    NSLog(@"[KEYCHAIN] Received %d from SecItemAdd! when setting %@", (int)res, key);
                    return NO;
                    break;
                }
            }
            if(res != errSecSuccess){
                
            }
        }
    } else if (res == errSecSuccess) {
        // Modify an existing one
        // Actually pull it now of the keychain at this point.
        NSDictionary *attributeDict = [NSDictionary dictionaryWithObjectsAndKeys:(id)kSecAttrAccessibleAlwaysThisDeviceOnly, (id)kSecAttrAccessible, (CFDataRef)data, (id)kSecValueData, nil];
        
        res = SecItemUpdate((CFDictionaryRef)query, (CFDictionaryRef)attributeDict);
        if(res != errSecSuccess){
            NSLog(@"[KEYCHAIN] SecItemUpdated returned %d! when setting %@", (int)res, key);
            return NO;
        }
        
    } else {
        NSLog(@"[KEYCHAIN] Received %d from SecItemCopyMatching!", (int)res);
        return NO;
    }
    
#else
    
    CFMutableDictionaryRef query = CFDictionaryCreateMutable(kCFAllocatorDefault, 3, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
    CFDictionaryAddValue(query, kSecReturnAttributes, kCFBooleanTrue);
    CFDictionaryAddValue(query, kSecReturnRef, kCFBooleanTrue);
    CFDictionaryAddValue(query, kSecMatchLimit, kSecMatchLimitAll);
    CFDictionaryAddValue(query, kSecClass, kSecClassGenericPassword);
    CFDictionaryAddValue(query, kSecAttrService, service);
    CFDictionaryAddValue(query, type, key);
    
    // get search results
    CFArrayRef result = nil;
    res = SecItemCopyMatching(query, (CFTypeRef*)&result);
    if (res == errSecItemNotFound) {
        if (string != nil) {
            CFMutableDictionaryRef newData = CFDictionaryCreateMutable(kCFAllocatorDefault, 3, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
            CFDictionaryAddValue(newData, kSecValueData, data);
            
            res = SecItemAdd((CFDictionaryRef)query, NULL);
            CFRelease(newData);
            if(res != errSecSuccess){
                NSLog(@"[KEYCHAIN] Recieved %d from SecItemAdd!", res);
                return NO;
            }
        }
    } else if (res == errSecSuccess) {
        // Modify an existing one
        // Actually pull it now of the keychain at this point.
        CFMutableDictionaryRef newData = CFDictionaryCreateMutable(kCFAllocatorDefault, 3, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
        CFDictionaryAddValue(newData, kSecValueData, data);
        
        res = SecItemUpdate(query, newData);
        CFRelease(newData);
        if(res != errSecSuccess){
            NSLog(@"[KEYCHAIN] SecItemUpdated returned %d!", res);
            return NO;
        }
        
    } else {
        NSLog(@"[KEYCHAIN] Received %d from SecItemCopyMatching!", res);
        return NO;
    }
    CFRelease(query);
#endif
    
    return YES;
    
}
- (BOOL)setString:(NSString *)string forKey:(NSString *)key forService:(NSString *)service {
    return [self setString:string forKey:key forService:service withType:kSecAttrAccount];
}

- (NSString *)getStringForKey:(NSString *)key forService:(NSString *)service withType:(CFTypeRef) type {
    key = [NSString stringWithFormat:@"%@ - %@ - %@", [self appName], key, service];
    NSLog(@"[KEYCHAIN] reading data for key %@", key);
    
    NSMutableDictionary *existsQueryDictionary = [NSMutableDictionary dictionary];
    
    [existsQueryDictionary setObject:(id)kSecClassGenericPassword forKey:(id)kSecClass];
    
    // Add the keys to the search dict
    [existsQueryDictionary setObject:service forKey:(id)kSecAttrService];
    [existsQueryDictionary setObject:key forKey:(id)type];
    
    // We want the data back!
    NSData *data = nil;
    
    [existsQueryDictionary setObject:(id)kCFBooleanTrue forKey:(id)kSecReturnData];
    
    OSStatus res = SecItemCopyMatching((CFDictionaryRef)existsQueryDictionary, (CFTypeRef *)&data);
    [data autorelease];
    if (res == errSecSuccess) {
        NSString *string = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
        NSLog(@"[KEYCHAIN] Data read successfully");
        return string;
    } else {
        if(res == errSecItemNotFound){
            NSLog(@"[KEYCHAIN] SecItemCopyMatching returned %d! when reading %@", (int)res, key);
            return nil;
        }
        NSLog(@"[KEYCHAIN] Read failed with error %d! when reading key %@", (int)res, key);
    }
    
    return nil;
}
- (NSString *)getStringForKey:(NSString *)key forService:(NSString *)service {
    return [self getStringForKey:key forService:service withType:kSecAttrAccount];
}

-(void) removeAllCreds
{
    NSArray *list = [NSArray arrayWithObjects:kServiceApp, kServiceXmpp, kServiceToken, kServiceIceServer, nil];
    for (NSString *account in list){
        NSLog(@"[KEYCHAIN] removing creds for %@", account);
        [self removeCredsForServiceType:account];
    }
}

- (void) removeCredsForServiceType:(NSString *) serviceType {
    NSMutableDictionary *query = [[NSMutableDictionary alloc] init];
    
    [query setObject:(id)kSecClassGenericPassword forKey:(id)kSecClass];
    [query setObject:serviceType forKey:(id)kSecAttrService];
    
    OSStatus status = SecItemDelete((CFDictionaryRef)query);
    if (status != errSecSuccess) {
        NSLog(@"[KEYCHAIN] SecItemDelete failed: %d", (int)status);
    } else {
        NSLog(@"[KEYCHAIN] Entry (%@) deleted", kSecAttrService);
    }
    [query release];
}

@end
