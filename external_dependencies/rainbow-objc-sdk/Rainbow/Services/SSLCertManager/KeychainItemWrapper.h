/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>

#define kUsername @"username"
#define kPassword @"password"
#define kToken @"token"
#define kJidTel @"jidTel"
#define kUserId @"userId"
#define kCompanyID @"companyId"
#define kCompanyName @"companyName"
#define kIsInitialized @"isInitialized"
#define kGuestMode @"guestMode"
#define kIsReadyToCreateConference @"isReadyToCreateConference"
#define kUserSettingsVersion @"userSettingsVersion"
#define kIceServersCredentials @"iceServersCredentials"
#define kResourceUniqueID @"resourceUniqueID"

#define kServiceApp @"app"
#define kServiceToken @"token"
#define kServiceXmpp @"xmpp"
#define kServiceIceServer @"iceServer"
#define kServiceDeviceResourceUniqueID @"deviceResourceUniqueID"

@interface KeychainItemWrapper : NSObject {

}

+(KeychainItemWrapper *) defaultKeychain;

- (void) removeAllCreds;
- (void) removeCredsForServiceType:(NSString *) serviceType;
- (BOOL)setString:(NSString *)string forKey:(NSString *)key forService:(NSString *)service;
- (BOOL)setString:(NSString *)string forKey:(NSString *)key forService:(NSString *)service withType:(CFTypeRef) type;
- (NSString *)getStringForKey:(NSString *)key forService:(NSString *)service;
- (NSString *)getStringForKey:(NSString *)key forService:(NSString *)service withType:(CFTypeRef) type;
/**
 * Method to override the default application name that is retreive automatically from CFBundleDisplayName
 */
- (void) overrideAppNameWithName:(NSString *) newAppName;

@end
