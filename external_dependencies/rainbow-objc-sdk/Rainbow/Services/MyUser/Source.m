/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "Source.h"
#import "Source+Internal.h"
#import "Tools.h"

#define kSourceEncodingDeviceSourceID @"deviceSourceID"
#define kSourceEncodingServerSourceID @"serverSourceID"
#define kSourceEncodingOsInformation @"osInformation"
#define kSourceEncodingContactsMapping @"contactsMapping"
#define kSourceEncodingSourceVersion @"sourceVersion"

@interface Source () <NSCoding>
@property (nonatomic, strong) NSObject *contactsMappingMutex;
@end

@implementation Source

-(instancetype) init {
    self = [super init];
    if(self){
        _deviceSourceId = [[[Tools generateUniqueID] stringByReplacingOccurrencesOfString:@"-" withString:@""] substringToIndex:24];
        _osInformation = [NSString stringWithFormat:@"%@ %@",[Tools currentOS], [Tools currentOSVersion]];
        _contactsMappingMutex = [NSObject new];
        _contactsMapping = [NSMutableDictionary dictionary];
        _sourceVersion = kSourceCurrentVersion;
    }
    return self;
}

-(void) dealloc {
    _deviceSourceId = nil;
    _osInformation = nil;
    _contactsMappingMutex = nil;
    [_contactsMapping removeAllObjects];
}

- (void)encodeWithCoder:(NSCoder *) encoder {
    [encoder encodeObject:_deviceSourceId forKey:kSourceEncodingDeviceSourceID];
    [encoder encodeObject:_serverSourceId forKey:kSourceEncodingServerSourceID];
    [encoder encodeObject:_osInformation forKey:kSourceEncodingOsInformation];
    [encoder encodeObject:_contactsMapping forKey:kSourceEncodingContactsMapping];
    [encoder encodeObject:_sourceVersion forKey:kSourceEncodingSourceVersion];
}

- (instancetype)initWithCoder:(NSCoder *) decoder {
    self = [super init];
    if(self){
        _serverSourceId = [decoder decodeObjectForKey:kSourceEncodingServerSourceID];
        _deviceSourceId = [decoder decodeObjectForKey:kSourceEncodingDeviceSourceID];
        _osInformation = [decoder decodeObjectForKey:kSourceEncodingOsInformation];
        _contactsMapping = [decoder decodeObjectForKey:kSourceEncodingContactsMapping];
        _sourceVersion = [decoder decodeObjectForKey:kSourceEncodingSourceVersion];
        _contactsMappingMutex = [NSObject new];
    }
    return self;
}

-(NSString *) description {
    return [NSString stringWithFormat:@"Source serverID:%@ deviceID:%@ os:%@", _serverSourceId, _deviceSourceId, _osInformation];
}

-(BOOL) needToUpdateSource {
    // Need to update if there is no source, or if there is a difference in os informations
    if(!_serverSourceId || !([[NSString stringWithFormat:@"%@ %@",[Tools currentOS], [Tools currentOSVersion]] isEqualToString:_osInformation]))
        return YES;
    
    return NO;
}

-(void) insertContactServerId:(NSString*) contactServerId contactInfo:(NSString *) contactInfo forAddressBookRecordId:(NSString *) addressBookRecordId {
    @synchronized(_contactsMappingMutex) {
        if(![_contactsMapping valueForKey:addressBookRecordId]){
            NSMutableDictionary *contactDictionary = [NSMutableDictionary dictionary];
            [contactDictionary setObject:contactInfo forKey:contactServerId];
            [_contactsMapping setObject:contactDictionary forKey:addressBookRecordId];
        } else
            NSLog(@"Contact server ID %@ already exist", addressBookRecordId);
    }
}

-(void) removeContactAddressBookRecordId:(NSString*) addressBookRecordId {
    @synchronized(_contactsMappingMutex) {
        if([_contactsMapping valueForKey:addressBookRecordId])
            [_contactsMapping removeObjectForKey:addressBookRecordId];
        else
            NSLog(@"Contact server ID %@ don't exist", addressBookRecordId);
    }
}

-(NSDictionary <NSString *, NSString *> *) getContactServerInformationForAddressBookRecordId:(NSString *) addressBookId {
    NSDictionary *contactInfos = nil;
    @synchronized(_contactsMappingMutex) {
        contactInfos = [_contactsMapping objectForKey:addressBookId];
    }
    return contactInfos;
}
@end