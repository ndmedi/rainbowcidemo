/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */
#import "Source.h"
#define kSourceCurrentVersion @"1.0"

@interface Source ()
@property (nonatomic, strong) NSString *sourceVersion;
@property (nonatomic, strong, readwrite) NSString *deviceSourceId;
@property (nonatomic, strong, readwrite) NSString *serverSourceId;
@property (nonatomic, strong, readwrite) NSString *osInformation;
@property (nonatomic, strong, readwrite) NSString *appID;
@property (nonatomic, strong, readwrite) NSString *secretKey;
// Key = addressBookRecordID, Value = NSDictionary Key = contactServerID, Value= Contact json representation
@property (nonatomic, strong, readwrite) NSMutableDictionary<NSString *, NSMutableDictionary <NSString *, NSString*> *> *contactsMapping;

-(void) insertContactServerId:(NSString*) contactServerId contactInfo:(NSString *) contactInfo forAddressBookRecordId:(NSString *) addressBookRecordId;
-(void) removeContactAddressBookRecordId:(NSString*) contactServerId;
-(NSDictionary <NSString *, NSString *> *) getContactServerInformationForAddressBookRecordId:(NSString *) addressBookId;
@end
