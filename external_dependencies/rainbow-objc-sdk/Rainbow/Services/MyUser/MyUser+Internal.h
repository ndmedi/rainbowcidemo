/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "MyUser.h"
#import "UserSettings.h"
#import "Source.h"
#import "XMPPService.h"

#define kServerSourceIdKey @"serverSourceId"

@class ContactsManagerService;
@class DownloadManager;
@class ApiUrlManagerService;
@class Presence;


typedef NS_ENUM(NSInteger, PresenceSettings) {
    PresenceSettingsUnknown = 0,
    PresenceSettingsOnline,
    PresenceSettingsAway,
    PresenceSettingsInvisible,
    PresenceSettingsDND
};

typedef void (^FetchPresenceSettingsCompletionHandler)(PresenceSettings newPresenceSettings);


@interface MyUser ()
@property (nonatomic, strong, readwrite) NSString *username;
@property (nonatomic, strong, readwrite) NSString *password;
@property (nonatomic, strong, readwrite) UserSettings *settings;
@property (nonatomic, strong, readwrite) Server *server;
@property (nonatomic, strong, readwrite) NSString *userID;
@property (nonatomic, strong, readwrite) NSString *token;
@property (nonatomic, strong, readwrite) NSDate *tokenExpireDate;
@property (nonatomic, readonly) BOOL isTokenValid;
@property (nonatomic, readonly) BOOL haveMinimalInformationsToStart;
@property (nonatomic, readonly) BOOL haveMinimalInformationsToStartWithOutToken;
@property (nonatomic, strong, readwrite) Source *source;
@property (nonatomic, strong) NSString *jid_im;
@property (nonatomic, strong) NSString *jid_password;
@property (nonatomic, strong) NSString *jid_tel;
@property (nonatomic, strong) NSString *companyID;
@property (nonatomic, strong) NSString *companyName;
@property (nonatomic, strong) NSMutableDictionary *contactSearchMatching;
@property (nonatomic, strong) NSObject *contactSearchMatchingMutex;
@property (nonatomic, readwrite) BOOL isInitialized;
@property (nonatomic, readwrite) BOOL isGuest;
@property (nonatomic, strong, readwrite) NomadicStatus *nomadicStatus;
@property (nonatomic, strong, readwrite) CallForwardStatus *callForwardStatus;
@property (nonatomic, strong, readwrite) MediaPillarStatus *mediaPillarStatus;
@property (nonatomic, readwrite) BOOL isAvailableThroughMediaPillar;
@property (nonatomic, strong, readwrite) NSString *voiceMailNumber;
@property (nonatomic, readwrite) NSInteger voiceMailCount;
@property (nonatomic, readwrite) BOOL isInDefaultCompany;
@property (nonatomic, readwrite, strong) NSMutableArray *roles;
@property (nonatomic, readwrite, strong) NSMutableArray *profilesName;
@property (nonatomic, readwrite, strong) NSString *pgiConfUserID;
@property (nonatomic, readwrite) BOOL isReadyToCreateConference;
@property (nonatomic, readwrite) BOOL isADSearchAvailable;
@property (nonatomic, strong, readwrite) NSString *appToken;
@property (nonatomic, strong, readwrite) NSString *appID;
@property (nonatomic, strong, readwrite) NSString *secretKey;
/**
 *  The presence settings retreived from server
 */
@property (nonatomic) PresenceSettings presenceSettings;

#pragma mark - Services
@property (nonatomic, weak) ContactsManagerService *contactsManagerService;
@property (nonatomic, weak) DownloadManager *downloadManager;
@property (nonatomic, weak) ApiUrlManagerService *apiURLManager;
@property (nonatomic, weak) XMPPService *xmppService;

-(void) setServerSourceID:(NSString *) serverSourceID;
-(void) writeSourceOnDisk;
// ContactServerInfo will contain two key contactServerId, and contactJid
-(void) insertContactServerInfo:(NSDictionary <NSString *, NSString *> *) contactServerInfo andAddressBookRecordId:(NSString *) addressBookRecordId;
-(void) removeContactWithAddressBookRecordId:(NSString*) addressBookRecordId;
-(NSDictionary *) getContactServerInfoForAddressBookRecordId:(NSString *) addressBookRecordId;
-(void) writeContactSearchMatchingToDisk;
-(void) reset;

#pragma mark - presence settings api
-(void) fetchPresenceSettingsWithCompletionHandler:(FetchPresenceSettingsCompletionHandler) completionHandler;
-(void) updatePresenceSettings:(PresenceSettings) newPresenceSettings;
+(PresenceSettings) presenceSettingsFromPresence:(Presence *) presence;
+(Presence *) presenceForPresenceSettings:(PresenceSettings) presenceSettings;
-(void) didRemoveCall:(NSNotification *)notification;
-(void) registerDefaultsSettings;
-(void) fetchProfilesFeatures;
-(void) fetchProfiles;
@end
