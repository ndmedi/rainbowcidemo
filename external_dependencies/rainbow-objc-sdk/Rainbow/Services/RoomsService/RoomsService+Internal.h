/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "RoomsService.h"
#import "DownloadManager.h"
#import "ApiUrlManagerService.h"
#import <Rainbow/ContactsManagerService.h>
#import "XMPPService.h"
#import "ConferencesManagerService.h"
#import "Room.h"
#import "RoomInvitationScenario.h"
#define kRoomsServiceDidJoinRoom @"didJoinRoom"
typedef void (^RoomsManagerCreateRoomWithRainbowID)(Room * room);
typedef void (^SendInvitationForRoomCompletionHandler)(NSDictionary *jsonResponse, NSError *error);

FOUNDATION_EXPORT NSString *const kRoomMediaTypePSTNAudio;
FOUNDATION_EXPORT NSString *const kRoomMediaTypeWebRTC;

@interface RoomsService ()

@property (nonatomic, strong) DownloadManager *downloadManager;
@property (nonatomic, strong) ApiUrlManagerService *apiUrlManagerService;
@property (nonatomic, strong) ContactsManagerService *contactsManagerService;
@property (nonatomic, strong) XMPPService *xmppService;
@property (nonatomic, strong) NSObject *roomsMutex;
@property (nonatomic, readwrite, strong) NSMutableArray<Room *> *rooms;
@property (nonatomic, strong) NSMutableArray <Room*> *roomsLoadedFromCache;
@property (nonatomic, strong) NSObject *cacheMutex;

/**
 * Boolean indicating wether rooms have been fully loaded from server
 */
@property (nonatomic) BOOL roomFullyLoaded;
@property (nonatomic) BOOL cacheLoadOngoing;

-(instancetype) initWithDownloadManager:(DownloadManager *) downloadManager apiUrlManagerService:(ApiUrlManagerService *) apiUrlManagerService contactsManagerService:(ContactsManagerService *) contactsManagerService xmppService:(XMPPService*) xmppService;

@property (nonatomic, weak) ConferencesManagerService *conferenceManagerService;

-(void) getRoomAsynchronouslyWithRainbowID:(NSString *) rainbowID withCompletionHandler:(RoomsManagerCreateRoomWithRainbowID) completionHandler;

-(Room *) getRoomByRainbowID:(NSString *) rainbowID;
-(Room *) getRoomByRTCJid:(NSString *) rtcJid;
-(Room *) createOrUpdateRoomFromJSON:(NSDictionary *) dict;

-(Room *) createOrUpdateRoomFromJID:(NSString *) jid withRainbowID:(NSString *) rainbowID;
-(void) removeRoom:(Room *) room;

-(void) addParticipant:(Participant *) participant inRoom:(Room *) room;
-(void) updateParticipant:(NSString *) participantJid withStatus:(ParticipantStatus) status inRoom:(Room *) room;
-(void) internalUpdateParticipant:(NSString *) participantJid withPrivilege:(ParticipantPrivilege)privilege inRoom:(Room *)room;

/* Used by ConferenceManagerService when the conference tied to the room need to be updated in the cache */
-(void) updateRoomInCache:(Room *)room;

-(void) sendInvitationForRoom:(Room * _Nonnull)room toCancel:(BOOL) cancelInvitation scenarioType:(InvitationScenario) scenario withConfId:(NSString *_Nullable) confId toUserIdList:(NSArray *_Nullable) users toGuestEmailList:(NSArray *_Nullable) emails withCompletionHandler:(SendInvitationForRoomCompletionHandler _Nullable ) completionHandler;

-(void) updateGuestUsers:(NSMutableArray <NSString *> * _Nonnull) guests forRoom:(Room * _Nonnull) room;

-(Room * _Nullable) getOrCreateRainbowRoomSynchronouslyWithJid:(NSString * _Nonnull) room_jid;

-(Room * _Nullable) createRoom:(NSString * _Nonnull) name withTopic:(NSString * _Nullable) topic mediaType:(NSString * _Nullable)mediaType error:(NSError ** _Nonnull) error;
@end
