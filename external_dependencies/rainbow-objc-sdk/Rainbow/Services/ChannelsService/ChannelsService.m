/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "ChannelsService.h"
#import "ChannelsService+Internal.h"
#import "Channel+Internal.h"
#import "ChannelItem+Internal.h"
#import "ChannelUser+Internal.h"
#import "ChannelItemImage+Internal.h"
#import "File+Internal.h"
#import "DownloadManager.h"
#import "LoginManager.h"
#import "LoginManager+Internal.h"
#import "NSDictionary+JSONString.h"
#import "NSDictionary+ChangedKeys.h"
#import "NSData+JSON.h"
#import "Tools.h"
#import "XMPPServiceProtocol.h"
#import <Rainbow/RainbowUserDefaults.h>
#import "PINCache.h"
#import "PINCache+OperationQueue.h"
#import "ServicesManager.h"
#import "FileSharingService+Internal.h"
#import "NSDate+JSONString.h"
#import "NSData+JSON.h"
#import "NSString+MD5.h"
#import "NSObject+NotNull.h"

NSString *const kChannelKey = @"channel";
NSString *const kChannelItemKey = @"channelItem";
NSString *const kChangedKeys = @"changedKeys";
NSString *const kChannelsServiceDidEndLoadingChannelsFromCache = @"channelsServiceDidEndLoadingChannelsFromCache";
NSString *const kChannelsServiceDidEndLoadingChannelsItemsFromCache = @"channelsServiceDidEndLoadingChannelsItemsFromCache";
NSString *const kChannelsServiceDidEndFetchingChannelsFromServer = @"channelsServiceDidEndFetchingChannelsFromServer";
NSString *const kChannelsServiceDidEndFetchingChannelsItemsFromServer = @"channelsServiceDidEndFetchingChannelsItemsFromServer";
NSString *const kChannelsServiceDidReceiveItem = @"channelsServiceDidReceiveItem";
NSString *const kChannelsServiceDidUpdateItem = @"channelsServiceDidUpdateItem";
NSString *const kChannelsServiceDidRetractItem = @"channelsServiceDidRetractItem";
NSString *const kChannelsServiceDidDeleteChannel = @"channelsServiceDidDeleteChannel";
NSString *const kChannelsServiceDidAddChannel = @"channelsManagerDidAddChannel";
NSString *const kChannelsServiceDidRemoveChannel = @"channelsManagerDidRemoveChannel";
NSString *const kChannelsServiceDidUpdateChannel = @"channelsManagerDidUpdateChannel";
NSString *const kChannelsFullyLoaded = @"channelsFullyLoaded";
NSString *const kChannelsServiceDidUpdateInvitationsCount = @"channelsServiceDidUpdateInvitationsCount";
NSString *const kChannelsServiceDidUpdateNewItemsCount = @"channelsServiceDidUpdateNewItemsCount";

typedef void (^ChannelsServiceLoadAvatarInternalCompletionHandler)(NSData *receivedData, NSError *error, NSURLResponse *response);

@interface ChannelDescription ()
@property (nonatomic, strong, readwrite) NSString *name;
@property (nonatomic, strong, readwrite) NSString *topic;
@property (nonatomic, strong, readwrite) NSString *id;
@property (nonatomic, readwrite) ChannelUserType userType;
@property (nonatomic, strong, readwrite) NSString *creatorId;
-(instancetype) initWithName:(NSString *)name topic:(NSString *)topic id:(NSString *)id userType:(ChannelUserType)userType;
-(instancetype) initWithName:(NSString *)name topic:(NSString *)topic id:(NSString *)id userType:(ChannelUserType)userType creatorId:(NSString *)creatorId;
@end

@implementation ChannelDescription
-(instancetype) initWithName:(NSString *)name topic:(NSString *)topic id:(NSString *)id userType:(ChannelUserType)userType{
    return [self initWithName:name topic:topic id:id userType:userType creatorId:nil];
}

-(instancetype) initWithName:(NSString *)name topic:(NSString *)topic id:(NSString *)id userType:(ChannelUserType)userType  creatorId:(NSString *)creatorId{
    self = [super init];
    if(self){
        _name = name;
        _topic = topic;
        _id = id;
        _userType = userType;
        _creatorId = creatorId;
    }
    return self;
}
@end

@interface ChannelsService () <XMPPServiceChannelsDelegate>
    @property (nonatomic, strong) XMPPService *xmppService;
    @property (nonatomic, strong) DownloadManager *downloadManager;
    @property (nonatomic, strong) ApiUrlManagerService *apiUrlManagerService;

    @property (nonatomic, strong) NSMutableArray *channels;
    @property (nonatomic, strong) PINCache *channelsCache;

    @property (nonatomic, strong) NSMutableArray *channelsItems;
    @property (nonatomic, strong) PINCache *channelsItemsCache;

    @property (nonatomic, strong) PINCache *channelsAvatarsCache;

    @property (nonatomic, strong) PINCache *channelsFileDescriptorCache;
@end

@implementation ChannelsService

-(instancetype) initWithXMPPService:(XMPPService *) xmppService downloadManager:(DownloadManager *) downloadManager apiUrlManagerService:(ApiUrlManagerService *) apiUrlManagerService  {
    self = [super init];
    if(self){
        _xmppService = xmppService;
        _xmppService.channelsDelegate = self;
        _downloadManager = downloadManager;
        _apiUrlManagerService = apiUrlManagerService;
        _channels = [NSMutableArray new];
        _channelsItems = [NSMutableArray new];
        _channelsFullyLoaded = [[RainbowUserDefaults sharedInstance] boolForKey:kChannelsFullyLoaded];
        _savedNewItems = [NSMutableArray new];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerInternalDidLoginSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kLoginManagerDidReconnect object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:kLoginManagerDidLogoutSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeUser:) name:kLoginManagerDidChangeUser object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateContact:) name:kContactsManagerServiceDidUpdateContact object:nil];
        
        _channelsCache = [[PINCache alloc] initWithName:@"channelsCache"];
        _channelsCache.memoryCache.removeAllObjectsOnEnteringBackground = YES;
        
        _channelsItemsCache = [[PINCache alloc] initWithName:@"channelsItemsCache"];
        _channelsItemsCache.memoryCache.removeAllObjectsOnEnteringBackground = YES;
        
        _channelsAvatarsCache = [[PINCache alloc] initWithName:@"channelsAvatarsCache"];
        _channelsAvatarsCache.memoryCache.removeAllObjectsOnEnteringBackground = YES;
        
        _channelsFileDescriptorCache = [[PINCache alloc] initWithName:@"channelsItemImagesCache"];
        _channelsFileDescriptorCache.memoryCache.removeAllObjectsOnEnteringBackground = YES;
        
        [self loadChannelsFromCache];
    }
    return self;
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerInternalDidLoginSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidReconnect object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidLogoutSucceeded object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLoginManagerDidChangeUser object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kContactsManagerServiceDidUpdateContact object:nil];
    
    _channels = nil;
    _xmppService.channelsDelegate = nil;
    _xmppService = nil;
    _downloadManager = nil;
    _apiUrlManagerService = nil;
    _channelsCache = nil;
    _channelsItemsCache = nil;
    _channelsAvatarsCache = nil;
    _channelsFileDescriptorCache = nil;
    
    [_channels removeAllObjects];
    [_channelsItems removeAllObjects];
    [_savedNewItems removeAllObjects];
}

-(void) didLogin:(NSNotification *) notification {
    [self fetchMyChannels];
}

-(void) didReconnect:(NSNotification *) notification {
    if (!_channelsFullyLoaded) {
        NSLog(@"Channels not fully loaded => reload");
        [self fetchMyChannels];
    }
}

-(void) didLogout:(NSNotification *) notification {
    @synchronized(self.channels) {
        [_channels removeAllObjects];
    }
    
    @synchronized(self.channelsItems) {
        [_channelsItems removeAllObjects];
    }
    
    [_channelsCache.operationQueue cancelAllOperations];
    [_channelsItemsCache.operationQueue cancelAllOperations];
    [_channelsAvatarsCache.operationQueue cancelAllOperations];
    [_channelsFileDescriptorCache.operationQueue cancelAllOperations];
}

-(void) didChangeUser:(NSNotification *) notification {
    [_channelsCache removeAllObjects];
    [_channelsItemsCache removeAllObjects];
    [_channelsAvatarsCache removeAllObjects];
    [_channelsFileDescriptorCache removeAllObjects];
}

-(void) didUpdateContact:(NSNotification *) notification {
    Contact *contact = (Contact *)[notification.object objectForKey:kContactKey];
    NSArray *changedKeys = (NSArray *)[notification.object objectForKey:kChangedAttributesKey];
    
    if (contact && [changedKeys count] > 0 && ([changedKeys containsObject:@"photoData"] || [changedKeys containsObject:@"displayName"])) {
        for (ChannelItem *item in [_channelsItems copy]) {
            if (item.contact == contact)
                [self didUpdateChannelItem:item withChangedKeys:changedKeys];
        }
    }
}

-(void) fetchMyChannels {
    _channelsFullyLoaded = NO;
    [[RainbowUserDefaults sharedInstance] setBool:_channelsFullyLoaded forKey:kChannelsFullyLoaded];
    
    @synchronized (_channels) {
        [_channels removeAllObjects];
    }
    
    [self getMyChannelsWithCompletionHandler:^(NSArray<ChannelDescription *> *channelDescriptions, NSError *error) {
        if (!error) {
            _channelsFullyLoaded = YES;
            [[RainbowUserDefaults sharedInstance] setBool:_channelsFullyLoaded forKey:kChannelsFullyLoaded];
            
            [self fetchLatestItemsList];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kChannelsServiceDidEndFetchingChannelsFromServer object:nil];
            // TODO : we can add a notification here kDidEndFetchingChannelsFromServer
        } else {
            OTCLog(@"fetchMyChannels error %@", error);
        }
    }];
}

-(void) fetchLatestItemsList {
    @synchronized (_channelsItems) {
        [_channelsItems removeAllObjects];
    }
    
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesChannelsGetLatestItems];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesChannelsGetLatestItems];
    NSLog(@"Get latest items from all subscribed channels");
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (!error) {
            NSDictionary *data = [receivedData objectFromJSONData];
            NSLog(@"%@", data);
            NSMutableArray *channelsItems = [[NSMutableArray alloc] init];
            for(NSDictionary *descr in data[@"data"][@"items"]){
                ChannelItem *channelItem = [self createOrUpdateChannelItemFromDictionary:descr postNotifications:NO];
                [channelsItems addObject:channelItem];
            }
            
            // Set back the new tag because it is managed locally
            for (ChannelItem *savedItem in _savedNewItems) {
                ChannelItem *item = [self getChannelItemById:savedItem.itemId];
                if (item)
                    item.isNew = YES;
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:kChannelsServiceDidEndFetchingChannelsItemsFromServer object:nil];
        } else {
            OTCLog(@"fetchLatestItemsList error %@", error);
        }
    }];
}

-(void) refreshLatestItemsList {
    [_channelsItemsCache.operationQueue cancelAllOperations];
    [_channelsItemsCache removeAllObjects];
    [self fetchLatestItemsList];
}

-(void) loadChannelsFromCache {
    __weak __typeof__(self) weakSelf = self;
    NSMutableArray *existingKeys = [NSMutableArray array];
    NSLog(@"Channels cache : Start loading cache");
    
    [_channelsCache.diskCache enumerateObjectsWithBlockAsync:^(NSString *key, NSURL *fileURL, BOOL *stop) {
        [existingKeys addObject:key];
    } completionBlock:^(id<PINCaching> cache) {
        NSLog(@"Channels cache : Load cache Completion block start");
        for (NSString *key in existingKeys) {
            NSDictionary *obj = [_channelsCache objectForKey:key];
            [weakSelf createOrUpdateChannelFromDictionary:obj postNotifications:YES ignoreUnsubscribed:YES];
        }
        NSLog(@"Channels cache : Load cache Completion block end");
        
        [self sendNotificationDidUpdateInvitationsCount];
        
        [self loadChannelsItemsFromCache];
        [[NSNotificationCenter defaultCenter] postNotificationName:kChannelsServiceDidEndLoadingChannelsFromCache object:nil];
    }];
}

-(void) loadChannelsItemsFromCache {
    __weak __typeof__(self) weakSelf = self;
    NSMutableArray *existingKeys = [NSMutableArray array];
    NSLog(@"Channels items cache : Start loading cache");
    
    [_channelsItemsCache.diskCache enumerateObjectsWithBlockAsync:^(NSString *key, NSURL *fileURL, BOOL *stop) {
        [existingKeys addObject:key];
    } completionBlock:^(id<PINCaching> cache) {
        NSLog(@"Channels items cache : Load cache Completion block start");
        for (NSString *key in existingKeys) {
            NSDictionary *obj = [_channelsItemsCache objectForKey:key];
            [weakSelf createOrUpdateChannelItemFromDictionary:obj postNotifications:NO];
        }
        NSLog(@"Channels items cache : Load cache Completion block end");
        [[NSNotificationCenter defaultCenter] postNotificationName:kChannelsServiceDidEndLoadingChannelsItemsFromCache object:nil];
    }];
}

-(Channel *) createOrUpdateChannelFromDictionary:(NSDictionary *)dictionary postNotifications:(BOOL) notify ignoreUnsubscribed:(BOOL) ignoreUnsubscribed {
    
    if (!dictionary[@"id"]) {
        OTCLog(@"createOrUpdateChannelFromDictionary : the id cannot be nil");
        return nil;
    }
    
    Channel *theChannel;
    BOOL newChannel = NO;
    
    theChannel = [self getChannelById:dictionary[@"id"]];
    
    if (!theChannel) {
        BOOL isInvited = [[dictionary objectForKey:@"invited"] notNull] && ((NSNumber *)[dictionary objectForKey:@"invited"]).boolValue;
        BOOL isSubscribed = [[dictionary objectForKey:@"subscribed"] notNull] && ((NSNumber *)[dictionary objectForKey:@"subscribed"]).boolValue;
        if (ignoreUnsubscribed && !isSubscribed && !isInvited)
            return nil;
        
        theChannel = [Channel new];
        newChannel = YES;
        [_channels addObject:theChannel];
    }
    NSDictionary *currentChannelInfo = [theChannel dictionaryRepresentation];
    
    theChannel.id = dictionary[@"id"];
    theChannel.name = (NSString *)[dictionary objectForKey:@"name"];
    theChannel.topic = (NSString *)[dictionary objectForKey:@"topic"];
    theChannel.visibility = [Channel channelVisibilityFromString:[dictionary objectForKey:@"visibility"]];
    theChannel.category = [Channel channelCategoryFromString:[dictionary objectForKey:@"category"]];
    theChannel.creatorId = (NSString *)[dictionary objectForKey:@"creatorId"];
    theChannel.companyId = (NSString *)[dictionary objectForKey:@"companyId"];
    theChannel.creationDate = (NSDate *)[Tools dateFromString:[dictionary objectForKey:@"creationDate"] withFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss.S'Z'"];
    theChannel.usersCount = [[dictionary objectForKey:@"users_count"] intValue];
    theChannel.subscribersCount = [[dictionary objectForKey:@"subscribers_count"] intValue];
    theChannel.userType = [Channel channelUserTypeFromString:[dictionary objectForKey:@"type"]];
    
    if ([[dictionary objectForKey:@"subscribed"] notNull])
        theChannel.subscribed = ((NSNumber *)[dictionary objectForKey:@"subscribed"]).boolValue;
    
    if ([[dictionary objectForKey:@"invited"] notNull])
        theChannel.invited = ((NSNumber *)[dictionary objectForKey:@"invited"]).boolValue;
    else
        theChannel.invited = NO;
    
    // If there is no last avatar, the server returns a lastAvatarUpdateDate with a <null> object in it, so we need the notNull test
    if ([[dictionary objectForKey:@"lastAvatarUpdateDate"] notNull])
        theChannel.lastAvatarUpdateDate = (NSDate *)[Tools dateFromString:[dictionary objectForKey:@"lastAvatarUpdateDate"] withFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss.S'Z'"];
    
    [_channelsCache setObjectAsync:dictionary forKey:theChannel.id completion:nil];
    
    if (newChannel)
        [self getAvatarInCacheForChannel:theChannel];
    
    if(notify){
        if (newChannel) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kChannelsServiceDidAddChannel object:@{kChannelKey: theChannel}];
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:kChannelsServiceDidUpdateChannel object:@{kChannelKey: theChannel, kChangedKeys: [currentChannelInfo changedKeysIn:[theChannel dictionaryRepresentation]]}];
        }
    }
    
    return theChannel;
}

-(ChannelItem *) createOrUpdateChannelItemFromDictionary:(NSDictionary *)dictionary postNotifications:(BOOL) notify {
    
    if (!dictionary[@"id"]) {
        OTCLog(@"createOrUpdateChannelItemFromDictionary : the id cannot be nil");
        return nil;
    }
    
    ChannelItem *channelItem;
    BOOL newChannelItem = NO;
    
    channelItem = [self getChannelItemById:dictionary[@"id"]];
    
    if (!channelItem) {
        channelItem = [ChannelItem new];
        newChannelItem = YES;
    }
    
    NSDictionary *currentChannelItemInfo = [channelItem dictionaryRepresentation];
    
    channelItem.itemId = dictionary[@"id"];
    if (dictionary[@"channelId"])
        channelItem.channelId = dictionary[@"channelId"];
    
    if (dictionary[@"from"]) {
        [[ServicesManager sharedInstance].contactsManagerService searchRemoteContactWithJid:dictionary[@"from"] withCompletionHandler:^(Contact *foundContact) {
            if (foundContact) {
                channelItem.contact = foundContact;
            } else {
                OTCLog(@"Error while searching the remote contact associated with jid = %@", dictionary[@"from"]);
            }
        }];
    }
    
    if (dictionary[@"type"])
        channelItem.type = dictionary[@"type"];
    
    NSDictionary *entry = [dictionary objectForKey:@"entry"];
    
    if (!entry)
        entry = dictionary;
    
    if ([entry objectForKey:@"title"])
        channelItem.title = [entry objectForKey:@"title"];
    if ([entry objectForKey:@"message"])
        channelItem.message = [entry objectForKey:@"message"];
    if ([entry objectForKey:@"url"])
        channelItem.url = [entry objectForKey:@"url"];
    if ([entry objectForKey:@"video"] && [[entry objectForKey:@"video"] objectForKey:@"id"])
        channelItem.youtubeVideoId = [[entry objectForKey:@"video"] objectForKey:@"id"];
    else
        channelItem.youtubeVideoId = nil;
    if ([entry objectForKey:@"isNew"] && [[entry objectForKey:@"isNew"] isEqualToString:@"YES"])
        channelItem.isNew = YES;
    if (dictionary[@"creation"] && dictionary[@"timestamp"]) {
        channelItem.date = [Tools dateFromString:[dictionary objectForKey:@"creation"] withFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss.S'Z'"];
        channelItem.editionDate = [Tools dateFromString:[dictionary objectForKey:@"timestamp"] withFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss.S'Z'"];
    } else if (dictionary[@"timestamp"]) {
        channelItem.date = [Tools dateFromString:[dictionary objectForKey:@"timestamp"] withFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss.S'Z'"];
    }
    
    // Do this at the end to wait if we need to download the first image
    [((NSMutableArray *)channelItem.images) removeAllObjects];
    if ([entry objectForKey:@"images"]) {
        if ([[entry objectForKey:@"images"] isKindOfClass:[NSArray class]]) {
            for (NSDictionary *image in [entry objectForKey:@"images"]) {
                if ([image isKindOfClass:[NSString class]]) {
                    [(NSMutableArray *)channelItem.images addObject:[[ChannelItemImage alloc] initWithRainbowId:(NSString *)image andChannelItemId:channelItem.itemId]]; // Sometimes the image is directly the id.....
                } else {
                    [(NSMutableArray *)channelItem.images addObject:[[ChannelItemImage alloc] initWithRainbowId:[image objectForKey:@"id"] andChannelItemId:channelItem.itemId]];
                }
            }
        } else {
            [(NSMutableArray *)channelItem.images addObject:[[ChannelItemImage alloc] initWithRainbowId:[[entry objectForKey:@"images"] objectForKey:@"id"] andChannelItemId:channelItem.itemId]];
        }
        
        // Force to load the first image
        ChannelItemImage *firstItemImage = (ChannelItemImage *)channelItem.images.firstObject;
        if (firstItemImage && !firstItemImage.image) {
            dispatch_semaphore_t sema = dispatch_semaphore_create(0);
            [self loadItemImageWith:firstItemImage forceOriginalFile:NO completionBlock:^(UIImage *image) {
                firstItemImage.image = image;
                dispatch_semaphore_signal(sema);
            }];
            dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
        }
    }
    
    // IF YOU NEED TO PARSE SOMETHING ELSE MORE, DO THIS BEFORE THE IMAGES PART

    if (newChannelItem) {
        // Insert the item depending on the date
        NSInteger index = 0;
        while (index < _channelsItems.count && [((ChannelItem *)[_channelsItems objectAtIndex:index]).date compare:channelItem.date] == NSOrderedDescending) {
            index++;
        }
        [_channelsItems insertObject:channelItem atIndex:index];
    }
    
    if (![[channelItem dictionaryRepresentation] objectForKey:@"from"]) {
        // If the channel item has no "from" property, it is because we are fetching it, so add the jid to the dictionary to be saved in cache
        NSMutableDictionary *dic = (NSMutableDictionary *)[channelItem dictionaryRepresentation];
        [dic setObject:dictionary[@"from"] forKey:@"from"];
        [_channelsItemsCache setObjectAsync:dic forKey:channelItem.itemId completion:nil];
    } else {
        [_channelsItemsCache setObjectAsync:[channelItem dictionaryRepresentation] forKey:channelItem.itemId completion:nil];
    }
    
    if (notify) {
        if (newChannelItem) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kChannelsServiceDidReceiveItem object:channelItem];
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:kChannelsServiceDidUpdateItem object:@{kChannelItemKey: channelItem, kChangedKeys: [currentChannelItemInfo changedKeysIn:[channelItem dictionaryRepresentation]]}];
        }
    }
    
    return channelItem;
}

// For method loadItemImageWith
-(void) updateItemImage:(ChannelItemImage *)itemImage withFile:(File *) file withThumbnailData:(BOOL) thumbnailData completionBlock:(void(^)(UIImage *image)) block {
    UIImage *image = nil;
    
    if (file) {
        NSData *data = thumbnailData ? file.thumbnailData : file.data;
        image = [UIImage imageWithData:data];
        itemImage.image = image;
        itemImage.isOriginalFile = !thumbnailData;
    }
    
    itemImage.isLoadingImage = NO;
    
    if (block)
        block(image);
}

-(void) loadItemImageWith:(ChannelItemImage *) itemImage forceOriginalFile:(BOOL) forceOriginalFile completionBlock:(void(^)(UIImage *image)) block {
    
    NSDictionary *fileDict = [_channelsFileDescriptorCache objectForKey:itemImage.rainbowId];
    
    if (itemImage.isLoadingImage)
        return;
    
    itemImage.isLoadingImage = YES;
    
    if (!fileDict || [fileDict count] == 0) {
        // fetch informations
        File *fakeFile = [File new];
        fakeFile.rainbowID = itemImage.rainbowId;
        [[ServicesManager sharedInstance].fileSharingService fetchFileInformation:fakeFile completionHandler:^(File *file, NSError *error) {
            //[_channelsFileDescriptorCache setObjectAsync:[file dictionaryRepresentation] forKey:itemImage.rainbowId completion:nil];
            
            if (forceOriginalFile) {
                // We ask the original file, so load or download it
                [[ServicesManager sharedInstance].fileSharingService loadOrDownloadDataForFile:file withCompletionHandler:^(File *file, NSError *error) {
                    [self updateItemImage:itemImage withFile:file withThumbnailData:NO completionBlock:block];
                }];
            } else if ([[ServicesManager sharedInstance].fileSharingService loadDataFromCacheForFile:file]) {
                // We try to get high resolution
                [self updateItemImage:itemImage withFile:file withThumbnailData:NO completionBlock:block];
            } else {
                // Just load or download thumbnail
                [[ServicesManager sharedInstance].fileSharingService loadOrDownloadDataForThumbnailFile:file withCompletionHandler:^(File *file, NSError *error) {
                    [self updateItemImage:itemImage withFile:file withThumbnailData:YES completionBlock:block];
                }];
            }
        }];
    } else {
        File *file = [[ServicesManager sharedInstance].fileSharingService getOrCreateFileWithRainbowID:itemImage.rainbowId withInfos:fileDict];

        // Same as above, but block does not work I don't know why
        if (forceOriginalFile) {
            [[ServicesManager sharedInstance].fileSharingService loadOrDownloadDataForFile:file withCompletionHandler:^(File *file, NSError *error) {
                [self updateItemImage:itemImage withFile:file withThumbnailData:NO completionBlock:block];
            }];
        } else if ([[ServicesManager sharedInstance].fileSharingService loadDataFromCacheForFile:file]) {
            [self updateItemImage:itemImage withFile:file withThumbnailData:NO completionBlock:block];
        } else {
            [[ServicesManager sharedInstance].fileSharingService loadOrDownloadDataForThumbnailFile:file withCompletionHandler:^(File *file, NSError *error) {
                [self updateItemImage:itemImage withFile:file withThumbnailData:YES completionBlock:block];
            }];
        }
    }
}

-(NSString *)channelIdFromNodeName:(NSString *)channel {
    NSArray *parts = [channel componentsSeparatedByString:@":"];
    if([parts count]>0){
        return [parts objectAtIndex: 0];
    } else {
        return nil;
    }
}

-(Channel *) createChannelFromJson:(NSDictionary *) jsonDictionary {
    Channel *channel = [[Channel alloc] init];
    channel.name = (NSString *)[jsonDictionary objectForKey:@"name"];
    channel.topic = (NSString *)[jsonDictionary objectForKey:@"topic"];
    channel.maxItems = [[jsonDictionary objectForKey:@"max_items"] intValue];
    channel.maxPayloadSize = [[jsonDictionary objectForKey:@"max_payload_size"] intValue];
    channel.visibility = [Channel channelVisibilityFromString:[jsonDictionary objectForKey:@"visibility"]];
    channel.id = (NSString *)[jsonDictionary objectForKey:@"id"];
    channel.creatorId = (NSString *)[jsonDictionary objectForKey:@"creatorId"];
    channel.companyId = (NSString *)[jsonDictionary objectForKey:@"companyId"];
    channel.creationDate = (NSDate *)[Tools dateFromString:[jsonDictionary objectForKey:@"creationDate"] withFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss.S'Z'"];
    channel.usersCount = [[jsonDictionary objectForKey:@"users_count"] intValue];
    channel.subscribersCount = [[jsonDictionary objectForKey:@"subscribers_count"] intValue];
    
    return channel;
}

-(void) createChannelWithName:(NSString *)name topic:(NSString *)topic visibility:(ChannelVisibility)visibility maxItems:(int)maxItems maxPayloadSize:(int)maxPayloadSize completionHandler:(ChannelsServiceGetChannelCompletionHandler) completionHandler {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesChannelsCreateChannel];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesChannelsCreateChannel];
    NSString *visibilityStr = [Channel stringFromChannelVisibility:visibility];
    NSMutableDictionary *bodyContent = [NSMutableDictionary dictionaryWithDictionary:@{@"name":name, @"visibility":visibilityStr}];
    if(topic){
        [bodyContent setObject:topic forKey:@"topic"];
    }
    if(maxItems>0){
        [bodyContent setObject:[NSNumber numberWithInt: maxItems] forKey:@"max_items"];
    }
    if(maxPayloadSize<0){
        maxPayloadSize = 10000;
    }
    [bodyContent setObject:[NSNumber numberWithInt: maxPayloadSize] forKey:@"max_payload_size"];

    NSLog(@"Create %@ channel with name %@", visibilityStr, name);
    [_downloadManager postRequestWithURL:url body:[bodyContent jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        Channel *channel = nil;
        if (error) {
            NSLog(@"Create channel returned an error: %@ response %@", error, [receivedData objectFromJSONData]);
        } else {
            NSDictionary *data = [receivedData objectFromJSONData];
            NSLog(@"Create channel succeeded, %@", data);
            if(data[@"data"]){
                channel = [self createChannelFromJson:data[@"data"]];
            }
        }
        if(completionHandler){
            completionHandler(channel, error);
        }
    }];
}

-(void) deleteChannelWithId:(NSString *)id completionHandler: (ChannelsServiceCompletionHandler) completionHandler {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesChannelsGetChannel, id];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesChannelsGetChannel];
    NSLog(@"Delete channel with id:%@",id);
    [_downloadManager deleteRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error){
           NSLog(@"Create channel returned an error: %@ response %@", error, [receivedData objectFromJSONData]);
        }
        if(completionHandler){
            completionHandler(error);
        }
    }];
}

-(void)getMyChannelsWithCompletionHandler: (ChannelsServiceGetMyChannelsCompletionHandler) completionHandler {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesChannelsGetMyChannels];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesChannelsGetMyChannels];
    NSLog(@"Get my channels");
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error){
            if(completionHandler){
                completionHandler(nil, error);
            }
        } else {
            NSDictionary *data = [receivedData objectFromJSONData];
            NSMutableArray *channels = [[NSMutableArray alloc] init];
            for(NSDictionary *descr in data[@"data"]){
                Channel *channel = [self createOrUpdateChannelFromDictionary:descr postNotifications:YES ignoreUnsubscribed:YES];
                if (channel) {
                    [self populateAvatarForChannel:channel forceReload:YES atSize:512];
                    [channels addObject: channel];
                }
            }
            if(completionHandler){
                completionHandler(channels, error);
            }
        }
    }];
}

-(void)fetchChannelById:(NSString *)channelId completionHandler: (ChannelsServiceGetChannelCompletionHandler) completionHandler {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesChannelsGetChannel, channelId];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesChannelsGetChannel];
    NSLog(@"Get channel by Id");
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        Channel *channel = nil;
        if(error){
            NSLog(@"Get channel by id returned an error: %@ response %@", error, [receivedData objectFromJSONData]);
        } else {
            NSDictionary *data = [receivedData objectFromJSONData];
            if(data[@"data"]){
                channel = [self createOrUpdateChannelFromDictionary:data[@"data"] postNotifications:YES ignoreUnsubscribed:NO];
                if (channel)
                    [self populateAvatarForChannel:channel forceReload:YES atSize:512];
            }
        }
        if(completionHandler){
            completionHandler(channel, error);
        }
    }];
}

-(Channel *)getChannelById:(NSString *)channelId {
    @synchronized (_channels) {
        for (Channel *channel in _channels) {
            if ([channel.id isEqualToString:channelId])
                return channel;
        }
        return nil;
    }
}

-(ChannelItem *) getChannelItemById:(NSString *)channelItemId {
    @synchronized (_channelsItems) {
        for (ChannelItem *channelItem in _channelsItems) {
            if ([channelItem.itemId isEqualToString:channelItemId])
                return channelItem;
        }
        return nil;
    }
}

-(void)publishMessageToChannel:(Channel *)channel title:(NSString *)title message:(NSString *)message url:(NSString *)link completionHandler:(ChannelsServiceCompletionHandler) completionHandler {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesChannelsPublish, channel.id];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesChannelsPublish];
    NSMutableDictionary *payload = [NSMutableDictionary dictionaryWithDictionary:@{@"title":title, @"message":message}];
    if(link){
        [payload setObject:link forKey:@"url"];
    }
    NSDictionary *bodyContent = @{@"payload":payload};
    NSLog(@"Publish to channel");
    [_downloadManager postRequestWithURL:url body:[bodyContent jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            NSLog(@"Publish to channel returned an error: %@ response %@", error, [receivedData objectFromJSONData]);
        }
        if(completionHandler){
            completionHandler(error);
        }
    }];
}

-(void)subscribeToChannel:(Channel *)channel completionHandler:(ChannelsServiceCompletionHandler) completionHandler {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesChannelsSubscribe, channel.id];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesChannelsSubscribe];
    NSDictionary *bodyContent = @{};
    NSLog(@"Subscribe to channel");
    [_downloadManager postRequestWithURL:url body:[bodyContent jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            NSLog(@"Subscribe to channel returned an error: %@ response %@", error, [receivedData objectFromJSONData]);
        }
        if(completionHandler){
            completionHandler(error);
        }
    }];
}

-(void)unsubscribeToChannel:(Channel *)channel completionHandler:(ChannelsServiceCompletionHandler) completionHandler {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesChannelsSubscribe, channel.id];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesChannelsSubscribe];
    NSDictionary *bodyContent = @{};
    NSLog(@"Unsubscribe to channel");
    [_downloadManager deleteRequestWithURL:url body:[bodyContent jsonStringWithPrettyPrint:NO] requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error) {
            NSLog(@"Unsubscribe to channel returned an error: %@ response %@", error, [receivedData objectFromJSONData]);
        }
        if(completionHandler){
            completionHandler(error);
        }
    }];
}

-(void)get:(NSInteger)count itemsFromChannel:(Channel *)channel completionHandler:(ChannelsServiceGetItemsCompletionHandler) completionHandler {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesChannelsGetItemsFromChannel, channel.id];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesChannelsGetItemsFromChannel];
    if(count >= 0){
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?max=%ld", url, count]];
    }
    NSLog(@"Get items from channel");
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        NSInteger availableItemsCount = 0;
        NSMutableArray<ChannelItem *> *items = nil;
        if (error) {
            NSLog(@"Subscribe to channel returned an error: %@ response %@", error, [receivedData objectFromJSONData]);
        } else {
            items = [[NSMutableArray alloc] init];
            NSDictionary *data = [receivedData objectFromJSONData];
            if(data[@"data"]){
                availableItemsCount = [data[@"data"][@"status"][@"count"] integerValue];
                if(count>0){
                    for(NSDictionary *item in data[@"data"][@"items"]){
                        NSString *id = item[@"id"];
                        NSDictionary *payload = item[@"entry"][@"payload"][0];
                        NSString *title = payload[@"title"][0];
                        NSString *message = payload[@"message"][0];
                        NSString *url = payload[@"url"][0];
                        ChannelItem *channelPayload = [[ChannelItem alloc] init];
                        channelPayload.channelId = channel.id;
                        channelPayload.itemId = id;
                        channelPayload.title = title;
                        channelPayload.message = message;
                        channelPayload.url = url;
                        [items addObject:channelPayload];
                    }
                }
            }
        }
        if(completionHandler){
            completionHandler(availableItemsCount, items, error);
        }
    }];
}

-(ChannelUser *)channelUserFromDict:(NSDictionary *)userDict {
    ChannelUser *user = [[ChannelUser alloc] init];
    user.rainbowId = userDict[@"id"];
    user.type = [Channel channelUserTypeFromString:userDict[@"type"]];
    user.additionDate = [Tools dateFromString:userDict[@"additionDate"] withFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss.S'Z'"];
    user.loginEmail = userDict[@"loginEmail"];
    user.displayName = userDict[@"displayName"];
    user.companyId = userDict[@"companyId"];
    user.companyName = userDict[@"companyName"];
    
    return user;
}

-(void)getFirstUsersFromChannel:(Channel *)channel completionHandler:(ChannelsServiceGetUsersCompletionHandler) completionHandler {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesChannelsGetFirstUsersFromChannel, channel.id];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesChannelsGetFirstUsersFromChannel];
    NSLog(@"Get first users from a channel");
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        NSMutableArray<ChannelUser *> *users = nil;
        if(!error){
            NSDictionary *data = [receivedData objectFromJSONData];
            if(data[@"data"]){
                users = [[NSMutableArray alloc] init];
                for(NSDictionary *userDict in data[@"data"]){
                    ChannelUser *user = [self channelUserFromDict:userDict];
                    if(user){
                        [users addObject:user];
                    }
                }
            }
        } else {
            NSLog(@"Get first users from channel returned an error: %@ response %@", error, [receivedData objectFromJSONData]);
        }
        if(completionHandler){
            completionHandler(users, error);
        }
    }];
}

-(void)getNextUsersFromChannel:(Channel *)channel atIndex:(NSInteger)index completionHandler:(ChannelsServiceGetUsersCompletionHandler) completionHandler {
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesChannelsGetNextUsersFromChannel, channel.id, index];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesChannelsGetNextUsersFromChannel];
    NSLog(@"Get next users from a channel starting at index %ld", (long)index);
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        NSMutableArray<ChannelUser *> *users = nil;
        if(!error){
            NSDictionary *data = [receivedData objectFromJSONData];
            if(data[@"data"]){
                users = [[NSMutableArray alloc] init];
                for(NSDictionary *userDict in data[@"data"]){
                    ChannelUser *user = [self channelUserFromDict:userDict];
                    if(user){
                        [users addObject:user];
                    }
                }
            }
        } else {
            NSLog(@"Get next users from channel returned an error: %@ response %@", error, [receivedData objectFromJSONData]);
        }
        if(completionHandler){
            completionHandler(users, error);
        }
    }];
}

-(void)findChannelByName:(NSString *)name limit:(NSInteger)limit completionHandler:(ChannelsServiceFindChannelsCompletionHandler) completionHandler {
    NSURL *serviceUrl = [_apiUrlManagerService getURLForService:ApiServicesChannelsFindChannels];
    NSURL *url;
    if(limit >= 0){
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?name=%@&limit=%ld", serviceUrl, name, limit]];
    } else {
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?name=%@", serviceUrl, name]];
    }
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesChannelsFindChannels];
    NSLog(@"Find channel by name using '%@'", name);
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error){
            if(completionHandler){
                completionHandler(nil, error);
            }
        } else {
            NSDictionary *data = [receivedData objectFromJSONData];
            NSMutableArray *channels = [[NSMutableArray alloc] init];
            for(NSDictionary *descr in data[@"data"]){
                ChannelDescription *channelDescription = [[ChannelDescription alloc] initWithName:descr[@"name"] topic:descr[@"topic"] id:descr[@"id"] userType:ChannelUserTypeNone creatorId:descr[@"creatorId"]];
                [channels addObject: channelDescription];
            }
            if(completionHandler){
                completionHandler(channels, error);
            }
        }
    }];
}

-(void)findChannelByTopic:(NSString *)topic limit:(NSInteger)limit completionHandler:(ChannelsServiceFindChannelsCompletionHandler) completionHandler {
    NSURL *serviceUrl = [_apiUrlManagerService getURLForService:ApiServicesChannelsFindChannels];
    NSURL *url;
    if(limit >= 0){
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?topic=%@&limit=%ld", serviceUrl, topic, limit]];
    } else {
        url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?topic=%@", serviceUrl, topic]];
    }
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesChannelsFindChannels];
    NSLog(@"Find channel by topic using '%@'", topic);
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(error){
            if(completionHandler){
                completionHandler(nil, error);
            }
        } else {
            NSDictionary *data = [receivedData objectFromJSONData];
            NSMutableArray *channels = [[NSMutableArray alloc] init];
            for(NSDictionary *descr in data[@"data"]){
                ChannelDescription *channelDescription = [[ChannelDescription alloc] initWithName:descr[@"name"] topic:descr[@"topic"] id:descr[@"id"] userType:ChannelUserTypeNone creatorId:descr[@"creatorId"]];
                [channels addObject: channelDescription];
            }
            if(completionHandler){
                completionHandler(channels, error);
            }
        }
    }];
}

-(void)updateChannelWithId:(NSString *)id name:(NSString*)name topic:(NSString *)topic visibility:(ChannelVisibility)visibility maxItems:(NSInteger)maxItems maxPayloadSize:(NSInteger)maxPayloadSize completionHandler:(ChannelsServiceCompletionHandler) completionHandler {
    NSURL *serviceUrl = [_apiUrlManagerService getURLForService:ApiServicesChannelsUpdateChannel, id];
    NSMutableString *query = [NSMutableString stringWithString:@"%@?"];
    
    if (name)
        [query appendString:[NSString stringWithFormat:@"name=%@", name]];
    
    if(topic){
        [query appendString:[NSString stringWithFormat:@"topic=%@", topic]];
    }
    if(visibility>=0){
        if(topic){
            [query appendString:@"&"];
        }
        [query appendString:[NSString stringWithFormat:@"visibility=%@", [Channel stringFromChannelVisibility:visibility]]];
    }
    if(maxItems>=0){
        if(topic || visibility>=0){
            [query appendString:@"&"];
        }
        [query appendString:[NSString stringWithFormat:@"max_items=%ld", maxItems]];
    }
    if(maxPayloadSize>=0){
        if(topic || visibility>=0 || maxItems>=0){
            [query appendString:@"&"];
        }
        [query appendString:[NSString stringWithFormat:@"max_payload_size=%ld", maxPayloadSize]];
    }
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:query, serviceUrl]];
    NSDictionary *headers = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesChannelsUpdateChannel];
    NSLog(@"Update channel");
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headers completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(completionHandler){
            completionHandler(error);
        }
    }];
}

-(void)updateChannelWithId:(NSString *)channelId name:(NSString *) name completionHandler:(ChannelsServiceCompletionHandler) completionHandler {
    [self updateChannelWithId:channelId name:name topic:nil visibility:-1 maxItems:-1 maxPayloadSize:-1 completionHandler:completionHandler];
}

-(void)updateChannelWithId:(NSString *)channelId topic:(NSString *)topic completionHandler:(ChannelsServiceCompletionHandler) completionHandler {
    [self updateChannelWithId:channelId name:nil topic:topic visibility:-1 maxItems:-1 maxPayloadSize:-1 completionHandler:completionHandler];
}

-(void)updateChannelWithId:(NSString *)channelId visibility:(ChannelVisibility)visibility completionHandler:(ChannelsServiceCompletionHandler) completionHandler {
    [self updateChannelWithId:channelId name:nil topic:nil visibility:visibility maxItems:-1 maxPayloadSize:-1 completionHandler:completionHandler];
}

-(void)updateChannelWithId:(NSString *)channelId maxItems:(NSUInteger)maxItems completionHandler:(ChannelsServiceCompletionHandler) completionHandler {
    [self updateChannelWithId:channelId name:nil topic:nil visibility:-1 maxItems:maxItems maxPayloadSize:-1 completionHandler:completionHandler];
}

-(void)updateChannelWithId:(NSString *)channelId maxPayloadSize:(NSUInteger)maxPayloadSize completionHandler:(ChannelsServiceCompletionHandler) completionHandler {
    [self updateChannelWithId:channelId name:nil topic:nil visibility:-1 maxItems:-1 maxPayloadSize:maxPayloadSize completionHandler:completionHandler];
}

-(void) didUpdateChannel:(Channel *) channel withChangedKeys:(NSArray *) changedKeys {
    if (!channel || !changedKeys || [changedKeys count] == 0)
        return;
    [[NSNotificationCenter defaultCenter] postNotificationName:kChannelsServiceDidUpdateChannel object:@{kChannelKey: channel, kChangedKeys: changedKeys}];
}

-(void) didUpdateChannelItem:(ChannelItem *) channelItem withChangedKeys:(NSArray *) changedKeys {
    if (!channelItem || !changedKeys || [changedKeys count] == 0)
        return;
    [[NSNotificationCenter defaultCenter] postNotificationName:kChannelsServiceDidUpdateItem object:@{kChannelItemKey: channelItem, kChangedKeys: changedKeys}];
}

-(void) updateChannelInCache:(Channel *) channel {
    [_channelsCache setObjectAsync:[channel dictionaryRepresentation] forKey:channel.id completion:nil];
}

#pragma mark - Invitations management

-(void) acceptInvitation:(Channel *) channel {
    if (!channel.id || [channel.id length] == 0) {
        OTCLog(@"No id for channel, cannot accept invitation");
        return;
    }
    
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesChannelsInvitationAccept, channel.id];
    NSDictionary *headersParameters = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesChannelsInvitationAccept];
    
    [_downloadManager putRequestWithURL:url body:nil requestHeadersParameter:headersParameters completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error)
            NSLog(@"Accept invitation to channel returned an error: %@ response %@", error, [receivedData objectFromJSONData]);
    }];
}

-(void) declineInvitation:(Channel *) channel {
    if (!channel.id || [channel.id length] == 0) {
        OTCLog(@"No id for channel, cannot decline invitation");
        return;
    }
    
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesChannelsInvitationDecline, channel.id];
    NSDictionary *headersParameters = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesChannelsInvitationDecline];
    
    [_downloadManager putRequestWithURL:url body:nil requestHeadersParameter:headersParameters completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (error)
            NSLog(@"Decline invitation to channel returned an error: %@ response %@", error, [receivedData objectFromJSONData]);
    }];
}

#pragma mark - Channel avatar

-(void) populateAvatarForChannel:(Channel *) channel forceReload:(BOOL) forceReload atSize:(int) size {
    if (channel.photoData && !forceReload)
        return;
    
    if (!channel.id || [channel.id length] == 0) {
        OTCLog(@"No id for channel, cannot get avatar");
        return;
    }
    
    OTCLog(@"Fetching avatar for channel with id = %@", channel.id);
    
    [self loadChannelPhotoData:channel atSize:size completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if (!error) {
            NSData *currentPhotoData;
            @synchronized (_channels) {
                currentPhotoData = channel.photoData;
                channel.photoData = receivedData;
            }
            
            OTCLog(@"Saving avatar for channel with id = %@", channel.id);
            [_channelsAvatarsCache setObjectAsync:channel.photoData forKey:channel.id completion:^(id<PINCaching>  _Nonnull cache, NSString * _Nonnull key, id  _Nullable object) {
                NSLog(@"Saved avatar object in cache for %@", key);
            }];
            
            if ((channel.photoData || currentPhotoData) && ![channel.photoData isEqualToData:currentPhotoData]) {
                 [self didUpdateChannel:channel withChangedKeys:@[@"photoData", @"lastAvatarUpdateDate"]];
            }
        }
    }];
    
}

-(void) loadChannelPhotoData:(Channel *) channel atSize:(int) size completionBlock:(ChannelsServiceLoadAvatarInternalCompletionHandler) completionHandler {
    NSString *update = [[NSDate jsonStringFromDate:channel.lastAvatarUpdateDate] MD5];

    NSMutableString *urlParam = [NSMutableString stringWithFormat:@"%u",size];
    if(update.length > 0)
        [urlParam appendFormat:@"&update=%@", update];
    
    NSURL *url = [_apiUrlManagerService getURLForService:ApiServicesChannelsAvatar, channel.id, urlParam];
    NSDictionary *headersParameters = [_apiUrlManagerService getHTTPHeaderParametersForService:ApiServicesChannelsAvatar];
    
    [_downloadManager getRequestWithURL:url requestHeadersParameter:headersParameters completionBlock:^(NSData *receivedData, NSError *error, NSURLResponse *response) {
        if(completionHandler)
            completionHandler(receivedData, error, response);
    }];
}

-(void) getAvatarInCacheForChannel:(Channel *) channel {
    NSData *object = [_channelsAvatarsCache objectForKey:channel.id];
    
    NSData *currentPhotoData;
    @synchronized (_channels) {
        currentPhotoData = channel.photoData;
        channel.photoData = object;
    }
    // Handle cases where old or new values can be nil.
    if ((channel.photoData || currentPhotoData) && ![channel.photoData isEqualToData:currentPhotoData]) {
        [self didUpdateChannel:channel withChangedKeys:@[@"photoData"]];
    }
}

-(void) removeChannelAvatar:(Channel *) channel {
    BOOL removed = NO;
    NSDictionary *currentChannelInfo = [channel dictionaryRepresentation];
    
    @synchronized (_channels) {
        if([_channels containsObject:channel]) {
            channel.photoData = nil;
            channel.lastAvatarUpdateDate = nil;
            removed = YES;
        }
    }
    
    if(removed) {
        [_channelsAvatarsCache removeObjectForKeyAsync:channel.id completion:nil];
        [_channelsCache setObjectAsync:[channel dictionaryRepresentation] forKey:channel.id completion:nil];
        [self didUpdateChannel:channel withChangedKeys:[currentChannelInfo changedKeysIn:[channel dictionaryRepresentation]]];
    }
}

-(void) updateChannelWithItem:(ChannelItem *)item {
    if(item.itemId){
        @synchronized(self.channels){
            NSString *channelId = item.channelId;
            Channel *channel = [self getChannelById:channelId];
            if(item.message){
                if(!channel){
                    [self fetchChannelById:item.channelId completionHandler:^(Channel *newChannel, NSError *error) {
                        if(!error){
                            [(NSMutableDictionary *)self.channels setObject:newChannel forKey:channelId];
                            [newChannel addOrUpdateItem: item];
                            [[NSNotificationCenter defaultCenter] postNotificationName:kChannelsServiceDidReceiveItem object:item];
                        }
                    }];
                } else {
                    [channel addOrUpdateItem: item];
                    [[NSNotificationCenter defaultCenter] postNotificationName:kChannelsServiceDidReceiveItem object:item];
                }
            } else {
                [channel removeItem:item];
                [[NSNotificationCenter defaultCenter] postNotificationName:kChannelsServiceDidRetractItem object:item];
            }
        }
    }
}

-(void) deleteChannelItemWithId:(NSString *) channelItemId {
    if (channelItemId) {
        ChannelItem *channelItem = [self getChannelItemById:channelItemId];
        
        if (channelItem) {
            BOOL removed = NO;
            @synchronized(_channelsItems) {
                if ([_channelsItems containsObject:channelItem]) {
                    [_channelsItems removeObject:channelItem];
                    removed = YES;
                }
            }
            
            if (removed) {
                [_channelsItemsCache removeObjectForKeyAsync:channelItemId completion:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:kChannelsServiceDidRetractItem object:channelItem];
            }
        }
    }
}

//-(Channel *)findChannelWithItemId:(NSString *)id {
//    for(Channel *channel in [self.channels allValues]){
//        for(ChannelPayload *item in channel.items){
//            if([id isEqualToString: item.id]){
//                return channel;
//            }
//        }
//    }
//    return nil;
//}

#pragma mark - Count notifications

-(void) sendNotificationDidUpdateNewItemsCount {
    @synchronized (_savedNewItems) {
        [_savedNewItems removeAllObjects];
    }
    
    for (ChannelItem *item in _channelsItems) {
        if (item.isNew) {
            [_savedNewItems addObject:item];
        }
    }
    
    _hasNewItems = [_savedNewItems count] > 0;
    [[NSNotificationCenter defaultCenter] postNotificationName:kChannelsServiceDidUpdateNewItemsCount object:nil];
}

-(void) sendNotificationDidUpdateInvitationsCount {
    _hasInvitations = NO;
    
    for (Channel *channel in [_channels copy]) {
        if (channel.invited) {
            _hasInvitations = YES;
            break;
        }
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kChannelsServiceDidUpdateInvitationsCount object:nil];
}

-(void) acknoledgeChannelItem:(ChannelItem *) item {
    item.isNew = NO;
    [self didUpdateChannelItem:item withChangedKeys:@[@"isNew"]];
    [self sendNotificationDidUpdateNewItemsCount];
}

#pragma mark - XMPPServiceChannelsDelegate protocol

-(void) xmppService:(XMPPService *) service didReceiveItems:(NSArray<NSDictionary *> *) items {
    for (NSDictionary *item in items) {
        if (item[@"retractId"]) {
            [self deleteChannelItemWithId:item[@"retractId"]];
        } else if ([item objectForKey:@"id"]) {
            BOOL isFromMe = [[ServicesManager sharedInstance].myUser.contact.jid isEqualToString:[item objectForKey:@"from"]];
            if (![item objectForKey:@"creation"] && !isFromMe)
                [((NSMutableDictionary *)item) setObject:@"YES" forKey:@"isNew"];
            [self createOrUpdateChannelItemFromDictionary:item postNotifications:YES];
            if (!isFromMe)
                [self sendNotificationDidUpdateNewItemsCount];
        }
    }
}

-(void) xmppService:(XMPPService *) service didCreateChannel:(NSString *) channelID {
    [self fetchChannelById:channelID completionHandler:^(Channel *channel, NSError *error) {
        if (!error && channel.invited)
            [self sendNotificationDidUpdateInvitationsCount];
    }];
}

-(void) xmppService:(XMPPService *) service didUpdateChannel:(NSString *) channelID {
    [self fetchChannelById:channelID completionHandler:nil];
}

-(void) xmppService:(XMPPService *) service didUpdateChannel:(NSString *) channelID withProperties:(NSDictionary *)properties {
    if (!properties || [properties count] == 0) {
        OTCLog(@"didUpdateChannel : properties is empty");
        return;
    }
    
    Channel *channel = [self getChannelById:channelID];
    
    if (!channel || [channel.id length] == 0) {
        OTCLog(@"didUpdateChannel : the channel cannot be found (or id is nil)");
        return;
    }
    
    if ([properties objectForKey:@"subscribersCount"]) {
        channel.subscribersCount = [[properties objectForKey:@"subscribersCount"] intValue];
        [self updateChannelInCache:channel];
        [self didUpdateChannel:channel withChangedKeys:@[@"subscribersCount"]];
    } else if ([properties objectForKey:@"name"]) {
        channel.name = [[properties objectForKey:@"name"] stringValue];
        [self updateChannelInCache:channel];
        [self didUpdateChannel:channel withChangedKeys:@[@"name"]];
    }
}

-(void) xmppService:(XMPPService *) service didDeleteChannel:(NSString *) channelID {
    Channel *channel = [self getChannelById:channelID];
    if (channel) {
        @synchronized (_channels) {
            [_channels removeObject:channel];
        }
        [_channelsCache removeObjectForKey:channel.id];
        [[NSNotificationCenter defaultCenter] postNotificationName:kChannelsServiceDidRemoveChannel object:@{kChannelKey: channel}];
        [self sendNotificationDidUpdateInvitationsCount];
        [self refreshLatestItemsList];
    }
}

-(void) xmppService:(XMPPService *) service didUpdateAvatarForChannelId:(NSString *) channelID withDate:(NSDate *) newUpdateDate {
    Channel *channel = [self getChannelById:channelID];
    
    if (channel) {
        channel.lastAvatarUpdateDate = newUpdateDate;
        [_channelsCache setObjectAsync:[channel dictionaryRepresentation] forKey:channel.id completion:nil];
        [self populateAvatarForChannel:channel forceReload:YES atSize:512];
    }
}

-(void) xmppService:(XMPPService *)service didDeleteAvatarForChannelId:(NSString *)channelID {
    Channel *channel = [self getChannelById:channelID];
    if (channel)
        [self removeChannelAvatar:channel];
}

-(void) xmppService:(XMPPService *)service didUnsubscribeFromChannelWithId:(NSString *) channelID {
    Channel *channel = [self getChannelById:channelID];
    if (channel) {
        @synchronized (_channels) {
            [_channels removeObject:channel];
        }
        [_channelsCache removeObjectForKey:channel.id];
        [[NSNotificationCenter defaultCenter] postNotificationName:kChannelsServiceDidRemoveChannel object:@{kChannelKey: channel}];
        [self refreshLatestItemsList];
    }
}

-(void) xmppService:(XMPPService *)service didSubscribeToChannelWithId:(NSString *) channelID {
    [self fetchChannelById:channelID completionHandler:^(Channel *channel, NSError *error) {
        [self refreshLatestItemsList];
        [self sendNotificationDidUpdateInvitationsCount];
    }];
}

@end
