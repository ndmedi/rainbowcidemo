/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import <Foundation/Foundation.h>
#import "Channel.h"
#import "ChannelItem.h"
#import "ChannelUser.h"
#import "ChannelItemImage.h"

/**
 * Constant for "channel"
 */
FOUNDATION_EXPORT NSString *const kChannelKey;

/**
 * Constant for "channelItem"
 */
FOUNDATION_EXPORT NSString *const kChannelItemKey;

/**
 * Constant for changed keys
 */
FOUNDATION_EXPORT NSString *const kChangedKeys;

/**
 * Constant for when all channels are loaded from the cache
 */
FOUNDATION_EXPORT NSString *const kChannelsServiceDidEndLoadingChannelsFromCache;

/**
 * Constant for when all channels items are loaded from the cache
 */
FOUNDATION_EXPORT NSString *const kChannelsServiceDidEndLoadingChannelsItemsFromCache;

/**
 * Constant for when all channels are fetched from server
 */
FOUNDATION_EXPORT NSString *const kChannelsServiceDidEndFetchingChannelsFromServer;

/**
 * Constant for when all channels items are fetched from the server
 */
FOUNDATION_EXPORT NSString *const kChannelsServiceDidEndFetchingChannelsItemsFromServer;

/**
 * Constant for channel add notifications
 */
FOUNDATION_EXPORT NSString *const kChannelsServiceDidAddChannel;

/**
 * Constant for channel remove notifications
 */
FOUNDATION_EXPORT NSString *const kChannelsServiceDidRemoveChannel;

/**
 * Constant for channel update notifications
 */
FOUNDATION_EXPORT NSString *const kChannelsServiceDidUpdateChannel;

/**
 * Constant for received channel's new item notifications
 */
FOUNDATION_EXPORT NSString *const kChannelsServiceDidReceiveItem;

/**
 * Constant for received channel's update item notifications
 */
FOUNDATION_EXPORT NSString *const kChannelsServiceDidUpdateItem;

/**
 * Constant for retracted channel's item notifications
 */
FOUNDATION_EXPORT NSString *const kChannelsServiceDidRetractItem;

/**
 * Constant for channel create notifications
 */
FOUNDATION_EXPORT NSString *const kChannelsServiceDidCreateChannel;

/**
 * Constant for channel deletion notifications
 */
FOUNDATION_EXPORT NSString *const kChannelsServiceDidDeleteChannel;

/**
 * Constant when the count of invitations changes
 */
FOUNDATION_EXPORT NSString *const kChannelsServiceDidUpdateInvitationsCount;

/**
 * Constant when the count of new items changes
 */
FOUNDATION_EXPORT NSString *const kChannelsServiceDidUpdateNewItemsCount;

/**
 * Channel description
 */
@interface ChannelDescription : NSObject
@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) NSString *topic;
@property (nonatomic, readonly) NSString *id;
@property (nonatomic, readonly) ChannelUserType userType;
@property (nonatomic, readonly) NSString *creatorId;
@end

#define MAX_ITEMS_IN_CHANNEL 100

/**
 * Manager channels
 */
@interface ChannelsService : NSObject

typedef void (^ChannelsServiceCompletionHandler) (NSError *error);
typedef void (^ChannelsServiceGetMyChannelsCompletionHandler) (NSArray<ChannelDescription *> *channelDescriptions, NSError *error);
typedef void (^ChannelsServiceFindChannelsCompletionHandler) (NSArray<ChannelDescription *> *channelDescriptions, NSError *error);
typedef void (^ChannelsServiceGetChannelCompletionHandler) (Channel *channel, NSError *error);
typedef void (^ChannelsServiceGetItemsCompletionHandler) (NSInteger availableItemsCount, NSArray<ChannelItem *> *items, NSError *error);
typedef void (^ChannelsServiceGetUsersCompletionHandler) (NSArray<ChannelUser *> *users, NSError *error);

/**
 * All channels managed by the ChannelsService
 * At login time this array is filled automatically and updated with
 * subscribed channel update notifications.
 */
@property (nonatomic, readonly) NSArray *channels;

@property (nonatomic, readonly) NSArray *channelsItems;

@property (nonatomic, assign) BOOL hasNewItems;

@property (nonatomic, assign) BOOL hasInvitations;

/**
 *  Create a channel
 *  @param  name                the channel name
 *  @param  topic               the optional channel topic, pass nil if not used
 *  @param  visibility          the channel visibility in ChannelVisibilityPublic, ChannelVisibilityCompany or ChannelVisibilityPrivate
 *  @param  maxItems            the optional max items to persist, pass -1 for default
 *  @param  maxPayloadSize      the optional max payload size, pass -1 for default (10000)
 *  @param  completionHandler   completion handler
 */
-(void) createChannelWithName:(NSString *)name topic:(NSString *)topic visibility:(ChannelVisibility)visibility maxItems:(int)maxItems maxPayloadSize:(int)maxPayloadSize completionHandler:(ChannelsServiceGetChannelCompletionHandler) completionHandler;

/**
 *  Delete a channel
 *  @param  completionHandler   completion handler
 */
-(void) deleteChannelWithId:(NSString *)id completionHandler: (ChannelsServiceCompletionHandler) completionHandler;

/**
 *  Get a channel by its id
 *  @param  channelId           the channel id
 */
-(Channel *)getChannelById:(NSString *)channelId;

/**
 *  Fetch a channel on server by its id
 *  @param  channelId           the channel id
 *  @param  completionHandler   completion handler
 */
-(void)fetchChannelById:(NSString *)channelId completionHandler: (ChannelsServiceGetChannelCompletionHandler) completionHandler;

/**
 *  Enumerate all my channels by kinds, the ones I'm member of, the ones I'm also publisher and the ones I'm the owner
 *  @param  completionHandler   completion handler
 */
-(void)getMyChannelsWithCompletionHandler: (ChannelsServiceGetMyChannelsCompletionHandler) completionHandler;

/**
 *  TODO
 */
-(ChannelItem *) getChannelItemById:(NSString *)channelItemId;

/**
 *  Load or download the image for a ChannelItemImage
 *  @param  itemImage           The ChannelItemImage object(s) that you can find in each ChannelItem (property images)
 *  @param  forceOriginalFile   If set to YES, the original file will be loaded or downloaded. If NO, the original file will be loaded if in cache, otherwise the thumbnail will be loaded or downloaded.
 *  @param  block               The block with a UIImage as a parameter
 */
-(void) loadItemImageWith:(ChannelItemImage *) itemImage forceOriginalFile:(BOOL) forceOriginalFile completionBlock:(void(^)(UIImage *image)) block;

/**
 *  Publish a message to a channel
 *  @param  title               the message title
 *  @param  message             the message text
 *  @param  url                 a optional link url
 *  @param  completionHandler   completion handler
 */
-(void)publishMessageToChannel:(Channel *)channel title:(NSString *)title message:(NSString *)message url:(NSString *)url completionHandler:(ChannelsServiceCompletionHandler) completionHandler;

/**
 *  Subscribe to a channel
 *  @param  channel             the channel to subscribe
 *  @param  completionHandler   completion handler
 */
-(void)subscribeToChannel:(Channel *)channel completionHandler:(ChannelsServiceCompletionHandler) completionHandler;

/**
 *  Unsubscribe to a channel
 *  @param  channel             the channel to unsubscribe
 *  @param  completionHandler   completion handler
 */
-(void)unsubscribeToChannel:(Channel *)channel completionHandler:(ChannelsServiceCompletionHandler) completionHandler;

/**
 *  Get the n last items from a channel
 *  @param  count               max items to get, 0 to only query the available items
 *  @param  channel             the channel
 *  @param  completionHandler   completion handler
 */
-(void)get:(NSInteger)count itemsFromChannel:(Channel *)channel completionHandler:(ChannelsServiceGetItemsCompletionHandler) completionHandler;

/**
 *  Get the first page of the channel users list
 *  @param  channel             the channel
 *  @param  completionHandler   completion handler
 */
-(void)getFirstUsersFromChannel:(Channel *)channel completionHandler:(ChannelsServiceGetUsersCompletionHandler) completionHandler;

/**
 *  Get a page of the channel users list starting at a given index
 *  @param  channel             the channel
 *  @param  index               start index in the channel user list
 *  @param  completionHandler   completion handler
 */
-(void)getNextUsersFromChannel:(Channel *)channel atIndex:(NSInteger)index completionHandler:(ChannelsServiceGetUsersCompletionHandler) completionHandler;

/**
 *  Find channels whose name match a given substring
 *  @param  name                the part of channels name to search
 *  @param  limit               allow to specify a limit to the number of channels to retrieve, pass -1 for the default (100)
 *  @param  completionHandler   completion handler
 */
-(void)findChannelByName:(NSString *)name limit:(NSInteger)limit completionHandler:(ChannelsServiceFindChannelsCompletionHandler) completionHandler;

/**
 *  Find channels whose topic match a given substring
 *  @param  topic               the part of channels topic to search
 *  @param  limit               allow to specify a limit to the number of channels to retrieve, pass -1 for the default (100)
 *  @param  completionHandler   completion handler
 */
-(void)findChannelByTopic:(NSString *)topic limit:(NSInteger)limit completionHandler:(ChannelsServiceFindChannelsCompletionHandler) completionHandler;

/**
 *  Update channels's name
 *  @param  channelId           the channel identifier
 *  @param  name                the channel name
 *  @param completionHandler    completion handler
 */
-(void)updateChannelWithId:(NSString *)channelId name:(NSString *) name completionHandler:(ChannelsServiceCompletionHandler) completionHandler;

/**
 *  Update channel's topic
 *  @param  id                  the channel Id
 *  @param  topic               the new topic
 *  @param  completionHandler   completion handler
 */
-(void)updateChannelWithId:(NSString *)id topic:(NSString *)topic completionHandler:(ChannelsServiceCompletionHandler) completionHandler;

/**
 *  Update channel's visibility
 *  @param  id                  the channel Id
 *  @param  visibility          the new visibility
 *  @param  completionHandler   completion handler
 */
-(void)updateChannelWithId:(NSString *)id visibility:(ChannelVisibility)visibility completionHandler:(ChannelsServiceCompletionHandler) completionHandler;

/**
 *  Update channel's max items
 *  @param  id                  the channel Id
 *  @param  maxItems            the new max of items
 *  @param  completionHandler   completion handler
 */
-(void)updateChannelWithId:(NSString *)id maxItems:(NSUInteger)maxItems completionHandler:(ChannelsServiceCompletionHandler) completionHandler;

/**
 *  Update channel's max payload size
 *  @param  id                  the channel Id
 *  @param  maxPayloadSize      the new max payload size
 *  @param  completionHandler   completion handler
 */
-(void)updateChannelWithId:(NSString *)id maxPayloadSize:(NSUInteger)maxPayloadSize completionHandler:(ChannelsServiceCompletionHandler) completionHandler;

-(void) acceptInvitation:(Channel *) channel;

-(void) declineInvitation:(Channel *) channel;

-(void) acknoledgeChannelItem:(ChannelItem *) item;

@end
