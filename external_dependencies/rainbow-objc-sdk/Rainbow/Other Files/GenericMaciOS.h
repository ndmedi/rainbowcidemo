/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */


#if (TARGET_OS_MAC && !(TARGET_OS_EMBEDDED || TARGET_OS_IPHONE))

typedef NSImage APPImage;
typedef NSUInteger UIBackgroundTaskIdentifier;

#endif

#if TARGET_OS_IPHONE
#import <UIKit/UIKit.h>
#define W_ABPropertyType ABPropertyID

typedef UIImage APPImage;
#endif

