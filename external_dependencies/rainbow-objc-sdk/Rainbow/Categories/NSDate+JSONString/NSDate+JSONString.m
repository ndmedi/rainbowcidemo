/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "NSDate+JSONString.h"

#define kJSONRFC3339DateFormat @"yyyy'-'MM'-'dd'T'HH':'mm':'ss.S'Z'"

@implementation NSDate (JSONString)

static NSDateFormatter *s_rfc3339DateFormatter = nil;

+(void)load {
    [super load];
    if(!s_rfc3339DateFormatter){
        s_rfc3339DateFormatter = [[NSDateFormatter alloc] init];
        [s_rfc3339DateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
        [s_rfc3339DateFormatter setDateFormat:kJSONRFC3339DateFormat];
        [s_rfc3339DateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    }
}

+ (NSDate *)dateFromJSONString:(NSString *)string {
    NSDate *date = [s_rfc3339DateFormatter dateFromString:string];
    return date;
}

+(NSString *) jsonStringFromDate:(NSDate*) date {
    NSString *dateString = [s_rfc3339DateFormatter stringFromDate:date];
    return dateString;
}

- (id)proxyForJson {
    return [NSDate jsonStringFromDate:self];
}

@end
