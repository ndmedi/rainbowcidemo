/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "NSString+FileSize.h"

@implementation NSString (FileSize)

+ (NSString *)formatFileSize:(unsigned long long)size
{
    if (size < 1024)
        {
        return [NSString stringWithFormat:@"%qu bytes", size];
        }
    else if (size >= 1024 && size < 1048576)
        {
        return [NSString stringWithFormat:NSLocalizedString(@"%.1f KB",nil), ((double)size / 1024)];
        }
    else if (size >= 1048576 && size < 1073741824)
        {
        return [NSString stringWithFormat:NSLocalizedString(@"%.1f MB",nil), ((double)size / 1024 / 1024)];
        }
    else if (size >= 1073741824)
        {
        return [NSString stringWithFormat:NSLocalizedString(@"%.1f GB",nil), ((double)size / 1024 / 1024 / 1024)];
        }
    
    return @"";
}

+ (NSString *)formatFileSizeWithoutUnit:(unsigned long long)size
{
    if (size < 1024)
        {
        return [NSString stringWithFormat:@"%qu", size];
        }
    else if (size >= 1024 && size < 1048576)
        {
        return [NSString stringWithFormat:@"%.1f", ((double)size / 1024)];
        }
    else if (size >= 1048576 && size < 1073741824)
        {
        return [NSString stringWithFormat:@"%.1f", ((double)size / 1024 / 1024)];
        }
    else if (size >= 1073741824)
        {
        return [NSString stringWithFormat:@"%.2f", ((double)size / 1024 / 1024 / 1024)];
        }
    
    return @"";
}


- (unsigned long long)fileSizeFromFormat
{
    // Extract the number value from the string
    NSCharacterSet *set = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789."] invertedSet];
    NSArray *numbersArray = [self componentsSeparatedByCharactersInSet:set];
    NSString *pureNumbers = [numbersArray componentsJoinedByString:@""];
    double fileSize = [pureNumbers doubleValue];
    
    // Extract the size multiplier and apply it if necessary
    NSArray *sizes = @[@"k", @(1024), @"m", @(1024*1024), @"g", @(1024*1024*1024)];
    for (int i = 0; i < [sizes count]; i+=2)
        {
        NSString *sizeString = [sizes objectAtIndex:i];
        double sizeMultiplier = [[sizes objectAtIndex:i+1] unsignedLongLongValue];
        NSRange range = [[self lowercaseString] rangeOfString:sizeString options:NSBackwardsSearch];
        if (range.location != NSNotFound)
            {
            // Found this size, so apply it
            fileSize *= sizeMultiplier;
            break;
            }
        }
    
    return (unsigned long long)fileSize;
}

@end
