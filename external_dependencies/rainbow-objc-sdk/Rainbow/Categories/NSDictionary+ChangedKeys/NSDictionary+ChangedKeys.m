/*
 * Rainbow
 *
 * Copyright (c) 2016, ALE International
 * All rights reserved.
 *
 * ALE International Proprietary Information
 *
 * Contains proprietary/trade secret information which is the property of
 * ALE International and must not be made available to, or copied or used by
 * anyone outside ALE International without its written authorization
 *
 * Not to be disclosed or used except in accordance with applicable agreements.
 */

#import "NSDictionary+ChangedKeys.h"

// copied+modified from : http://stackoverflow.com/a/14550969

@implementation NSDictionary (ChangedKeys)

-(NSArray*) changedKeysIn:(NSDictionary*) other {
    NSMutableArray *changedKs = [NSMutableArray array];
    // This loop handles 2 cases :
    // The key exists in me and the other dict but values are differents.
    // The key exists in me but not in the other dict.
    [self enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        if(![obj isEqual:[other objectForKey:key]])
            [changedKs addObject:key];
    }];
    // Also add the keys which exists in other dict but not in me.
    for(id key in other) {
        if (![self objectForKey:key]) {
            [changedKs addObject:key];
        }
    }
    return changedKs;
}

@end
