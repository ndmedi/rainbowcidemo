//
//  File+Caching.m
//  Rainbow
//
//  Created by Alaa Bzour on 12/13/18.
//  Copyright © 2018 ALE. All rights reserved.
//

#import "File+Caching.h"
#import "File+Internal.h"

@implementation File (Caching)
-(void)saveFileDataInCache {
    if (self.url && [[NSFileManager defaultManager] fileExistsAtPath:self.url.path] ) {
        return;
    }
    NSURL *cacheURL = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@",[self applicationDocumentsCache], self.fileName]];
    if ([[NSFileManager defaultManager] fileExistsAtPath:cacheURL.path]){
        
        NSString * newFileName = [[[self.fileName componentsSeparatedByString:@"."]firstObject] stringByAppendingString:[NSString stringWithFormat:@"_%@",[NSDate date]]];
        newFileName = [NSString stringWithFormat:@"%@.%@",newFileName,[[self.fileName componentsSeparatedByString:@"."] lastObject]];
        cacheURL = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@",[self applicationDocumentsCache], newFileName]];
    }
    self.url = cacheURL;
    if (self.data) {
        if (self.type == FileTypeVideo) {
            [self.data writeToFile:cacheURL.path atomically:YES];
        }
        else{
            [[NSFileManager defaultManager] createFileAtPath:cacheURL.path contents:self.data attributes:nil];
        }
    }
}

-(NSString *)applicationDocumentsCache {
    NSString * pathFromApp = @"";
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    if([paths count] > 0){
        pathFromApp = [paths objectAtIndex:0];
    } else {
        pathFromApp = nil;
    }
    return pathFromApp;
}

@end
