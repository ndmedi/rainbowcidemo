## SDK for iOS: Getting Started
---

### Preamble
---


Welcome to the Alcatel-Lucent Enterprise **Rainbow Software Development Kit for iOS**!

Its powerful APIs enable you to create the best iOS applications that connect to Alcatel-Lucent Enterprise [Rainbow](https://www.openrainbow.com).

`The SDK is a Objective C library also embedding a audio/video stack from Google`

### Prerequisites
---

#### iOS
---

The Rainbow SDK for iOS supports older versions of iOS up to :

| Pre-requisites | Version supported |
|:-------------- |---------------- |
| iOS | >= 10.0 |

#### XCode
---

| Pre-requisites | Version supported |
|:-------------- |---------------- |
| XCode | >= 8.0 |


#### Compatibility
---

The SDK has been extensively tested on the iPhone platform both on real devices and simulator, the iPad support is ongoing. No other iOS device is actually supported.

The SDK is developped in Objective-C, it is the primary target language for using it. Support of Swift language is on the way but this hasn't been fully tested yet but you could have a look on this [Swift sample](https://github.com/Rainbow-CPaaS/Rainbow-iOS-SDK-Samples/tree/master/RainbowSwiftSample).

### Rainbow developer account
---

You need a Rainbow **developer** account in order to use the Rainbow SDK for iOS.

Please contact the Rainbow [support](mailto:support@openrainbow.com) team if you need one.


### Installation
---
#### Carthage

[Carthage](https://github.com/Carthage/Carthage) is a decentralized dependency manager, flexible and pretty easy to use. The minimum Carthage version required is 0.27.0.

You can install Carthage in two different ways :

* Download and install the latest .pkg installer [here](https://github.com/Carthage/Carthage/releases)
* With [Homebrew](http://brew.sh/) using the following command :

```bash
$ brew update
$ brew install carthage
```

To add Rainbow SDK into your Xcode project using Carthage, you need to create a `Cartfile` into your project root directory :

```bash
$ cd ~/Path/Of/Your/Project
$ touch Cartfile
```
Open the `Cartfile` with your favorite text editor and add the following lines

```
# Rainbow SDK binary framework
binary "https://sdk.openrainbow.io/ios/carthage/RainbowSDK.json" == 1.52.0
```

Run `$ carthage update` command to download both prebuild frameworks into your Xcode project. These files are located in the following folder :

```
~/Path/Of/Your/Project/Carthage/Build/iOS/xxxx.framework

```
Add RainbowSDK and WebRTC framework into embedded binaries :

Open your application **Targets** and go to **General** settings, in the **Embedded Binaries** section, click the + button and select each framework from the Carthage/Build/iOS folder.

![](../assets/add-frameworks.gif)

Then, go to the **Build Phases** tab, click the + button at the top and choose **New Run Script Phase**. Add the following line under **Shell** :

```
/usr/local/bin/carthage copy-frameworks

```
Click the + button under **Input Files** and add the paths to the frameworks :

```
$(SRCROOT)/Project-Name/Carthage/Build/iOS/Rainbow.framework
$(SRCROOT)/Project-Name/Carthage/Build/iOS/WebRTC.framework

```
Add the paths to the copied frameworks to the **Output Files** :

```
$(BUILT_PRODUCTS_DIR)/$(FRAMEWORKS_FOLDER_PATH)/Rainbow.framework
$(BUILT_PRODUCTS_DIR)/$(FRAMEWORKS_FOLDER_PATH)/WebRTC.framework

```

![](../assets/add-script.gif)

#### Manually

1. Download the [RainbowSDK](https://sdk.openrainbow.io/ios/sdk/SDK_1_52_0.zip).
2. Unzip and see next steps for which frameworks to include in to your project.
3. Drag-n-drop RainbowSDK.framework into your xcode project.
4. Drag-n-drop WebRTC.framework into your xcode project.
5. Add RainbowSDK framework and WebRTC framework into embebed binaries
6. Select your project, select your Target, select General, drag-n-drop RainbowSDK.framework and WebRTC.framework from Navigator to the Embedded Binaries section.


### Configuration
---
#### Rainbow SDK setup

- Add in your **info.plist** file the following entries : 
    - `UIBackgroundModes` (type Array)
        - `audio` (type String)
    - `NSCameraUsageDescription` (type String) 
        - `a text explaining that you want access to camera`
    - `NSMicrophoneUsageDescription` (type String) 
        - `a text explaining that you want access to  microphone`
    - `NSAppTransportSecurity` (type Dictionary)
        - `NSAllowsArbitraryLoads` (type Boolean) YES
    - `NSContactsUsageDescription` (type String) 
        - `a text explaining that you want access to contacts`
    - `NSPhotoLibraryUsageDescription` (type String) 
        - `a text explaining that you want access to photo library`
    - `NSUserActivityTypes` (type Array)
        - `Item 0` (type String)
            - `INStartAudioCallIntent`
		- `Item 1` (type String)
            - `INStartVideoCallIntent`

- Disable bitcode :
    - Select your project, select your Target, select **Build settings**, search Enable Bitcode, select **NO**

![](../assets/bitcode.gif)

#### Push notifications setup

####  Creating iOS certificates

Before creating the certificates you must have an Apple account and enroll the Apple Developer Program. It is required to configure push notifications as you will need a push notification certificate for your App ID.

###### Development and Production push notification certificates

1. Go to Apple developer portal and click on App IDs in the Identifiers category on the left.

2. Select your App ID and click on Edit button at the end of the App Services section to enable push notifications service for this App ID.
![](../assets/push-enable-service.png)

3. Create a development or production SSL certificate for push by following process given by Apple.

4. Read the information and follow the instructions to create a CSR using Keychain Access in macOS.

5. When you have the CSR file return to the developer portal with your web browser and click Continue.

6. Select the CSR file you just created and saved and click Continue.

7. Click Download to download the certificate (give it the **aps.cer** name).

8. Open the downloaded certificate file (it should automatically be opened in Keychain Access, otherwise open it manually in Keychain Access).

9. Find the certificate you just opened/imported in Keychain Access and expand the certificate to show the Private Key.

10. Export the certificate from the keychain and save it with a specific name for production or development, for example **rainbow-production-cert.p12** or **rainbow-development-cert.p12**. (right click, export and enter your password)

11. Expend the certificate and export the private key associated to this certificate, for example **rainbow-production-key.p12** or **rainbow-development-key.p12**. (right click, export and enter your password)

12. These files need to be converted to the PEM format by executing these commands from the terminal (Please always enter a passphrase and replace the correct file name for production or development):

	```bash
	$ openssl pkcs12 -clcerts -nokeys -out rainbow-production-cert.pem -in rainbow-production-cert.p12
	$ openssl pkcs12 -nocerts -out rainbow-production-key.pem -in rainbow-production-key.p12
	```

13. Remove the passphrase from the PEM file key :

	```bash
	$ openssl rsa -in rainbow-production-key.pem -out rainbow-production-key-noenc.pem
	```


14. Combine the key and cert files into a cert-production.pem :

	```bash
	$ cat rainbow-production-cert.pem rainbow-production-key-noenc.pem > cert-production.pem
	```


15. Upload the **cert-production.pem** file with [Rainbow CLI](https://hub.openrainbow.com/#/documentation/doc/sdk/cli/tutorials/Managing_applications) or directly by using your developer dashboard on [Rainbow Hub](https://hub.openrainbow.com/#/dashboard/overview). For the Rainbow Sandbox certificate repeat all commands from step 12 to create your **cert-development.pem**

###### VoIP Services certificate

1. Go to Apple developer portal and click on All in the Certificates category on the left.

2. Click the Add button (+) in the upper-right corner.

3. Select VoIP Services Certificate under Production section and click Continue.

4. Select an App ID for your VoIP Service Certificate and click Continue.

5. Read the information and follow the instructions to create a CSR using Keychain Access in macOS.

6. When you have the CSR file return to the developer portal with your web browser and click Continue.

7. Select the CSR file you just created and saved and click Continue.

8. Click Download to download the certificate (give it the **voip_services.cer** name).

9. Open the downloaded certificate file (it should automatically be opened in Keychain Access, otherwise open it manually in Keychain Access).

10. Find the certificate you just opened/imported in Keychain Access and expand the certificate to show the Private Key.

11. Export the certificate from the keychain and save it with a specific name for production or development, for example **rainbow-voip-cert.p12**. (right click, export and enter your password)

12. Expend the certificate and export the private key associated to this certificate, for example **rainbow-voip-key.p12**. (right click, export and enter your password)

13. These files need to be converted to the PEM format by executing these commands from the terminal (Please always enter a passphrase):

	```bash
	$ openssl pkcs12 -clcerts -nokeys -out rainbow-voip-cert.pem -in rainbow-voip-cert.p12
	$ openssl pkcs12 -nocerts -out rainbow-voip-key.pem -in rainbow-voip-key.p12
	```

14. Remove the passphrase from the PEM file key :

	```bash
	$ openssl rsa -in rainbow-voip-key.pem -out rainbow-voip-key-noenc.pem
	```

15. Combine the key and cert files into a rainbow-voip.pem :

	```bash
	$ cat rainbow-voip-cert.pem rainbow-voip-key-noenc.pem > rainbow-voip.pem
	```

16. Upload the **rainbow-voip.pem** file with [Rainbow CLI](https://hub.openrainbow.com/#/documentation/doc/sdk/cli/tutorials/Managing_applications) or directly by using your developer dashboard on [Rainbow Hub](https://hub.openrainbow.com/#/dashboard/overview).

#####  Enable push with Rainbow SDK

1. Enable push notification in project capabilities

2. In your code ask to the user the authorisation to use Push by adding the following code :

	```objective-c
	[[ServicesManager sharedInstance].notificationsManager 	registerForUserNotificationsSettings];
	```

	- This code will register the UIUserNotificationActions needed by Rainbow.

	- The first time you execute this code, Apple will automatically trigger a notification to the user to got his authorisation, `didRegisterUserNotificationSettings` delegate is trigger on user acceptation.

3. Monitor delegates

	- On user acceptation this delegate will be triggered, you must request to the iOS platform a push token and give it to Rainbow SDK.

	```objective-c
	-(void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
            // Request for push token
            [[UIApplication sharedApplication] registerForRemoteNotifications];
   }
   
	```
	
	- Delegate invoked when the push token is generated by iOS platform

	```objective-c
    -(void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
            [[ServicesManager sharedInstance].notificationsManager didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
    }
    

    -(void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
            NSLog(@"User refuse to enable push notification");
    }
    
    ```

	- Those delegates can be invoked (based on the iOS platform version) when a push notification is received and clicked by the user, you must send the received information from notification to Rainbow SDK.

	```objective-c
     -(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *) userInfo fetchCompletionHandler:(nonnull void (^)(UIBackgroundFetchResult))completionHandler {
            [[ServicesManager sharedInstance].notificationsManager didReceiveNotificationWithUserInfo:userInfo];
            completionHandler(UIBackgroundFetchResultNoData);
     }

     -(void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler {
            [[ServicesManager sharedInstance].notificationsManager handleNotificationWithIdentifier:identifier withUserInfo:userInfo withResponseInformation:nil];
            completionHandler();
     }
     
	```

4. Give your app id and secret key to RainbowSDK

	- This method must be invoked before calling `[[ServicesManager sharedInstance].loginManager connect]`  method we recommand to add it into the delegate `application:didFinishLaunchingWithOptions`

	```objective-c
   [[ServicesManager sharedInstance] setAppID:@"MyAppID" secretKey:@"MyToken"];
	```


#### Enable push for VoIP calls

1. Enable Background Modes capabilities in your project

2. Edit the Info.plist file, to add the voip into your application

	- Search "Required background modes" section, add a new item, and type as value `voip`

3. Request your user access to his microphone

	- Add in your Info.plist, the Privacy information for microphone access
	- Create a new entry in the root of yor plist, search for an entry called `Privacy - Microphone Usage Description`
	- And set the value explaining what you will do with the microphone `Rainbow needs access to your microphone to allow calls`
	- Now request the user is authorization to use microphone

	```objective-c
   [[ServicesManager sharedInstance].rtcService requestMicrophoneAccess];
	```

4. Configure CallKit and Rainbow SDK with your audio file and your application logo template

	- Rainbow SDK needs to known which audio file must be played for incoming calls, outgoings call and for hangup
	- Supported audio file types are MP3 or wav files

	```objective-c
   [[ServicesManager sharedInstance].rtcService startCallKitWithIncomingSoundName:@"incoming-call.mp3" iconTemplate:@"logo" appName:[Tools applicationName]];
   [ServicesManager sharedInstance].rtcService.appSoundOutgoingCall = @"outgoing-rings.mp3";
   [ServicesManager sharedInstance].rtcService.appSoundHangup = @"hangup.wav";

	```

The icon image should be a square with side length of 40 points. The alpha channel of the image is used to create a white image mask, which is used in the system native in-call UI for the button which takes the user from this system UI to the 3rd-party app.
   
   
### Usage
---

1. Import the Rainbow module in your UIApplicationDelegate subclass:

	```objective-c
	#import <Rainbow/Rainbow.h>
	```

2. Set your username and your password

	```objective-c
	[[ServicesManager sharedInstance].loginManager 	setUsername:@"myRainbowUser@domain.com" andPassword:@"MyPassword"];
	[[ServicesManager sharedInstance].loginManager connect];
	```

3. Monitor login manager notifications

	```objective-c
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:)
		name:kLoginManagerDidLoginSucceeded object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:)
		name:kLoginManagerDidReconnect object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(failedToAuthenticate:)
		name:kLoginManagerDidFailedToAuthenticate object:nil];
	```
	
	```objective-c
	-(void) didLogin:(NSNotification *) notification {
		// Called when the connexion to the Rainbow server succeeded and
		// that a new session was created 
  		NSLog(@"Did login");
	}
	
	-(void) didReconnect:(NSNotification *) notification {
		// Called when the connexion to the Rainbow server succeeded and
		// that a session was still alive, in this case the server will not
		// send automatically all the initial data (Contacts, Conversations,...)
    	NSLog(@"Did reconnect");
    }
	
	-(void) failedToAuthenticate:(NSNotification *) notification {
		// Called when the connexion to the Rainbow server failed
   		NSLog(@"Failed to login");
	}
	```


#### Sandbox test environment
---

The previous code will login to Rainbow official server, if you want to login to a sandbox server you'll have to change the server as follow:

```objective-c
[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerDidLoginSucceeded object:nil];
[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReconnect:) name:kLoginManagerDidReconnect object:nil];
[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeServer:) name:kLoginManagerDidChangeServer object:nil];
        
[[NSNotificationCenter defaultCenter] postNotificationName:kChangeServerURLNotification object:@{ @"serverURL": @"you sandbox IP address"}];
```

```objective-c
-(void)didChangeServer:(NSNotification *) notification {
    NSLog(@"Did changed server to : %@", (NSString *)notification.object);
	[[ServicesManager sharedInstance].loginManager connect];
}

-(void) didLogin:(NSNotification *) notification {
	NSLog(@"Did login");
}

-(void) didReconnect:(NSNotification *) notification {
    NSLog(@"Did reconnect");
}
```

You should note that the current Rainbow server is cached between the app runs, posting `kChangeServerURLNotification` do nothing if the url is already the current server and the `didChangeServer` notification is not called.
To reset the default server to the official Rainbow URL you may have to reinstall your application on the device or the simulator. 

#### Stopping the SDK
---

At any time, you can stop the connection to Rainbow by calling the API `disconnect`. This will stop all services. The only way to reconnect is to call the API `connect` again.

```objective-c
[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogout:) name:kLoginManagerDidLogoutSucceeded object:nil];
[[ServicesManager sharedInstance].loginManager disconnect];
[[ServicesManager sharedInstance].loginManager resetAllCredentials];
```

You should be aware that if you stop your application without stopping the SDK, at the next `connect` the server might not re-send all the informations about the contacts, conversations,... assuming the application has preserved its state.


#### Application ID
---

Each application using the SDK to connect to Rainbow must register on the server with its own AppID/AppSecretKey couple. 
This could be done in the application delegate's `application:didFinishLaunchingWithOptions:` method like this :

```
- (BOOL)application:(UIApplication *)application 
		didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [[ServicesManager sharedInstance] setAppID:kAppID secretKey:kSecretKey];
    return YES;
}
```



That's all! Your application should be connected to Rainbow, congratulation!

### SDK samples
---

You could go further with the [Rainbow iOS SDK sample projects on GitHub](https://github.com/Rainbow-CPaaS/Rainbow-iOS-SDK-Samples).

### Deployment on Apple Store
---

#### Clean the SDK
---

The SDK distribution is built to support both real device and simulator architecture for development but to deploy on Apple Store it must be stripped of the simulator part. 
To remove the simulator architecture from the SDK distribution, you have to follow these steps on command line into a terminal :

```
Change directory to WebRTC.framework folder
lipo -remove x86_64 WebRTC -output WebRTC
Change directory to Rainbow.framework folder
lipo -remove x86_64 Rainbow -output Rainbow
```



