## Legals

This page recaps all the legals aspects regarding the SDK for iOS.

### Beta Disclaimer
--

Please note that this is a Beta version of the Rainbow SDK for iOS which is still undergoing final testing before its official release. The SDK for iOS, the application samples and the documentation are provided on a "as is" and "as available" basis. Before releasing the official release, all these content can change depending on the feedback we receive in one hand and the developpement of the Rainbow official product in the other hand.

Alcatel-Lucent Enterprise will not be liable for any loss, whether such loss is direct, indirect, special or consequential, suffered by any party as a result of their use of the Rainbow SDK for iOS, the application sample software or the documentation content.

If you encounter any bugs, lack of functionality or other problems regarding the Rainbow SDK for iOS, the application samples or the documentation, please let us know immediately so we can rectify these accordingly. Your help in this regard is greatly appreciated.

### Software License
--

*Copyright (c) 2018, Alcatel-Lucent Enterprise*

*Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:*

*The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.*

*THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*

### 3rd-Party Libraries and Software Licenses
--

The Rainbow SDK includes the following third parties libraries:

Opensource name     			| Website          | Licence |Modified by ALE       | Version |
------------------------------|------------------|---------|--------------|--------------|
LinqToObjectiveC  				| <https://github.com/ColinEberhardt/LinqToObjectiveC.git> | [MIT](https://raw.githubusercontent.com/ColinEberhardt/LinqToObjectiveC/master/MIT-LICENSE.txt)        | YES (Forked) | 2.1.1
SocketRocket     				| <https://github.com/square/SocketRocket.git>  | [Apache2.0](https://raw.githubusercontent.com/square/SocketRocket/master/LICENSE)    | YES (Forked) | 0.6.0
XMPPFramework          			| <https://github.com/robbiehanson/XMPPFramework.git>    | BSD Style License  | YES (Forked) | 3.7.0
GCDAsyncSocket (included in XMPPFramework)	| <https://github.com/robbiehanson/CocoaAsyncSocket>  | Public Domain | NO | 7.4.1
CocoaLumberJack (included in XMPPFramework) | <https://github.com/CocoaLumberjack/CocoaLumberjack>  | [BSD](https://raw.githubusercontent.com/CocoaLumberjack/CocoaLumberjack/1.9.0/LICENSE.txt) | NO | 1.9.0
KissXML (included in XMPPFramework) 		| <https://github.com/robbiehanson/KissXML>  | [MIT](https://raw.githubusercontent.com/robbiehanson/KissXML/5.0.1/LICENSE.txt) | NO | 5.0
NSURLSession-SynchronousTask	| <https://github.com/floschliep/NSURLSession-SynchronousTask>  | [MIT](https://raw.githubusercontent.com/floschliep/NSURLSession-SynchronousTask/1.1/LICENSE.txt) | NO | 1.1
NSString+Emoji          | <https://github.com/nebulist/NSString-Emoji.git> | [MIT](https://raw.githubusercontent.com/nebulist/NSString-Emoji/master/LICENSE) | YES (Forked) | 0.3.0
NSDate-Extensions           | <https://github.com/khendry/NSDate-Extensions.git> | BSD | YES (Forked) | 1.0
PINCache                    | <https://github.com/pinterest/PINCache.git> | [Apache2.0](https://raw.githubusercontent.com/pinterest/PINCache/master/LICENSE.txt) | NO | 3.0.1-beta6
OrderedDictionary           | <https://github.com/nicklockwood/OrderedDictionary.git> | [MIT](https://raw.githubusercontent.com/nicklockwood/OrderedDictionary/master/LICENCE.md) | NO | 1.4
UIUserNotificationSettings-Extension | <https://github.com/alexruperez/UIUserNotificationSettings-Extension> | [MIT](https://raw.githubusercontent.com/alexruperez/UIUserNotificationSettings-Extension/master/LICENSE) | YES (Forked) | 0.1.3
