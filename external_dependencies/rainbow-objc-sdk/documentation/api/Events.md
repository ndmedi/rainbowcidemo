<a name="Events"></a>

## Events
---

### Listen to events

Once you have called the `connect()` method, you will begin receiving events from the SDK. If you want to catch them, you have simply to add the following lines to your code:

```objective-c
[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didLogin:) name:kLoginManagerDidLoginSucceeded object:nil];

[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didFailedToLogin:) name:kLoginManagerDidFailedToAuthenticate object:nil];

```

```objective-c
-(void) didLogin:(NSNotification *) notification {
 	// do something when the SDK is ready to be used
	NSLog(@"DID LOGIN");
}
```

```objective-c
-(void) didFailedToLogin:(NSNotification *) notification {
  	NSLog(@"DID FAILED TO LOGIN");
}
```

### List of events

Here is the complete list of the events that you can subscribe on:

#### Connection Events

| Name | Description |
|------|------------|
| **`kLoginManagerDidLoginSucceeded`** | Fired when the SDK is connected to Rainbow and ready to be used |
| **`kLoginManagerDidLogoutSucceeded`** | Fired when the SDK has successfully logout from the server |
| **`kLoginManagerDidLostConnection`** | Fired when the SDK lost the connection with Rainbow |
| **`kLoginManagerDidReconnect`** | Fired when the SDK didn't succeed to reconnect |
| **`kLoginManagerDidFailedToAuthenticate`** |Fired when something goes wrong (ie: bad 'configurations' parameter...) |
| **`kLoginManagerDidChangeServer`** | Fired when the message has been received by the server |
| **`kLoginManagerDidChangeUser`** | Fired when the SDK is connected to Sandbox Server |
| **`kLoginManagerTryToReconnect`** | Fired when the SDK tries to reconnect |

#### Contacts Events
| Name | Description |
|------|------------|
| **`kContactsManagerServiceDidAddContact`** | Fired when the SDK has successfully retrieve you contacts |
| **`kContactsManagerServiceDidUpdateContact`** | Fired when the a contact is updated |
| **`kContactsManagerServiceDidRemoveContact`** | Fired when the contact is removed from your contacts list |
| **`kContactsManagerServiceDidInviteContact`** | Fired when you invite contact to Rainbow or add a rainbow user to your contact list |
| **`kContactsManagerServiceDidFailedToInviteContact`** | Fired when the SDK is failed to invite |
| **`kContactsManagerServiceDidUpdateMyContact`** | Fired when you update your information |
| **`kContactsManagerServiceDidChangeContactDisplayUserSettings`** | notification sent when device contact display parameter is changed |
| **`kContactsManagerServiceLocalAccessGrantedNotification`** | Fired when the addressBook access is granted) |
| **`kContactsManagerServiceDidAddInvitation`** | Fired when a new invitation is added (could be a sent or received one) |
| **`kContactsManagerServiceDidUpdateInvitation`** | Fired when  an invitation is updated (status changed) |
| **`kContactsManagerServiceDidRemoveInvitation`** |  |
| **`kContactsManagerServiceDidUpdateInvitationPendingNumber`** |  |
| **`kContactsManagerServiceLocalAccessGrantedNotification`** | Fired when the addressBook access is granted) |

#### Conversation Events

| Name | Description |
|------|------------|
| **`kConversationsManagerDidAddConversation`** | Fired when the SDK has successfully retrieve you conversation |
| **`kConversationsManagerDidRemoveConversation`** | Fired when the you remove a conversation  |
| **`kConversationsManagerDidRemoveAllConversations`** | Fired when the you remove all conversation |
| **`kConversationsManagerDidUpdateConversation`** | Fired when the conversation is updated |
| **`kConversationsManagerDidStartConversation`** | Fired when you start a conversation with contact |
| **`kConversationsManagerDidStopConversation`** | Fired when you stop a conversation with contact |
| **`kConversationsManagerDidReceiveNewMessageForConversation`** | Fired when the conversation receive a new message |
| **`kConversationsManagerDidReceiveComposingMessage`** |  |
| **`kConversationsManagerDidAckMessageNotification`** |  |
| **`kConversationsManagerDidUpdateMessagesUnreadCount`** | Fired when the SDK did update Messages unread count |

#### Audio / Video Call Events

| Name | Description |
|------|------------|
| **`kRTCServiceDidAddCallNotification`** | Fired when the SDK has successfully retrieve or start a call |
| **`kRTCServiceDidUpdateCallNotification`** | Fired when current call status is updated   |
| **`kRTCServiceDidRemoveCallNotification`** | Fired when the you remove current call |
| **`kRTCServiceCallStatsNotification`** | Fired when new stats available for current call |
| **`kRTCServiceDidAllowMicrophoneNotification`** | Fired when you allow Microphone access |
| **`kRTCServiceDidRefuseMicrophoneNotification`** |Fired when you refuse Microphone access |
| **`kConversationsManagerDidReceiveNewMessageForConversation`** | Fired when the conversation receive a new message |
| **`kRTCServiceDidAddLocalVideoTrackNotification`** | Fired when you add local video to call |
| **`kRTCServiceDidRemoveLocalVideoTrackNotification`** | Fired when you remove local video from the call |
| **`kRTCServiceDidAddRemoteVideoTrackNotification`** | Fired when you add remote video to call]
| **`kRTCServiceDidRemoveRemoteVideoTrackNotification`** | Fired when you remove remote video from the call |
