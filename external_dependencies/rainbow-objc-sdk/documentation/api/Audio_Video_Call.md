## Audio / Video Call
---

You can start Audio call or video call with contact as follow,

### Register for Notifications

```objective-c
// Register for Notifications ..

[[NSNotificationCenter defaultCenter] addObserver:viewController selector:@selector(didCallSuccess:) name:kRTCServiceDidAddCallNotification object:nil];
 
[[NSNotificationCenter defaultCenter] addObserver:viewController selector:@selector(didUpdateCall:) name:kRTCServiceDidUpdateCallNotification object:nil];
        
[[NSNotificationCenter defaultCenter] addObserver:viewController selector:@selector(statusChanged:) name:kRTCServiceCallStatsNotification object:nil];
        
[[NSNotificationCenter defaultCenter] addObserver:viewController selector:@selector(didRemoveCall:) name:kRTCServiceDidRemoveCallNotification object:nil];
        
[[NSNotificationCenter defaultCenter] addObserver:viewController selector:@selector(didAllowMicrophone:) name:kRTCServiceDidAllowMicrophoneNotification object:nil];
        
[[NSNotificationCenter defaultCenter] addObserver:viewController selector:@selector(didRefuseMicrophone:) name:kRTCServiceDidRefuseMicrophoneNotification object:nil]; 

[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didAddLocalVideoTrack:) name:kRTCServiceDidAddLocalVideoTrackNotification object:nil];  
```

### Start Audio Call

```objective-c
RTCCall *currentVideoCall = [[ServicesManager sharedInstance].rtcService beginNewOutgoingCallWithPeer:_aContact withFeatures:(RTCCallFeatureAudio)];
```

 This will begin a new call with selected contact and notify **kRTCServiceDidAddCallNotification**

```objective-c
-(void) didCallSuccess : (NSNotification * ) notification {
    
    if ([notification.object class] == [RTCCall class]) {
        currentCall = notification.object;
    }
}

-(void) didUpdateCall : (NSNotification * ) notification {
    
    if ([notification.object class] == [RTCCall class]) {
        currentCall = notification.object;
    }
    // change status, do something with UI  
}

-(void) statusChanged : (NSNotification * ) notification {
  // Other contact Cancel the call ...
    dispatch_async(dispatch_get_main_queue(), ^{
    // if you start a video call , remove it .
        RTCMediaStream * remoteVideoStream = [[ServicesManager sharedInstance].rtcService remoteVideoStreamForCall:currentCall];
        [self dismissViewControllerAnimated:NO completion:^{
            
        }];
    }); 
}

-(void) didRemoveCall : (NSNotification * ) notification {
    // cancel Video
}

-(void) didAllowMicrophone : (NSNotification * ) notification {
    // Allow Microphone Access
}

-(void) didRefuseMicrophone : (NSNotification * ) notification {
    // Refuse Microphone Access
}

```
OR

### Start Video Call

```objective-c
RTCCall * currentCall = [[ServicesManager sharedInstance].rtcService beginNewOutgoingCallWithPeer:_aContact withFeatures:(RTCCallFeatureLocalVideo)];

```

This will begin a new video call with selected contact and notify **kRTCServiceDidAddCallNotification**


### Add Video to Audio Call

If you start Audio Call you can Add Video to current call later as follow,

```objective-c
[[ServicesManager sharedInstance].rtcService addVideoMediaToCall:currentCall];       
```
The notification `kRTCServiceDidAddLocalVideoTrackNotification` will be triggered when the local video track will be available

```
-(void) didAddLocalVideoTrack:(NSNotification *) notification {
    if(![NSThread isMainThread]){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self didAddLocalVideoTrack:notification];
        });
        return;
    }
    
    RTCVideoTrack *localVideoTrack = (RTCVideoTrack *) notification.object;
    RTCAVFoundationVideoSource *source = nil;
    if ([localVideoTrack.source isKindOfClass:[RTCAVFoundationVideoSource class]]) {
        source = (RTCAVFoundationVideoSource*)localVideoTrack.source;
    }
    // LocalVideoView is an instance of RTCCameraPreviewView, you can use the component you want to display it
    _localVideoView.captureSession = source.captureSession;
} 
```
### Remove Video from call

You can remove Video From current call as follow,

```objective-c
[[ServicesManager sharedInstance].rtcService removeVideoMediaFromCall:currentCall];
[[[ServicesManager sharedInstance].rtcService localVideoStreamForCall:currentCall].videoTracks.lastObject removeRenderer:_localVideoStream];
```
### Cancel Current Call

```objective-c
[[ServicesManager sharedInstance].rtcService cancelOutgoingCall:currentCall];
[[ServicesManager sharedInstance].rtcService hangupCall:currentCall];
```

### Handle INStartAudioCallIntent or INStartVideoCallIntent
In application delegate you must implement the following delegate
You have to inform give to the RTCService that it must handle the userActivity.

The best way do to it, it's to define into the ServicesManager the actionToPerformHandler block and call in it the rtcService `handleContinueUserActivity:error:waitingForResponse:` method, like that if the SDK must reconnect, it will do it automatically and right after the reconnection it will invoke the actionToPerformHandler block.

```objective-c
-(BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray * _Nullable))restorationHandler{
    if([userActivity.interaction.intent isKindOfClass:[INStartAudioCallIntent class]] || [userActivity.interaction.intent isKindOfClass:[INStartVideoCallIntent class]]) {
        [ServicesManager sharedInstance].actionToPerformHandler = ^{
            [[ServicesManager sharedInstance].rtcService handleContinueUserActivity:userActivity error:nil waitingForResponse:^(BOOL shouldShowActivity){
                // Do some stuf here if you want display something for ex
           } restorationHandler:restorationHandler];
        };
        return YES;
    }
}
```


### RTC Call Status

The following Status are supported for current call,

| Presence constant | value | Meaning |
|------------------ | ----- | ------- |
| **`RTCCallStatusRinging`** | 0 | Call is ringing |
| **`RTCCallStatusConnecting`** | 1 | Call is accepted, we can proceed and establish |
| **`RTCCallStatusDeclined`** | 2 | Call is declined |
| **`RTCCallStatusTimeout`** | 3 | Call has not been accepted/decline in time. |
| **`RTCCallStatusCanceled`** | 4 | Call has been canceled |
| **`RTCCallStatusEstablished`** | 5 |  Call has been established |
| **`RTCCallStatusHangup`** | 6 |  Call has been hangup |

