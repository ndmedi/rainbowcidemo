<a name="Contacts"></a>

## Contacts
---

### Retrieve the list of contacts

Once connected, you can retrieve the list of your contact as follow,

```objective-c
[[ServicesManager sharedInstance].contactsManagerService requestAddressBookAccess];
    
[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didAddContact:) name:kContactsManagerServiceDidAddContact object:nil];
 
```

```objective-c
-(void) didAddContact:(NSNotification *) notification {
 
    Contact *contact = (Contact *)notification.object;
    // add contact object to your contactsArray 
    
}

```

**Note**: `requestAddressBookAccess`  will add your local contact to your contact list, so you can invite them to use Rainbow.

### Retrieve a contact information

Accessing individually an existing contact can be done using the API `fetchRemoteContactDetail:`

```objective-c
[[ServicesManager sharedInstance].contactsManagerService fetchRemoteContactDetail:_aContact];
    
[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didGetInfo:) name:kContactsManagerServiceDidUpdateContact object:nil];

```

```objective-c
-(void) didGetInfo:(NSNotification *) notification {
     Contact *contact = (Contact *)[notification.object objectForKey:@"contact"];  
}
```


### Searching for a contact by name
You can search for a contact by his name as follow,

```objective-c
[[ServicesManager sharedInstance].contactsManagerService searchRemoteContactsWithPattern:searchedText withCompletionHandler:^(NSString *searchPattern, NSArray<Contact *> *foundContacts) {
       // do something with **foundContacts** 
}];  
```

### Adding the contact to the user network
You can add a Rainbow user to your network or invite a local contact to use Rainbow, by using this notification:`kContactsManagerServiceDidInviteContact` as follow,

```objective-c
[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didInviteContact:) name:kContactsManagerServiceDidInviteContact object:nil];
```

``` objective-c
-(void) didInviteContact:(NSNotification *) notification {
    NSLog(@"Invited");
}
```

### Removing the contact from the user network
You can remove a contact from your network using this notification:`kContactsManagerServiceDidRemoveContact` as follow,

```objective-c
[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRemoveContact:) name:kContactsManagerServiceDidRemoveContact object:nil];
```

```objective-c
-(void) didRemoveContact:(NSNotification *) notification {
    NSLog(@"Removed");
}
```
### Listen to contact presence change

When the presence of a contact changes, the following event is fired:

```objective-c
[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateContact:) name: kContactsManagerServiceDidUpdateContact object:nil];
```

```objective-c
-(void) didUpdateContact:(NSNotification *) notification {

    Contact *contact = (Contact *)[notification.object objectForKey:@"contact"];
    // check property **presence**
}

```

The presence and status of a Rainbow user can take several values as described in the following table:

| Presence | Status | Meaning |
|----------------|--------------|---------|
| **`ContactPresenceAvailable`** | | The contact is connected to Rainbow through a desktop application and is available |
| **`ContactPresenceAvailable`** | **mobile** | The contact is connected to Rainbow through a mobile application and is available |
| **`ContactPresenceAway`** | | The contact is connected to Rainbow but hasn't have any activity for several minutes |
| **`ContactPresenceDoNotDisturb`** | | The contact is connected to Rainbow and doesn't want to be disturbed at this time |
| **`ContactPresenceBusy`** | **presentation** | The contact is connected to Rainbow and uses an application in full screen (presentation mode) |
| **`ContactPresenceBusy`** | **phone** | The contact is connected to Rainbow and currently engaged in an audio call (PBX) |
| **`ContactPresenceBusy`** | **audio** | The contact is connected to Rainbow and currently engaged in an audio call (WebRTC) |
| **`ContactPresenceBusy`** | **video** | The contact is connected to Rainbow and currently engaged in a video call (WebRTC) |
| **`ContactPresenceBusy`** | **sharing** | The contact is connected to Rainbow and currently engaged in a screen sharing presentation (WebRTC) |
| **`ContactPresenceInvisible`** | | The contact is not connected to Rainbow |
| **`ContactPresenceUnavailable`** | | The presence of the Rainbow user is not known (not shared with the connected user) |

Notice: With this SDK version, if the contact uses several devices at the same time, only the latest presence information is taken into account.
